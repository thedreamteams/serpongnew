<?php
namespace App\Library;
use CodeZero\Courier\Courier;
use CodeZero\Courier\CurlCourier;
use CodeZero\Courier\Exceptions\HttpException;
use Session;
use Request;
use App\Models\StockManagement\Role;
use Image;

class Library{
	public static function roleMenu($module,$action=''){
		
		if(Session::get('group')=='root')
			return true;
		
		if(Session::get('group')!='root'){
			if($action!=''){
				$role = Role::getJoinRow(['c.name'=>$module,'a.group_id'=>Session::get('group_id')],'role_id','desc')->first();
				if($role){
					if($role->$action==1)
						return true;
				
					return false;
				}
				else{
					return false;	
				}
			}
			else{
				$role = Role::getJoinRow(['c.name'=>$module,'a.group_id'=>Session::get('group_id')],'role_id','desc')->first();
				if($role)
					return true;
			}
		}

		return false;
	}
	public static function getIp(){
		$ipaddress = '';
	    if (getenv('HTTP_CLIENT_IP'))
	        $ipaddress = getenv('HTTP_CLIENT_IP');
	    else if(getenv('HTTP_X_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	    else if(getenv('HTTP_X_FORWARDED'))
	        $ipaddress = getenv('HTTP_X_FORWARDED');
	    else if(getenv('HTTP_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_FORWARDED_FOR');
	    else if(getenv('HTTP_FORWARDED'))
	       $ipaddress = getenv('HTTP_FORWARDED');
	    else if(getenv('REMOTE_ADDR'))
	        $ipaddress = getenv('REMOTE_ADDR');
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}

	
	public static function cleanFileName($file){
		$result = '';
		$f = substr($file, strlen(IMAGE_URL));
		$exp = explode('/', $f);
		$filename = '';
		if(count($exp)>0)
			$filename = $exp[count($exp)-1];
		if($filename!=''){
			$pos = strpos($filename, '.');
			$filename = substr($filename, 0, $pos);
			$filename = ucwords(self::cleanText($filename,false));
		}

		$result = $filename;
		return $result;
	}

	public static function cleanFileNameArray($fname=[]){
		if(count($fname)==0)
			return [];

		$result = [];
		foreach ($fname as $file) {
			# code...
			$f = substr($file, strlen(IMAGE_URL));
			$exp = explode('/', $f);
			$filename = '';
			if(count($exp)>0)
				$filename = $exp[count($exp)-1];
			if($filename!=''){
				$pos = strpos($filename, '.');
				$filename = substr($filename, 0, $pos);
				$filename = ucwords(self::cleanText($filename,false));
			}

			$result[] = $filename;
		}
		return $result;
	}

	public static function cleanText($str,$separator=true){
		if($separator==true){
			$str = str_replace('-', '', $str);
			$str = str_replace('  ', ' ', $str);
			$str = str_replace(' ', '-', $str);
		}
		else{
			$arrNumeric =  range(0,9);
			$str = str_replace($arrNumeric, '', $str);
			$str = str_replace('-', '', $str);
		}
		return strtolower($str);
	}

	public static function nextPage($urlString){
		$currentUrl = Request::url();
		$queryString = Request::getQueryString();
		$explode = explode('?',$urlString);
		$url = '';
		
		if(count($explode)>1){
			$expUrl1 = explode('=', $explode[1]);
			$arr1 = [
				$expUrl1[0]=>$expUrl1[1]
			];
//			dd($arr1);
			$expUrl2 = explode('&', $queryString);
			$arrURL = $arr1;
			if($expUrl2[0]!=''){
				$arr2 =[];
				foreach ($expUrl2 as $value) {
					$vUrl2 = explode('=', $value);
					$k = trim($vUrl2[0]);
					$v = trim($vUrl2[1]);
					$arr2[$k]= $v;
				}
				$arrURL = array_replace($arr2,$arr1);
			}
			$urlStr = '';
			foreach ($arrURL as $key => $value) {
				# code...
				$urlStr .= $key .'='.$value.'&';
			}
			if($urlStr!='')
				$urlStr = substr($urlStr, 0,-1);

			$url = $currentUrl.'?'.$urlStr;
			 
		}
		return $url;			
	}

	public static function convertToArrKeyValue($delimeter,$input){
		$return = [];
		if(is_array($input)){

		}
		else{
			$exp = explode($delimiter, $input);
		}
		return $return;
	}
	public static function uploadFile($source,$type,$dir='',$newFilename='',$resize=[]){
		if(trim(strtolower($type))=='image')
			return self::uploadImage($source,$dir,$newFilename,$resize);
		elseif (trim(strtolower($type))=='csv') 
			return self::uploadCSV($source,$dir,$newFilename);

		return array('filename'=>'','error'=>Request::file($source)->getErrorMessage());
	} 
	public static function createDir($dir){
		if(!is_dir($dir))
			mkdir($dir);
	}
	public static function csvToArray($filename = '', $delimiter = ';'){
		//$filename = base_path('public/csv/'.$filename);
	    if (!file_exists($filename) || !is_readable($filename))
	        return false;

	    $header = null;
	    $data = array();
	    if (($handle = fopen($filename, 'r')) !== false)
	    {
	        while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
	        {
	            if (!$header)
	                $header = array_map('strtolower',$row);
	            else
	                $data[] = array_combine($header, $row);
	        }
	        fclose($handle);
	    }

	    return $data;
	}
	private static function uploadImage($source,$dir='',$newFilename='',$resize=[]){
		$path = '';
		$path = '';
		$defaultResize = [
			'small_square' => '128x128',
	        'medium_square' => '256x256',
	        'large_square' => '512x512',
	        'xlarge_square' => '2048x2048',
	        // 'small_cover' => '240x_',
	        // 'normal_cover' => '360x_',
	        // 'medium_cover' => '480x_',
	        // 'large_cover' => '1280x_',
	        // 'small_banner' => '_x240',
	        // 'normal_banner' => '_x360',
	        // 'medium_banner' => '_x480',
	        // 'large_banner' => '_x1280'
		];
		$resizing = count($resize)>0?$resize:$defaultResize;

		$destinationUpload = PUBLIC_PATH.'/'.$dir;
		$file = Request::file($source);
		$ext = $file->getClientOriginalExtension();
		
		if(trim($newFilename)=='')
			$newFilename = $file->getClientOriginalName();

		// if($file->move($destinationUpload,$newFilename.'.'.$ext))
		// 	return array('filename'=>$newFilename.'.'.$ext,'error'=>'');
		$image=getimagesize($file);
	    $width=$image[0]/2; 
    	$height=$image[1]/2;
	
		// dd($ext);
		$newFilename = $newFilename.'.'.$ext;
		$fname = $destinationUpload.'/'.$newFilename;
        if($file->move($destinationUpload,$newFilename)){
    		$pathThumb = $destinationUpload.'/thumb/'. $newFilename;

	    	self::createDir($destinationUpload.'/thumb');  
	    	if($ext!='svg'){
	    		$uploadThumb = Image::make($fname)->resize($width,$height)->save($pathThumb);
	    		foreach ($resizing as $krz => $vrz) {
    	    		list($w,$h) = explode('x', $vrz);
    	    		$pathRsz = $destinationUpload.'/'.$krz.'/'.$newFilename;

    	    		self::createDir($destinationUpload.'/'.$krz);
    	    		
    	    		$img = Image::make($fname);
	    	    	$img->orientate();
	    	    	$img->resize($w, $h, function ($constraint) {
	    	    		$constraint->upsize();
					    $constraint->aspectRatio();
					});
					$img->save($pathRsz);
					unset($img);
    	    	}
    	    }

			// Canvas image
			// $canvas = Image::canvas(200, 200);
			// $canvas->insert($img, 'center');
			// $canvas->save($pathThumb);
           	
           	return array('filename'=>$newFilename,'error'=>'');
       }
	}
	private static function uploadCSV($source,$dir='',$newFilename=''){
		$path = base_path('public/csv');
		
		$file = Request::file($source);
		$ext = $file->getClientOriginalExtension();
		
		if(trim($newFilename)=='')
			$newFilename = $file->getClientOriginalName();

		$destinationUpload = trim($dir)!=''?$path.'/'.$dir:$path;

		$fname = $destinationUpload.'/'.$newFilename;
        if($file->move($destinationUpload,$newFilename)){
        	return array('filename'=>$fname,'error'=>'');
       	}
	}
	public static function getDomainPrefix(){
		$str = '';
		if(env('APP_PUBLISH')=='dev')
			$str = Request::route()->getPrefix();
		elseif(env('APP_PUBLISH')=='production')
			$str = '';
			// $str = Request::root();

		return $str;
	}
}





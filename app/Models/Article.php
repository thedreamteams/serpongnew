<?php
namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Session;
use App\Library\StringLibrary;

class Article extends Model{
	private static $_tablename = 'smb_article';
    protected $table = 'smb_article';
    protected $primaryKey = 'article_id';
    public $timestamps = false;

    protected $fillable = [
        'article_id',
        'site_id',
        'category_id',
        'name',
        'slug',
        'url',
        'brief',
        'contents',
        'thumb',
        'image_1',
        'image_2',
        'files',
        'start_date',
        'end_date',
        'status',
        'hot',
        'hot_datetime',
        'clicked',
        'meta_name',
        'meta_keyword',
        'meta_description',
        'note_1',
        'note_2',
        'note_3',
        'created_date',
        'modified_date',
        'created_by',
        'modified_by'
    ];

	public static function add($data=[]){
		$userId = Session::get('userId');

		if(count($data)>0){
			$data['created_date'] = isset($data['created_date'])?$data['created_date']:Carbon::now();
			$data['modified_date'] = isset($data['modified_date'])?$data['modified_date']:Carbon::now();
			$data['created_by'] = isset($data['created_by'])?$data['created_by']:$userId;
			$data['modified_by'] = isset($data['modified_by'])?$data['modified_by']:$userId;
			if(isset($data['slug']))
				$data['slug'] = self::uniqueSlug($data['slug']);

		}
		$add = DB::table(self::$_tablename)->insert($data);
		if($add)
			return true;

		return false;
	}

	public static function addId($data=[]){
		$userId = Session::get('userId');

		$add = '';

		if(count($data)>0){
			$data['created_date'] = isset($data['created_date'])?$data['created_date']:Carbon::now();
			$data['modified_date'] = isset($data['modified_date'])?$data['modified_date']:Carbon::now();
			$data['created_by'] = isset($data['created_by'])?$data['created_by']:$userId;
			$data['modified_by'] = isset($data['modified_by'])?$data['modified_by']:$userId;
			if(isset($data['slug']))
				$data['slug'] = self::uniqueSlug($data['slug']);

		}
		$add = DB::table(self::$_tablename)->insertGetId($data);

		return $add;
	}

	public static function change($data=[],$id){
		$userId = Session::get('userId');

		if(count($data)>0){
			$data['modified_date'] = isset($data['modified_date'])?$data['modified_date']:Carbon::now();
			$data['modified_by'] = isset($data['modified_by'])?$data['modified_by']:$userId;
			if(isset($data['slug']))
				$data['slug'] = self::uniqueSlug($data['slug'],$id);
		}
		// dd($data);
		$update = DB::table(self::$_tablename)
			->where('article_id',$id)
			->update($data);

		if($update)
			return true;

		return false;
	}

	public static function getRow($where=[],$order='',$asc='asc'){
		$data = DB::table(self::$_tablename);
		if(count($where)>0)
			$data = $data->where($where);

		if($order!='')
			$data=$data->orderBy($order,$asc);

		return $data;
	}

	public static function remove($id){
		return DB::table(self::$_tablename)->where('article_id',$id)->delete();
	}

	public static function getJoinRow($where=[],$order='',$asc='asc'){
		$data = DB::table(self::$_tablename.' AS a')
			->join('smb_category AS b','a.category_id','=','b.category_id')
			->leftJoin('smb_category AS c','c.category_id','=','b.parent_id')
			->select('a.*', 'b.name AS category','b.slug AS category_slug','c.name AS parent','c.slug AS parent_slug');

		if(count($where)>0)
			$data = $data->where($where);

		if($order!='')
			$data=$data->orderBy($order,$asc);

		return $data;
	}
	private static function uniqueSlug($slug,$id=''){
		$response = $slug;
		$check = self::getRow(['slug'=>$slug,'site_id'=>Session::get('siteIdSession')])
			->where('article_id','<>',$id)
			->get();

		if($check){
			$count = count($check);
			$additioanalSlug = '-'.($count + 1);

			if($id!=''){ //update
				if($count==1){
					$additioanalSlug = '';
					$response = $response.$additioanalSlug;
				}
				else{
					$additioanalSlug = '-'.StringLibrary::random(2);
					$response = $response.$additioanalSlug;
					self::uniqueSlug($response,$id);
				}
			}
			else{
				$additioanalSlug = '-'.StringLibrary::random(2);
				$response = $response.$additioanalSlug;
				self::uniqueSlug($response,$id);
			}
		}
		return $response;
	}
}

<?php
namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Session;

class UserComment extends Model{
	public static $_tablename = 'users_comments';

	public static function add($data=[]){
		$add = DB::table(self::$_tablename)->insert($data);
		if($add)
			return true;

		return false;
	}

	public static function getRow($where=[],$order='',$asc='asc'){
		$data = DB::table(self::$_tablename);
		if(count($where)>0)
			$data = $data->where($where);

		if($order!='')
			$data=$data->orderBy($order,$asc);

		return $data;
	}

	public static function change($data=[],$id){
		$update = DB::table(self::$_tablename)
			->where('id',$id)
			->update($data);

		if($update)
			return true;

		return false;
	}

	public static function remove($id){
		return DB::table(self::$_tablename)->where('id',$id)->delete();
	}
}
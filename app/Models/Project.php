<?php
namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Session;

class Project extends Model{
	public static $_tablename = 'smb_project';

	public static function add($data=[]){
		$userId = Session::get('userId')?Session::get('userId'):0;

		if(count($data)>0){
			$data['created_date'] = isset($data['created_date'])?$data['created_date']:Carbon::now();
			$data['modified_date'] = isset($data['modified_date'])?$data['modified_date']:Carbon::now();
			$data['created_by'] = isset($data['created_by'])?$data['created_by']:$userId;
			$data['modified_by'] = isset($data['modified_by'])?$data['modified_by']:$userId;
		}

		$add = DB::table(self::$_tablename)->insert($data);
		if($add)
			return true;

		return false;
	}

	public static function change($data=[],$id){
		$userId = Session::get('userId')?Session::get('userId'):0;

		if(count($data)>0){
			$data['modified_date'] = isset($data['modified_date'])?$data['modified_date']:Carbon::now();
			$data['modified_by'] = isset($data['modified_by'])?$data['modified_by']:$userId;
		}

		$update = DB::table(self::$_tablename)
			->where('project_id',$id)
			->update($data); 

		if($update)
			return true;

		return false;	
	}

	public static function getRow($where=[],$orderBy='',$asc='asc'){
		$data = DB::table(self::$_tablename);
		if(count($where)>0)
			$data = $data->where($where);

		if(trim($orderBy)!='')
			$data = $data->orderBy($orderBy,$asc);

		return $data;
	}

	public static function getJoinRow($where=[],$orderBy='',$asc='asc'){
		$data = DB::table(self::$_tablename.' AS a')
			->join('smb_category AS b', "a.category_id","=","b.category_id")
			->select('a.*','b.name AS category');

		if(count($where)>0)
			$data = $data->where($where);

		if(trim($orderBy)!='')
			$data = $data->orderBy($orderBy,$asc);

		return $data;
	}

	public static function remove($id){
		return DB::table(self::$_tablename)->where('project_id',$id)->delete();
	}
}
<?php
namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Session;

class Req extends Model{
	private static $_tablename = 'smb_request';

	public static function add($data=[]){
		$userId = Session::get('userId');
		
		if(count($data)>0){
			$data['created_date'] = isset($data['created_date'])?$data['created_date']:Carbon::now();
			$data['modified_date'] = isset($data['modified_date'])?$data['modified_date']:Carbon::now();
			$data['created_by'] = isset($data['created_by'])?$data['created_by']:$userId;
			$data['modified_by'] = isset($data['modified_by'])?$data['modified_by']:$userId;
		}

		$add = DB::table(self::$_tablename)->insert($data);
		if($add)
			return true;

		return false;
	}

	public static function addId($data=[]){
		$userId = Session::get('userId');
		
		$add = '';

		if(count($data)>0){
			$data['created_date'] = isset($data['created_date'])?$data['created_date']:Carbon::now();
			$data['modified_date'] = isset($data['modified_date'])?$data['modified_date']:Carbon::now();
			$data['created_by'] = isset($data['created_by'])?$data['created_by']:$userId;
			$data['modified_by'] = isset($data['modified_by'])?$data['modified_by']:$userId;
		}

		$add = DB::table(self::$_tablename)->insertGetId($data);
		
		return $add;	
	}

	public static function change($data=[],$id){
		$userId = Session::get('userId');
		
		if(count($data)>0){
			$data['modified_date'] = isset($data['modified_date'])?$data['modified_date']:Carbon::now();
			$data['modified_by'] = isset($data['modified_by'])?$data['modified_by']:$userId;
		}
		$update = DB::table(self::$_tablename)
			->where('request_id',$id)
			->update($data);

		if($update)
			return true;

		return false;
	}

	public static function getRow($where=[],$order='',$asc='asc'){
		$data = DB::table(self::$_tablename);
		if(count($where)>0)
			$data = $data->where($where);

		if($order!='')
			$data=$data->orderBy($order,$asc);

		return $data;
	}

	public static function getJoinRow($where=[],$order='',$asc='asc'){
		$data = DB::table(self::$_tablename.' AS a')
			->join('smb_project AS b','a.project_id','=','b.project_id')
			->select('a.*','b.name AS project','b.slug AS project_slug');
		if(count($where)>0)
			$data = $data->where($where);

		if($order!='')
			$data=$data->orderBy($order,$asc);

		return $data;
	}
}
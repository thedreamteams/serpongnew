<?php
namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Session;

class Gallery extends Model{
	public static $_tablename = 'smb_gallery';

	public static function add($data=[]){
		$userId = Session::get('userId');
		
		if(count($data)>0){
			$data['created_date'] = isset($data['created_date'])?$data['created_date']:Carbon::now();
			$data['modified_date'] = isset($data['modified_date'])?$data['modified_date']:Carbon::now();
			$data['created_by'] = isset($data['created_by'])?$data['created_by']:$userId;
			$data['modified_by'] = isset($data['modified_by'])?$data['modified_by']:$userId;
		}

		if(DB::table(self::$_tablename)->insert($data))
			return true;

		return false;
	}

	public static function change($data=[],$id){
		$userId = Session::get('userId');

		if(count($data)>0){
			$data['modified_date'] = isset($data['modified_date'])?$data['modified_date']:Carbon::now();
			$data['modified_by'] = isset($data['modified_by'])?$data['modified_by']:$userId;
		}

		$update = DB::table(self::$_tablename)
			->where('gallery_id',$id)
			->update($data);

		if($update)
			return true;

		return false;
	}

	public static function getRow($where=[],$order='',$asc='asc'){
		$data = DB::table(self::$_tablename);

		if(count($where)>0)
			$data->where($where);

		if(trim($order)!='')
			$data->orderBy($order,$asc);

		return $data;
	} 

	public static function getJoinRow($where=[],$order='',$asc='asc'){
		$data = DB::table(self::$_tablename.' AS a')
			->leftJoin('smb_article AS b',"a.article_id",'=','b.article_id')
			->select('a.*');

		if(count($where)>0)
			$data->where($where);

		if(trim($order)!='')
			$data->orderBy($order,$asc);

		return $data;
	}

	public static function remove($id){
		return DB::table(self::$_tablename)->where('gallery_id',$id)->delete();
	}
}
<?php
namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Session;

class Area extends Model{
	public static $_tablename = 'smb_area';
	
	public static function add($data=[]){
		$userId = Session::get('userId');

		if(count($data)>0){
			$data['created_date'] = isset($data['created_date'])?$data['created_date']:Carbon::now();
			$data['modified_date'] = isset($data['modified_date'])?$data['modified_date']:Carbon::now();
			$data['created_by'] = isset($data['created_by'])?$data['created_by']:$userId;
			$data['modified_by'] = isset($data['modified_by'])?$data['modified_by']:$userId;
		}

		$add = DB::table(self::$_tablename)->insert($data);
		if($add)
			return true;

		return false;
	}

	public static function change($data=[],$id){
		$userId = Session::get('userId');
		if(count($data)>0){
			$data['modified_date'] = isset($data['modified_date'])?$data['modified_date']:Carbon::now();
			$data['modified_by'] = isset($data['modified_by'])?$data['modified_by']:$userId;
		}
		$update = DB::table(self::$_tablename)
			->where('area_id',$id)
			->update($data);

		if($update)
			return true;

		return false;
	}

	public static function getRow($where=[],$orderBy='',$asc='asc'){
		$data = DB::table(self::$_tablename);
		if(count($where)>0)
			$data = $data->where($where);

		if(trim($orderBy)!='')
			$data = $data->orderBy($orderBy,$asc);

		return $data;
	}

	
	public static function remove($id){
		$data = DB::table(self::$_tablename)->where(['area_id'=>$id])->delete();
		if($data)
			return true;

		return false;
	}	
}
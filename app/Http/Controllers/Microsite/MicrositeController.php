<?php

namespace App\Http\Controllers\Microsite;

use App\Http\Controllers\Controller;
use CodeZero\Courier\Courier;
use CodeZero\Courier\Exceptions\HttpException;
use Session;
use SEOMeta;
use Request;
use Mail;
use App\Models\Project;
use App\Models\Article;
use App\Library\Library;
use App\Models\UserComment;
use App\Models\Comment;
use Validator;

class MicrositeController extends Controller
{
	public $_courier;
	public function __construct(Courier $courier)
	{
		$this->_courier = $courier;
	}

    public function action_rumah()
	{
		$url = API_URL . 'contact/create';
		$data = [
			'site_id' => 9,
			'name' => trim(Request::Input('name')),
			'email' => trim(Request::Input('email')),
			'phone' => trim(Request::Input('phone')),
			'subject' => trim(Request::Input('subject')),
			'message' => trim(Request::Input('message')),
			'utm_source' => Request::Input('utm_source'),
			'utm_medium' => Request::Input('utm_medium'),
			'utm_campaign' => Request::Input('utm_campaign'),
			'g-recaptcha-response' => trim(Request::Input('g-recaptcha-response')),
		];
		if ($this->validator_lp($data)->fails()) {
			$str = '';
			foreach ($this->validator_lp($data)->errors()->messages() as $key => $value) {
				foreach ($value as $v) {
					$str .= $key . " = " . $v . ' ';
				}
			}
			$response = [
				'response' => 'error',
				'message' => $str
			];
			return response()->json($response);
		}
		try {
			$dataInsert = [
				'params' => [
					'site_id' => 9,
					'name' => trim(Request::Input('name')),
					'email' => trim(Request::Input('email')),
					'phone' => trim(Request::Input('phone')),
					'subject' => trim(Request::Input('subject')),
					'message' => trim(Request::Input('message')),
					'utm_source' => Request::Input('utm_source'),
					'utm_medium' => Request::Input('utm_medium'),
					'utm_campaign' => Request::Input('utm_campaign'),
					'ip' => Library::getIp(),
				]
			];
			$insert = $this->_courier->post($url, $dataInsert);

			// SEND TO ADMIN
			Mail::send('site.contact.email_getintouch', ['data' => $data], function ($message) use ($data) {
				$message->to('marketing_ss@summarecon.com', 'Marketing Summarecon Serpong');
				$message->subject('Inquiry Online From Promo Kemerdekaan Summarecon Serpong');
				$message->from($data['email'], $data['name']);
				//$message->cc("marketing_ss@summarecon.com",'Customer Care');

			});

			//SEND TO USER
			Mail::send('site.contact.email_send_getintouch', ['data' => $data], function ($message) use ($data) {
				$message->from('marketing_ss@summarecon.com', 'Marketing Summarecon Serpong');
				$message->subject('Inquiry Online From Promo Kemerdekaan Summarecon Serpong');
				$message->to($data['email'], $data['name']);
			});

			$status = $insert->toObjects();
			if ($status->status == "true") {
				$response = [
					'response' => 'success',
					'message' => "We have successfully received your Message and will get Back to you as soon as possible."
				];
				return response()->json($response);
			}
		} catch (HttpException $exception) {
			$msg = $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
			$response = [
				'response' => 'error',
				'message' => $msg
			];
			return response()->json($response);
		}

		$response = [
			'response' => 'error',
			'message' => 'Sorry, Please fill out the field correctly'
		];

		return response()->json($response);
	}

	public function mtown()
	{
		$data['wa'] = true;
		$data['chat'] = true;

		$data['banners'] =[];
		$data['gallery'] =[];

		$urlArticle = API_URL . 'article-advance';
		$url = API_URL.'article/row';
		$urlGallery = API_URL . 'gallery/';

		SEOMeta::SetTitle(Session::get('meta_name'));
		SEOMeta::SetKeywords([Session::get('meta_keyword')]);
		SEOMeta::SetDescription(Session::get('meta_description'));

		// banner
		$banner = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'banner-lp-m-town-apartment',
                'a.slug' => 'image-banner-lp-m-town-apartment'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		// ARTICLE
		$articles = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'lp-m-town-apartment',
                // 'a.slug' => 'mengapa-harus-memilih-carson-d4'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

        //Form Text
        $forms = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'banner-lp-m-town-apartment',
                'a.slug' => 'text-form-lp-m-town-apartment'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		//gallery
		$paramGallery = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>Session::get('site_id'),
                'b.slug' => 'gallery-lp-m-town-apartment',
				'a.slug' => 'image-gallery-lp-m-town-apartment'
			],
		];

		$paramVideo = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>Session::get('site_id'),
                'b.slug' => 'gallery-lp-m-town-apartment',
				'a.slug' => 'video-1-lp-m-town-apartment'
			],
		];

		$banners = $this->_courier->post($urlArticle, $banner);
			if ($banners->getBody() != '[]') {
				$bannersObj = $banners->toObjects();
				$data['banners'] = $bannersObj;
		}

		$articles = $this->_courier->post($urlArticle, $articles);
			if ($articles->getBody() != '[]') {
				$articlesObj = $articles->toObjects();
				$data['articles'] = $articlesObj;
		}

        $forms = $this->_courier->post($urlArticle, $forms);
			if ($forms->getBody() != '[]') {
				$formsObj = $forms->toObjects();
				$data['forms'] = $formsObj;
		}

		$imagesList = $this->_courier->post($url, $paramGallery);
			$images = $imagesList->toObjects();
			$idImage = $images->article_id;
			if ($imagesList->getBody() != "[]") {
				$arr = [];
				foreach ($images as $image) {
					$gallery = $this->_courier->get($urlGallery . $idImage);

					// if ($gallery->getBody() != "[]")
						// $arr['pic'] = $gallery->toObjects();
				}
				$data['images'] = $images;
				$data['gallery'] = $gallery->toObjects();
				// dd($data['gallery']);
		}

		$video = $this->_courier->post($url, $paramVideo);
			if ($video->getBody() != '[]') {
				$videoObj = $video->toObjects();
				$data['video'] = $videoObj;
		}
			// dd($data);

		return view('microsite.mtown.index')->with($data);
	}

	public function heron()
	{
		$data['wa'] = true;
		$data['chat'] = true;

		$data['banners'] =[];
		$data['gallery'] =[];

		$urlArticle = API_URL . 'article-advance';
		$url = API_URL.'article/row';
		$urlGallery = API_URL . 'gallery/';

		SEOMeta::SetTitle(Session::get('meta_name'));
		SEOMeta::SetKeywords([Session::get('meta_keyword')]);
		SEOMeta::SetDescription(Session::get('meta_description'));

		// banner
		$banner = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'banner-lp-cluster-heron',
                'a.slug' => 'image-banner-lp-cluster-heron'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		// ARTICLE
		$articles = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'lp-cluster-heron',
                // 'a.slug' => 'mengapa-harus-memilih-carson-d4'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

        //Form Text
        $forms = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'banner-lp-cluster-heron',
                'a.slug' => 'text-form-lp-cluster-heron'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		//gallery
		$paramGallery = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>Session::get('site_id'),
                'b.slug' => 'gallery-lp-cluster-heron',
				'a.slug' => 'image-gallery-lp-cluster-heron'
			],
		];

		$paramVideo = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>Session::get('site_id'),
                'b.slug' => 'gallery-lp-cluster-heron',
				'a.slug' => 'video-1-lp-cluster-heron'
			],
		];

		$banners = $this->_courier->post($urlArticle, $banner);
			if ($banners->getBody() != '[]') {
				$bannersObj = $banners->toObjects();
				$data['banners'] = $bannersObj;
		}

		$articles = $this->_courier->post($urlArticle, $articles);
			if ($articles->getBody() != '[]') {
				$articlesObj = $articles->toObjects();
				$data['articles'] = $articlesObj;
		}

        $forms = $this->_courier->post($urlArticle, $forms);
			if ($forms->getBody() != '[]') {
				$formsObj = $forms->toObjects();
				$data['forms'] = $formsObj;
		}

		$imagesList = $this->_courier->post($url, $paramGallery);
			$images = $imagesList->toObjects();
			$idImage = $images->article_id;
			if ($imagesList->getBody() != "[]") {
				$arr = [];
				foreach ($images as $image) {
					$gallery = $this->_courier->get($urlGallery . $idImage);

					// if ($gallery->getBody() != "[]")
						// $arr['pic'] = $gallery->toObjects();
				}
				$data['images'] = $images;
				$data['gallery'] = $gallery->toObjects();
				// dd($data['gallery']);
		}

		$video = $this->_courier->post($url, $paramVideo);
			if ($video->getBody() != '[]') {
				$videoObj = $video->toObjects();
				$data['video'] = $videoObj;
		}
			// dd($data);

		return view('microsite.heron.index')->with($data);
	}

    public function ardea()
	{
		$data['wa'] = true;
		$data['chat'] = true;

		$data['banners'] =[];
		$data['image_gallery'] =[];

		$urlArticle = API_URL . 'article-advance';
		$url = API_URL.'article/row';
		$urlGallery = API_URL . 'gallery/';
		$urltype = API_URL . 'type/';

		SEOMeta::SetTitle(Session::get('meta_name'));
		SEOMeta::SetKeywords([Session::get('meta_keyword')]);
		SEOMeta::SetDescription(Session::get('meta_description'));

		// banner
		$banner = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'banner-lp-cluster-ardea',
                'a.slug' => 'image-banner-lp-cluster-ardea'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];


		// ARTICLE
		$articles = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'lp-cluster-ardea',
                // 'a.slug' => 'mengapa-harus-memilih-carson-d4'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		$articlesfirst = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'lp-cluster-ardea',
                'a.slug' => 'rumah-modern-serpong'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		$articlessecond = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'lp-cluster-ardea',
                'a.slug' => 'memilih-ardea'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		$articlesthree = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'lp-cluster-ardea',
                'a.slug' => 'the-symbol-of-upper-class-living-bringing-modern-luxury-living'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

        //Form Text
        $forms = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'banner-lp-cluster-ardea',
                'a.slug' => 'text-form-lp-cluster-ardea'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		//gallery
		$paramProduct = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>Session::get('site_id'),
                'b.slug' => 'type-lp-cluster-ardea'
			],
			'order' => 'a.start_date',
			'asc' => 'asc',
		];

		//gallery
		$paramGallery = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>Session::get('site_id'),
				'c.slug' => 'lp-cluster-ardea',
				'b.slug' => 'gallery'
			],
		];

		$paramOther = [
            'params' => [
                'a.site_id' => Session::get('site_id'),
                'a.status' => 1,
                // 'c.slug' => $slug
            ],
            'order' => 'a.start_date',
            'asc' => 'asc'
        ];

		$banners = $this->_courier->post($urlArticle, $banner);
			if ($banners->getBody() != '[]') {
				$bannersObj = $banners->toObjects();
				$data['banners'] = $bannersObj;
		}

		$articles = $this->_courier->post($urlArticle, $articles);
			if ($articles->getBody() != '[]') {
				$articlesObj = $articles->toObjects();
				$data['articles'] = $articlesObj;
		}

		$articlesfirst = $this->_courier->post($urlArticle, $articlesfirst);
			if ($articlesfirst->getBody() != '[]') {
				$articlesfirstObj = $articlesfirst->toObjects();
				$data['articlesfirst'] = $articlesfirstObj;

		}

		$articlessecond = $this->_courier->post($urlArticle, $articlessecond);
			if ($articlessecond->getBody() != '[]') {
				$articlessecondObj = $articlessecond->toObjects();
				$data['articlessecond'] = $articlessecondObj;

		}

		$articlesthree = $this->_courier->post($urlArticle, $articlesthree);
			if ($articlesthree->getBody() != '[]') {
				$articlesthreeObj = $articlesthree->toObjects();
				$data['articlesthree'] = $articlesthreeObj;

		}

        $forms = $this->_courier->post($urlArticle, $forms);
			if ($forms->getBody() != '[]') {
				$formsObj = $forms->toObjects();
				$data['forms'] = $formsObj;
		}

		$products = $this->_courier->post($urlArticle, $paramProduct);
		if ($products->getBody() != '[]') {
			$productsObj = $products->toObjects();
			$data['products'] = $productsObj;

			foreach ($productsObj as $gallery) {
				$typeGallery = $this->_courier->get($urltype . $gallery->article_id);
				if ($typeGallery->getBody() != "[]") {
					$typeObj[$gallery->slug] = $typeGallery->toObjects();
					$data['typeGallery'] = $typeObj;
				}
			}
		}

		$imagesList = $this->_courier->post($url, $paramGallery);

        $images = $imagesList->toObjects();
        $idImage = $images->article_id;
        if ($imagesList->getBody() != "[]") {
            $arr = [];
            foreach ($images as $image) {
                $gallery = $this->_courier->get($urlGallery . $idImage);

                if ($gallery->getBody() != "[]")
                    $arr['pic'] = $gallery->toObjects();
            }
            $data['images'] = $images;
			if ($gallery->getBody() != "[]") {
				$data['image_gallery'] = $gallery->toObjects();
			} else {
				$data['image_gallery'] = (Object)[];
			}
            // $data['gallery_count'] = count($data['gallery']);
            // dd($data['gallery']);
		}

        // return Session::all();
        // return($data);

		return view('microsite.ardea.index')->with($data);
	}

	public function ruko()
	{
		$data['wa'] = true;
		$data['chat'] = true;

		$data['banners'] =[];
		$data['image_gallery'] =[];

		$urlArticle = API_URL . 'article-advance';
		$url = API_URL.'article/row';
		$urlGallery = API_URL . 'gallery/';
		$urltype = API_URL . 'type/';

		SEOMeta::SetTitle(Session::get('meta_name'));
		SEOMeta::SetKeywords([Session::get('meta_keyword')]);
		SEOMeta::SetDescription(Session::get('meta_description'));

		// banner
		$banner = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'banner-lp-ruko-modern',
                'a.slug' => 'banner-lp-ruko-modern-serpong'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];


		// ARTICLE
		$articles = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'lp-ruko-modern-serpong',
                'a.slug' => 'luxury-retails-in-the-heartbeat-of-serpongs-commerce'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		$articlesfirst = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'lp-ruko-modern-serpong',
                'a.slug' => 'luxury-retails-in-the-heartbeat-of-serpongs-commerce'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		$articlessecond = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'lp-ruko-modern-serpong',
                'a.slug' => 'mengapa-carson-commercial'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		$articlesthree = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'lp-ruko-modern-serpong',
                'a.slug' => 'the-symbol-of-luxury-commercial-center-for-smart-shopper'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		$articlesfour = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'lp-ruko-modern-serpong',
                'a.slug' => 'accessibility'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

        //Form Text
        $forms = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'banner-lp-cluster-ardea',
                'a.slug' => 'text-form-lp-cluster-ardea'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		//gallery
		$paramProduct = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>Session::get('site_id'),
                'b.slug' => 'gallery-lp-ruko-modern'
			],
			'order' => 'a.start_date',
			'asc' => 'asc',
		];

		//gallery
		$paramGallery = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>Session::get('site_id'),
				'c.slug' => 'lp-cluster-ardea',
				'b.slug' => 'gallery'
			],
		];

		$paramOther = [
            'params' => [
                'a.site_id' => Session::get('site_id'),
                'a.status' => 1,
                // 'c.slug' => $slug
            ],
            'order' => 'a.start_date',
            'asc' => 'asc'
        ];

		$banners = $this->_courier->post($urlArticle, $banner);
			if ($banners->getBody() != '[]') {
				$bannersObj = $banners->toObjects();
				$data['banners'] = $bannersObj;
		}

		$articles = $this->_courier->post($urlArticle, $articles);
			if ($articles->getBody() != '[]') {
				$articlesObj = $articles->toObjects();
				$data['articles'] = $articlesObj;
		}

		$articlesfirst = $this->_courier->post($urlArticle, $articlesfirst);
			if ($articlesfirst->getBody() != '[]') {
				$articlesfirstObj = $articlesfirst->toObjects();
				$data['articlesfirst'] = $articlesfirstObj;

		}

		$articlessecond = $this->_courier->post($urlArticle, $articlessecond);
			if ($articlessecond->getBody() != '[]') {
				$articlessecondObj = $articlessecond->toObjects();
				$data['articlessecond'] = $articlessecondObj;

		}

		$articlesthree = $this->_courier->post($urlArticle, $articlesthree);
			if ($articlesthree->getBody() != '[]') {
				$articlesthreeObj = $articlesthree->toObjects();
				$data['articlesthree'] = $articlesthreeObj;

		}

		$articlesfour = $this->_courier->post($urlArticle, $articlesfour);
			if ($articlesfour->getBody() != '[]') {
				$articlesfourObj = $articlesfour->toObjects();
				$data['articlesfour'] = $articlesfourObj;

		}

        $forms = $this->_courier->post($urlArticle, $forms);
			if ($forms->getBody() != '[]') {
				$formsObj = $forms->toObjects();
				$data['forms'] = $formsObj;
		}

		$products = $this->_courier->post($urlArticle, $paramProduct);
		if ($products->getBody() != '[]') {
			$productsObj = $products->toObjects();
			$data['products'] = $productsObj;

			foreach ($productsObj as $gallery) {
				$typeGallery = $this->_courier->get($urltype . $gallery->article_id);
				if ($typeGallery->getBody() != "[]") {
					$typeObj[$gallery->slug] = $typeGallery->toObjects();
					$data['typeGallery'] = $typeObj;
				}
			}
		}

		// $imagesList = $this->_courier->post($url, $paramGallery);

        // $images = $imagesList->toObjects();
        // $idImage = $images->article_id;
        // if ($imagesList->getBody() != "[]") {
        //     $arr = [];
        //     foreach ($images as $image) {
        //         $gallery = $this->_courier->get($urlGallery . $idImage);

        //         // if ($gallery->getBody() != "[]")
        //             // $arr['pic'] = $gallery->toObjects();
        //     }
        //     $data['images'] = $images;
        //     $data['image_gallery'] = $gallery->toObjects();
        //     // $data['gallery_count'] = count($data['gallery']);
        //     // dd($data['gallery']);
		// }

        // return Session::all();
        // return($data);

		return view('microsite.ruko.index')->with($data);
	}

	public function downtown()
	{
		$data['wa'] = true;
		$data['chat'] = true;

		$data['banners'] =[];
		$data['gallery'] =[];

		$urlArticle = API_URL . 'article-advance';
		$url = API_URL.'article/row';
		$urlGallery = API_URL . 'gallery/';

		SEOMeta::SetTitle(Session::get('meta_name'));
		SEOMeta::SetKeywords([Session::get('meta_keyword')]);
		SEOMeta::SetDescription(Session::get('meta_description'));

		// banner
		$banner = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
                'a.slug' => 'image-banner-lp-downtown-drive'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];


		// ARTICLE
		$articles = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'lp-downtown-drive',
				'a.hot' => 1,
			],
			// 'advance' => [
			// 	'<>' => ['a.slug' => 'image-banner-lp-downtown-drive'],
			// 	'!=' => ['a.slug' => 'strategic-location'],
			// 	'not like' => ['a.slug' => '%stunning-downtown-city-vibes%']
			// ],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		$articlestop = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'lp-downtown-drive',
                'a.slug' => 'strategic-location'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		$articlesbottom = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'lp-downtown-drive',
                'a.slug' => 'stunning-downtown-city-vibes'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];


        //Form Text
        $forms = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'banner-lp-cluster-heron',
                'a.slug' => 'text-form-lp-cluster-heron'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		//gallery
		$paramGallery = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>Session::get('site_id'),
                'b.slug' => 'gallery-lp-cluster-heron',
				'a.slug' => 'image-gallery-lp-cluster-heron'
			],
		];

		$paramVideo = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>Session::get('site_id'),
                'b.slug' => 'lp-downtown-drive',
				'a.slug' => 'video-lp-downtown-drive'
			],
		];

		$banners = $this->_courier->post($urlArticle, $banner);
			if ($banners->getBody() != '[]') {
				$bannersObj = $banners->toObjects();
				$data['banners'] = $bannersObj;
		}

		$articles = $this->_courier->post($urlArticle, $articles);
			if ($articles->getBody() != '[]') {
				$articlesObj = $articles->toObjects();
				$data['articles'] = $articlesObj;

		}
		$articlestop = $this->_courier->post($urlArticle, $articlestop);
			if ($articlestop->getBody() != '[]') {
				$articlestopObj = $articlestop->toObjects();
				$data['articlestop'] = $articlestopObj;

		}

		$articlesbottom = $this->_courier->post($urlArticle, $articlesbottom);
			if ($articlesbottom->getBody() != '[]') {
				$articlesbottomObj = $articlesbottom->toObjects();
				$data['articlesbottom'] = $articlesbottomObj;

		}

        $forms = $this->_courier->post($urlArticle, $forms);
			if ($forms->getBody() != '[]') {
				$formsObj = $forms->toObjects();
				$data['forms'] = $formsObj;
		}

		$imagesList = $this->_courier->post($url, $paramGallery);
			$images = $imagesList->toObjects();
			$idImage = $images->article_id;
			if ($imagesList->getBody() != "[]") {
				$arr = [];
				foreach ($images as $image) {
					$gallery = $this->_courier->get($urlGallery . $idImage);

					// if ($gallery->getBody() != "[]")
						// $arr['pic'] = $gallery->toObjects();
				}
				$data['images'] = $images;
				$data['gallery'] = $gallery->toObjects();
				// dd($data['gallery']);
		}

		$video = $this->_courier->post($url, $paramVideo);
			if ($video->getBody() != '[]') {
				$videoObj = $video->toObjects();
				$data['video'] = $videoObj;
		}
		// dd($data);

		return view('microsite.downtown.index')->with($data);
	}

	public function strozzi()
	{
		$data['wa'] = true;
		$data['chat'] = true;

		$data['banners'] =[];
		$data['gallery'] =[];

		$urlArticle = API_URL . 'article-advance';
		$url = API_URL.'article/row';
		$urlGallery = API_URL . 'gallery/';

		SEOMeta::SetTitle(Session::get('meta_name'));
		SEOMeta::SetKeywords([Session::get('meta_keyword')]);
		SEOMeta::SetDescription(Session::get('meta_description'));

		// banner
		$banner = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
                'a.slug' => 'image-banner-lp-cluster-strozzi'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];


		// ARTICLE
		$articles = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'lp-cluster-strozzi',
				'a.hot' => 1,
			],
			// 'advance' => [
			// 	'<>' => ['a.slug' => 'image-banner-lp-downtown-drive'],
			// 	'!=' => ['a.slug' => 'strategic-location'],
			// 	'not like' => ['a.slug' => '%stunning-downtown-city-vibes%']
			// ],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		$articlesfirst = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'lp-cluster-strozzi',
                'a.slug' => 'experience-the-comfort-of-the-real-attic'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		$articlestop = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'lp-cluster-strozzi',
                'a.slug' => 'why-should-strozzi'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		$articlesbottom = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'lp-cluster-strozzi',
                'a.slug' => 'miliki-strozzi-dapatkan-kesempatan-menangkan-grand-prize'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

        $articles = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'lp-cluster-strozzi',
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

        $programs = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'c.slug' => 'lp-cluster-strozzi',
				'b.slug' => 'program-lp-strozzi',
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];


        //Form Text
        $forms = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'banner-lp-cluster-heron',
                'a.slug' => 'text-form-lp-cluster-heron'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		//gallery
		$paramGallery = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>Session::get('site_id'),
                'b.slug' => 'gallery-lp-cluster-heron',
				'a.slug' => 'image-gallery-lp-cluster-heron'
			],
		];

		$paramVideo = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>Session::get('site_id'),
                'c.slug' => 'lp-cluster-strozzi',
                'b.slug' => 'gallery-lp-strozzi',
				'a.slug' => 'video-lp-strozzi'
			],
		];

		$banners = $this->_courier->post($urlArticle, $banner);
			if ($banners->getBody() != '[]') {
				$bannersObj = $banners->toObjects();
				$data['banners'] = $bannersObj;
		}

		$articles = $this->_courier->post($urlArticle, $articles);
			if ($articles->getBody() != '[]') {
				$articlesObj = $articles->toObjects();
				$data['articles'] = $articlesObj;

		}

        $programs = $this->_courier->post($urlArticle, $programs);
			if ($programs->getBody() != '[]') {
				$programsObj = $programs->toObjects();
				$data['programs'] = $programsObj;

		}

		$articlesfirst = $this->_courier->post($urlArticle, $articlesfirst);
			if ($articlesfirst->getBody() != '[]') {
				$articlesfirstObj = $articlesfirst->toObjects();
				$data['articlesfirst'] = $articlesfirstObj;

		}

		$articlestop = $this->_courier->post($urlArticle, $articlestop);
			if ($articlestop->getBody() != '[]') {
				$articlestopObj = $articlestop->toObjects();
				$data['articlestop'] = $articlestopObj;

		}

		$articlesbottom = $this->_courier->post($urlArticle, $articlesbottom);
			if ($articlesbottom->getBody() != '[]') {
				$articlesbottomObj = $articlesbottom->toObjects();
				$data['articlesbottom'] = $articlesbottomObj;

		}

        $forms = $this->_courier->post($urlArticle, $forms);
			if ($forms->getBody() != '[]') {
				$formsObj = $forms->toObjects();
				$data['forms'] = $formsObj;
		}

		// $imagesList = $this->_courier->post($url, $paramGallery);
		// 	$images = $imagesList->toObjects();
		// 	$idImage = $images->article_id;
		// 	if ($imagesList->getBody() != "[]") {
		// 		$arr = [];
		// 		foreach ($images as $image) {
		// 			$gallery = $this->_courier->get($urlGallery . $idImage);

		// 			// if ($gallery->getBody() != "[]")
		// 				// $arr['pic'] = $gallery->toObjects();
		// 		}
		// 		$data['images'] = $images;
		// 		$data['gallery'] = $gallery->toObjects();
		// 		// dd($data['gallery']);
		// }

		$video = $this->_courier->post($url, $paramVideo);
			if ($video->getBody() != '[]') {
				$videoObj = $video->toObjects();
				$data['video'] = $videoObj;
		}

		// return($data);

		return view('microsite.strozzi.index')->with($data);
	}

	public function carson()
	{
		$data['wa'] = true;
		$data['chat'] = true;

		$data['banners'] =[];
		$data['gallery'] =[];

		$urlArticle = API_URL . 'article-advance';
		$url = API_URL.'article/row';
		$urlGallery = API_URL . 'gallery/';

		SEOMeta::SetTitle(Session::get('meta_name'));
		SEOMeta::SetKeywords([Session::get('meta_keyword')]);
		SEOMeta::SetDescription(Session::get('meta_description'));

		// banner
		$banner = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'banner-lp-cluster-heron',
                'a.slug' => 'image-banner-lp-cluster-heron'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		// ARTICLE
		$articles = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'lp-cluster-heron',
                'a.slug' => 'mengapa-harus-memilih-carson-d4'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		//gallery
		$paramGallery = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>Session::get('site_id'),
                'b.slug' => 'gallery-lp-cluster-heron',
				'a.slug' => 'image-gallery-lp-cluster-heron'
			],
		];

		$paramVideo = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>Session::get('site_id'),
                'b.slug' => 'gallery-lp-cluster-heron',
				'a.slug' => 'video-1-lp-cluster-heron'
			],
		];

		$banners = $this->_courier->post($urlArticle, $banner);
			if ($banners->getBody() != '[]') {
				$bannersObj = $banners->toObjects();
				$data['banners'] = $bannersObj;
		}

		$articles = $this->_courier->post($urlArticle, $articles);
			if ($articles->getBody() != '[]') {
				$articlesObj = $articles->toObjects();
				$data['articles'] = $articlesObj;
		}

		$imagesList = $this->_courier->post($url, $paramGallery);
			$images = $imagesList->toObjects();
			$idImage = $images->article_id;
			if ($imagesList->getBody() != "[]") {
				$arr = [];
				foreach ($images as $image) {
					$gallery = $this->_courier->get($urlGallery . $idImage);

					// if ($gallery->getBody() != "[]")
						// $arr['pic'] = $gallery->toObjects();
				}
				$data['images'] = $images;
				$data['gallery'] = $gallery->toObjects();
				// dd($data['gallery']);
		}

		$video = $this->_courier->post($url, $paramVideo);
			if ($video->getBody() != '[]') {
				$videoObj = $video->toObjects();
				$data['video'] = $videoObj;
		}
			// dd($data);

		return view('microsite.carson.index')->with($data);
	}

	public function mega()
	{
		$data['headerlanding'] = true;
		$data['wa'] = true;
		$data['chat'] = true;

		$data['banners'] =[];
		$data['gallery'] =[];

		$urlArticle = API_URL . 'article-advance';
		$url = API_URL.'article/row';
		$urlGallery = API_URL . 'gallery/';

		SEOMeta::SetTitle(Session::get('meta_name'));
		SEOMeta::SetKeywords([Session::get('meta_keyword')]);
		SEOMeta::SetDescription(Session::get('meta_description'));

		// banner
		$banner = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'banner-lp-special-promo'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		// ARTICLE
		$articles = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'lp-special-promo'
			],
			'order' => 'a.brief',
			'asc' => 'asc',
		];

		//gallery
		$paramGallery = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>Session::get('site_id'),
				'b.slug' => 'gallery-lp-special-promo'
			],
		];

        //Siteplan
		$paramSiteplan = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>Session::get('site_id'),
				'b.slug' => 'siteplan-lp-special-promo'
			],
		];

		$paramVideo = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>Session::get('site_id'),
				'a.article_id' => '15585'
			],
		];

		$banners = $this->_courier->post($urlArticle, $banner);
			if ($banners->getBody() != '[]') {
				$bannersObj = $banners->toObjects();
				$data['banners'] = $bannersObj;
		}

		$articles = $this->_courier->post($urlArticle, $articles);
			if ($articles->getBody() != '[]') {
				$articlesObj = $articles->toObjects();
				$data['articles'] = $articlesObj;
		}

		$imagesList = $this->_courier->post($url, $paramGallery);

			$images = $imagesList->toObjects();
			$idImage = $images->article_id;
			if ($imagesList->getBody() != "[]") {
				$arr = [];
				foreach ($images as $image) {
					$gallery = $this->_courier->get($urlGallery . $idImage);

					// if ($gallery->getBody() != "[]")
						// $arr['pic'] = $gallery->toObjects();
				}
				$data['images'] = $images;
				$data['gallery'] = $gallery->toObjects();
				// dd($data['gallery']);
		}

        $spimagesList = $this->_courier->post($url, $paramSiteplan);
        if($spimagesList->getBody() != '[]') {
            $spimages = $spimagesList->toObjects();
            $idspImage = $spimages->article_id;
            if ($spimagesList->getBody() != "[]") {
                $arr = [];
                foreach ($spimages as $image) {
                    $siteplan = $this->_courier->get($urlGallery . $idspImage);
                }
                $data['spimages'] = $spimages;
                $data['siteplan'] = $siteplan->toObjects();
            }
        } else {
            $data['siteplan'] = [];
            $data['spimages'] = [];
        }

		$video = $this->_courier->post($url, $paramVideo);
			if ($video->getBody() != '[]') {
				$videoObj = $video->toObjects();
				$data['video'] = $videoObj;
		}
			// dd($data);

		return view('microsite.mega.index')->with($data);
	}

	public function getintouch()
	{
		$data = [];
		$data['wa'] = true;
		$data['chat'] = true;
		$data['footer'] = true;
		SEOMeta::SetTitle(Session::get('meta_name'));
		SEOMeta::SetKeywords([Session::get('meta_keyword')]);
		SEOMeta::SetDescription(Session::get('meta_description'));

		return view('microsite.rainbow.getintouch')->with($data);
	}

	public function action_getintouch()
	{
		$url = API_URL . 'contact/create';
		$data = [
			'site_id' => 9,
			'name' => trim(Request::Input('name')),
			'email' => trim(Request::Input('email')),
			'phone' => trim(Request::Input('phone')),
			'subject' => trim(Request::Input('subject')),
			'message' => trim(Request::Input('message')),
			'utm_source' => Request::Input('utm_source'),
			'utm_medium' => Request::Input('utm_medium'),
			'utm_campaign' => Request::Input('utm_campaign'),
			'g-recaptcha-response' => trim(Request::Input('g-recaptcha-response')),
		];
		if ($this->validator_lp($data)->fails()) {
			$str = '';
			foreach ($this->validator_lp($data)->errors()->messages() as $key => $value) {
				foreach ($value as $v) {
					$str .= $key . " = " . $v . ' ';
				}
			}
			$response = [
				'response' => 'error',
				'message' => $str
			];
			return response()->json($response);
		}
		try {
			$dataInsert = [
				'params' => [
					'site_id' => 9,
					'name' => trim(Request::Input('name')),
					'email' => trim(Request::Input('email')),
					'phone' => trim(Request::Input('phone')),
					'subject' => trim(Request::Input('subject')),
					'message' => trim(Request::Input('message')),
					'utm_source' => Request::Input('utm_source'),
					'utm_medium' => Request::Input('utm_medium'),
					'utm_campaign' => Request::Input('utm_campaign'),
					'ip' => Library::getIp(),
				]
			];
			$insert = $this->_courier->post($url, $dataInsert);

			// SEND TO ADMIN
			Mail::send('site.contact.email_getintouch', ['data' => $data], function ($message) use ($data) {
				$message->to('marketing_ss@summarecon.com', 'Marketing Summarecon Serpong');
				$message->subject('Inquiry Online From GetInTouch');
				$message->from($data['email'], $data['name']);
				$message->cc("juanberlianto@gmail.com",'Juan');

			});

			//SEND TO USER
			Mail::send('site.contact.email_send_getintouch', ['data' => $data], function ($message) use ($data) {
				$message->from('marketing_ss@summarecon.com', 'Marketing Summarecon Serpong');
				$message->subject('Inquiry Online From GetInTouch');
				$message->to($data['email'], $data['name']);
			});

			$status = $insert->toObjects();
			if ($status->status == "true") {
				$response = [
					'response' => 'success',
					'message' => "We have successfully received your Message and will get Back to you as soon as possible."
				];
				return response()->json($response);
			}
		} catch (HttpException $exception) {
			$msg = $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
			$response = [
				'response' => 'error',
				'message' => $msg
			];
			return response()->json($response);
		}

		$response = [
			'response' => 'error',
			'message' => 'Sorry, Please fill out the field correctly'
		];

		return response()->json($response);
	}

	// THANKYOU PAGE LANDING PAGE
	public function response_serpong($status)
	{
		$data['wa'] = true;
		$data['chat'] = true;
		$data['footer'] = true;

		$response = [
			'thumb' => 'cross.png',
			'statusHeader' => 'Failed!',
			'statusMessage' => 'Kesalahan ditemukan, Harap coba beberapa saat lagi!'
		];
		if ($status == 'success') {
			$response = [
				'thumb' => 'check.png',
				'statusHeader' => 'Thank You',
				'statusMessage' => 'The Form Submitted Successfully.'
			];
		}
		$data['status'] = $response;
		return view('microsite.thankspage.response_serpong', $data);
	}

	public function response_rumah($status)
	{
		$data['wa'] = true;
		$data['chat'] = true;
		$data['footer'] = true;

		$response = [
			'thumb' => 'cross.png',
			'statusHeader' => 'Failed!',
			'statusMessage' => 'Kesalahan ditemukan, Harap coba beberapa saat lagi!'
		];
		if ($status == 'success') {
			$response = [
				'thumb' => 'check.png',
				'statusHeader' => 'Thank You',
				'statusMessage' => 'The Form Submitted Successfully.'
			];
		}
		$data['status'] = $response;
		return view('microsite.thankspage.response_serpong', $data);
	}



	public function response($status)
	{
		$data = [];
		$slug = 'rainbow-springs-condovillas';
		$urlGallery = API_URL . 'gallery/';

		$projectparams = [
			'a.status' => 1,
			'a.site_id' => 9,
			'a.slug' => $slug,
			'b.slug' => 'the-springs'
		];
		$project = Project::getJoinRow($projectparams)->first();
		if ($project) {
			$data['project'] = $project;

			SEOMeta::SetTitle($project->meta_name != '' ? $project->meta_name : Session::get('meta_name'));
			SEOMeta::SetKeywords($project->meta_keyword != '' ? [$project->meta_keyword] : [Session::get('meta_keyword')]);
			SEOMeta::SetDescription($project->meta_description != '' ? $project->meta_description : Session::get('meta_description'));
		}
		$response = [
			'thumb' => 'cross.png',
			'statusHeader' => 'Failed!',
			'statusMessage' => 'Kesalahan ditemukan, Harap coba beberapa saat lagi!'
		];
		if ($status == 'success') {
			$response = [
				'thumb' => 'check.png',
				'statusHeader' => 'Thank You',
				'statusMessage' => 'The Form Submitted Successfully.'
			];
		}
		$data['status'] = $response;
		return view('microsite.rainbow.response', $data);
	}
	public function rainbow()
	{
		$data = [];
		$data['banner'] =[];
		$data['registrasi']=[];
		$data['introducing']=[];
		$data['registrasi']=[];

		$url = API_URL.'article/row';

		$slug = 'rainbow-springs-condovillas';
		$urlGallery = API_URL . 'gallery/';

		//BANNER TOP
		$paramBanner = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>9,
				'a.article_id'=>'13776',
			],
		];

		$paramRegister = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>9,
				'a.article_id'=>'13777',
			],
		];

		$paramIntroducing = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>9,
				'a.article_id'=>'13778',
			],
		];

		$paramFacilities = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>9,
				'a.article_id'=>'13779',
			],
		];

		$paramFacilitiesPic = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>9,
				'a.article_id'=>'13781',
			],
		];

		$paramVideo = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>9,
				'a.article_id'=>'13784',
			],
		];

		$projectparams = [
			'a.status' => 1,
			'a.site_id' => 9,
			'a.slug' => $slug,
			'b.slug' => 'the-springs'
		];

		$banner = $this->_courier->post($url,$paramBanner);
			if($banner->getBody()!='[]'){
				$data['banner'] = $banner->toObjects();
			}

		$registrasi = $this->_courier->post($url,$paramRegister);
			if($registrasi->getBody()!='[]'){
				$data['registrasi'] = $registrasi->toObjects();
		}

		$introducing = $this->_courier->post($url,$paramIntroducing);
			if($introducing->getBody()!='[]'){
				$data['introducing'] = $introducing->toObjects();
		}

		$facilities = $this->_courier->post($url,$paramFacilities);
			if($facilities->getBody()!='[]'){
				$data['facilities'] = $facilities->toObjects();
		}

		$facilitiesPic = $this->_courier->post($url,$paramFacilitiesPic);
			if($facilitiesPic->getBody()!='[]'){
				$data['facilitiesPic'] = $facilitiesPic->toObjects();
		}

		$video = $this->_courier->post($url,$paramVideo);
			if($video->getBody()!='[]'){
				$data['videocondovillas'] = $video->toObjects();
		}

		$project = Project::getJoinRow($projectparams)->first();
		if ($project) {
			$data['project'] = $project;

			SEOMeta::SetTitle($project->meta_name != '' ? $project->meta_name : Session::get('meta_name'));
			SEOMeta::SetKeywords($project->meta_keyword != '' ? [$project->meta_keyword] : [Session::get('meta_keyword')]);
			SEOMeta::SetDescription($project->meta_description != '' ? $project->meta_description : Session::get('meta_description'));
		}

		$articleParams = [
			'a.site_id' => 9,
			'b.slug' => $slug,
			'a.status' => 1
		];
		$articles = Article::getJoinRow($articleParams)->get();


		if ($articles) {
			foreach ($articles as $article) {
				if (strpos($article->slug, 'feature') !== false) {
					$article->galleries = [];
					$gallery = $this->_courier->get($urlGallery . $article->article_id);
					if ($gallery->getBody() != "[]")
						$article->galleries = $gallery->toObjects();

					$data['feature'] = $article;
				} elseif (strpos($article->slug, 'progress') !== false) {
					$article->galleries = [];
					$gallery = $this->_courier->get($urlGallery . $article->article_id);
					if ($gallery->getBody() != "[]")
						$article->galleries = $gallery->toObjects();

					$data['progress'] = $article;
				}
			}
		}

		$otherParams = [
			'a.site_id' => 9,
			'a.status' => 1,
			'c.slug' => $slug
		];


		$others = Article::getJoinRow($otherParams)->get();
		// dd($others);
		if ($others) {
			foreach ($others as $other) {
				if (strpos($other->slug, 'type') !== false) {
					$other->galleries = [];
					$gallery = $this->_courier->get($urlGallery . $other->article_id);
					if ($gallery->getBody() != "[]")
						$other->galleries = $gallery->toObjects();

					$data['type'] = $other;
				} elseif (strpos($other->slug, 'brochure') !== false) {
					$data['brochure'] = $other;
				} elseif (strpos($other->slug, 'video') !== false) {
					$data['video'] = $other;
				}
			}
		}
		$newParams = [
			'a.site_id' => 9,
			'a.status' => 1,
			'b.slug' => 'articles',
			'c.slug' => 'condovillas'
		];

		$news = Article::getJoinRow($newParams, 'a.start_date', 'desc')->select('a.name', 'a.brief', 'a.slug', 'a.start_date', 'a.thumb', 'a.image_1')->get();
		if ($news)
			$data['news'] = $news;

		$layoutParams = [
			'a.site_id' => 9,
			'a.status' => 1,
			'b.slug' => 'layout',
			'c.slug' => 'condovillas'
		];

		$layout = Article::getJoinRow($layoutParams, 'a.start_date', 'desc')->select('a.name', 'a.brief', 'a.slug', 'a.start_date', 'a.thumb', 'a.image_1')->get();
		if ($layout)
			$data['layout'] = $layout;

		$paramsParalax = [
			'a.site_id' => 9,
			'a.status' => 1,
			'b.slug' => 'background-paralax',
			'c.slug' => 'condovillas'
		];

		$paralax = Article::getJoinRow($paramsParalax, 'a.brief', 'asc')->select('a.*')->get();
		if ($paralax)
			$data['paralax'] = $paralax;



		return view('microsite.rainbow.index', $data);
	}

	public function detail($slug)
	{
		$data = [];

		$urlGallery = API_URL . 'gallery/';

		$projectparams = [
			'a.status' => 1,
			'a.site_id' => 9,

		];
		$project = Project::getJoinRow($projectparams)->first();
		if ($project) {
			$data['project'] = $project;

			SEOMeta::SetTitle($project->meta_name != '' ? $project->meta_name : Session::get('meta_name'));
			SEOMeta::SetKeywords($project->meta_keyword != '' ? [$project->meta_keyword] : [Session::get('meta_keyword')]);
			SEOMeta::SetDescription($project->meta_description != '' ? $project->meta_description : Session::get('meta_description'));
		}
		$articleParams = [
			'a.site_id' => 9,
			'a.slug' => $slug,
			'a.status' => 1
		];
		$articles = Article::getJoinRow($articleParams)->first();
		if ($articles) {
			$data['article'] = $articles;
		}
		$otherParams = [
			'a.site_id' => 9,
			'a.status' => 1,
			'b.slug' => 'condovillas',
			'c.slug' => 'microsite'
		];
		$others = Article::getJoinRow($otherParams, 'a.start_date', 'desc')->select('a.name', 'a.start_date', 'a.slug')->where('a.slug', '<>', $slug)->get();
		if ($others) {
			$data['others'] = $others;
		}

		return view('microsite.rainbow.detail')->with($data);
	}

	public function download($pid, $id)
	{
		$data = [];
		$urlGallery = API_URL . 'gallery/';
		$data['id'] = $id;
		$data['pid'] = $pid;
		$projectparams = [
			'a.status' => 1,
			'a.site_id' => 9,
			'a.project_id' => $pid,
		];
		$project = Project::getJoinRow($projectparams)->first();
		if ($project) {
			$data['project'] = $project;

			SEOMeta::SetTitle($project->meta_name != '' ? $project->meta_name : Session::get('meta_name'));
			SEOMeta::SetKeywords($project->meta_keyword != '' ? [$project->meta_keyword] : [Session::get('meta_keyword')]);
			SEOMeta::SetDescription($project->meta_description != '' ? $project->meta_description : Session::get('meta_description'));
		}
		return view('microsite.rainbow.download', $data);
	}

	protected function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required',
			'email' => 'required',
			'phone' => 'required',
			'subject' => 'required',
			'message' => 'required',
			'g-recaptcha-response' => 'required|recaptcha',
		]);
	}

    protected function validator_lp(array $data)
	{
		return Validator::make($data, [
			'name' => 'required',
            'email' => 'required',
			'subject' => 'required',
			'phone' => 'required',
			'message' => 'required',
			'g-recaptcha-response' => 'required|recaptchav3:contact,0.5'
		]);
	}

	public function action()
	{
		$url = API_URL . 'contact/create';
		$data = [
			'site_id' => 9,
			'name' => trim(Request::Input('name')),
			'email' => trim(Request::Input('email')),
			'phone' => trim(Request::Input('phone')),
			'subject' => trim(Request::Input('subject')),
			'message' => trim(Request::Input('message')),
			'utm_source' => Request::Input('utm_source'),
			'utm_medium' => Request::Input('utm_medium'),
			'utm_campaign' => Request::Input('utm_campaign'),
			'g-recaptcha-response' => trim(Request::Input('g-recaptcha-response')),
		];
		if ($this->validator_lp($data)->fails()) {
			$str = '';
			foreach ($this->validator_lp($data)->errors()->messages() as $key => $value) {
				foreach ($value as $v) {
					$str .= $key . " = " . $v . ' ';
				}
			}
			$response = [
				'response' => 'error',
				'message' => $str
			];
			return response()->json($response);
		}
		try {
			$dataInsert = [
				'params' => [
					'site_id' => Session::get('site_id'),
					'name' => trim(Request::Input('name')),
					'email' => trim(Request::Input('email')),
					'phone' => trim(Request::Input('phone')),
					'subject' => trim(Request::Input('subject')),
					'message' => trim(Request::Input('message')),
					'utm_source' => Request::Input('utm_source'),
					'utm_medium' => Request::Input('utm_medium'),
					'utm_campaign' => Request::Input('utm_campaign'),
					'ip' => Library::getIp(),
				]
			];
			$insert = $this->_courier->post($url, $dataInsert);

			// SEND TO ADMIN
			Mail::send('site.contact.email', ['data' => $data], function ($message) use ($data) {
				$message->to('marketing_ss@summarecon.com', 'Marketing Summarecon Serpong');
				$message->subject('Inquiry Online From lending page summareconserpong.com');
				$message->from($data['email'], $data['name']);
				$message->cc("marketing_ss@summarecon.com", 'Customer Care');
			});

			//SEND TO USER
			Mail::send('site.contact.email_send', ['data' => $data], function ($message) use ($data) {
				$message->from('marketing_ss@summarecon.com', 'Marketing Summarecon Serpong');
				$message->subject('Inquiry Online From summareconserpong.com');
				$message->to($data['email'], $data['name']);
			});

			$status = $insert->toObjects();
			if ($status->status == "true") {
				$response = [
					'response' => 'success',
					'message' => "We have successfully received your Message and will get Back to you as soon as possible."
				];
				return response()->json($response);
			}
		} catch (HttpException $exception) {
			$msg = $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
			$response = [
				'response' => 'error',
				'message' => $msg
			];
			return response()->json($response);
		}

		$response = [
			'response' => 'error',
			'message' => 'Sorry, Please fill out the field correctly'
		];

		return response()->json($response);
	}

	public function commentRegister()
	{
		$response = [];
		$data = [
			'fullname' => Request::input('commenter_name'),
			'email' => Request::input('commenter_email'),
			'profile_picture_url' => "https://viima-app.s3.amazonaws.com/media/public/defaults/user-icon.png"
		];
		$check = UserComment::getRow([
			'fullname' => Request::input('commenter_name'),
			'email' => Request::input('commenter_email'),
		])->first();

		if (!$check)
			$add = UserComment::add($data);

		$response = $data;
		return response()->json($response);
	}
	public function getUserComments()
	{
		$response = [];
		$response = UserComment::getRow()->get();

		return response()->json($response);
	}
	public function getComments($id)
	{
		$response = [];

		$response = Comment::getRow(['article_id' => $id])->get();

		return response()->json($response);
	}
	public function postComments($id)
	{
		$response = [];
		$data = Request::all();
		$data['fullname'] = Request::input('name');
		$data['profile_picture_url'] = url('assets/microsite/rainbow/images/chat.svg');
		$data['article_id'] = $id;
		$data['site_id'] = 9;

		unset($data['created_by_current_user']);
		unset($data['name']);
		unset($data['id']);
		if ($data['parent'] == '')
			unset($data['parent']);

		$commentId = Comment::add($data);

		$response = Comment::getRow(['id' => $commentId])->first();

		return response()->json($response);
	}
}

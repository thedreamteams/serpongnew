<?php

namespace App\Http\Controllers\Release;

use App\Http\Controllers\Controller;

use CodeZero\Courier\Courier;
use CodeZero\Courier\Exceptions\HttpException;
use Session;
use SEOMeta;
use OpenGraph;
use App\Library\Library;

class ReleaseController extends Controller
{
    //
    public $_courier;

    public function __construct(Courier $courier)
    {
        $this->_courier = $courier;
    }

    public function index()
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['articles'] = [];
        $url = API_URL . 'article-advance';
        $urlBanner = API_URL . 'article/row';

        SEOMeta::SetTitle('Download Press Release terbaru Summarecon Serpong');
        SEOMeta::SetKeywords(['press release,  informasi Summarecon Serpong, produk-produk Summarecon Serpong, media masa, media nasional, media masa nasional, media masa daerah']);
        SEOMeta::SetDescription('Download press release terbaru seputar informasi dan produk-produk Summarecon Serpong untuk rekan-rekan media baik media nasional maupun media masa daerah');

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'b.slug' => 'article-ss',
            ],
        ];

        $paramArticle = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'b.slug' => 'release',
                'c.slug' => 'article-ss'
            ],
            'order' => 'a.name',
            'asc' => 'asc',
        ];


        try {
            $banner = $this->_courier->post($urlBanner, $paramBanner);
            if ($banner->getBody() != '[]') {
                $data['banners'] = $banner->toObjects();
            }

            $articles = $this->_courier->post($url, $paramArticle);
            if ($articles->getBody() != '[]') {
                $articlesObj = $articles->toObjects();
                $data['articles'] = $articlesObj;
            }

        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);
        return view('site.release.index')->with($data);
    }

}

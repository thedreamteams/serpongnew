<?php

namespace App\Http\Controllers\Explore;

use App\Http\Controllers\Controller;

use CodeZero\Courier\Courier;
use CodeZero\Courier\Exceptions\HttpException;
use Session;
use SEOMeta;
use OpenGraph;
use App\Library\Library;

class ExploreController extends Controller
{
    //
    public $_courier;

    public function __construct(Courier $courier)
    {
        $this->_courier = $courier;
    }

    public function index()
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $url = API_URL . 'article-advance';
        $urlcat = API_URL . 'category';
        $urlabout = API_URL . 'explore';
        $urlRow = API_URL . 'article/row';

        // SEOMeta::SetTitle(Session::get('meta_name'));
        // SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        // SEOMeta::SetDescription(Session::get('meta_description'));

        $paramBanner = [
            'params' => [
                'b.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => 'explore',
            ],
        ];


        $paramArticle = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => 'thumbnail',
                'c.slug' => 'explore',
			],
            'order' => 'a.name',
            'asc' => 'asc',
		];

        try {
            $banner = $this->_courier->post($url, $paramBanner);
            if ($banner->getBody() != '[]') {
                $seoObj = $banner->toObjects()[0];
                SEOMeta::SetTitle($seoObj->meta_name != '' ? $seoObj->meta_name : Session::get('meta_name'));
                SEOMeta::SetKeywords($seoObj->meta_keyword != '' ? [$seoObj->meta_keyword] : [Session::get('meta_keyword')]);
                SEOMeta::SetDescription($seoObj->meta_description != '' ? $seoObj->meta_description : Session::get('meta_description'));
                OpenGraph::setDescription($seoObj->meta_description != '' ? $seoObj->meta_description : Session::get('meta_description'));
                OpenGraph::setTitle($seoObj->meta_name != '' ? $seoObj->meta_name : Session::get('meta_name'));
                $data['banners'] = $banner->toObjects();
            }
            $articles = $this->_courier->post($url, $paramArticle);
			if ($articles->getBody() != '[]') {
				$articlesObj = $articles->toObjects();
				$data['articles'] = $articlesObj;
			}


        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);
        return view('site.explore.index')->with($data);
    }

    public function detail()
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['project'] = [];
        $data['category'] = [];
        $data['category_leisure'] = null;
        $data['slug'] = '';

        $url = API_URL . 'article-advance';
        $urlproject = API_URL . 'project/vivaldi-residence';
        $urlcat = API_URL . 'category-advance';
        $urlabout = API_URL . 'explore';
        $urlhot = API_URL . 'article/row';

        // SEOMeta::SetTitle(Session::get('meta_name'));
        // SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        // SEOMeta::SetDescription(Session::get('meta_description'));

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'b.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => 'explore',
            ],
        ];

        // ARTICLE GAMBAR
		$paramArticle = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => 1014,
				'c.slug' => 'explore',
			],
			'order' => 'a.name',
            'asc' => 'asc',

		];

        //CATEGORY TITLE
		$paramCategory = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => 1014,
				'b.slug' => 'explore',
			],
            'advance' => [
                '<>' => ['a.slug'=>'leisure']
            ]
		];

        $paramCategoryLeisure = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => 1014,
				'b.slug' => 'leisure',
			],
		];

        $paramProject = [
            'params' => [
                //'status'=>1,
                'site_id' => Session::get('site_id'),
            ],
        ];

        $param = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => 1014,
                'c.slug' => 'explore',
                'a.hot' => 1
            ],
        ];

        try {
            $banner = $this->_courier->post($url, $paramBanner);
            if ($banner->getBody() != '[]') {
                $data['banners'] = $banner->toObjects();
            }

            $articles = $this->_courier->post($url, $paramArticle);
			if ($articles->getBody() != '[]') {
				$articlesObj = $articles->toObjects();
				$data['articles'] = $articlesObj;
			}

            $category = $this->_courier->post($urlcat, $paramCategory);
			if ($category->getBody() != '[]') {
				$data['category'] = $category->toObjects();
			}

            $categoryLeisure = $this->_courier->post($urlcat, $paramCategoryLeisure);
			if ($categoryLeisure->getBody() != '[]') {
				$data['category_leisure'] = $categoryLeisure->toObjects();

                $arrLeisureSlug = [];
                foreach ($data['category_leisure'] as $key => $item) {
                    $arrLeisureSlug[$key] = $item->slug;
                }
                $data['category_leisure_slug'] = $arrLeisureSlug;
			}

            $projectList = $this->_courier->post($urlproject, $paramProject);
            if ($projectList->getBody() != "[]") {
                $projectObj = $projectList->toObjects();
                SEOMeta::SetTitle($projectObj->meta_name != '' ? $projectObj->meta_name : Session::get('meta_name'));
                SEOMeta::SetKeywords($projectObj->meta_keyword != '' ? [$projectObj->meta_keyword] : [Session::get('meta_keyword')]);
                SEOMeta::SetDescription($projectObj->meta_description != '' ? $projectObj->meta_description : Session::get('meta_description'));
                OpenGraph::setDescription($projectObj->meta_description != '' ? $projectObj->meta_description : Session::get('meta_description'));
                OpenGraph::setTitle($projectObj->meta_name != '' ? $projectObj->meta_name : Session::get('meta_name'));
                $data['project'] = $projectObj;
            }

            $article = $this->_courier->post($url, $param);
            if ($article->getBody() != '[]') {
                $articleObj = $article->toObjects();
                $data['articlehot'] = $articleObj;
            }
        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);
        return view('site.explore.detail')->with($data);
    }

    public function detailcategory($category)
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['project'] = [];
        $data['category'] = [];
        $data['category_leisure'] = null;
        $data['slug'] = $category;

        $url = API_URL . 'article-advance';
        $urlproject = API_URL . 'project/vivaldi-residence';
        $urlcat = API_URL . 'category-advance';
        $urlabout = API_URL . 'explore';
        $urlhot = API_URL . 'article/row';

        // SEOMeta::SetTitle(Session::get('meta_name'));
        // SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        // SEOMeta::SetDescription(Session::get('meta_description'));

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'b.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => 'explore',
            ],
        ];
        // ARTICLE GAMBAR
		$paramArticle = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => 1014,
				// 'c.slug' => 'explore',
                'b.slug' => $category,
			],
			'order' => 'a.name',
            'asc' => 'asc',

		];

        //CATEGORY TITLE
		$paramCategory = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => 1014,
				'b.slug' => 'explore',
			],
            'advance' => [
                '<>' => ['a.slug'=>'leisure']
            ]
		];

        $paramCategoryLeisure = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => 1014,
				'b.slug' => 'leisure',
			],
		];

        $paramProject = [
            'params' => [
                //'status'=>1,
                'site_id' => Session::get('site_id'),
            ],
        ];

        $param = [
            'params' => [
                'a.status' => 1,
                'a.site_id' =>  Session::get('site_id'),
                'c.slug' => 'explore',
                'a.hot' => 1
            ],
        ];

        try {
            $banner = $this->_courier->post($url, $paramBanner);
            if ($banner->getBody() != '[]') {
                $data['banners'] = $banner->toObjects();
            }


            $articles = $this->_courier->post($url, $paramArticle);
			if ($articles->getBody() != '[]') {
				$articlesObj = $articles->toObjects();
				$data['articles'] = $articlesObj;
			}

            $category = $this->_courier->post($urlcat, $paramCategory);
			if ($category->getBody() != '[]') {
				$data['category'] = $category->toObjects();
			}

            $categoryLeisure = $this->_courier->post($urlcat, $paramCategoryLeisure);
			if ($categoryLeisure->getBody() != '[]') {
				$data['category_leisure'] = $categoryLeisure->toObjects();

                $arrLeisureSlug = [];
                foreach ($data['category_leisure'] as $key => $item) {
                    $arrLeisureSlug[$key] = $item->slug;
                }
                $data['category_leisure_slug'] = $arrLeisureSlug;
			}

            $projectList = $this->_courier->post($urlproject, $paramProject);
            if ($projectList->getBody() != "[]") {
                $projectObj = $projectList->toObjects();
                $data['project'] = $projectObj;
            }

            $article = $this->_courier->post($url, $param);
            if ($article->getBody() != '[]') {
                $articleObj = $article->toObjects();
                $data['articlehot'] = $articleObj;

            }

        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);
        return view('site.explore.detail')->with($data);
    }

    public function shuttle()
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['category'] = [];
        $data['articles'] = [];
        $url = API_URL . 'article-advance';
        $urlabout = API_URL . 'true-commitment';

        SEOMeta::SetTitle(Session::get('meta_name'));
        SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        SEOMeta::SetDescription(Session::get('meta_description'));

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'b.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => 'explore',
            ],
        ];

        try {
            $banner = $this->_courier->post($url, $paramBanner);
            if ($banner->getBody() != '[]') {
                $data['banners'] = $banner->toObjects();
            }

            $about = $this->_courier->post($urlabout);
            if ($about->getBody() != '[]') {
                $aboutObj = $about->toObjects();
                $data['about'] = $aboutObj;
            }

        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);
        return view('site.explore.shuttle')->with($data);
    }

}

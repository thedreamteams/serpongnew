<?php

namespace App\Http\Controllers\Virtual;

use App\Http\Controllers\Controller;
use CodeZero\Courier\Courier;
use CodeZero\Courier\Exceptions\HttpException;
use Session;
use SEOMeta;
use OpenGraph;
use Request;
use Mail;
use App\Library\Library;
use App\Models\Project;
use App\Models\Download;
use Carbon\Carbon;


class VirtualController extends Controller
{
	public $_courier;
	public $_siteId;
	public $_library;

	public function __construct(Courier $courier)
	{
		$this->_courier = $courier;
		$this->_siteId = Session::get('site_id');
	}

	public function index()
	{
		// dd(!Session::get('is_login'));
		$data = [];
		$data['header'] = true;
		$data['banners'] = [];
		$data['nextPage'] = '';
		$url = API_URL . 'project';
		$urlCategory = API_URL . 'category';
		$urlBanner = API_URL . 'article-advance';
		$page = Request::input('page');
		if ($page)
			$url = $url . '?page=' . $page;

		// SEOMeta::SetTitle(Session::get('meta_name'));
		// SEOMeta::SetKeywords([Session::get('meta_keyword')]);
		// SEOMeta::SetDescription(Session::get('meta_description'));

		$paramProject = [
			'params' => [
				'a.site_id' => Session::get('site_id'),
				'b.type' => 'project',
				'a.on_sale' => 1,
				'a.status' => 1
			],
			'order' => 'a.created_date',
			'asc' => 'desc'
		];

		//BANNER TOP
		$paramBanner = [
			'params' => [
				'b.status' => 1,
				'b.site_id' => Session::get('site_id'),
				'b.slug' => 'virtual-tour',
			],
		];

		try {
			$data['projects'] = [];
			$project = $this->_courier->post($url, $paramProject);
			if ($project->getBody() != "[]") {
				$projectObj = $project->toObjects();
				$data['projects'] = $projectObj;
			}

			$banner = $this->_courier->post($urlBanner, $paramBanner);
			if ($banner->getBody() != '[]') {
                $seoObj = $banner->toObjects();
                SEOMeta::SetTitle($seoObj->meta_name != '' ? $seoObj->meta_name : Session::get('meta_name'));
                SEOMeta::SetKeywords($seoObj->meta_keyword != '' ? [$seoObj->meta_keyword] : [Session::get('meta_keyword')]);
                SEOMeta::SetDescription($seoObj->meta_description != '' ? $seoObj->meta_description : Session::get('meta_description'));
                OpenGraph::setDescription($seoObj->meta_description != '' ? $seoObj->meta_description : Session::get('meta_description'));
                OpenGraph::setTitle($seoObj->meta_name != '' ? $seoObj->meta_name : Session::get('meta_name'));
				$data['banners'] = $banner->toObjects();
			}
		} catch (HttpException $exception) {
			return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
		}
		// dd($data);
		return view('site.virtual.index')->with($data);
	}

	public function new()
	{
		// dd(!Session::get('is_login'));
		$data = [];
		$data['header'] = true;
		$data['banners'] = [];
		$urlBanner = API_URL . 'article-advance';
        $url = API_URL . 'article-advance';
		$page = Request::input('page');
		if ($page)
			$url = $url . '?page=' . $page;

		// SEOMeta::SetTitle(Session::get('meta_name'));
		// SEOMeta::SetKeywords([Session::get('meta_keyword')]);
		// SEOMeta::SetDescription(Session::get('meta_description'));


		//BANNER TOP
		$paramBanner = [
			'params' => [
				'b.status' => 1,
				'b.site_id' => Session::get('site_id'),
				'b.slug' => 'virtual-tour',
			],
		];

		$paramArticle360 = [
            'params' => [
                'a.status' => 1,
                'b.slug' => '360',

            ],
            'order' => 'a.created_date',
            'asc' => 'desc',
        ];

		try {
			$banner = $this->_courier->post($urlBanner, $paramBanner);
			if ($banner->getBody() != '[]') {
                $seoObj = $banner->toObjects()[0];
                SEOMeta::SetTitle($seoObj->meta_name != '' ? $seoObj->meta_name : Session::get('meta_name'));
                SEOMeta::SetKeywords($seoObj->meta_keyword != '' ? [$seoObj->meta_keyword] : [Session::get('meta_keyword')]);
                SEOMeta::SetDescription($seoObj->meta_description != '' ? $seoObj->meta_description : Session::get('meta_description'));
                OpenGraph::setDescription($seoObj->meta_description != '' ? $seoObj->meta_description : Session::get('meta_description'));
                OpenGraph::setTitle($seoObj->meta_name != '' ? $seoObj->meta_name : Session::get('meta_name'));
				$data['banners'] = $banner->toObjects();
			}

			$articles360 = $this->_courier->post($url, $paramArticle360);
            if ($articles360->getBody() != '[]') {
                $articles360Obj = $articles360->toObjects();
                $data['articles360'] = $articles360Obj;
            }
		} catch (HttpException $exception) {
			return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
		}
		// dd($data);
		return view('site.virtual.new')->with($data);
	}

	public function detail($slug)
	{
		// dd(!Session::get('is_login'));
		$data = [];
		$data['header'] = true;
		$data['banners'] = [];

		$urlRow = API_URL . 'article/row';
		$urlBanner = API_URL . 'article-advance';
		$page = Request::input('page');
		if ($page)
			$url = $url . '?page=' . $page;

		// SEOMeta::SetTitle(Session::get('meta_name'));
		// SEOMeta::SetKeywords([Session::get('meta_keyword')]);
		// SEOMeta::SetDescription(Session::get('meta_description'));

		//BANNER TOP
		$paramBanner = [
			'params' => [
				'b.status' => 1,
				'b.site_id' => Session::get('site_id'),
				'b.slug' => 'virtual-tour',
			],
		];

		$paramArticle = [
			'params' => [
				'b.status' => 1,
				'b.site_id' => Session::get('site_id'),
				'b.slug' => '360',
				'a.slug' =>$slug,
			],
		];

		try {
			$article = $this->_courier->post($urlRow, $paramArticle);
            if ($article->getHttpCode() == 200) {
                $pageObj = $article->toObjects();
                SEOMeta::SetTitle($pageObj->meta_name != '' ? $pageObj->meta_name : Session::get('meta_name'));
                SEOMeta::SetKeywords($pageObj->meta_keyword != '' ? [$pageObj->meta_keyword] : [Session::get('meta_keyword')]);
                SEOMeta::SetDescription($pageObj->meta_description != '' ? $pageObj->meta_description : Session::get('meta_description'));
                OpenGraph::setDescription($pageObj->meta_description != '' ? $pageObj->meta_description : Session::get('meta_description'));
                OpenGraph::setTitle($pageObj->meta_name != '' ? $pageObj->meta_name : Session::get('meta_name'));
                $data['page'] = $pageObj;

            }

			$banner = $this->_courier->post($urlBanner, $paramBanner);
			if ($banner->getBody() != '[]') {
				$data['banners'] = $banner->toObjects();
			}
		} catch (HttpException $exception) {
			return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
		}
		// dd($data);
		return view('site.virtual.detail')->with($data);
	}
}

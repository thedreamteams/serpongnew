<?php

namespace App\Http\Controllers\Facilities;

use App\Http\Controllers\Controller;
use CodeZero\Courier\Courier;
use CodeZero\Courier\Exceptions\HttpException;
use Session;
use SEOMeta;
use App\Library\Library;

class FacilitiesController extends Controller
{
    public $_courier;

    public function __construct(Courier $courier)
    {
        $this->_courier = $courier;
    }

    public function index()
    {
        $data = [];
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['category'] = [];
        $data['articles'] = [];
        $url = API_URL . 'article-advance';
        $urlcat = API_URL . 'category';
        $urlabout = API_URL . 'about-us';
        $urlBanner = API_URL . 'gallery/row';

        SEOMeta::SetTitle(Session::get('meta_name'));
        SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        SEOMeta::SetDescription(Session::get('meta_description'));

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'b.status' => 1,
                'b.site_id' => 9,
                'b.slug' => 'page-banner-about',
            ],
        ];

        try {
            $banner = $this->_courier->post($urlBanner, $paramBanner);
            if ($banner->getBody() != '[]') {
                $data['banners'] = $banner->toObjects();
            }
        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        return view('site.facilities.index')->with($data);
    }
}

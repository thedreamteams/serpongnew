<?php

namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use CodeZero\Courier\Courier;
use CodeZero\Courier\Exceptions\HttpException;
use Session;
use Request;
use SEOMeta;
use OpenGraph;
use App\Library\Library;
use App\Models\Article;

class NewsController extends Controller
{
    public $_courier;

    public function __construct(Courier $courier)
    {
        $this->_courier = $courier;
    }

    public function index($category)
    {
        $data = [];
        $data['articles'] = [];
        $data['nextPage'] = '';
        $data['category'] = $category;
        $url = API_URL . 'article-advance';
        $urlss = API_URL . 'article-advance-ss';
        $urlHot = API_URL . 'article/row';
        $page = Request::input('page');
        if ($page)
            $url .= '?page=' . $page;

        if($category=='news'){
            SEOMeta::SetTitle('Kabar Terkini Seputar Summarecon Serpong');
            SEOMeta::SetKeywords(['Kabar, Kabar Terkini, Berita, Berita Terkini, Kabar Serpong, Berita Serpong, Kabar Summarecon Serpong, Berita Seputar Serpong']);
            SEOMeta::SetDescription('Simak Kabar Terkini Seputar Summarecon Serpong Dan Berita Seputar Event, Promo, Fasilitas dan Produk-Produk Terbaru Summarecon Serpong');
        } else if($category=='information'){
            SEOMeta::SetTitle('Tips dan Informasi Terkini Seputar Summarecon Serpong');
            SEOMeta::SetKeywords(['Tips Rumah Minimalis, Tips Memilih Warna Cat Rumah, Tips Merawat Tanaman, Tips Desain Interior, Tips Desain Exterior, Informasi KPR, Ifo Summarecon Serpong, Tips Memilih KPR']);
            SEOMeta::SetDescription('Simak tips dan informasi seputar Summarecon Serpong juga tips dan informasi seputar Kredit Kepemilikan Rumah ( KPR ), Desain Exterior dan Desain Interior');
        }else {
            SEOMeta::SetTitle(Session::get('meta_name'));
            SEOMeta::SetKeywords([Session::get('meta_keyword')]);
            SEOMeta::SetDescription(Session::get('meta_description'));
        }


        //BANNER TOP
        $paramBanner = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'a.slug' => 'news-update',
            ],
        ];


        // ARTICLE
        $paramHot = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'b.slug' => $category
            ],
            'order' => 'a.start_date',
            'asc' => 'desc'
        ];

        $paramArticleNew = [
            'params' => [
                'a.status' => 1,
                'b.slug' => $category
            ],
            'order' => 'a.start_date',
            'asc' => 'desc',
            'paginate' => 20,
        ];

        try {
            $hot = $this->_courier->post($urlHot, $paramHot);
            if ($hot->getBody() != '[]') {
                $hotObj = $hot->toObjects();
                $data['hot'] = $hotObj;
                //adding param
                $paramArticleNew['advance'] = [
                    '<>' => ['a.slug' => $hotObj->slug]
                ];
            }

            $articlesNew = $this->_courier->post($urlss, $paramArticleNew);
            if ($articlesNew->getBody() != '[]') {
                $articlesNewObj = $articlesNew->toObjects();
                $data['articlesNew'] = $articlesNewObj;
                $data['nextPage'] = Library::nextPage($articlesNewObj->next_page_url);
            }

            $banner = $this->_courier->post($urlHot, $paramBanner);
            if ($banner->getBody() != '[]') {
                $bannerObj = $banner->toObjects();
                $data['banners'] = $banner->toObjects();
            }
        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }
        // dd($data);
        return view('site.news.index', $data);
    }

    public function detail($category, $slug)
    {
        $url = API_URL . 'article-advance';
        $data = [];
        $data['courier'] = $this->_courier;
        $data['category'] = $category;

        $urlArticle = API_URL . $slug;
        $urlGalllery = API_URL . 'gallery/';
        $urlProject = API_URL . 'project';
        $urlRow = API_URL . 'article/row';

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'a.slug' => 'news-update',
            ],
        ];

        $paramArticleCat = [
            'params' => [
                'a.status' => 1,
                'a.slug' => $slug,
                'b.slug' => $category,
            ],
        ];

        $paramOther = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => 9,
                'b.slug' => $category,
                'c.slug' => 'whats-on'
            ],
            'order' => 'a.start_date',
            'asc' => 'desc',
            'paginate' => 3,
        ];

        try {

            $article = $this->_courier->post($urlArticle);
            if ($article->getHttpCode() == 200) {
                $pageObj = $article->toObjects();
                $data['page'] = $pageObj;
                $paramOther['advance'] = [
                    '<>' => ['a.slug' => $pageObj->slug]
                ];

                SEOMeta::SetTitle($data['page']->meta_name);
                SEOMeta::SetKeywords([$data['page']->meta_keyword]);
                SEOMeta::SetDescription($data['page']->meta_description);

                $gallery = $this->_courier->get($urlGalllery . $data['page']->article_id);
                if ($gallery->getBody() != "[]")
                    $data['gallery'] = $gallery->toObjects();
            }
            $banner = $this->_courier->post($urlRow, $paramBanner);
            if ($banner->getBody() != '[]') {
                $data['banners'] = $banner->toObjects();
            }
            $articleCat = $this->_courier->post($urlRow, $paramArticleCat);
            if ($articleCat->getBody() != '[]') {
                $data['articleCat'] = $articleCat->toObjects();
            }
            $other = $this->_courier->post($url, $paramOther);
            if ($other->getBody() != "[]") {
                $otherObj = $other->toObjects();
                $data['others'] = $otherObj;
            }
        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }
        // dd($data);
        return view('site.news.detail', $data);
    }

    public function detailhtml()
    {
        return view('site.news.detailhtml');
    }
}

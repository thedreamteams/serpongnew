<?php

namespace App\Http\Controllers\Simulasi;

use App\Http\Controllers\Controller;

use CodeZero\Courier\Courier;
use CodeZero\Courier\Exceptions\HttpException;
use Session;
use SEOMeta;
use OpenGraph;
use App\Library\Library;

class SimulasiController extends Controller
{
    //
    public $_courier;

    public function __construct(Courier $courier)
    {
        $this->_courier = $courier;
    }

    public function index()
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['category'] = [];
        $data['articles'] = [];
        $url = API_URL . 'article-advance';
        $urlcat = API_URL . 'category';
        $urlsimulasi = API_URL . 'about-us';
        $urlProject = API_URL . 'project';

        // SEOMeta::SetTitle(Session::get('meta_name'));
        // SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        // SEOMeta::SetDescription(Session::get('meta_description'));

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'b.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => 'hitung-kpr',
            ],
        ];

        $paramArticle = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'c.slug' => 'hitung-kpr',
                'b.slug' => 'article',
			],
            'order' => 'a.brief',
			'asc' => 'asc'
		];

        $paramProject = [
			'params' => [
				'a.site_id' => Session::get('site_id'),
				'b.type' => 'project',
				'a.status' => 1,
                'a.hot' =>1,
			],
			'asc' => 'desc'
		];

        try {
            $banner = $this->_courier->post($url, $paramBanner);
            if ($banner->getBody() != '[]') {
                $bannerObj = $banner->toObjects()[0];
                SEOMeta::SetTitle($bannerObj && $bannerObj->meta_name != '' ? $bannerObj->meta_name : Session::get('meta_name'));
                SEOMeta::SetKeywords($bannerObj && $bannerObj->meta_keyword != '' ? [$bannerObj->meta_keyword] : [Session::get('meta_keyword')]);
                SEOMeta::SetDescription($bannerObj && $bannerObj->meta_description != '' ? $bannerObj->meta_description : Session::get('meta_description'));
                OpenGraph::setDescription($bannerObj && $bannerObj->meta_description != '' ? $bannerObj->meta_description : Session::get('meta_description'));
                OpenGraph::setTitle($bannerObj && $bannerObj->meta_name != '' ? $bannerObj->meta_name : Session::get('meta_name'));

                $data['banners'] = $banner->toObjects();
            }

            $articles = $this->_courier->post($url, $paramArticle);
			if ($articles->getBody() != '[]') {
				$articlesObj = $articles->toObjects();
				$data['articles'] = $articlesObj;
			}
            $project = $this->_courier->post($urlProject, $paramProject);
			if ($project->getBody() != "[]") {
				$projectObj = $project->toObjects();
				$data['projects'] = $projectObj;
			}
        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);
        return view('site.simulasi.index')->with($data);
    }
}

<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;

use CodeZero\Courier\Courier;
use CodeZero\Courier\Exceptions\HttpException;
use Session;
use Response;
use SEOMeta;
use OpenGraph;
use Request;
use Validator;
use Jenssegers\Agent\Agent;

use Mail;
use App\Library\Library;

class HomeController extends Controller
{
    //
    public $_courier;

    public function __construct(Courier $courier)
    {
        $this->_courier = $courier;
        $this->_now = date('Y-m-d', strtotime('NOW'));
    }
    public function index()
    {
        $data = [];
        $data['header'] = true;
        $data['banner'] = [];
        $data['projects'] = [];
        $data['videos'] = [];
        $data['articles'] = [];
        $data['brochures'] = [];
        $data['theBest'] = [];
        $data['livingProduct'] = [];
        $data['welcome']=[];

        $url = API_URL . 'article-advance';
        $urlProject = API_URL . 'project';
        $urlRow = API_URL . 'article/row';

        SEOMeta::SetTitle(Session::get('meta_name'));
		SEOMeta::SetKeywords([Session::get('meta_keyword')]);
		SEOMeta::SetDescription(Session::get('meta_description'));
		OpenGraph::SetTitle(Session::get('meta_name'));
		OpenGraph::SetDescription(Session::get('meta_description'));

        $data['agent'] = new Agent();

        //Banner
        $paramBanner = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'b.slug' => 'banner',
            ],
            'order' => 'a.start_date',
            'asc' => 'desc',
        ];

        //ProjectHot
        $paramProjectHot = [
            'params' => [
                'a.status' => 1,
                'a.hot' => 1,
                'a.site_id' => Session::get('site_id'),

            ],
            'asc' => 'desc',
            'paginate' => 9
        ];

        //article
        $paramArticle = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'c.slug' => 'article-ss',
            ],
            'order' => 'a.start_date',
            'asc' => 'desc',
            'paginate' => 2
        ];

        //brochure
        $paramBrochure = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'a.hot' => 1,
            ],
            'advance' => [
                'like' => ['b.slug' => '%brochure%']
            ],
            'order' => 'a.start_date',
            'asc' => 'desc'
        ];

        $paramTheBest = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'b.slug' => 'the-best-3',
                'c.slug' => 'home'
            ],
            'order' => 'a.start_date',
            'asc' => 'desc',
        ];

        $paramLivingProduct = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'b.slug' => '3-product',
                'c.slug' => 'home'
            ],
            'order' => 'a.start_date',
            'asc' => 'desc',
        ];

        $paramWelcome = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'b.slug' => 'welcome',
                'c.slug' => 'home'
            ],
            'order' => 'a.start_date',
            'asc' => 'desc',
        ];

        // $paramVirtual = [
		// 	'params' => [
		// 		'a.site_id' => Session::get('site_id'),
		// 		'b.type' => 'project',
        //         'a.on_sale' => 1,
		// 		'a.hot' => 1,
		// 		'a.status' => 1
		// 	],
		// 	'order' => 'a.created_date',
		// 	'asc' => 'desc'
		// ];

        $paramVirtual = [
            'params' => [
                'a.status' => 1,
                'a.hot' => 1,
                'b.slug' => '360',

            ],
            'order' => 'a.created_date',
            'asc' => 'desc',
        ];

        try {
            $banner = $this->_courier->post($url, $paramBanner);
            if ($banner->getBody() != '[]') {
                $data['banner'] = $banner->toObjects();
            }

            $projectsHot = $this->_courier->post($urlProject, $paramProjectHot);
            if ($projectsHot->getBody() != '[]') {
                $data['projectsHot'] = $projectsHot->toObjects();
            }

            $theBest = $this->_courier->post($url, $paramTheBest);
            if ($theBest->getBody() != '[]') {
                $theBestObj = $theBest->toObjects();
                $data['theBest'] = $theBestObj;
            }

            $livingProduct = $this->_courier->post($url, $paramLivingProduct);
            if ($livingProduct->getBody() != '[]') {
                $livingProductObj = $livingProduct->toObjects();
                $data['livingProduct'] = $livingProductObj;
            }

            $welcome = $this->_courier->post($url, $paramWelcome);
            if ($welcome->getBody() != '[]') {
                $paramWelcome = $welcome->toObjects();
                $data['welcome'] = $paramWelcome;
            }

            $articles = $this->_courier->post($url, $paramArticle);
            if ($articles->getBody() != '[]') {
                $data['articles'] = $articles->toObjects();
            }

            $brochure = $this->_courier->post($url, $paramBrochure);
            if ($brochure->getBody() != '[]') {
                $data['brochures'] = $brochure->toObjects();
            }

            $data['virtual'] = [];
			$virtual = $this->_courier->post($url, $paramVirtual);
			if ($virtual->getBody() != "[]") {
				$virtualtObj = $virtual->toObjects();
				$data['virtual'] = $virtualtObj;
			}
        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);
        // return view('site.home.index')->with($data);
        return view('site.home.index_new')->with($data);
    }

    public function preview()
    {
        $data = [];
        $data['header'] = true;
        $data['banner'] = [];
        $data['projects'] = [];
        $data['videos'] = [];
        $data['articles'] = [];
        $data['brochures'] = [];
        $data['theBest'] = [];
        $data['livingProduct'] = [];
        $data['welcome']=[];

        $url = API_URL . 'article-advance';
        $urlProject = API_URL . 'project';
        $urlRow = API_URL . 'article/row';

        SEOMeta::SetTitle(Session::get('meta_name'));
		SEOMeta::SetKeywords([Session::get('meta_keyword')]);
		SEOMeta::SetDescription(Session::get('meta_description'));
		OpenGraph::SetTitle(Session::get('meta_name'));
		OpenGraph::SetDescription(Session::get('meta_description'));

        $data['agent'] = new Agent();

        //Banner
        $paramBanner = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'b.slug' => 'banner',
            ],
            'order' => 'a.start_date',
            'asc' => 'desc',
        ];

        //ProjectHot
        $paramProjectHot = [
            'params' => [
                'a.status' => 1,
                'a.hot' => 1,
                'a.site_id' => Session::get('site_id'),

            ],
            'asc' => 'desc',
            'paginate' => 9
        ];

        //article
        $paramArticle = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'c.slug' => 'article-ss',
            ],
            'order' => 'a.start_date',
            'asc' => 'desc',
            'paginate' => 2
        ];

        //brochure
        $paramBrochure = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'a.hot' => 1,
            ],
            'advance' => [
                'like' => ['b.slug' => '%brochure%']
            ],
            'order' => 'a.start_date',
            'asc' => 'desc'
        ];

        $paramTheBest = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'b.slug' => 'the-best-3',
                'c.slug' => 'home'
            ],
            'order' => 'a.start_date',
            'asc' => 'desc',
        ];

        $paramLivingProduct = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'b.slug' => '3-product',
                'c.slug' => 'home'
            ],
            'order' => 'a.start_date',
            'asc' => 'desc',
        ];

        $paramWelcome = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'b.slug' => 'welcome',
                'c.slug' => 'home'
            ],
            'order' => 'a.start_date',
            'asc' => 'desc',
        ];

        $paramVirtual = [
			'params' => [
				'a.site_id' => Session::get('site_id'),
				'b.type' => 'project',
				'a.hot' => 1,
				'a.status' => 1
			],
			'order' => 'a.project_id',
			'asc' => 'desc'
		];

        try {
            $banner = $this->_courier->post($url, $paramBanner);
            if ($banner->getBody() != '[]') {
                $data['banner'] = $banner->toObjects();
            }

            $projectsHot = $this->_courier->post($urlProject, $paramProjectHot);
            if ($projectsHot->getBody() != '[]') {
                $data['projectsHot'] = $projectsHot->toObjects();
            }

            $theBest = $this->_courier->post($url, $paramTheBest);
            if ($theBest->getBody() != '[]') {
                $theBestObj = $theBest->toObjects();
                $data['theBest'] = $theBestObj;
            }

            $livingProduct = $this->_courier->post($url, $paramLivingProduct);
            if ($livingProduct->getBody() != '[]') {
                $livingProductObj = $livingProduct->toObjects();
                $data['livingProduct'] = $livingProductObj;
            }

            $welcome = $this->_courier->post($url, $paramWelcome);
            if ($welcome->getBody() != '[]') {
                $paramWelcome = $welcome->toObjects();
                $data['welcome'] = $paramWelcome;
            }

            $articles = $this->_courier->post($url, $paramArticle);
            if ($articles->getBody() != '[]') {
                $data['articles'] = $articles->toObjects();
            }

            $brochure = $this->_courier->post($url, $paramBrochure);
            if ($brochure->getBody() != '[]') {
                $data['brochures'] = $brochure->toObjects();
            }

            $data['virtual'] = [];
			$virtual = $this->_courier->post($urlProject, $paramVirtual);
			if ($virtual->getBody() != "[]") {
				$virtualtObj = $virtual->toObjects();
				$data['virtual'] = $virtualtObj;
			}
        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);
        return view('site.home.preview')->with($data);
    }

    public function getBrochure()
	{
        $articleBrochureId = Request::input('article_id');
        $articleFiles = Request::input('file');
        $articleProName = Request::input('projectName');

		$url = API_URL . 'download/create';
        $file= $articleFiles;
        $proname= $articleProName;
        API_URL . 'article/download/'. $articleBrochureId;

        $slugProject=Request::input('projectName');
        $urlProject = API_URL . 'project/' . $slugProject;
        $paramProject = [
            'params' => [
                //'status'=>1,
                'site_id' => Session::get('site_id'),
            ],
        ];

        $projectList = $this->_courier->post($urlProject, $paramProject);

            if ($projectList->getBody() != "[]") {
                $projectObj = $projectList->toObjects();
                $project = $projectObj;}

		$datachek = [
			'name' => Request::input('name'),
			'phone' => Request::input('phone'),
			'email' => Request::input('email'),
            'subject' => 'Download Brochure',
            'message' => 'Download Brochure From summareconserpong.com',
			'g-recaptcha-response' => trim(Request::Input('g-recaptcha-response')),
		];
        $response = [
			'response' => 'failed',
			'message' => 'Sorry, the contact process is failed',
			'process' => 'validation'
		];

        if ($this->validator($datachek)->fails())
            return response()->json($response);

		$data = [
            'params' => [
			'site_id' => Session::get('site_id'),
			'project_id' => $project->project_id,
			'name' => Request::input('name'),
			'phone' => Request::input('phone'),
			'email' => Request::input('email'),
			'utm_source' => Request::input('utm_source'),
			'utm_medium' => Request::input('utm_medium'),
			'utm_campaign' => Request::input('utm_campaign'),
			'ip' => Library::getIp(),
            ]
		];

        $add = $this->_courier->post($url, $data);
		// $add = true;
		if ($add) {

			$sendMail = Mail::send('site.contact.email', ['data' => $datachek], function ($message) use ($datachek) {
				$sub = 'Download Brochure Summarecon Serpong';

				$message->subject($sub);
				$message->from($datachek['email'], $datachek['name']);
                // $message->to("jelistina_zai@summarecon.com", "Marketing Summarecon Serpong");
				$message->to("website.summareconserpong@gmail.com", "Marketing Summarecon Serpong");
                $message->cc("veronica_cristina@summarecon.com", 'Veronica');
			});

			$response = [
				'response' => 'success',
				'message' => "We have <strong>successfully</strong> received your inquiry, Our marketing team will contact you shortly.",
				'file' => $file,
                'proname' => $proname,
                'articleid'=>$articleBrochureId
			];
		}
		return response()->json($response);
	}

	protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'g-recaptcha-response' => 'required|recaptcha',
        ]);
    }
}

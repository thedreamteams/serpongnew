<?php
namespace App\Http\Controllers\Search;

use App\Http\Controllers\Controller;
use CodeZero\Courier\Courier;
use CodeZero\Courier\Exception\HttpException;
use Session;
use Request;
use SEOMeta;
use App\Library\Library;

class SearchController extends Controller{
	public $_courier;
	public function __construct(Courier $courier){
		$this->_courier = $courier;
	}

	public function index(){
		$data = [];
		$data['header']=true;

		return view('site.search.index')->with($data);
	}

	public function action(){
		$search = trim(Request::Input('search'));
		$urlArticle = API_URL.'search/article';
		$urlProject = API_URL.'project-advance';

		SEOMeta::SetTitle(Session::get('meta_name'));
		SEOMeta::SetKeywords([Session::get('meta_keyword')]);
		SEOMeta::SetDescription(Session::get('meta_description'));
		//dd($urlProject);
		$data['articles'] = [];
		$data['articlesNew'] = [];
		$data['projects'] = [];
		$data['search'] = $search;

		try{
			$postArticle=[
				'params'=>[
					'a.site_id'=>9
				],
				'advance'=>[
					'like'=>['a.contents'=>'%'.$search.'%'],
					'not like'=>['a.slug'=>'%banner%']
				]
			];
			$article = [];
			$articles = $this->_courier->post($urlArticle,$postArticle);
			if($articles->getBody()!='[]'){
				$data['articles'] = $articles->toObjects();
			}

			$postArticleNew=[
				'params'=>[
					'a.site_id'=>Session::get('site_id')
				],
				'advance'=>[
					'like'=>['a.contents'=>'%'.$search.'%'],
					'not like'=>['a.slug'=>'%banner%']
				]
			];
			$articleNew = [];
			$articlesNew = $this->_courier->post($urlArticle,$postArticleNew);
			if($articlesNew->getBody()!='[]'){
				$data['articlesNew'] = $articlesNew->toObjects();
			}


			//PROJECT
			$postProject=[
				'params'=>[
					'a.site_id'=>Session::get('site_id')
				],
				'advance'=>[
					'like'=>['a.description'=>'%'.$search.'%']
				]
			];

			$projects = $this->_courier->post($urlProject,$postProject);
			if($projects->getBody()!='[]')
				$data['projects'] = $projects->toObjects();

			$keyword = ['karir','carer','career','carrer','carreer','lowongan','kerja','summarecon','lowong','loker','sumarecon'];
			$expWord = explode(' ' , $search);
			$visibleLink = '';
			foreach($expWord as $word){
				if(trim($visibleLink)==""){
					if(in_array($word, $keyword)){
						$visibleLink .='true';
					}
				}
			}
			
			$data['visibleLink'] = $visibleLink;
		}

		catch(HttpException $exception){
			return $exception->getCode().' : '.$exception->getMessage().' => '. $exception->response();
		}
		// dd($data);
		return view('site.search.index')->with($data);
	}
}
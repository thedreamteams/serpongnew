<?php

namespace App\Http\Controllers\Town;

use App\Http\Controllers\Controller;

use CodeZero\Courier\Courier;
use CodeZero\Courier\Exceptions\HttpException;
use Session;
use SEOMeta;
use App\Library\Library;

class TownController extends Controller
{
    //
    public $_courier;

    public function __construct(Courier $courier)
    {
        $this->_courier = $courier;
    }

    public function index()
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['town'] = [];
        $data['category'] = [];
        $data['articles'] = [];
        $url = API_URL . 'article-advance';
        $urlcat = API_URL . 'category';
        $urltown = API_URL . 'about-us';
        $urlBanner = API_URL . 'article/row';

        SEOMeta::SetTitle(Session::get('meta_name'));
        SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        SEOMeta::SetDescription(Session::get('meta_description'));

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'b.slug' => 'town-management',
            ],
        ];

        //TM
        $paramTown = [
            'params' => [
                'a.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => 'article',
                'c.slug' => 'town-management',

            ],
            'order' => 'a.brief',
            'asc' => 'asc'
        ];
        try {
            $banner = $this->_courier->post($urlBanner, $paramBanner);
            if ($banner->getBody() != '[]') {
                $data['banners'] = $banner->toObjects();
            }
            $town = $this->_courier->post($url, $paramTown);
            if ($town->getBody() != '[]') {
                $data['town'] = $town->toObjects();
            }
        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);
        return view('site.town.index')->with($data);
    }

    public function index_new()
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['town'] = [];
        $data['category'] = [];
        $data['articles'] = [];
        $url = API_URL . 'article-advance';
        $urlcat = API_URL . 'category';
        $urltown = API_URL . 'about-us';
        $urlBanner = API_URL . 'article/row';

        SEOMeta::SetTitle(Session::get('meta_name'));
        SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        SEOMeta::SetDescription(Session::get('meta_description'));

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'a.slug' => 'banner-town-management',
                'b.slug' => 'town-management',
            ],
        ];

        //TM
        $paramTown = [
            'params' => [
                'a.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'a.slug' => 'town-management',
                'b.slug' => 'town-management',

            ],
            'order' => 'a.brief',
            'asc' => 'asc'
        ];
        $paramArticle = [
            'params' => [
                'a.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => 'article',
                'c.slug' => 'town-management',

            ],
            'order' => 'a.brief',
            'asc' => 'asc'
        ];
        try {
            $banner = $this->_courier->post($urlBanner, $paramBanner);
            if ($banner->getBody() != '[]') {
                $data['banners'] = $banner->toObjects();
            }
            $town = $this->_courier->post($url, $paramTown);
            if ($town->getBody() != '[]') {
                $data['town'] = $town->toObjects();
            }
            $articles = $this->_courier->post($url, $paramArticle);
            if ($articles->getBody() != '[]') {
                $data['articles'] = $articles->toObjects();
            }
        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);
        return view('site.town.index_new')->with($data);
    }

    public function detail($slug)
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['town'] = [];
        $data['category'] = [];
        $data['articles'] = [];
        $url = API_URL . 'article-advance';
        $urlcat = API_URL . 'category';
        $urltown = API_URL . 'about-us';
        $urlBanner = API_URL . 'article/row';

        SEOMeta::SetTitle(Session::get('meta_name'));
        SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        SEOMeta::SetDescription(Session::get('meta_description'));

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'a.slug' => 'banner-town-management',
                'b.slug' => 'town-management',
            ],
        ];

        //TM
        $paramTown = [
            'params' => [
                'a.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => 'article',
                'c.slug' => 'town-management',
                'a.slug' => $slug,

            ],
            'order' => 'a.brief',
            'asc' => 'asc'
        ];
        $paramArticle = [
            'params' => [
                'a.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => 'article',
                'c.slug' => 'town-management',

            ],
            'advance' => [
                '!=' => ['a.slug' => $slug],
            ],
            'order' => 'a.brief',
            'asc' => 'asc'
        ];
        try {
            $banner = $this->_courier->post($urlBanner, $paramBanner);
            if ($banner->getBody() != '[]') {
                $data['banners'] = $banner->toObjects();
            }
            $town = $this->_courier->post($urlBanner, $paramTown);
            if ($town->getBody() != '[]') {
                $data['town'] = $town->toObjects();
            }
            $articles = $this->_courier->post($url, $paramArticle);
            if ($articles->getBody() != '[]') {
                $data['articles'] = $articles->toObjects();
            }
        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);
        return view('site.town.detail')->with($data);
    }
}

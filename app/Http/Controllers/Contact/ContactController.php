<?php

namespace App\Http\Controllers\Contact;

use App\Http\Controllers\Controller;

use CodeZero\Courier\Courier;
use CodeZero\Courier\Exceptions\HttpException;
use Session;
use Validator;
use SEOMeta;
use OpenGraph;
use Mail;
use Request;
use App\Library\Library;
use App\Models\{Article};

class ContactController extends Controller
{
    //
    public $_courier;

    public function __construct(Courier $courier)
    {
        $this->_courier = $courier;
    }

    protected function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required',
            'email' => 'required',
			'subject' => 'required',
			'phone' => 'required',
			'message' => 'required',
			'g-recaptcha-response' => 'required|recaptcha',
		]);
	}

    protected function validator_contact(array $data)
	{
		return Validator::make($data, [
			'name' => 'required',
            'email' => 'required',
			'subject' => 'required',
			'phone' => 'required',
			'message' => 'required',
			'g-recaptcha-response' => 'required|recaptchav3:contact,0.5'
		]);
	}

    protected function validator_catalog(array $data)
	{
		return Validator::make($data, [
			'name' => 'required',
            // 'email' => 'required',
			'subject' => 'required',
			'phone' => 'required',
			// 'message' => 'required',
			'g-recaptcha-response' => 'required|recaptchav3:catalog,0.5'
		]);
	}

    public function index()
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['category'] = [];
        $data['articles'] = [];
        $url = API_URL . 'article-advance';
        $urlcat = API_URL . 'category';
        $urlcontact = API_URL . 'contact-us';

        SEOMeta::SetTitle(Session::get('meta_name'));
        SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        SEOMeta::SetDescription(Session::get('meta_description'));

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'b.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => 'contact-us',
            ],
        ];

        try {
            $banner = $this->_courier->post($url, $paramBanner);
            if ($banner->getBody() != '[]') {
                $bannerObj = $banner->toObjects()[0];
                SEOMeta::SetTitle($bannerObj && $bannerObj->meta_name != '' ? $bannerObj->meta_name : Session::get('meta_name'));
                SEOMeta::SetKeywords($bannerObj && $bannerObj->meta_keyword != '' ? [$bannerObj->meta_keyword] : [Session::get('meta_keyword')]);
                SEOMeta::SetDescription($bannerObj && $bannerObj->meta_description != '' ? $bannerObj->meta_description : Session::get('meta_description'));
                OpenGraph::setDescription($bannerObj && $bannerObj->meta_description != '' ? $bannerObj->meta_description : Session::get('meta_description'));
                OpenGraph::setTitle($bannerObj && $bannerObj->meta_name != '' ? $bannerObj->meta_name : Session::get('meta_name'));

                $data['banners'] = $banner->toObjects();
            }
        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);
        return view('site.contact.index')->with($data);
    }

	public function detaillanded()
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['category'] = [];
        $data['articles'] = [];
        $url = API_URL . 'article-advance';
        $urlcat = API_URL . 'category';
        $urlcontact = API_URL . 'contact-us';

        // SEOMeta::SetTitle(Session::get('meta_name'));
        // SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        // SEOMeta::SetDescription(Session::get('meta_description'));

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'b.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => 'sales-executive',
            ],
        ];
		//Sales Landed
        $paramSalesLanded = [
            'params' => [
                'a.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => 'Sales-rumah-dan-ruko',
				'c.slug' => 'sales-executive',
            ],
			'order' => 'a.name',
            'asc' => 'asc',
        ];
        try {
            $banner = $this->_courier->post($url, $paramBanner);
            if ($banner->getBody() != '[]') {
                $bannerObj = $banner->toObjects()[0];
                SEOMeta::SetTitle($bannerObj && $bannerObj->meta_name != '' ? $bannerObj->meta_name : Session::get('meta_name'));
                SEOMeta::SetKeywords($bannerObj && $bannerObj->meta_keyword != '' ? [$bannerObj->meta_keyword] : [Session::get('meta_keyword')]);
                SEOMeta::SetDescription($bannerObj && $bannerObj->meta_description != '' ? $bannerObj->meta_description : Session::get('meta_description'));
                OpenGraph::setDescription($bannerObj && $bannerObj->meta_description != '' ? $bannerObj->meta_description : Session::get('meta_description'));
                OpenGraph::setTitle($bannerObj && $bannerObj->meta_name != '' ? $bannerObj->meta_name : Session::get('meta_name'));

                $data['banners'] = $banner->toObjects();
            }
			$salesLanded = $this->_courier->post($url, $paramSalesLanded);
            if ($salesLanded->getBody() != '[]') {
                $data['sales'] = $salesLanded->toObjects();
            }
        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);
        return view('site.contact.detail')->with($data);
    }

    public function detailstrata()
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['category'] = [];
        $data['articles'] = [];
        $url = API_URL . 'article-advance';
        $urlcat = API_URL . 'category';
        $urlcontact = API_URL . 'contact-us';

        // SEOMeta::SetTitle(Session::get('meta_name'));
        // SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        // SEOMeta::SetDescription(Session::get('meta_description'));

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'b.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => 'sales-executive',
            ],
        ];
		//Sales Strata
        $paramSalesStrata = [
            'params' => [
                'a.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => 'sales-apartment-condovillas',
				'c.slug' => 'sales-executive',
            ],
			'order' => 'a.name',
            'asc' => 'asc',
        ];

        try {
            $banner = $this->_courier->post($url, $paramBanner);
            if ($banner->getBody() != '[]') {
                $bannerObj = $banner->toObjects()[0];
                SEOMeta::SetTitle($bannerObj && $bannerObj->meta_name != '' ? $bannerObj->meta_name : Session::get('meta_name'));
                SEOMeta::SetKeywords($bannerObj && $bannerObj->meta_keyword != '' ? [$bannerObj->meta_keyword] : [Session::get('meta_keyword')]);
                SEOMeta::SetDescription($bannerObj && $bannerObj->meta_description != '' ? $bannerObj->meta_description : Session::get('meta_description'));
                OpenGraph::setDescription($bannerObj && $bannerObj->meta_description != '' ? $bannerObj->meta_description : Session::get('meta_description'));
                OpenGraph::setTitle($bannerObj && $bannerObj->meta_name != '' ? $bannerObj->meta_name : Session::get('meta_name'));

                $data['banners'] = $banner->toObjects();
            }
			$salesStrata = $this->_courier->post($url, $paramSalesStrata);
            if ($salesStrata->getBody() != '[]') {
                $data['sales'] = $salesStrata->toObjects();
            }

        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);
        return view('site.contact.detail')->with($data);
    }

    public function catalog($slug)
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['category'] = [];
        $data['articles'] = [];
        $data['projects'] = [];
        $data['projects3'] = [];
        $data['welcome']=[];
        $data['nextPage'] = '';
        $url = API_URL . 'project';
        $urlcat = API_URL . 'category';
        $urlproject = API_URL . 'about-us';
        $urlBanner = API_URL . 'article-advance';
        $urlRow = API_URL . 'article/row';

        $page = Request::input('page');
        if ($page)
            $url = $url . '?page=' . $page;


        if (request()->is('sales-executive-landed/*')){
            $iCategory = 'rumah';
            $iCategory2 = 'ruko-komersial';
        }else {
            $iCategory = 'apartment-condovillas';
        }

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'b.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => 'sales-executive',
            ],
        ];

        //Sales Landed
        $paramSalesLanded = [
            'params' => [
                'a.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => 'Sales-rumah-dan-ruko',
				'c.slug' => 'sales-executive',
				'a.slug' => $slug
            ],
			'order' => 'a.name',
            'asc' => 'asc',
        ];

        //Sales Strata
        $paramSalesStrata = [
            'params' => [
                'a.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => 'sales-apartment-condovillas',
				'c.slug' => 'sales-executive',
                'a.slug' => $slug
            ],
			'order' => 'a.name',
            'asc' => 'asc',
        ];

        if (request()->is('sales-executive-landed/*')){
            $paramProject = [
                'params' => [
                    'a.site_id' => Session::get('site_id'),
                    'b.type' => 'project',
                    'a.status' => 1,
                    'a.on_sale' => 1,
                    'b.slug' => $iCategory
                ],
                'order' => 'a.hot',
                'asc' => 'desc',
                'paginate' => 8
            ];

            $paramProject2 = [
                'params' => [
                    'a.site_id' => Session::get('site_id'),
                    'b.type' => 'project',
                    'a.status' => 1,
                    'a.on_sale' => 1,
                    'b.slug' => $iCategory2
                ],
                'order' => 'a.hot',
                'asc' => 'desc',
                'paginate' => 8
            ];
        }else{
            $paramProject = [
                'params' => [
                    'a.site_id' => Session::get('site_id'),
                    'b.type' => 'project',
                    'a.status' => 1,
                    'a.on_sale' => 1,
                    'b.slug' => $iCategory
                ],
                'order' => 'a.hot',
                'asc' => 'desc',
                'paginate' => 8
            ];
        }

        $paramProject3 = [
            'params' => [
                'a.site_id' => Session::get('site_id'),
                'b.type' => 'project',
                'a.status' => 1,
                'a.on_sale' => 0,
            ],
            'order' => 'a.hot',
            'asc' => 'desc',
            'paginate' => 4
        ];

        $paramVirtual = [
            'params' => [
                'a.site_id' => Session::get('site_id'),
                'b.type' => 'project',
                'a.hot' => 1,
                'a.status' => 1
            ],
            'order' => 'a.project_id',
            'asc' => 'desc'
        ];

        try {
            $banner = $this->_courier->post($urlBanner, $paramBanner);
            if ($banner->getBody() != '[]') {
                $data['banners'] = $banner->toObjects();
            }

            $salesLanded = $this->_courier->post($urlRow, $paramSalesLanded);
            if ($salesLanded->getBody() != '[]') {
                $data['sales'] = $salesLanded->toObjects();
            }

            $salesStrata = $this->_courier->post($urlRow, $paramSalesStrata);
            if ($salesStrata->getBody() != '[]') {
                $data['sales'] = $salesStrata->toObjects();
            }

            SEOMeta::SetTitle($data['sales'] != '' && $data['sales']->meta_name != '' ? $data['sales']->meta_name : $data['sales']->name.' - '.$data['sales']->parent.' '.$data['sales']->category.' - '.Session::get('meta_name'));
            SEOMeta::SetKeywords($data['sales'] && $data['sales']->meta_keyword != '' ? [$data['sales']->meta_keyword] : [Session::get('meta_keyword')]);
            SEOMeta::SetDescription($data['sales'] && $data['sales']->meta_description != '' ? $data['sales']->meta_description : $data['sales']->name.' - '.$data['sales']->parent.' '.$data['sales']->category.' - '.Session::get('meta_description'));
            OpenGraph::setDescription($data['sales'] && $data['sales']->meta_description != '' ? $data['sales']->meta_description : $data['sales']->name.' - '.$data['sales']->parent.' '.$data['sales']->category.' - '.Session::get('meta_description'));
            OpenGraph::setTitle($data['sales'] && $data['sales']->meta_name != '' ? $data['sales']->meta_name : $data['sales']->name.' - '.$data['sales']->parent.' '.$data['sales']->category.' - '.Session::get('meta_name'));

            $project = $this->_courier->post($url, $paramProject);
            if ($project->getBody() != "[]") {
                $projectObj = $project->toObjects();
                $data['projects'] = $projectObj;
                $data['nextPage'] = Library::nextPage($projectObj->next_page_url);
                $data['prevPage'] = Library::nextPage($projectObj->prev_page_url);
                $data['projects'] = $projectObj;
            }

            if (isset($paramProject2)) {
                $project2 = $this->_courier->post($url, $paramProject2);
                if ($project2->getBody() != "[]") {
                    $projectObj2 = $project2->toObjects();
                    $data['projects2'] = $projectObj2;
                    $data['nextPage2'] = Library::nextPage($projectObj2->next_page_url);
                    $data['prevPage2'] = Library::nextPage($projectObj2->prev_page_url);
                    $data['projects2'] = $projectObj2;
                }
            }

            $project3 = $this->_courier->post($url, $paramProject3);
            if ($project3->getBody() != "[]") {
                $projectObj3 = $project3->toObjects();
                $data['projects3'] = $projectObj3;
                $data['nextPage'] = Library::nextPage($projectObj3->next_page_url);
                $data['prevPage'] = Library::nextPage($projectObj3->prev_page_url);
                $data['projects3'] = $projectObj3;
            }
            $data['virtual'] = [];
            $virtual = $this->_courier->post($url, $paramVirtual);
            if ($virtual->getBody() != "[]") {
                $virtualtObj = $virtual->toObjects();
                $data['virtual'] = $virtualtObj;
            }
        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);
        $ipclients=request()->ip();
        $ipcookiesclicked = Session::get('ipcookiesclicked');
        $salescookies = Session::get('salescookies');
        // dd($salescookies);
        if (!isset($ipcookiesclicked) && !isset($salescookies) && $ipcookiesclicked != $ipclients) {
            $getClicked = Article::where('article_id',$data['sales']->article_id)->first();
            $countClicked = $getClicked->clicked + 1;
            $getClicked->update(['clicked'=>$countClicked]);
            $newLifetime=1440;
            config(['session.lifetime' => $newLifetime]);
            Session::put('ipcookiesclicked', $ipclients);
            Session::put('salescookies', $slug);
        }

        return view('site.contact.catalog')->with($data);
    }

    public function action()
	{
		$url = API_URL . 'contact/create';
		$data = [
			'site_id' =>Session::get('site_id'),
			'name' => trim(Request::Input('name')),
            'email' => trim(Request::Input('email')),
			'phone' => trim(Request::Input('phone')),
			'subject' => trim(Request::Input('subject')),
			'message' => trim(Request::Input('message')),
			'g-recaptcha-response' => trim(Request::Input('g-recaptcha-response')),
		];
		if ($this->validator_contact($data)->fails()) {
			$str = '';
			foreach ($this->validator_contact($data)->errors()->messages() as $key => $value) {
				foreach ($value as $v) {
					$str .= $key . " = " . $v . '<br/>';
				}
			}
			$response = [
				'response' => 'error',
				'message' => $str
			];
			return response()->json($response);
		}
		try {
			$dataInsert = [
				'params' => [
					'site_id' => Session::get('site_id'),
					'name' => trim(Request::Input('name')),
                    'email' => trim(Request::Input('email')),
					'phone' => trim(Request::Input('phone')),
					'subject' => trim(Request::Input('subject')),
					'message' => trim(Request::Input('message')),
					'utm_source' => Request::Input('utm_source'),
					'utm_medium' => Request::Input('utm_medium'),
					'utm_campaign' => Request::Input('utm_campaign'),
					'ip' => Library::getIp(),
				]
			];
			$insert = $this->_courier->post($url, $dataInsert);

			 // SEND TO ADMIN
             Mail::send('site.contact.email', ['data' => $data], function ($message) use ($data) {
                $subjects = [
                    'marketing' => 'website.summareconserpong@gmail.com',
                    'estate' => 'website.summareconserpong@gmail.com',
                    'hrd' => 'website.summareconserpong@gmail.com',
                    'about' => 'website.summareconserpong@gmail.com',
                    'others' => 'website.summareconserpong@gmail.com'
                ];
                $to = $subjects[$data['subject']];

                $message->to($to);
                $message->subject('Inquiry Online From summareconserpong.com');
                $message->from($data['email'], $data['name']);
                $message->cc("veronica_cristina@summarecon.com", 'Veronica');
            });

            //SEND TO USER
            Mail::send('site.contact.email_send', ['data' => $data], function ($message) use ($data) {
                $subjects = [
                    'marketing' => 'website.summareconserpong@gmail.com',
                    'estate' => 'website.summareconserpong@gmail.com',
                    'hrd' => 'website.summareconserpong@gmail.com',
                    'about' => 'website.summareconserpong@gmail.com',
                    'others' => 'website.summareconserpong@gmail.com'
                ];
                $from = $subjects[$data['subject']];

                $message->from($from);
                $message->subject('Inquiry Online From summareconserpong.com');
                $message->to($data['email'], $data['name']);
                $message->cc("veronica_cristina@summarecon.com", 'Veronica');
            });

			$status = $insert->toObjects();
			if ($status->status == "true") {
				$response = [
					'response' => 'success',
					'message' => "We have <strong>successfully</strong> received your Message and will get Back to you as soon as possible."
				];
				return response()->json($response);
			}
		} catch (HttpException $exception) {
			$msg = $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
			$response = [
				'response' => 'error',
				'message' => $msg
			];
			return response()->json($response);
		}

		$response = [
			'response' => 'error',
			'message' => 'Sorry, Please fill out the field correctly'
		];

		return response()->json($response);
	}

    public function action_catalog()
	{
        // return Request::Input('g-recaptcha-response');
		$url = API_URL . 'contact/create';
		$data = [
			'site_id' =>Session::get('site_id'),
			'name' => trim(Request::Input('name')),
            // 'email' => trim(Request::Input('email')),
			'phone' => trim(Request::Input('phone')),
			'subject' => trim(Request::Input('subject')),
			// 'message' => trim(Request::Input('message')),
			'g-recaptcha-response' => Request::Input('g-recaptcha-response'),
		];
		if ($this->validator_catalog($data)->fails()) {
			$str = '';
			foreach ($this->validator_catalog($data)->errors()->messages() as $key => $value) {
				foreach ($value as $v) {
					$str .= $key . " = " . $v . '<br/>';
				}
			}
			$response = [
				// 'message' => '<span class="text-white">'.$str.'</span>',
				'message' => $str,
                'response' => 'Error'
			];
			return redirect()->back()->withErrors($response);
		}
		try {
			$dataInsert = [
				'params' => [
					'site_id' => Session::get('site_id'),
					'name' => trim(Request::Input('name')),
                    // 'email' => trim(Request::Input('email')),
					'phone' => trim(Request::Input('phone')),
					'subject' => trim(Request::Input('subject')),
					'message' => trim(Request::Input('message')),
					'utm_source' => Request::Input('utm_source'),
					'utm_medium' => Request::Input('utm_medium'),
					'utm_campaign' => Request::Input('utm_campaign'),
					'ip' => Library::getIp(),
				]
			];
			$insert = $this->_courier->post($url, $dataInsert);
            $redirectWA = 'https://api.whatsapp.com/send?phone='.trim(Request::Input('agent_phone')).'&text=Halo%20mau%20tanya%20info%20Summarecon%20Serpong%20';

            return redirect()->to($redirectWA);

			 // SEND TO ADMIN
            // Mail::send('site.contact.email', ['data' => $data], function ($message) use ($data) {

            //     $to = 'website.summareconserpong@gmail.com';

            //     $message->to($to);
            //     $message->subject($data['subject'].' From summareconserpong.com');
            //     $message->from($data['email'], $data['name']);
            //     $message->cc("veronica_cristina@summarecon.com", 'Veronica');
            // });

            //SEND TO USER
            // Mail::send('site.contact.email_send', ['data' => $data], function ($message) use ($data) {
            //     $from = 'website.summareconserpong@gmail.com';

            //     $message->from($from);
            //     $message->subject('Inquiry Online From summareconserpong.com');
            //     $message->to($data['email'], $data['name']);
            //     $message->cc("veronica_cristina@summarecon.com", 'Veronica');
            // });

			$status = $insert->toObjects();
			if ($status->status == "true") {
				// $response = [
				// 	'response' => 'success',
				// 	'message' => "We have <strong>successfully</strong> received your Message and will get Back to you as soon as possible."
				// ];
				// return response()->json($response);
                return redirect()->to($redirectWA);
			}
		} catch (HttpException $exception) {
			// $msg = $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
			// $response = [
			// 	'response' => 'error',
			// 	'message' => $msg
			// ];
			// return response()->json($response);
            return redirect()->to($redirectWA);
		}

		$response = [
			'response' => 'error',
			'message' => 'Sorry, Please fill out the field correctly'
		];

		return response()->json($response);
	}

    public function disclaimer(){
        $url = API_URL . 'article-advance';

        SEOMeta::SetTitle(Session::get('meta_name'));
        SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        SEOMeta::SetDescription(Session::get('meta_description'));

        //BANNER TOP
        $param = [
            'params' => [
                'b.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => 'disclaimer',
            ],
        ];

        try {
            $banner = $this->_courier->post($url, $param);
            if ($banner->getBody() != '[]') {
                $data['article'] = $banner->toObjects();
            }
        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);

        return view('site.contact.disclaimer')->with($data);
    }

}






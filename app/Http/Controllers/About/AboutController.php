<?php

namespace App\Http\Controllers\About;

use App\Http\Controllers\Controller;

use CodeZero\Courier\Courier;
use CodeZero\Courier\Exceptions\HttpException;
use Session;
use SEOMeta;
use App\Library\Library;

class AboutController extends Controller
{
    //
    public $_courier;

    public function __construct(Courier $courier)
    {
        $this->_courier = $courier;
    }

    public function index()
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['category'] = [];
        $data['articles'] = [];
        $url = API_URL . 'article-advance';
        $urlcat = API_URL . 'category';
        $urlabout = API_URL . 'tentang-summarecon';
        $urlSEO = API_URL . 'tentang-kawasan';
        $urlBanner = API_URL . 'article/row';
        $urlCard = API_URL . 'summarecon-serpong-club-card';

        // SEOMeta::SetTitle(Session::get('meta_name'));
        // SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        // SEOMeta::SetDescription(Session::get('meta_description'));

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'a.slug' => 'a-truly-liveable-city',
            ],
        ];

        $paramArticle = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'b.slug' => 'thumbnail',
                'c.slug' => 'about-ss'
            ],
            'order' => 'a.note_1',
            'asc' => 'asc',
        ];

        $paramTentang = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'b.slug' => 'tentang',
                'c.slug' => 'about'
            ],
            'order' => 'a.start_date',
            'asc' => 'desc',
        ];

        // $paramAwards = [
        //     'params' => [
        //         'a.status' => 1,
        //         'a.site_id' => 9,
        //         'b.slug' => 'about-us-awards',
        //         'c.slug' => 'about-us'
        //     ],
        //     'order' => 'a.start_date',
        //     'asc' => 'desc',
        // ];
        try {
            $banner = $this->_courier->post($urlBanner, $paramBanner);
            if ($banner->getBody() != '[]') {
                $data['banners'] = $banner->toObjects();
            }

            $about = $this->_courier->post($urlabout);
            if ($about->getBody() != '[]') {
                $aboutObj = $about->toObjects();
                $data['about'] = $aboutObj;
            }

            $articles = $this->_courier->post($url, $paramArticle);
            if ($articles->getBody() != '[]') {
                $articlesObj = $articles->toObjects();
                $data['articles'] = $articlesObj;
            }

            $tentang = $this->_courier->post($url, $paramTentang);
            if ($tentang->getBody() != '[]') {
                $tentangObj = $tentang->toObjects();
                $data['tentang'] = $tentangObj;
            }

            // $awards = $this->_courier->post($url, $paramAwards);
            // if ($awards->getBody() != '[]') {
            //     $awardsObj = $awards->toObjects();
            //     $data['awards'] = $awardsObj;
            // }
            $card = $this->_courier->post($urlCard);
            if ($card->getBody() != '[]') {
                $cardObj = $card->toObjects();
                $data['card'] = $cardObj;
            }
        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        $seo = $this->_courier->post($urlSEO);
        if ($seo->getBody() != '[]') {
            $seoObj = $seo->toObjects();
            $data['seo'] = $seoObj;
        }

        SEOMeta::SetTitle($seoObj->meta_name != '' ? $seoObj->meta_name : Session::get('meta_name'));
        SEOMeta::SetKeywords($seoObj->meta_keyword != '' ? [$seoObj->meta_keyword] : [Session::get('meta_keyword')]);
        SEOMeta::SetDescription($seoObj->meta_description != '' ? $seoObj->meta_description : Session::get('meta_description'));

        // dd($data);
        return view('site.about.index')->with($data);
    }
    public function kawasan()
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['category'] = [];
        $data['articles'] = [];
        $url = API_URL . 'article-advance';
        $urlcat = API_URL . 'category';
        $urlabout = API_URL . 'tentang-kawasan';
        $urlCard = API_URL . 'summarecon-serpong-club-card';
        $urlBanner = API_URL . 'article/row';

        // SEOMeta::SetTitle(Session::get('meta_name'));
        // SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        // SEOMeta::SetDescription(Session::get('meta_description'));

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'a.slug' => 'a-truly-liveable-city',
            ],
        ];


        try {
            $banner = $this->_courier->post($urlBanner, $paramBanner);
            if ($banner->getBody() != '[]') {
                $data['banners'] = $banner->toObjects();
            }

            $about = $this->_courier->post($urlabout);
            if ($about->getBody() != '[]') {
                $aboutObj = $about->toObjects();
                $data['about'] = $aboutObj;
            }

            $card = $this->_courier->post($urlCard);
            if ($card->getBody() != '[]') {
                $cardObj = $card->toObjects();
                $data['card'] = $cardObj;
            }

        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        SEOMeta::SetTitle($aboutObj->meta_name != '' ? $aboutObj->meta_name : Session::get('meta_name'));
        SEOMeta::SetKeywords($aboutObj->meta_keyword != '' ? [$aboutObj->meta_keyword] : [Session::get('meta_keyword')]);
        SEOMeta::SetDescription($aboutObj->meta_description != '' ? $aboutObj->meta_description : Session::get('meta_description'));

        // dd($data);
        return view('site.about.kawasan')->with($data);
    }

    public function pengembang()
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['category'] = [];
        $data['articles'] = [];
        $url = API_URL . 'article-advance';
        $urlcat = API_URL . 'category';
        $urlabout = API_URL . 'tentang-summarecon';
        $urlCard = API_URL . 'summarecon-serpong-club-card';
        $urlBanner = API_URL . 'article/row';

        // SEOMeta::SetTitle(Session::get('meta_name'));
        // SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        // SEOMeta::SetDescription(Session::get('meta_description'));

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'a.slug' => 'a-truly-liveable-city',
            ],
        ];


        try {
            $banner = $this->_courier->post($urlBanner, $paramBanner);
            if ($banner->getBody() != '[]') {
                $data['banners'] = $banner->toObjects();
            }

            $about = $this->_courier->post($urlabout);
            if ($about->getBody() != '[]') {
                $aboutObj = $about->toObjects();
                $data['about'] = $aboutObj;
            }

            $card = $this->_courier->post($urlCard);
            if ($card->getBody() != '[]') {
                $cardObj = $card->toObjects();
                $data['card'] = $cardObj;
            }

        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        SEOMeta::SetTitle($aboutObj->meta_name != '' ? $aboutObj->meta_name : Session::get('meta_name'));
        SEOMeta::SetKeywords($aboutObj->meta_keyword != '' ? [$aboutObj->meta_keyword] : [Session::get('meta_keyword')]);
        SEOMeta::SetDescription($aboutObj->meta_description != '' ? $aboutObj->meta_description : Session::get('meta_description'));

        // dd($data);
        return view('site.about.pengembang')->with($data);
    }

    public function awards()
    {
        $data = [];
		$data['header'] = true;
		$data['banners'] = [];
		$data['articles']=[];
		$awards = [];
		$url = API_URL.'article';
		$urlBanner = API_URL.'gallery/row';
        $urlBanner = API_URL . 'article/row';

		SEOMeta::SetTitle(Session::get('meta_name'));
		SEOMeta::SetKeywords([Session::get('meta_keyword')]);
		SEOMeta::SetDescription(Session::get('meta_description'));

		//BANNER TOP
        $paramBanner = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'a.slug' => 'a-truly-liveable-city',
            ],
        ];

		//PARAMS
		$params = [
			'params'=>[
				'a.status'=>1,
				'a.site_id'=>Session::get('site_id'),
				'b.slug'=>'award',
			],
			'order'=>'a.name',
			'asc'=>'desc',
		];

		try{
			$article = $this->_courier->post($url,$params);
			if($article->getHttpCode()!='[]'){
				$articleObj = $article->toObjects();
				foreach ($articleObj as $article) {
					$awards[$article->note_1][] = $article;
				}
				krsort($awards);
				$data['articles'] = $awards;
			}

			$banner = $this->_courier->post($urlBanner, $paramBanner);
            if ($banner->getBody() != '[]') {
                $data['banners'] = $banner->toObjects();
            }
		}

		catch(HttpException $exception){
			return $exception->getCode().' : '.$exception->getMessage.' => '. $exception->response();
		}

        // dd($data);
        return view('site.about.awards')->with($data);
    }

    public function access()
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['category'] = [];
        $data['articles'] = [];
        $url = API_URL . 'article-advance';
        $urlcat = API_URL . 'category';
        $urlabout = API_URL . 'strategic-location-easy-accessibility';
        $urlCard = API_URL . 'summarecon-serpong-club-card';
        $urlBanner = API_URL . 'article/row';

        SEOMeta::SetTitle(Session::get('meta_name'));
        SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        SEOMeta::SetDescription(Session::get('meta_description'));

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'a.slug' => 'a-truly-liveable-city',
            ],
        ];


        try {
            $banner = $this->_courier->post($urlBanner, $paramBanner);
            if ($banner->getBody() != '[]') {
                $data['banners'] = $banner->toObjects();
            }

            $about = $this->_courier->post($urlabout);
            if ($about->getBody() != '[]') {
                $aboutObj = $about->toObjects();
                $data['about'] = $aboutObj;
            }

            $card = $this->_courier->post($urlCard);
            if ($card->getBody() != '[]') {
                $cardObj = $card->toObjects();
                $data['card'] = $cardObj;
            }

        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);
        return view('site.about.access')->with($data);
    }

    public function masterplan()
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['category'] = [];
        $data['articles'] = [];
        $url = API_URL . 'article-advance';
        $urlcat = API_URL . 'category';
        $urlabout = API_URL . 'tentang-kawasan';
        $urlCard = API_URL . 'summarecon-serpong-club-card';
        $urlBanner = API_URL . 'article/row';

        SEOMeta::SetTitle(Session::get('meta_name'));
        SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        SEOMeta::SetDescription(Session::get('meta_description'));

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
                'a.slug' => 'a-truly-liveable-city',
            ],
        ];


        try {
            $banner = $this->_courier->post($urlBanner, $paramBanner);
            if ($banner->getBody() != '[]') {
                $data['banners'] = $banner->toObjects();
            }

            $about = $this->_courier->post($urlabout);
            if ($about->getBody() != '[]') {
                $aboutObj = $about->toObjects();
                $data['about'] = $aboutObj;
            }

            $card = $this->_courier->post($urlCard);
            if ($card->getBody() != '[]') {
                $cardObj = $card->toObjects();
                $data['card'] = $cardObj;
            }

        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);
        return view('site.about.masterplan')->with($data);
    }

}

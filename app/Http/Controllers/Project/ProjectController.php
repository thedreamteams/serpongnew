<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\Controller;

use CodeZero\Courier\Courier;
use CodeZero\Courier\Exceptions\HttpException;
use Session;
use SEOMeta;
use Mail;
use OpenGraph;
use Request;
use Validator;
use Jenssegers\Agent\Agent;

use App\Library\Library;

use App\Models\Download;
use App\Models\ProjectArticle;
use App\Models\Article;


class ProjectController extends Controller
{
    //
    public $_courier;

    public function __construct(Courier $courier)
    {
        $this->_courier = $courier;
    }

    public function index()
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['category'] = [];
        $data['articles'] = [];
        $data['projects'] = [];
        $data['projects3'] = [];
        $data['welcome']=[];
        $data['nextPage'] = '';
        $url = API_URL . 'project';
        $urlcat = API_URL . 'category';
        $urlproject = API_URL . 'about-us';
        $urlBanner = API_URL . 'article-advance';
        $urlRow = API_URL . 'article/row';

        $page = Request::input('page');
		if ($page)
			$url = $url . '?page=' . $page;

        // SEOMeta::SetTitle(Session::get('meta_name'));
        // SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        // SEOMeta::SetDescription(Session::get('meta_description'));

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'b.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => 'rumah',
            ],
        ];

        $paramProject = [
            'params' => [
                'a.site_id' => Session::get('site_id'),
                'b.type' => 'project',
                'a.status' => 1,
                'a.on_sale' => 1,
            ],
            'order' => 'a.hot',
            'asc' => 'desc',
            'paginate' => 8
        ];

        $paramProject3 = [
            'params' => [
                'a.site_id' => Session::get('site_id'),
                'b.type' => 'project',
                'a.status' => 1,
                'a.on_sale' => 0,
            ],
            'order' => 'a.hot',
            'asc' => 'desc',
            'paginate' => 4
        ];

        $paramVirtual = [
			'params' => [
				'a.site_id' => Session::get('site_id'),
				'b.type' => 'project',
				'a.hot' => 1,
				'a.status' => 1
			],
			'order' => 'a.project_id',
			'asc' => 'desc'
		];

        try {
            $banner = $this->_courier->post($urlBanner, $paramBanner);
            if ($banner->getBody() != '[]') {
                $data['banners'] = $banner->toObjects();
            }

            $project = $this->_courier->post($url, $paramProject);
			if ($project->getBody() != "[]") {
				$projectObj = $project->toObjects();
				$data['projects'] = $projectObj;
                $data['nextPage'] = Library::nextPage($projectObj->next_page_url);
                $data['prevPage'] = Library::nextPage($projectObj->prev_page_url);
				$data['projects'] = $projectObj;
			}

            $project3 = $this->_courier->post($url, $paramProject3);
			if ($project3->getBody() != "[]") {
				$projectObj3 = $project3->toObjects();
				$data['projects3'] = $projectObj3;
                $data['nextPage'] = Library::nextPage($projectObj3->next_page_url);
                $data['prevPage'] = Library::nextPage($projectObj3->prev_page_url);
				$data['projects3'] = $projectObj3;
			}
            $data['virtual'] = [];
			$virtual = $this->_courier->post($url, $paramVirtual);
			if ($virtual->getBody() != "[]") {
				$virtualtObj = $virtual->toObjects();
				$data['virtual'] = $virtualtObj;
			}
        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);
        return view('site.project.index')->with($data);
    }

    public function category($category)
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['category'] = [];
        $data['articles'] = [];
        $data['projects'] = [];
        $data['projects3'] = [];
        $data['nextPage'] = '';
        $url = API_URL . 'project';
        $urlcat = API_URL . 'category';
        $urlproject = API_URL . 'about-us';
        $urlBanner = API_URL . 'article-advance';

        $page = Request::input('page');
		if ($page)
			$url = $url . '?page=' . $page;

        // SEOMeta::SetTitle(Session::get('meta_name'));
        // SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        // SEOMeta::SetDescription(Session::get('meta_description'));

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'a.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => $category,
            ],
        ];

        $paramProject = [
            'params' => [
                'a.site_id' => Session::get('site_id'),
                'b.type' => 'project',
                'a.status' => 1,
                'a.on_sale' => 1,
                'b.slug' => $category
            ],
            'order' => 'a.hot',
            'asc' => 'desc',
            'paginate' => 8
        ];

        $paramProject3 = [
            'params' => [
                'a.site_id' => Session::get('site_id'),
                'b.type' => 'project',
                'a.status' => 1,
                'a.on_sale' => 0,
                'b.slug' => $category
            ],
            'order' => 'a.hot',
            'asc' => 'desc',
            'paginate' => 4
        ];

        $paramVirtual = [
			'params' => [
				'a.site_id' => Session::get('site_id'),
				'b.type' => 'project',
				'a.hot' => 1,
				'a.status' => 1,
                'b.slug' => $category
			],
			'order' => 'a.project_id',
			'asc' => 'desc'
		];

        try {
            $banner = $this->_courier->post($urlBanner, $paramBanner);
            if ($banner->getBody() != '[]') {
                $bannerObj = $banner->toObjects()[0];
                SEOMeta::SetTitle($bannerObj && $bannerObj->meta_name != '' ? $bannerObj->meta_name : Session::get('meta_name'));
                SEOMeta::SetKeywords($bannerObj && $bannerObj->meta_keyword != '' ? [$bannerObj->meta_keyword] : [Session::get('meta_keyword')]);
                SEOMeta::SetDescription($bannerObj && $bannerObj->meta_description != '' ? $bannerObj->meta_description : Session::get('meta_description'));
                OpenGraph::setDescription($bannerObj && $bannerObj->meta_description != '' ? $bannerObj->meta_description : Session::get('meta_description'));
                OpenGraph::setTitle($bannerObj && $bannerObj->meta_name != '' ? $bannerObj->meta_name : Session::get('meta_name'));

                $data['banners'] = $banner->toObjects();
            }

            $project = $this->_courier->post($url, $paramProject);            ;
			if ($project->getBody() != "[]") {
				$projectObj = $project->toObjects();
				$data['projects'] = $projectObj;
                $data['nextPage'] = Library::nextPage($projectObj->next_page_url);
                $data['prevPage'] = Library::nextPage($projectObj->prev_page_url);
				$data['projects'] = $projectObj;
			}

            $project3 = $this->_courier->post($url, $paramProject3);
			if ($project3->getBody() != "[]") {
				$projectObj3 = $project3->toObjects();
				$data['projects3'] = $projectObj3;
                $data['nextPage'] = Library::nextPage($projectObj3->next_page_url);
                $data['prevPage'] = Library::nextPage($projectObj3->prev_page_url);
				$data['projects3'] = $projectObj3;
			}

            $data['virtual'] = [];
			$virtual = $this->_courier->post($url, $paramVirtual);
			if ($virtual->getBody() != "[]") {
				$virtualtObj = $virtual->toObjects();
				$data['virtual'] = $virtualtObj;
			}

        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);

        return view('site.project.index')->with($data);
    }

    public function detail($category, $slug)
    {
        $data = [];

        $data['projects'] = [];
        $data['category'] = [];
        $data['project'] = [];
        $data['promos'] = [];
        $data['articles'] = [];
        $data['others'] = [];
        $data['projectarticle360'] = [];
        // $data['downloadForm'] = [
        //     'mozart-residence',
        //     'vivaldi-residence',
        //     'verdi-residence',
        //     'rossini-residence'
        // ];

        $url = API_URL . 'project/' . $slug;

        $urlGallery = API_URL . 'gallery/';
        $urltype = API_URL . 'type/';
        $urlOther = API_URL . 'article-advance';
        $urlOtherProject = API_URL . 'project-advance';
        $urlcat = API_URL . 'category';
        $urlprojectarticle360 = API_URL.'project-article';

        $data['agent'] = new Agent();

        //PROJECTS
        $paramProject = [
            'params' => [
                //'status'=>1,
                'site_id' => Session::get('site_id'),
            ],
        ];

        $paramOther = [
            'params' => [
                'a.site_id' => Session::get('site_id'),
                'a.status' => 1,
                'c.slug' => $slug
            ],
            'order' => 'a.start_date',
            'asc' => 'asc'
        ];


        $paramOtherProject = [
            'params' => [
                'a.status' => 1,
                'a.site_id' => Session::get('site_id'),
            ],
        ];

        //CATEGORY TITLE
		$paramCategory = [
			'params' => [
				'a.status' => 1,
				'a.site_id' => Session::get('site_id'),
				'b.slug' => $slug,
			],
		];

        $param360=[
            'params'=>[
                'a.status' => 1,
                'c.category_id'=> 4002,
            ]
        ];

        try {
            $projectList = $this->_courier->post($url, $paramProject);
            if ($projectList->getBody() != "[]") {
                $projectObj = $projectList->toObjects();
                $data['project'] = $projectObj;

                SEOMeta::SetTitle($projectObj->meta_name != '' ? $projectObj->meta_name : Session::get('meta_name'));
                SEOMeta::SetKeywords($projectObj->meta_keyword != '' ? [$projectObj->meta_keyword] : [Session::get('meta_keyword')]);
                SEOMeta::SetDescription($projectObj->meta_description != '' ? $projectObj->meta_description : Session::get('meta_description'));
                OpenGraph::setDescription($projectObj->meta_description != '' ? $projectObj->meta_description : Session::get('meta_description'));
                OpenGraph::setTitle($projectObj->meta_name != '' ? $projectObj->meta_name : Session::get('meta_name'));

                $paramOtherProject['params']['a.category_id'] = $projectObj->category_id;
                $paramOtherProject['advance'] = [
                    '<>' => ['a.slug' => $projectObj->slug]
                ];

                $param360['params']['a.project_id']=$projectObj->project_id;

                $data['categorydetail'] = [];
                $urlcategory = API_URL . 'category/' . $projectObj->category_id;

                $categorydetailObj = $this->_courier->post($urlcategory);
                if ($categorydetailObj->getBody() != "[]") {
                    $categoryObj = $categorydetailObj->toObjects();
                    $data['categorydetail'] = $categoryObj;
                }


                $data['areadetail'] = [];
                if ($projectObj->area_id) {
                    $urlarea = API_URL . 'area/' . $projectObj->area_id;
                    $areadetailObj = $this->_courier->post($urlarea);
                    if ($areadetailObj->getBody() != "[]") {
                        $areaObj = $areadetailObj->toObjects();
                        $data['areadetail'] = $areaObj;
                    }
                }
            }

            $others = $this->_courier->post($urlOther, $paramOther);

            if ($others->getBody() != "[]") {
                $otherObj = $others->toObjects();
                $arrObj = [];
                $typeObj = [];
                foreach ($otherObj as $other) {
                    $galleryOther = $this->_courier->get($urlGallery . $other->article_id);
                    if ($galleryOther->getBody() != "[]")
                        $arrObj[$other->slug] = $galleryOther->toObjects();

                    $typeOther = $this->_courier->get($urltype . $other->article_id);
                    if ($typeOther->getBody() != "[]") {
                        $typeObj[$other->slug] = $typeOther->toObjects();
                        $data['typeOther'] = $typeObj;
                    }
                }
                $data['others'] = $otherObj;
                $data['galleryOther'] = $arrObj;
            }

            // $projectOther = $this->_courier->post($urlOtherProject, $paramOtherProject);
            // if ($projectOther->getBody() != "[]") {
            //     $projectObj = $projectOther->toObjects();
            //     $data['projects'] = $projectObj;
            // }

            $category = $this->_courier->post($urlcat, $paramCategory);
			if ($category->getBody() != '[]') {
				$data['category'] = $category->toObjects();
			}

            $article360= $this->_courier->post($urlprojectarticle360, $param360);
            if($article360->getBody() !='[]'){
                $data['projectarticle360']= $article360->toObjects();
            }

        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }
        // dd($urlGallery);
        return view('site.product.detail')->with($data);
    }

    public function index_past()
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['category'] = [];
        $data['articles'] = [];
        $data['projects'] = [];
        $data['projects3'] = [];
        $data['welcome']=[];
        $data['nextPage'] = '';
        $url = API_URL . 'project';
        $urlcat = API_URL . 'category';
        $urlproject = API_URL . 'about-us';
        $urlBanner = API_URL . 'article-advance';
        $urlRow = API_URL . 'article/row';

        $page = Request::input('page');
		if ($page)
			$url = $url . '?page=' . $page;

        SEOMeta::SetTitle(Session::get('meta_name'));
        SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        SEOMeta::SetDescription(Session::get('meta_description'));

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'a.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => 'rumah',
            ],
        ];

        $paramProject = [
            'params' => [
                'a.site_id' => Session::get('site_id'),
                'b.type' => 'project',
                'a.status' => 1,
                'a.on_sale' => 0,
            ],
            'asc' => 'desc',
            'paginate' => 8
        ];

        try {
            $banner = $this->_courier->post($urlBanner, $paramBanner);
            if ($banner->getBody() != '[]') {
                $data['banners'] = $banner->toObjects();
            }

            $project = $this->_courier->post($url, $paramProject);
			if ($project->getBody() != "[]") {
				$projectObj = $project->toObjects();
				$data['projects'] = $projectObj;
                $data['nextPage'] = Library::nextPage($projectObj->next_page_url);
                $data['prevPage'] = Library::nextPage($projectObj->prev_page_url);
				$data['projects'] = $projectObj;
			}

        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);
        return view('site.project.past')->with($data);
    }

    public function category_past($category)
    {
        $data = [];
        $data['header'] = true;
        $data['banners'] = [];
        $data['category'] = [];
        $data['articles'] = [];
        $data['projects'] = [];
        $data['projects3'] = [];
        $data['welcome']=[];
        $data['nextPage'] = '';
        $url = API_URL . 'project';
        $urlcat = API_URL . 'category';
        $urlproject = API_URL . 'about-us';
        $urlBanner = API_URL . 'article-advance';
        $urlRow = API_URL . 'article/row';

        $page = Request::input('page');
		if ($page)
			$url = $url . '?page=' . $page;

        SEOMeta::SetTitle(Session::get('meta_name'));
        SEOMeta::SetKeywords([Session::get('meta_keyword')]);
        SEOMeta::SetDescription(Session::get('meta_description'));

        //BANNER TOP
        $paramBanner = [
            'params' => [
                'a.status' => 1,
                'b.site_id' => Session::get('site_id'),
                'b.slug' => $category,
            ],
        ];

        $paramProject = [
            'params' => [
                'a.site_id' => Session::get('site_id'),
                'b.type' => 'project',
                'a.status' => 1,
                'a.on_sale' => 0,
                'b.slug' => $category
            ],
            'asc' => 'desc',
            'paginate' => 8
        ];

        try {
            $banner = $this->_courier->post($urlBanner, $paramBanner);
            if ($banner->getBody() != '[]') {
                $data['banners'] = $banner->toObjects();
            }

            $project = $this->_courier->post($url, $paramProject);
			if ($project->getBody() != "[]") {
				$projectObj = $project->toObjects();
				$data['projects'] = $projectObj;
                $data['nextPage'] = Library::nextPage($projectObj->next_page_url);
                $data['prevPage'] = Library::nextPage($projectObj->prev_page_url);
				$data['projects'] = $projectObj;
			}

        } catch (HttpException $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage() . ' => ' . $exception->response();
        }

        // dd($data);
        return view('site.project.past')->with($data);
    }

    public function getBrochure($slug, $id)
	{
        $articleBrochureId = Request::input('article_id');
        $articleFiles = Request::input('file');
        $articleProName = Request::input('projectName');

		$url = API_URL . 'download/create';
        $file= $articleFiles;
        $proname= $articleProName;
        API_URL . 'article/download/'. $articleBrochureId;

		$datachek = [
			'name' => Request::input('name'),
			'phone' => Request::input('phone'),
			'email' => Request::input('email'),
            'subject' => 'Download Brochure',
            'message' => 'Download Brochure From summareconserpong.com',
			'g-recaptcha-response' => trim(Request::Input('g-recaptcha-response')),
		];
        $response = [
			'response' => 'failed',
			'message' => 'Sorry, the contact process is failed',
			'process' => 'validation'
		];

        if ($this->validator($datachek)->fails())
            return response()->json($response);

		$data = [
            'params' => [
			'site_id' => Session::get('site_id'),
			'project_id' => $id,
			'name' => Request::input('name'),
			'phone' => Request::input('phone'),
			'email' => Request::input('email'),
			'utm_source' => Request::input('utm_source'),
			'utm_medium' => Request::input('utm_medium'),
			'utm_campaign' => Request::input('utm_campaign'),
			'ip' => Library::getIp(),
            ]
		];

        $add = $this->_courier->post($url, $data);
		// $add = true;
		if ($add) {

			$sendMail = Mail::send('site.contact.email', ['data' => $datachek], function ($message) use ($datachek) {
				$sub = 'Download Brochure Summarecon Serpong';

				$message->subject($sub);
				$message->from($datachek['email'], $datachek['name']);
                // $message->to("jelistina_zai@summarecon.com", "Marketing Summarecon Serpong");
                $message->to("website.summareconserpong@gmail.com", "Marketing Summarecon Serpong");
				$message->cc("veronica_cristina@summarecon.com", 'Veronica');
			});

			$response = [
				'response' => 'success',
				'message' => "We have <strong>successfully</strong> received your inquiry, Our marketing team will contact you shortly.",
				'file' => $file,
                'proname' => $proname,
                'articleid'=>$articleBrochureId
			];
		}
		return response()->json($response);
	}

	protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'g-recaptcha-response' => 'required|recaptcha',
        ]);
    }

    public function download($name, $id)
	{
		$article = Article::getRow(['article_id' => $id])->first();
		$fPath = FILE_URL . $article->files;
		$fname = str_replace(' ', '-', $name) . '.pdf';
		header('Content-type: ' . 'application/pdf');
		header('Content-Disposition: ' . 'attachment; filename=' . $fname);
		flush();
		readfile($fPath);
		exit;
	}

    public function type($slug)
    {
        $response = [
        'response' => 'success'
        ];
    return response()->json($response);
    }
}

<?php
namespace App\Http\Middleware;

use Closure;

class VariableMiddleware{
	/**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        //define('API_URL','http://localhost/api/');
        // define('API_URL','http://219.83.125.250/api-residence/');
        if(!defined('API_URL')) define('API_URL','https://api-residence.summarecon.com/');
        //if(!defined('API_URL')) define('API_URL','http://103.96.146.174:8001/');
	
	if(!defined('INITIAL_SITE')) define('INITIAL_SITE','summarecon-serpong-2021');
        if(!defined('PUBLIC_URL')) define('PUBLIC_URL','https://images-residence.summarecon.com/');
        //if(!defined('PUBLIC_URL')) define('PUBLIC_URL','http://103.96.146.174:8003/');
	
	if(!defined('IMAGE_URL')) define('IMAGE_URL',PUBLIC_URL.'images/');
        if(!defined('GALLERY_URL')) define('GALLERY_URL',PUBLIC_URL.'gallery/');
        if(!defined('FILE_URL')) define('FILE_URL',PUBLIC_URL.'files/');
        if(!defined('STOCK_MANAGEMENT')) define('STOCK_MANAGEMENT','stock-management-system');

        return $next($request);
    }
}

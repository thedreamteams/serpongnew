<?php
namespace App\Http\Middleware;

use Closure;
use App\Library\Library;
use CodeZero\Courier\Courier;
use Session;
use App\Models\Site;

class SiteMiddleware{
    public $_courier;
    public function __construct(Courier $courier){
        $this->_courier = $courier;

    }
	/**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param \CodeZero\Courier\Courier $courier
     * @return mixed
     */
    public function handle($request, Closure $next){
        $url = API_URL.'site/'.INITIAL_SITE;

        if(!Session::has('site_id')){
            $site = $this->_courier->get($url,[]); 
            if($site->getHttpCode()==200){
                $arr = $site->toArray();
                Session::put($arr);
            }
        }
        return $next($request);
    }
}
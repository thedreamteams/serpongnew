@extends('layout.layout')
@section('content')
<div id="slider" class="inspiro-slider slider-halfscreen padding-slider dots-creative d-none d-sm-block d-sm-none d-md-block" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    @foreach($banners as $banner)
    <div class="slide background-image" data-bg-image="{{IMAGE_URL.$banner->image_1}}">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class=" text-center text-light">
                <h2 class="text-medium text-light font-weight-700 font-italic">{{$banner->name}}</h2>
                <p class="lead text-light font-weight-600">{{$banner->brief}}</p>
            </div>
        </div>
    </div>
    @endforeach
</div>

@foreach($banners as $banner)
<section id="page-title" class="text-light d-md-none d-lg-none d-xl-none" data-bg-parallax="{{IMAGE_URL.$banner->image_1}}">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title text-center">
            <h2 class="text-medium text-light font-weight-700 font-italic">{{$banner->name}}</h2>
            <p class="lead text-light font-weight-600">{{$banner->brief}}</p>
        </div>
    </div>
</section>
@endforeach

<!-- DEKSTOP -->
<section class="p-t-50 p-b-40 d-none d-sm-block d-sm-none d-md-block">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 m-b-40 text-center">
                <p class="text-dark">{!!$banner->contents!!}</p>
            </div>
        </div>
        <div id="portfolio" class="grid-layout portfolio-4-columns" data-margin="10" da>
            <div class="portfolio-item portfolio-none img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <img src="{{asset('assets/images/serpong/explore/Image-001.jpg')}}" alt="">
                    </div>
                </div>
            </div>
            <div class="portfolio-item portfolio-none img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <img src="{{asset('assets/images/serpong/explore/Image-009.jpg')}}" alt="">
                    </div>
                </div>
            </div>
            <div class="portfolio-item img-zoom">
                <a href="{{URL::to('explore-detail')}}">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="{{asset('assets/images/serpong/explore/leisure.svg')}}" alt="">
                        </div>
                    </div>
                </a>
            </div>
            
            <div class="portfolio-item portfolio-none img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="#"><img src="{{asset('assets/images/serpong/explore/Image-002.jpg')}}" alt=""></a>
                    </div>
                </div>
            </div>
            <div class="portfolio-item img-zoom">
                <a href="{{URL::to('explore-detail/commercial-and-business')}}">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <img src="{{asset('assets/images/serpong/explore/commercial-business.svg')}}" alt="">
                    </div>
                </div>
                </a>
            </div>
            <div class="portfolio-item portfolio-none img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="#"><img src="{{asset('assets/images/serpong/explore/Image-011.jpg')}}" alt=""></a>
                    </div>
                </div>
            </div>
            <div class="portfolio-item img-zoom">
                <a href="{{URL::to('explore-detail/education')}}">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <img src="{{asset('assets/images/serpong/explore/education.svg')}}" alt="">
                    </div>
                </div>
            </a>
            </div>
            <div class="portfolio-item img-zoom">
                <a href="{{URL::to('explore-detail/health-care')}}">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="{{asset('assets/images/serpong/explore/health-care.svg')}}" alt="">
                        </div>
                    </div>
                </a>
            </div>
            <div class="portfolio-item portfolio-none img-zoom">            
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <img src="{{asset('assets/images/serpong/explore/Image-003.jpg')}}" alt="">
                    </div>
                </div>                
            </div>
            <div class="portfolio-item portfolio-none img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <img src="{{asset('assets/images/serpong/explore/Image-010.jpg')}}" alt="">
                    </div>
                </div>
            </div>
            <div class="portfolio-item img-zoom">
                <a href="{{URL::to('explore-detail/green-and-open-space')}}">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="{{asset('assets/images/serpong/explore/green-and-open-space.svg')}}" alt="">
                        </div>
                    </div>
                </a>
            </div>
            <div class="portfolio-item portfolio-none img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <img src="{{asset('assets/images/serpong/explore/pray.jpg')}}" alt="">
                    </div>
                </div>
            </div>
            <div class="portfolio-item img-zoom">
                <a href="{{URL::to('explore-detail/place-of-worship')}}">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="{{asset('assets/images/serpong/explore/place-of-worship.svg')}}" alt="">
                        </div>
                    </div>
                <a>
            </div>
            <div class="portfolio-item portfolio-none img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <img src="{{asset('assets/images/serpong/explore/Image-005.jpg')}}" alt="">
                    </div>
                </div>            
            </div>
        </div>
    </div>
</section>
<!-- DEKSTOP -->

<!-- MOBILE -->
<section class="p-t-30 p-b-30 p-t-0-m p-b-0-m d-md-none d-lg-none d-xl-none">
    <div class="container-fullscreen">
        <div class="portfolio-2-columns">
        <div class="portfolio-item no-overlay p-b-0">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <!-- <img src="{{asset('assets/images/serpong/explore/Image-001.jpg')}}" class="img-responsive" alt=""> -->
                    </div>
                    
                </div>
            </div>
            <div class="portfolio-item no-overlay p-b-0">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="{{URL::to('explore-detail/commercial-and-business')}}">
                            <!-- <img src="{{asset('assets/images/serpong/explore/commercial-business.svg')}}" class="img-responsive" alt=""> -->
                        </a>
                    </div>
                </div>
            </div>
            
            <div class="portfolio-item no-overlay p-b-0">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <img src="{{asset('assets/images/serpong/explore/Image-001.jpg')}}" class="img-responsive" alt="">
                    </div>
                    
                </div>
            </div>
            <div class="portfolio-item no-overlay p-b-0">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="{{URL::to('explore-detail/commercial-and-business')}}">
                            <img src="{{asset('assets/images/serpong/explore/commercial-business.svg')}}" class="img-responsive" alt="">
                        </a>
                    </div>
                </div>
            </div>

            <div class="portfolio-item no-overlay p-b-0">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="{{URL::to('explore-detail')}}">
                            <img src="{{asset('assets/images/serpong/explore/leisure.svg')}}" class="img-responsive" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="portfolio-item no-overlay p-b-0">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <img src="{{asset('assets/images/serpong/explore/Image-002.jpg')}}" class="img-responsive" alt="">
                    </div>
                </div>
            </div>

            <div class="portfolio-item no-overlay p-b-0">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <img src="{{asset('assets/images/serpong/explore/Image-011.jpg')}}" class="img-responsive" alt="">
                    </div>
                    
                </div>
            </div>
            <div class="portfolio-item no-overlay p-b-0">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="{{URL::to('explore-detail/education')}}">
                            <img src="{{asset('assets/images/serpong/explore/education.svg')}}" class="img-responsive" alt="">
                        </a>
                    </div>
                </div>
            </div>

            <div class="portfolio-item no-overlay p-b-0">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="{{URL::to('explore-detail/health-care')}}">
                            <img src="{{asset('assets/images/serpong/explore/health-care.svg')}}" class="img-responsive" alt="">    
                        </a>
                    </div>
                </div>
            </div>
            <div class="portfolio-item no-overlay p-b-0">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <img src="{{asset('assets/images/serpong/explore/Image-003.jpg')}}" class="img-responsive" alt="">
                    </div>
                </div>
            </div>

            <div class="portfolio-item no-overlay p-b-0">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <img src="{{asset('assets/images/serpong/explore/Image-005.jpg')}}" class="img-responsive" alt="">
                    </div>
                    
                </div>
            </div>
            <div class="portfolio-item no-overlay p-b-0">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="{{URL::to('explore-detail/green-and-open-space')}}">
                            <img src="{{asset('assets/images/serpong/explore/green-and-open-space.svg')}}" class="img-responsive" alt="">
                        </a>
                    </div>
                </div>
            </div>

            <div class="portfolio-item no-overlay p-b-0">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="{{URL::to('explore-detail/place-of-worship')}}">
                            <img src="{{asset('assets/images/serpong/explore/place-of-worship.svg')}}" class="img-responsive" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="portfolio-item no-overlay p-b-0">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <img src="{{asset('assets/images/serpong/explore/pray.jpg')}}" class="img-responsive" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- MOBILE -->

@php $no=1; @endphp
@foreach($articles as $explore)
@if($explore->slug!='banner-explore')
@php $no++; @endphp
@if($no %2==0)
<section id="image-block" class="no-padding">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 no-padding d-md-none d-lg-none d-xl-none">
                <img src="{{IMAGE_URL.$explore->thumb}}" class="img-responsive" alt="">
            </div>
            <div class="col-lg-5 p-60 p-t-40 p-b-40 p-45-m background-green-1">
                <div class=" text-left">
                    <h5 class="text-light text-uppercase">{{$explore->note_1}}</h5>
                    <h2 class="text-light font-weight-600 m-t-20 m-b-10">{{$explore->name}}</h2>
                    <p class="text-light">{{$explore->brief}}</p>
                    @if($explore->url)
                    <a class="btn btn-light btn-rounded m-t-10" target="_blank" href="{{$explore->url}}">See More</a>
                    @endif
                </div>
            </div>
            <div class="col-lg-7 img-explore-large d-none d-sm-block d-sm-none d-md-block" style="background:url({{IMAGE_URL.$explore->thumb}}) 50% 50% / cover no-repeat;"></div>
        </div>
    </div>
</section>
@else
<section id="image-block" class="no-padding">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 no-padding d-md-none d-lg-none d-xl-none">
                <img src="{{IMAGE_URL.$explore->thumb}}" class="img-responsive" alt="">
            </div>
            <div class="col-lg-7 img-explore-large d-none d-sm-block d-sm-none d-md-block" style="background:url({{IMAGE_URL.$explore->thumb}}) 50% 50% / cover no-repeat;"></div>
            <div class="col-lg-5 p-60 p-t-40 p-b-40 p-45-m background-grey">
                <div class="text-right text-left-m">
                    <h5 class="text-dark text-uppercase">{{$explore->note_1}}</h5>
                    <h2 class="font-green font-weight-600 m-t-20 m-b-10">{{$explore->name}}</h2>
                    <p class="text-dark">{{$explore->brief}}</p>
                    @if($explore->url)
                    <a class="btn btn-outline-dark btn-rounded m-t-10" target="_blank" href="{{$explore->url}}">See More</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endif
@endif
@endforeach

@stop
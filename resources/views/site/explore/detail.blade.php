@extends('layout.layout')
@section('content')
<div id="slider" class="inspiro-slider slider-halfscreen padding-slider dots-creative d-none d-sm-block d-sm-none d-md-block" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    @foreach($banners as $banner)
    <div class="slide background-image" data-bg-image="{{IMAGE_URL.$banner->image_1}}">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class=" text-center text-light">
                <h2 class="text-medium text-light font-weight-700 font-italic">{{$banner->name}}</h2>
                <p class="lead text-light font-weight-600">{{$banner->brief}}</p>
            </div>
        </div>
    </div>
    @endforeach
</div>

@foreach($banners as $banner)
<section id="page-title" class="text-light d-md-none d-lg-none d-xl-none" data-bg-parallax="{{IMAGE_URL.$banner->image_1}}">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title text-center">
            <h2 class="text-medium text-light font-weight-700 font-italic">{{$banner->name}}</h2>
            <p class="lead text-light font-weight-600">{{$banner->brief}}</p>
        </div>
    </div>
</section>
@endforeach

<section class="p-t-60  p-t-40-m p-b-40 p-b-50-m">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <div class="carousel arrows-visibile testimonial testimonial-single testimonial-left" data-items="1" data-animate-in="fadeIn" data-animate-out="fadeOut" data-arrows="false">
                @foreach($articlehot as $article)
                <div class="testimonial-item p-0 m-b-0">                        
                        <div class="row">                        
                            <div class="col-lg-5 col-12 text-center">
                                <img src="@if($article->thumb){{IMAGE_URL.$article->thumb}}@else{{asset('assets/homepages/fashion/images/author.jpg')}} @endif" class="img-fluid rounded-circle img-450" alt="@if($article->meta_name) {{$article->meta_name}} @else {{$article->name}} @endif">
                            </div>
                            <div class="col-lg-7 col-12 text-left m-t-0 m-t-20-m">
                                <h5 class="m-b-15 font-gold font-italic">{{$article->category}}</h5>
                                <h3 class="font-green">{{$article->name}}</h3>
                                <div class="heading-text heading-line">
                                    <h5 class="m-b-0 font-grey">{{$article->brief}}</h5>
                                </div>
                                <div class="d-none d-sm-block d-sm-none d-md-block">
                                    <p class="p-t-10">{!!$article->contents!!}</p>
                                </div>
                                <ul class="list list-legend m-t-50">
                                    <li class="text-uppercase m-b-5 text-center border-right p-r-15">
                                        @if($article->note_1)
                                        <a href="tel:{{$article->note_1}}"><img src="{{asset('assets/images/serpong/icon/phone-01.svg')}}" class="img-explore"></a><br>
                                        <a href="tel:{{$article->note_1}}" class="d-none d-sm-block d-sm-none d-md-block"><h6 class="text-blue">{{$article->note_1}}</h6></a>
                                        @endif
                                    </li>
                                    <li class="text-uppercase m-b-5 text-center border-right p-r-15">
                                        @if($article->url)
                                        <a href="{{$article->url}}"><img src="{{asset('assets/images/serpong/icon/web-01.svg')}}" class="img-explore"></a><br>
                                        <a href="{{$article->url}}" class="d-none d-sm-block d-sm-none d-md-block"><h6 class="text-blue">Go To Website</h6></a>
                                        @endif
                                    </li>
                                    <li class="text-uppercase m-b-5 text-center border-right p-r-15">
                                        @if($article->note_2)
                                        <a href="{{$article->note_2}}" target="_blank"><img src="{{asset('assets/images/serpong/icon/instagram-01.svg')}}" class="img-explore"></a><br>
                                        <a href="{{$article->note_2}}" target="_blank" class="d-none d-sm-block d-sm-none d-md-block"><h6 class="text-blue">Go To Instagram</h6></a>
                                        @endif
                                    </li>
                                    <li class="text-uppercase m-b-5 text-center">
                                        @if($article->note_3)
                                        <a href="{{$article->note_3}}" target="_blank"><img src="{{asset('assets/images/serpong/icon/gmaps-01.svg')}}" class="img-explore"></a><br>
                                        <a href="{{$article->note_3}} " target="_blank" class="d-none d-sm-block d-sm-none d-md-block"><h6 class="text-blue">Go To Maps</h6></a>
                                        @endif
                                    </li>
                                </ul>
                            </div>
                        </div>                        
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

<section id="page-content" class="sidebar-right p-t-0-m">
    <div class="container">
        <div class="row">
            <div class="text-center" style="margin:0 auto !important;">
                <ul class="portfolio-filter nav nav-tabs nav-project nav-tabs-light m-b-40 m-b-10-m no-border-bottom wow fadeInUp" data-wow-duration="300ms">
                    <a  class="btn btn-outline-green btn-rounded btn-sm @if(Request::is('explore-detail')) active @else btn-transparent-custom @endif m-l-5 text-light" href="{{URL::to('explore-detail')}}">ALL</a>                                 
                    @if($category_leisure)
                    <div class="dropdown">
                        <button type="button" class="btn btn-outline-green btn-rounded btn-transparent-custom btn-sm m-l-5 dropdown-toggle {{ (in_array($slug,$category_leisure_slug))?'active':'' }}" data-toggle="dropdown">
                            {{ $category_leisure[0]->parent_name }}
                        </button>
                        <div class="dropdown-menu">
                            @foreach($category_leisure as $l)
                                <a class="dropdown-item btn-outline-green btn-transparent-custom" href="{{URL::to('explore-detail/'.$l->slug)}}">{{ $l->name }}</a>
                            @endforeach
                        </div>
                    </div> 
                    @endif
                    @foreach($category as $key=>$cat)
                    @if($cat->slug!='thumbnail')
                    <a  class="btn btn-outline-green btn-rounded @if(Request::is('explore-detail/'.$cat->slug)) active @endif btn-transparent-custom btn-sm m-l-5" href="{{URL::to('explore-detail/'.$cat->slug)}}">{{$cat->name}}</a>             
                    @endif
                    @endforeach
                </ul>
            </div>

            <div class="content col-lg-12">
                <div id="blog" class="grid-layout post-thumbnails post-explore post-1-columns m-b-30" data-item="post-item">
                    @foreach($articles as $explore)
                    @if($explore->category_slug!='thumbnail')
                    <div class="post-item border-bottom m-t-20 p-b-0-m">
                        <div class="post-item-wrap">
                            <div class="post-image">
                                <a href="#">
                                    <img alt="@if($explore->meta_name) {{$explore->meta_name}} @else {{$explore->name}} @endif" src="@if($explore->thumb){{IMAGE_URL.$explore->thumb}}@else{{asset('assets/homepages/fashion/images/author.jpg')}} @endif">
                                </a>
                            </div>
                            <div class="post-item-description p-t-5-m">
                                <h2 class="font-green m-b-10"><a href="#">{{$explore->name}}</a></h2>
                                <h5 class="m-b-20 font-gold d-none d-sm-block d-sm-none d-md-block">{{$explore->brief}}</h5>
                                <div class="d-none d-sm-block d-sm-none d-md-block">{!!$explore->contents!!}</div>
                                <ul class="list list-legend">  
                                    <li class="text-uppercase m-b-5 m-r-15">
                                        @if($explore->note_1)
                                        <a href="tel:{{$explore->note_1}}"><img src="{{asset('assets/images/serpong/icon/phone-01.svg')}}" class="img-explore img-explore-detail"></a>
                                        @endif
                                    </li>                          
                                    <li class="text-uppercase m-b-5 m-r-15">
                                        @if($explore->url)
                                        <a href="{{$explore->url}}" target="_blank"><img src="{{asset('assets/images/serpong/icon/web-01.svg')}}" class="img-explore img-explore-detail"></a>
                                        @endif
                                    </li>
                                    <li class="text-uppercase m-b-5 m-r-15">
                                        @if($explore->note_2)
                                        <a href="{{$explore->note_2}}" target="_blank"><img src="{{asset('assets/images/serpong/icon/instagram-01.svg')}}" class="img-explore img-explore-detail"></a>
                                        @endif
                                    </li>
                                    <li class="text-uppercase m-b-5 m-r-15">
                                        @if($explore->note_3)
                                        <a href="{{$explore->note_3}}" target="_blank"><img src="{{asset('assets/images/serpong/icon/gmaps-01.svg')}}" class="img-explore img-explore-detail"></a>
                                        @endif
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
                <!-- <div id="pagination" class="infinite-scroll">
                    <a href="blog-left-image-load-more-2.html"></a>
                </div> -->
                <!-- <div id="showMore">
                    <a href="#" class="btn btn-rounded btn-light"><i class="icon-refresh-cw"></i> Load More</a>
                </div> -->
            </div>
        </div>
    </div>
</section>

@stop

@section('css')
<style>
.dropdown-toggle::after {margin-top: 12px;}
.dropdown:hover>.dropdown-menu {display: block;}
</style>    
@endsection
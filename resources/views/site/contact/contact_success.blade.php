<!DOCTYPE html>
<html lang="en">

<head>
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-42299418-1', 'summareconserpong.com');
        ga('send', 'pageview');
    </script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <link href="{{asset('assets/images/favicon.ico')}}" rel="shortcut icon">
    {!!SEO::generate()!!}

    <!-- BOOTSTRAP CORE CSS -->
    <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/fontawesome/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('assets/vendor/animateit/animate.min.css')}}" rel="stylesheet">

    <!-- TEMPLATE BASE -->
    <link href="{{asset('assets/css/theme-base.css')}}" rel="stylesheet">

    <!-- TEMPLATE ELEMENTS -->
    <link href="{{asset('assets/css/theme-elements.css')}}" rel="stylesheet">

    <!-- RESPONSIVE CLASSES-->
    <!-- <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet"> -->

    <!-- TEMPLATE COLOR -->
    <link href="{{asset('assets/css/color-variations/orange.css')}}" rel="stylesheet" type="text/css" media="screen" title="blue">

    <!-- LOAD GOOGLE FONTS -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,800,700,600%7CRaleway:100,300,600,700,800" rel="stylesheet" type="text/css" />
    <!--VENDOR SCRIPT-->
    <script src="{{asset('assets/vendor/jquery/jquery-1.11.2.min.js')}}"></script>
    <script src="{{asset('assets/vendor/plugins-compressed.js')}}"></script>

    <!-- GMAPS -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqgVIaPYgQ2NhYwOzDjSyMp7v47HdQnak">
    </script>
    <style type="text/css">
        #book {
            background-color: rgb(255, 255, 255, 0.4);
            border-radius: 10px;
            padding: 40px;
            color: #fff;
        }

        #book h2 {
            font-size: 35px;
            color: #000;
            line-height: 1.1;
        }

        #book label {
            color: #000;
        }

        .social-icons.social-icons-rounded li a {
            background-color: #126132;
        }

        .social-icons.social-icons-border li a {
            color: #fff;
            border: 0;
        }

        .social-icons li a {
            height: 40px;
            width: 40px;
            line-height: 40px;
        }

        button {
            background-color: #681036;
            color: #FFF;
        }

        @media (min-width: 320px) and (max-width: 767px) {
            .social-icons span {
                font-size: 16px !important;
            }

            .logo-s {
                display: block;
                margin: 0 auto;
                margin-bottom: 30px;
            }

            section {
                padding-top: 20px;
                padding-bottom: 20px;
            }
        }
    </style>
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '630345290492282');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=630345290492282&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->
</head>

<body class="wide">
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NHMXB9S" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NHMXB9S');
    </script>
    <!-- Optimized Indonesia Tag Manager -->

    <!-- Facebook Pixel Code -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=137646242928429";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <script>
        ! function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (!d.getElementById(id)) {
                js = d.createElement(s);
                js.id = id;
                js.src = "https://platform.twitter.com/widgets.js";
                fjs.parentNode.insertBefore(js, fjs);
            }
        }(document, "script", "twitter-wjs");
    </script>

    <!-- WRAPPER -->
    <div class="wrapper" data-animation-icon="three-dots.svg">
        <div id="logo">
            <a href="{{URL::to(Library::getDomainPrefix())}}" class="logo" data-dark-logo="images/logo-dark.png">
                <img src="{{asset('assets/images/microsite/logorainbow.png')}}" alt="Logo Rainbow">
            </a>
        </div>
        <section class="p-b-70 p-t-40" style="background-image:url({{IMAGE_URL.'project/rainbow-springs-condovillas/Leisure-Escapade-rainbow.jpg'}}); background-position:center center; background-size:cover;">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <img class="logo-s" src="{{asset('assets/images/serpong/rossini/logo.png')}}" alt="Summarecon Serpong" style="width:160px;">
                        <!-- <img src="{{asset('assets/images/serpong/rossini/rossini.png')}}" class="img-responsive m-t-70"> -->
                    </div>
                    <div class="col-md-5 col-md-offset-3">
                        <div id="book">
                            <h4 class="text-dark m-b-30"><i>Be the one to know our latest product.</i></h4>
                            <h2>Submit for<br> download brochure Success</h2>
                            <a href="{{URL::to(Library::getDomainPrefix())}}" class="btn btn-default">
                                www.condovillas.co.id
                            </a>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="social-icons social-icons-border social-icons-rounded social-icons-colored-hover">
                            <ul>
                                <li class="p-r-10" style="line-height:2;"><span style="font-size:20px;" class="text-light">Connect with us!</span></li>
                                <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li class="social-youtube"><a href="#"><i class="fa fa-youtube"></i></a></li>
                                <li class="social-instagram"><a href="#"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
    <!-- END: WRAPPER -->
    <!-- LOADING PAGE LOADER -->
    <script type="text/javascript">
        var baseURL = "{{ URL::to('') }}";
    </script>

    <!-- Theme Base, Components and Settings -->
    <script src="{{asset('assets/js/theme-functions.js')}}"></script>
    <script>
        var _baseUrl = $('input[name="base_url"]').val();
        $('#contact-submit-button').click(function() {

            setTimeout(function() {
                window.location.assign('http://www.condovillas.co.id/download/success');
            }, 1000)
        })
    </script>
    @yield('js')
</body>

</html>
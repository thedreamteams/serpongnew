@extends('layout.layout')
@section('content')
<div id="slider" class="inspiro-slider slider-halfscreen padding-slider dots-creative d-none d-sm-block d-sm-none d-md-block" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    @foreach($banners as $banner)
    <div class="slide background-image" data-bg-image="{{IMAGE_URL.$banner->image_1}}">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class=" text-center text-light">
                <h2 class="text-medium text-light font-weight-700 font-italic">{{$banner->name}}</h2>
                <p class="lead text-light font-weight-600">{{$banner->brief}}</p>
            </div>
        </div>
    </div>
    @endforeach
</div>

@foreach($banners as $banner)
<section id="page-title" class="text-light d-md-none d-lg-none d-xl-none" data-bg-parallax="{{IMAGE_URL.$banner->image_1}}">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title text-center">
            <h2 class="text-medium text-light font-weight-700 font-italic">{{$banner->name}}</h2>
            <p class="lead text-light font-weight-600">{{$banner->brief}}</p>
        </div>
    </div>
</section>
@endforeach


<section class="p-b-20-m p-t-20-m p-t-40 p-b-20">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <h3 class="text-uppercase font-green">Message Us</h3>
                <p>Untuk informasi lebih lanjut tentang perusahaan atau produk kami, silahkan menghubungi kami dengan meninggalkan data pada form berikut.<br>
                <em>For further information, please contact our Marketing Team or you can just fill in the form below.</em></p>
                <div class="m-t-30">
                    <form class="widget-contact-form" novalidate action="{{URL::to('contact/action')}}" role="form" method="post">
                    <input name="_token" value="{{csrf_token()}}" type="hidden">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <select class="form-control form-control-us required" id="subject" name="subject">
                                    <option value="select" disabled selected>Subject</option>
                                    <option value="marketing">Marketing</option>
                                    <option value="hrd">HRD</option>
                                    <option value="others">Others</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <input type="text" aria-required="true" name="name" required class="form-control form-control-us required name char-only " placeholder="Fullname">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <input type="email" aria-required="true" name="email" required class="form-control form-control-us required email  " placeholder="Email">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <input type="text" aria-required="true" name="phone" required class="form-control form-control-us required name number-only" placeholder="Phone Number">
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea type="text" name="message" required rows="10" class="form-control form-control-message required" placeholder="Message"></textarea>
                        </div>

                        <div class="form-group">
                            {{-- {!! Recaptcha::render() !!} --}}
                            {!! RecaptchaV3::field('contact') !!}
                        </div>

                        <input type="hidden" name="utm_source" id="utm_source" class="utm_source" value={{ app('request')->input('utm_source') }}>
                        <input type="hidden" name="utm_medium" id="utm_medium" class="utm_medium" value={{ app('request')->input('utm_medium') }}>
                        <input type="hidden" name="utm_campaign" id="utm_campaign" class="utm_campaign" value={{ app('request')->input('utm_campaign') }}>
                        <button class="btn btn-gold-custom btn-radius" type="submit" id="form-submit"><i class="fa fa-paper-plane"></i>&nbsp;Submit</button>
                    </form>
                </div>
            </div>
            <div class="col-lg-3 offset-lg-1">
                <h3 class="text-uppercase font-green m-t-20-m">Office</h3>
                <div class="row">
                    <div class="col-lg-12">
                        <address class="m-b-30">
                            <strong>PT SERPONG CIPTA KREASI</strong><br>
                            Plaza Summarecon Serpong<br>
                            Jl Boulevard Raya Gading Serpong<br>
                            Blok M5 No. 3, Tangerang 15810<br>
                            <a href="https://g.page/sms_serpong?share" target="_blank" class="font-weight-700 font-italic m-b-60"><strong>Open in Google Maps</strong></a>
                        </address>
                        <h3 class="text-uppercase font-green d-none d-block d-md-none">Phone</h3>
                        <abbr title="Phone" class="d-none d-block d-md-none"><strong>Telp</strong><span> +62 21 5421 0008</span><br><span class="m-l-35">+62 21 54 666 10</span></abbr>
                        <!-- <abbr title="Fax" class="d-none d-block d-md-none m-b-20"><strong>Fax</strong><span class="m-l-5"> +62 21 5421 0007</span></abbr> -->
                        <iframe src="https://www.google.com/maps/embed?pb=!1m26!1m12!1m3!1d253853.9861255254!2d106.63605732608863!3d-6.201754407652722!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m11!3e6!4m3!3m2!1d-6.2242413999999995!2d106.9240557!4m5!1s0x2e69fc0a0f186afd%3A0x355ee4742c69b52c!2sgoogle%20map%20summarecon%20serpong!3m2!1d-6.241113299999999!2d106.62849659999999!5e0!3m2!1sid!2sid!4v1623173410405!5m2!1sid!2sid" width="100%" height="200" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        <div class="border-bottom border-bottom-gold m-b-20 m-t-20"></div>
                    </div>
                </div>
                <h3 class="text-uppercase font-green d-none d-md-block">Phone</h3>
                <abbr title="Phone" class="d-none d-md-block"><strong>Telp</strong><span> +62 21 5421 0008</span><br><span class="m-l-35">+62 21 54 666 10</span></abbr>
                <!-- <abbr title="Fax" class="d-none d-md-block"><strong>Fax</strong><span class="m-l-5"> +62 21 5421 0007</span></abbr> -->
            </div>
        </div>
    </div>
</section>

@stop

<style type="text/css">
	body{
		font-family: "Source Sans Pro",sans-serif !important;
	}
</style>

<body>
	<div><p>Yang Terhormat,</p></div>
	<div><p>Bapak/Ibu {{$data['name']}},</p></div>
	<div>
		<p>Salam dari Summarecon Serpong.</p>
		<p> Terima kasih atas kepercayaan Anda memilih kami, Summarecon Serpong sebagai pilihan dalam memilih hunian idaman dan peluang investasi terbaik Anda. Permintaan Anda atas informasi terkait produk properti Summarecon Serpong akan segera kami proses melalui Sales Executive. Mohon menunggu. Untuk informasi detail dan penawaran spesial dari produk dari Summarecon Serpong juga dapat diakses melalui platform media sosial Summarecon Serpong di bawah.</p>
	</div>
	<div><p>Terima Kasih,</p></div>
	<div>
		<p>		
		<div id="logo"> 
                <a href="{{URL::to('/')}}">
                    <span class="logo-default"><img style="max-width:120px;" src="{{asset('assets/images/serpong/Logo-SS-new.png')}}"></span>                    
                </a> 
            </div>	
			<b>Summarecon Serpong Team</b><br/>
			<a href="{{URL::to('/')}}"><span>summareconserpong.com</span></a><br/>
			Telp <a href="tel:+622154210008">+62 21 5421 0008 </a><a href="tel:+62215466610">+62 21 54 666 10</a><br/>
			Email <a href="website_summareconserpong@gmail.com" target="_top">website_summareconserpong@gmail.com</a><br/><a href="marketing_ss@summarecon.com" target="_top">marketing_ss@summarecon.com</a><br/>

			<a href="http://instagram.com/summarecon_serpong" target="_blank"><i class="fab fa-instagram"></i></a>
			<a href="https://www.facebook.com/summareconserpong/?ref=br_rs" target="_blank"><i class="fab fa-facebook-f"></i></a>
			<a href="https://www.youtube.com/channel/UColuLhICEhcPWh_kBqTvgUg" target="_blank"><i class="fab fa-youtube"></i></a>
			<a href="https://twitter.com/summarecon_srpg" target="_blank"><i class="fab fa-twitter"></i></a>
			<a href="https://vt.tiktok.com/ZSJ3786ae/" target="_blank"><i class="fab fa-tiktok"></i></a>

			Plaza Summarecon Serpong<br/>
			Jl. Boulevard Raya Gading Serpong<br/>
			Blok M5 No. 3<br/>
			Tangerang 15810, Indonesia<br/>
			
		</p>
	</div>
</body>

  


@extends('layout.layout')
@section('content')
<div id="slider" class="inspiro-slider slider-halfscreen padding-slider dots-creative d-none d-sm-block d-sm-none d-md-block" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    @foreach($banners as $banner)
    <div class="slide background-image" data-bg-image="{{IMAGE_URL.$banner->image_1}}">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class=" text-center text-light">
                <h2 class="text-medium text-light font-weight-700 font-italic">{{$banner->name}}</h2>
                <p class="lead text-light font-weight-600">{{$banner->brief}}</p>
            </div>
        </div>
    </div>
    @endforeach
</div>

@foreach($banners as $banner)
<section id="page-title" class="text-light d-md-none d-lg-none d-xl-none" data-bg-parallax="{{IMAGE_URL.$banner->image_1}}">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title text-center">
            <h2 class="text-medium text-light font-weight-700 font-italic">{{$banner->name}}</h2>
            <p class="lead text-light font-weight-600">{{$banner->brief}}</p>
        </div>
    </div>
</section>
@endforeach

<section id="page-content" class="p-b-0 p-b-20-m background-grey p-t-20-m p-b-0-m">
    <div class="container">
        <div class="tabs tabs-custom tabs-sales text-center">
            <ul class="nav nav-tabs display-flex p-b-60 p-b-40-m" id="myTab2" role="tablist">
                <li class="nav-item font-size-10">
                    <a class="nav-link @if(Request::is('sales-executive-landed')) active @endif"  href="{{URL::to('sales-executive-landed')}}" role="tab" aria-controls="home" aria-selected="true">Sales Team</a>
                </li>
                <li class="nav-item font-size-10">
                    <a class="nav-link @if(Request::is('sales-executive-strata')) active @endif"  href="{{URL::to('sales-executive-strata')}}" role="tab" aria-controls="profile" aria-selected="false">Sales Team</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent2">
                <div class="tab-pane fade show active" id="home2" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row team-members team-members-circle m-b-0 m-b-0-m">
                        @foreach($sales as $data)
                        <div class="col col-lg-4 col-sm-6 col-6">
                            <div class="team-member m-b-0">
                                <a href="{{URL::current().'/'.$data->slug}}">
                                    <div class="team-image"><img src="{{IMAGE_URL.$data->thumb}}"></div>
                                </a>
                                <div class="team-desc d-none d-sm-block d-sm-none d-md-block">
                                    <h3 class="font-green m-b-10">{{$data->name}}</h3>
                                    {{--<a class="btn btn-rounded btn-outline btn-reveal btn-sales btn-green-custom text-light m-b-0" target="_blank" href="https://api.whatsapp.com/send?phone={{$data->brief}}&text=Hallo,%20Saya%20ingin%20mengetahui%20product%20Summarecon%20Serpong%20�%20"><span>Contact Whatsapp</span><i class="fab fa-whatsapp"></i></a>--}}
                                    <a class="btn btn-rounded btn-outline btn-reveal btn-sales btn-green-custom text-light m-b-0" href="{{URL::current().'/'.$data->slug}}"><span>Contact Sales</span></a>
                                    {{--
                                    @if($data->url)
                                    <a href="{{$data->url}}" target="_blank"><img src="{{asset('assets/images/serpong/icon/instagram-icon-01.svg')}}" class="img-explore p-t-10-m"></a>
                                    @endif
                                    --}}
                                </div>

                                <div class="team-desc d-md-none d-lg-none d-xl-none">
                                    <h3 class="font-green m-b-10">{{$data->name}}</h3>
                                    <a class="btn btn-rounded btn-outline btn-reveal btn-sales btn-green-custom text-light m-b-0 btn-xs" href="{{URL::current().'/'.$data->slug}}"><span class="small">Contact</span></a>
                                    <a href="https://api.whatsapp.com/send?phone={{$data->brief}}&text=Hallo,%20Saya%20ingin%20mengetahui%20product%20Summarecon%20Serpong%20�%20"><img src="{{asset('assets/images/serpong/icon/whatsapp-01.svg')}}" class="img-explore p-t-10-m"></a>
                                    {{--
                                    @if($data->url)
                                    <a href="{{$data->url}}" target="_blank"><img src="{{asset('assets/images/serpong/icon/instagram-icon-01.svg')}}" class="img-explore p-t-10-m"></a>
                                    @endif
                                    --}}
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

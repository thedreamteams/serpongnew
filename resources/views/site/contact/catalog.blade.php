@extends('layout.layout_ecatalog')

@section('content')
@php
    $checkPhone = substr($sales->brief, 0, 2);
    if ($checkPhone=='62') {
        $agentPhone = $sales->brief;
    } else {
        $agentPhone = '62'.substr($sales->brief, 0, 2);
    }
@endphp

<div id="slider" class="inspiro-slider slider-halfscreen padding-slider dots-creative d-none d-sm-block d-sm-none d-md-block" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    @foreach($banners as $banner)
    <div class="slide background-image" data-bg-image="{{IMAGE_URL.$banner->image_1}}">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class=" text-center text-light">
                <h2 class="text-medium text-light font-weight-700 font-italic">{{$banner->name}}</h2>
                <p class="lead text-light font-weight-600">{{$banner->brief}}</p>
            </div>
        </div>
    </div>
    @endforeach
</div>

@foreach($banners as $banner)
<section id="page-title" class="text-light d-md-none d-lg-none d-xl-none" data-bg-parallax="{{IMAGE_URL.$banner->image_1}}">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title text-center">
            <h2 class="text-medium text-light font-weight-700 font-italic">{{$banner->name}}</h2>
            <p class="lead text-light font-weight-600">{{$banner->brief}}</p>
        </div>
    </div>
</section>
@endforeach

<div class="p-t-0 background-grey">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-xl-12">
                <div id="book" class="shadow book-catalog p-30-m">
                    <form class="widget-form" novalidate action="{{URL::to('contact/action-catalog')}}" role="form" method="post">
                        <input name="_token" value="{{csrf_token()}}" type="hidden">

                        <div class="text-center">
                            <h2 class="text-uppercase font-green font-weight-700 m-b-30">Get in Touch With Me<br>For More Info</h2>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-xl-4">
                                <div class="form-group m-b-20">
                                    <input aria-required="true" name="name" class="form-control form-control-custom char-only required" id="name" minlength="3" maxlength="50" placeholder="Fullname" type="text" required>
                                </div>
                            </div>
                            {{-- <div class="col-lg-4 col-xl-3">
                                <div class="form-group m-b-20">
                                    <input aria-required="true" name="email" id="email" class="form-control form-control-custom required" placeholder="Email" type="email" required>
                                </div>
                            </div> --}}
                            <div class="col-lg-4 col-xl-4">
                                <div class="form-group m-b-20">
                                    <input class="form-control form-control-custom number-only required" id="phone" name="phone" placeholder="Whatsapp No" minlength="10" maxlength="15" type="tel" aria-required="true" required>
                                </div>
                            </div>
                            <div class="col-lg-4 col-xl-4">
                                <div class="form-group m-b-20">
                                    <select class="form-control" id="message" name="message" required>
                                        <option disabled selected>Refference</option>
                                        <option value="Refference From Billboard">Billboard</option>
                                        <option value="Refference From Instagram">Instagram</option>
                                        <option value="Refference From Google Ads">Google Ads</option>
                                        <option value="Refference From Tiktok">Tiktok</option>
                                        <option value="Refference From Facebook">Facebook</option>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" name="utm_source" id="utm_source" class="utm_source" value={{ app('request')->input('utm_source') }}>
                            <input type="hidden" name="utm_medium" id="utm_medium" class="utm_medium" value={{ app('request')->input('utm_medium') }}>
                            <input type="hidden" name="utm_campaign" id="utm_campaign" class="utm_campaign" value={{ app('request')->input('utm_campaign') }}>
                            <input type="hidden" name="subject" id="subject" value="E-Catalog Form - {!! $sales->name !!}  - {!! $sales->parent !!}">
                            <input type="hidden" name="agent_phone" id="agent_phone" value="{{ $agentPhone }}">
                            <div class="col-lg-12 col-xl-12 col-12 text-center">
                                <div class="form-group">
                                    {{-- {!! \Recaptcha::render() !!} --}}
                                    {!! RecaptchaV3::field('catalog') !!}
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-lg btn-dark btn-radius btn-form btn-outline text-success" type="submit" id="form-submit"><i class="fab fa-whatsapp"></i>&nbsp; Send Us Message</button>
                                    <div class="form-status"></div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<section id="page-content" class="p-b-0 p-b-20-m background-grey p-t-20-m p-b-0-m">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-xl-6">
                <img src="{{IMAGE_URL.$sales->thumb}}" class="img-responsive img-fluid img-radius shadow">
            </div>
            <div class="col-lg-6 col-xl-6 button-wa-footer button-wa-catalog">
                <div class="viewed">
                    <strong>
                        <i class="fa fa-eye"></i> {!! $sales->clicked !!} View
                    </strong>
                </div>
                <h2 class="text-md font-green m-t-30-m m-b-0-m">{!! $sales->name !!}</h2>
                <h3 class="font-green m-b-0">
                    {!! $sales->parent !!}
                    <img src="https://www.summareconserpong.com/assets/images/badge-check.png" style="width:30px;">
                </h3>
                <h5 class="font-green">{!! $sales->category !!}</h5>
                @if($sales->url)
                <a class="btn btn-green-custom btn-radius" href="{{$sales->url}}" target="_blank">
                    <div class="align-items-center d-flex rounded-pill">
                        <i class="fab fa-instagram fa-2x"></i>
                        <span class="text-light">&nbsp;Instagram</span>
                    </div>
                </a>
                @endif
                {{-- <p class="text-justify-m">Hi, I'm Tasya. I'm a Property sales executive who has been successful in selling 120 unit of property products. I am ready to help you find your dream property.</p> --}}

                <div class="post-thumbnail-list">
                    <div class="post-thumbnail-entry m-t-20">
                        <div class="post-thumbnail-content">
                            <a href="https://api.whatsapp.com/send?phone={!! $agentPhone !!}&amp;text=Halo%20mau%20tanya%20info%20Summarecon%20Serpong%20" target="_blank" class="m-t-5">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="font-green">
                                        Chat Our Representative
                                        <p class="post-date font-weight-600 font-green m-b-0">+{!! $agentPhone !!}</p>
                                    </div>
                                    <div>
                                        <img class="w-80" src="https://www.summareconserpong.com/assets/images/serpong/icon/PNG-round-whatsapp.png">
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="p-t-20 p-b-20 background-grey project-sale">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-xl-12  text-center">
                <div data-animate="fadeInUp" data-animate-delay="0" class="animated fadeInUp visible p-b-20 text-center">
                    <h2 class="font-green m-t-50 m-b-0 m-t-30-m text-uppercase font-weight-800">My Best Recommended Product</h2>
                </div>
                <div class="row infinite-scroll  text-center"  data-margin="10">
                    @foreach($projects->data as $project)
                        <div class="portfolio-item col-md-4 overlay-dark m-t-15 infinite-scroll-item item-project"
                        style="padding: 0px 20px 20px 0px;">
                            <a href="{{URL::to('projects/'.$project->category_slug.'/'.$project->slug)}}">
                            <div class="portfolio-item-wrap">
                                <div class="portfolio-image background-white radius-top-catalog">
                                    <a href="{{URL::to('projects/'.$project->category_slug.'/'.$project->slug)}}">
                                        <img alt="@if($project->meta_name) {{$project->meta_name}} @else Welcome To {{$project->name}} @endif" src="{{IMAGE_URL.'project/'.$project->image_1}}" class="img-radius">
                                    </a>
                                </div>
                                <div class="portfolio-description">
                                    <a href="{{URL::to('projects/'.$project->category_slug.'/'.$project->slug)}}">
                                        <h3>View More</h3>
                                    </a>
                                </div>
                            </div>
                            <div class="product-description background-white p-t-20 p-b-20 radius-bottom-catalog">
                                <div class="product-title text-center">
                                    <h3 class="font-green"><a href="{{URL::to('projects/'.$project->category_slug.'/'.$project->slug)}}" class="font-green">{{$project->name}}</a></h3>
                                </div>
                                <div class="product-category text-center">
                                    <span> @if($project->area !='Other'){{$project->area}}@endif</span>
                                </div>
                            </div>
                            </a>
                        </div>
                    @endforeach
                    <div class="infinite-pagination text-center">
                        <a href="{{$nextPage}}" class="infinite-next text-center">next</a>
                    </div>

                    @if (isset($projects2))
                        @foreach($projects2->data as $project2)
                            <div class="portfolio-item col-md-4 overlay-dark m-t-15 infinite-scroll-item item-project"
                            style="padding: 0px 20px 20px 0px;">
                                <a href="{{URL::to('projects/'.$project2->category_slug.'/'.$project2->slug)}}">
                                <div class="portfolio-item-wrap">
                                    <div class="portfolio-image background-white radius-top-catalog">
                                        <a href="{{URL::to('projects/'.$project2->category_slug.'/'.$project2->slug)}}">
                                            <img alt="@if($project2->meta_name) {{$project2->meta_name}} @else Welcome To {{$project2->name}} @endif" src="{{IMAGE_URL.'project/'.$project2->image_1}}" class="img-radius">
                                        </a>
                                    </div>
                                    <div class="portfolio-description">
                                        <a href="{{URL::to('projects/'.$project2->category_slug.'/'.$project2->slug)}}">
                                            <h3>View More</h3>
                                        </a>
                                    </div>
                                </div>
                                <div class="product-description background-white p-t-20 p-b-20 radius-bottom-catalog">
                                    <div class="product-title text-center">
                                        <h3 class="font-green"><a href="{{URL::to('projects/'.$project2->category_slug.'/'.$project2->slug)}}" class="font-green">{{$project2->name}}</a></h3>
                                    </div>
                                    <div class="product-category text-center">
                                        <span> @if($project2->area !='Other'){{$project2->area}}@endif</span>
                                    </div>
                                </div>
                                </a>
                            </div>
                        @endforeach
                        <div class="infinite-pagination text-center">
                            <a href="{{$nextPage2}}" class="infinite-next text-center">next</a>
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </div>
</section>

@foreach($virtual as $product)
@if($product->link360)
<section id="slider" class="p-t-60 p-b-80 background-grey">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="font-green m-b-20 text-uppercase">3D Virtual Tour</h2>
                <div class="carousel" data-video="true" data-items="1">
                @foreach($virtual as $product)
                @if($product->link360)
                    <div class="col-md-12 p-0 no-padding text-center">
                        <iframe width="100%" height="400px" src="{{$product->link360}}" class="margin-two-bottom" style="border:0 !important;"></iframe>
                    </div>
                    @endif
                @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@break
@endif
@endforeach

{{-- @if($projects->latitude)
<section class="no-padding background-grey">
    <div class="container-fullscreen">
        <div class="row">
            <div class="col-lg-12 col-xl-12 no-padding text-center">
                <h2 class="font-green m-b-20 text-uppercase">Our Locations</h2>
                <div class="mapouter">
                    <div class="gmap_canvas">
                    <div id="map-canvas" style="width:100%;height:500px;" data-latitude="{{$projects->latitude}}" data-longitude="{{$project->first()->longitude}}" data-title="{{$project->first()->name}}"></div>
                    <!-- <iframe width="100%" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=summarecon%20serpong&t=&z=9&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div> -->
                </div>
            </div>
        </div>
    </div>
</section>
@endif --}}

@stop


@section('js')
<!-- INFINITE SCROLL -->
<script src="{{asset('assets/plugins/infinite-scroll/infinite-ajax-scroll/jquery-ias.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/infinite-scroll/infinite-ajax-scroll/jquery.infinitescroll.js')}}" defer></script>
<script src="{{asset('assets/js/functions/infinite_scroll.js')}}" defer></script>
<script>
    $(function () {
        @if($errors->any())
            // $.notify({
            //     message: "{!! $errors->first() !!}"
            // }, {
            //     class:"my-class", delay:0, align:"center", verticalAlign:"middle", animationType:"scale",
            //     type: 'danger'
            // });
            // $.notify("{!! $errors->first() !!}", "error");
            swal({ html:true, title:'<i>Error</i>', text:'<b>{!! $errors->first() !!}</b>', type:'error'});
        @endif
    });
</script>
@stop

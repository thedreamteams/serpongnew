@extends('layout.layout')
@section('content')
<section class="p-b-20-m p-t-20-m">
    <div class="container">
        <div class="row">
            <h2>{{$article[0]->name}}</h2>
            {!!$article[0]->contents!!}
        </div>
    </div>
</section>
@stop
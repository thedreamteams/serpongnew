@extends('layout.layout')
@section('content')
<div id="slider" class="inspiro-slider slider-fullscreen dots-creative d-none d-sm-block d-sm-none d-md-block" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    @foreach($banner as $image)
    <div data-href="{{$image->url!=''?URL::to($image->url):'#'}}" class="slide background-image slide-href" style="background-image:url('{{IMAGE_URL.$image->image_1}}');"></div>
    @endforeach
</div>

<div class="carousel carousel-mobile-home d-md-none d-lg-none d-xl-none" data-items="1" data-dots="true" data-autoplay="true" style="top:-80px;">
    @foreach($banner as $image)
    @if($image->url!='')
    <a href="{{$image->url!=''?URL::to($image->url):''}}" target="_blank" rel="noopener noreferrer">
    <img src="{{IMAGE_URL.$image->image_2}}" alt="image" /></a>
    @else
    <img src="{{IMAGE_URL.$image->image_2}}" alt="image" />
    @endif
    @endforeach
</div>

<section class="p-t-60  p-t-0-m p-b-20" id="welcome">
    <div class="container">
        <div class="row">
        @foreach($welcome as $welcomedata)
            <div class="col-lg-6 text-center">
                <img src="{{IMAGE_URL.$welcomedata->thumb}}" class="img-fluid" alt="@if($welcomedata->meta_name) {{$welcomedata->meta_name}} @else Welcome To {{$welcomedata->name}} @endif">
            </div>
            <div class="col-lg-6 text-left text-center-m m-t-80 m-t-30-m">
                <h5 class="m-b-0 font-grey font-italic">Welcome To</h5>
                <h3 class="font-green">{{$welcomedata->name}}</h3>
                {!!$welcomedata->contents!!}
            </div>
            @endforeach
        </div>
    </div>
</section>

<!-- DEKSTOP -->
<section class="text-center p-t-60 p-b-60 d-none d-sm-block d-sm-none d-md-block">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="font-green m-b-0">Living in Environmental Friendly & City Scale Infrastructure</h2>
                <h5 class="m-b-30 font-grey font-italic">Discover The Charms of Better Living Better Future at Summarecon Serpong</h5>
            </div>
            @foreach($theBest as $thebestdata)
            <div class="col-lg-4">
                <img src="{{IMAGE_URL.$thebestdata->thumb}}" class="img-fluid m-b-10" alt="@if($thebestdata->meta_name) {{$thebestdata->meta_name}} @else {{$thebestdata->name}} -  Summarecon Serpong @endif">
                <h5 class="m-b-0 font-grey">{{$thebestdata->name}}</h5>
            </div>
            @endforeach
        </div>
    </div>
</section>
<!-- END : DESKTOP -->

<!-- MOBILE -->
<section class="text-center p-t-40 p-b-40 d-md-none d-lg-none d-xl-none">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="font-green m-b-20">Living in Environmental Friendly & City Scale Infrastructure</h2>
                <h5 class="m-b-30 font-grey font-italic">Discover The Charms of Better Living Better Future at Summarecon Serpong</h5>
                <div class="carousel arrows-visibile carousel-mobile" data-items="1" data-dots="false" data-arrows="true">
                    @foreach($theBest as $thebestdata)
                    <div class="item">
                        <img src="{{IMAGE_URL.$thebestdata->thumb}}" class="img-fluid m-b-10" alt="@if($thebestdata->meta_name) {{$thebestdata->meta_name}} @else {{$thebestdata->name}} -  Summarecon Serpong @endif">
                        <h4 class="m-b-0 m-t-20 font-grey">{{$thebestdata->name}}</h4>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END : MOBILE -->

<div class="card carousel carousel-parallax" data-items="1" data-dots="true" data-loop="true" data-autoplay="false" data-autoplay="3500" data-arrows="false">
    @foreach($projectsHot->data as $projectHot)
    <div class="p-t-50 p-b-0 p-t-150-m @if($agent->isMobile()) bg-overlay-mobile @endif" style="background:url(@if($agent->isMobile()) {{IMAGE_URL.'project/'.$projectHot->image_3}} @else {{IMAGE_URL.'project/'.$projectHot->image_4}}@endif) 0% 50% / cover no-repeat; height:400px;">
        <div class="bg-overlay bg-overlay-home"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 center text-center text-light @if($agent->isMobile()) @else m-t-20-m @endif">
                    <div class="heading-text heading-text-home heading-section text-center">
                        <h2 class="text-uppercase text-small font-weight-600 d-none d-sm-block d-sm-none d-md-block">{{$projectHot->name}}</h2>
                        <h5 class="text-uppercase text-medium font-weight-600 d-md-none d-lg-none d-xl-none">{{$projectHot->name}}</h5>
                    </div>
                    <ul class="list list-legend display-inline text-center m-b-0 font-weight-700 m-l-25">
                        <li class="m-b-0"><h5>@if($projectHot->area !='Other'){{$projectHot->area}}@endif</h5></li>
                    </ul>
                    <p class="m-b-30 font-weight-600 font-size-12">{{str_limit($projectHot->description,200)}}</p>
                    <a href="{{URL::to('projects/'.$projectHot->category_slug.'/'.$projectHot->slug)}}" class="btn btn-rounded btn-light btn-mobile-home btn-xs-m">View More</a>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>

<section id="slider" class="p-t-60 p-b-60 p-t-20-m p-b-20-m">
    <div class="container ">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="font-green m-b-5">3D Virtual Tour</h2>
                <p class="lead text-dark font-weight-600 font-italic m-b-30">Discover The Charms of Living at Summarecon Serpong Houses</p>
                <!-- DEKSTOP -->
                <div class="d-none d-sm-block d-sm-none d-md-block">
                    <div class="carousel mb-4" data-video="true" data-items="1">
                        @foreach($virtual as $product)
                        @if($product->link360)
                        <div class="col-md-12 text-center">
                            <iframe width="100%" height="400px" src="{{$product->link360}}" class="margin-two-bottom" style="border:0 !important;"></iframe>
                        </div>
                        @endif
                        @endforeach
                    </div>
                    <a href="{{URL::to('virtual-tour ')}}" class="m-t-30 btn btn-dark btn-outline btn-rounded btn-outline-green">See Others</a>
                </div>
                <!-- END : DEKSTOP -->
            </div>
        </div>
    </div>
    <!-- MOBILE -->
    <div class="container-fullscreen d-md-none d-lg-none d-xl-none">
        <div class="row">
            <div class="col-lg-12 no-padding text-center">
                <div class="carousel mb-4" data-video="true" data-items="1">
                    @foreach($virtual as $product)
                    @if($product->link360)
                    <div class="col-md-12 text-center">
                        <iframe width="100%" height="400px" src="{{$product->link360}}" class="margin-two-bottom" style="border:0 !important;"></iframe>
                    </div>
                    @endif
                    @endforeach
                </div>
                <a href="{{URL::to('virtual-tour ')}}" class="m-t-30 btn btn-dark btn-outline btn-rounded btn-outline-green btn-xs-m">See Others</a>
            </div>
        </div>
    </div>
    <!-- END : MOBILE -->
</section>

<section class="image-block text-center p-t-60 p-b-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="font-green m-b-40">Living at Summarecon Serpong</h2>
                <!-- MOBILE -->
                <div class="carousel carousel-mobile arrows-visibile d-md-none d-lg-none d-xl-none" data-items="1" data-dots="false" data-arrows="true">
                    @foreach($livingProduct as $product)
                    <div class="item">
                        <a href="{{URL::to('projects/'.$product->url)}}">
                            <img src="{{IMAGE_URL.$product->thumb}}" class="img-fluid m-b-10" alt="@if($product->meta_name) {{$product->meta_name}} @else {{$product->name}} - Summarecon Serpong @endif">
                        </a>
                        <h5 class="m-b-0 m-t-10"><a class="font-green font-medium" href="{{URL::to('projects/'.$product->url)}}">{{$product->name}}</a></h5>
                    </div>
                    @endforeach
                </div>
                <!-- END : MOBILE -->
            </div>
            <!-- DEKSTOP -->
            @foreach($livingProduct as $product)
            <div class="col-lg-4 d-none d-sm-block d-sm-none d-md-block">
                <a href="{{URL::to('projects/'.$product->url)}}">
                    <img src="{{IMAGE_URL.$product->thumb}}" class="img-fluid m-b-10" alt="@if($product->meta_name) {{$product->meta_name}} @else {{$product->name}} - Summarecon Serpong @endif">
                </a>
                <h5 class="m-b-0 m-t-10"><a class="font-green font-medium" href="{{URL::to('projects/'.$product->url)}}">{{$product->name}}</a></h5>
            </div>
            @endforeach
            <!-- END : DEKSTOP -->
        </div>
    </div>
</section>

<section id="image-block" class="no-padding d-none d-sm-block d-sm-none d-md-block">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7" style="background:url({{IMAGE_URL.$articles->data[0]->thumb}}) 50% 50% / cover no-repeat;"></div>
            <div class="col-lg-5 col-12">
                <div class="row">
                    <div class="col-lg-6 p-30">
                        <div class="heading-text heading-section heading-custom">
                            <h2 class="font-green">{{$articles->data[0]->name}}</h2>
                            <span class="post-date">{{date('F j, Y',strtotime($articles->data[0]->start_date))}}</span>
                        </div>
                    </div>
                    <div class="col-lg-6 p-30">
                        <span class="lead line-height-27">
                            {{str_limit($articles->data[0]->brief,150)}}
                        </span>
                        <br>
                        <a href="{{URL::to('whats-on/'.$articles->data[0]->category_slug.'/'.$articles->data[0]->slug)}}" class="m-t-30 btn btn-dark btn-outline btn-rounded btn-outline-green">See Others</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="image-block2" class="no-padding d-none d-sm-block d-sm-none d-md-block">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7">
                <div class="row">
                    <div class="col-lg-6 p-30">
                        <span class="lead line-height-27">
                            {{str_limit($articles->data[1]->brief,150)}}
                        </span>
                        <br>
                        <a href="{{URL::to('whats-on/'.$articles->data[1]->category_slug.'/'.$articles->data[1]->slug)}}" class="m-t-30 btn btn-dark btn-outline btn-rounded btn-outline-green">See Others</a>
                    </div>
                    <div class="col-lg-6 p-30">
                        <div class="heading-text heading-section heading-custom text-right">
                            <h2 class="font-green">{{$articles->data[1]->name}}</h2>
                            <span class="post-date">{{date('F j, Y',strtotime($articles->data[1]->start_date))}}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5" style="background:url({{IMAGE_URL.$articles->data[1]->thumb}}) 50% 50% / cover no-repeat;"></div>
        </div>
    </div>
</section>

<!-- MOBILE -->
<section class="p-t-0 d-md-none d-lg-none d-xl-none">
    <div class="container-fullscreen">
        <div class="row">
            <div class="col-12">
                <div class="carousel carousel-mobile-news" data-items="1" data-dots="true">
                    @foreach($articles->data as $article)
                    <div class="item">
                        <img src="{{IMAGE_URL.$article->thumb}}" class="img-responsive">
                        <div class="p-30 p-t-10">
                            <div class="heading-text heading-section heading-custom">
                                <h2 class="font-green">{{$article->name}}</h2>
                                <span class="post-date">{{date('F j, Y',strtotime($article->start_date))}}</span>
                            </div>
                            <br>
                            <a href="{{URL::to('whats-on/'.$article->category_slug.'/'.$article->slug)}}" class="btn btn-dark btn-outline btn-rounded btn-outline-green btn-xs-m">See Others</a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END : MOBILE -->

<section class="call-to-action background-call-to-action">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 text-center">
                <h3 class="text-light m-t-0"><strong>Get E-Brochure</strong></h3>
            </div>
            <div class="col-lg-9 text-center">
                @foreach($brochures as $brochure)
                <!-- <a href="{{ FILE_URL.$brochure->files  }}" target="_blank" class="btn btn-rounded btn-light-custom m-t-0">{{$brochure->name}}</a> -->
                <button type="button" class="a-brochure btn btn-rounded btn-reveal btn-light-custom m-t-0" data-toggle="modal" data-target="#modal-{{$brochure->article_id}}" data-dismiss="modal" data-aid="{{$brochure->article_id}}" data-fil="{{FILE_URL.$brochure->files}}" data-pro="{{$brochure->parent_category_slug}}" data-bro="{{$brochure->name}}">
                    <span>{{$brochure->name}}</span><i class="fa fa-download"></i></button>
                @endforeach
            </div>
        </div>
    </div>
</section>

@foreach($brochures as $brochure)
<!-- MODAL POP UP -->
<div class="modal fade" id="modal-{{$brochure->article_id}}" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="namebro">Form Get Brochure <label>{{$brochure->name}}</label> </h4>
            </div>
            <div class="modal-body text-center">
                <form method="post" action="{{URL::to('home/get-brochure')}}" id="form-brochure">
                    <input type="hidden" value="{{csrf_token()}}" name="_token">
                    <input type="hidden" name="projectName" class="projectName">
                    <input type="hidden" value="brochure" name="type">
                    <input type="hidden" name="article_id" class="article_id">
                    <input type="hidden" name="file" class="file">

                    <div class="row">
                        <div class="form-group col-md-12">
                            <input class="form-control form-control-us char-only" minlength="3" maxlength="50" id="name-brochure" type="text" class="validate" placeholder="Your Name" name="name" required><br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <input class="form-control form-control-us" minlength="3" maxlength="50" id="email-brochure" type="text" class="validate" placeholder="Your Email" name="email" required><br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <input class="form-control form-control-us number-only" minlength="10" maxlength="15" id="phone-brochure" type="text" class="validate" placeholder="Your Phone" name="phone" required>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        {!!Recaptcha::render()!!}
                    </div>

                    <input type="hidden" name="utm_source" id="utm_source" value={{ app('request')->input('utm_source') }}>
                    <input type="hidden" name="utm_medium" id="utm_medium" value={{ app('request')->input('utm_medium') }}>
                    <input type="hidden" name="utm_campaign" id="utm_campaign" value={{ app('request')->input('utm_campaign') }}>
                    <span class="required text-left">*Please complete all fields correctly</span>

                    <div class="modal-footer text-center">
                        <div class="form-status"></div>
                        <button class="btn btn-gold-custom btn-radius no-margin-bottom " type="submit">Submit</button><br>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach
@stop

@section('js')
<script type="text/javascript">
    $('.modal.fade').on('shown.bs.modal', function () {
        $(".modal-backdrop.fade").hide();
    })

    $('.a-type').click(function() {
        var menuSlug = $(this).data('slug');
        $('.list-type').each(function(i, obj) {
            if($(this).data('slug')==menuSlug) {
                $(this).show()
                $(this).find('.flickity-viewport').css('height','443.594px')
            } else {
                $(this).hide()
            }
        });
    })

    $(".arrange-view").trigger('click');
    $('.a-brochure').click(function() {
        var aid = $(this).data('aid');
        var fil = $(this).data('fil');
        var pro = $(this).data('pro');
        var bro = $(this).data('bro');
        $('#form-brochure').find('.article_id').val(aid);
        $('#form-brochure').find('.file').val(fil);
        $('#form-brochure').find('.projectName').val(bro);
        // $('#namebro label').text(bro);
    })

    $("#form-brochure").submit(function(e) {
        e.preventDefault();
        // var instance = M.Modal.getInstance(elem);
        var url = $(this).attr('action');
        var data = $(this).serializeArray();
        $.ajax({
            url: url,
            type: 'post',
            data: data,
            beforeSend: function() {
                var status = $(".form-status");
                status.append('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn();
            },
            success: function(text) {
                var brochureProject = $('#brochure-project-name');
                if (text.response == 'success') {
                    // window.open(
                        // text.file,
                        // '_blank' // <- This is what makes it open in a new window.
                        // );
                        window.location.href = "{{URL::to('download')}}" + '/' + text.proname + '/' + text.articleid;
                    swal({
                        title: 'Terima Kasih',
                        html: text.message,
                        type: 'success'
                    });
                    $('#name-brochure').val('');
                    $('#email-brochure').val('');
                    $('#phone-brochure').val('');
                    var status = $(".form-status");
                    status.html('');

                        // window.location.href = text.file;
                } else {
                    swal({
                        title: 'Error on Connection',
                        html: text.message,
                        type: 'error'
                    });
                }
                // $('#modal').leanModal();
                // $('.modal').modal('hide');
                // $('.btn-close').trigger();
            },
            error: function() {
                swal({
                    title: 'Error',
                    text: 'Error!!',
                    type: 'error'
                });
                var status = $(".form-status");
                status.html('');
            }
        });
    });


$(".slide-href").click(function() {
    if ($(this).attr("data-href") === '#') {
    } else {
    window.open($(this).attr("data-href"),'_blank');
    return false;
    }
});
</script>

@stop

@extends('layout.layout')
@section('content')
<div id="slider" class="inspiro-slider slider-halfscreen padding-slider dots-creative" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    @foreach($banners as $banner)
    <div class="slide background-image" data-bg-image="{{$banner}}">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class=" text-center text-light">
                <h2 class="text-uppercase text-medium text-light font-weight-700 font-italic">Explore Summarecon Serpong</h2>
                <p class="lead text-light font-weight-600">Wonderful Living at Summarecon Serpong</p>
            </div>
        </div>
    </div>
    @endforeach
</div>

<section class="p-t-40 p-b-60">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div data-animate="fadeInUp" data-animate-delay="0" class="animated fadeInUp visible">
                    <p>Lorem ipsum dolor sit amet, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis aliquam. Lorem ipsum dolor sit amet, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis aliquam. Lorem ipsum dolor sit amet, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis aliquam. Lorem ipsum dolor sit amet, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis aliquam.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="p-t-40 p-b-40">
    <div class="container">
        <div id="portfolio" class="grid-layout portfolio-4-columns" data-margin="20">
            <div class="portfolio-item img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="#"><img src="{{asset('assets/homepages/fitness/images/gallery/7.jpg')}}" alt=""></a>
                    </div>
                    <div class="portfolio-description">
                        <a href="portfolio-page-grid-gallery.html">
                            <h3>Let's Go Outside</h3>
                            <span>Illustrations / Graphics</span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="portfolio-item img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="#"><img src="{{asset('assets/homepages/fitness/images/gallery/6.jpg')}}" alt=""></a>
                    </div>
                    <div class="portfolio-description">
                        <a href="portfolio-page-grid-gallery.html">
                            <h3>Let's Go Outside</h3>
                            <span>Illustrations / Graphics</span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="portfolio-item img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="#"><img src="{{asset('assets/homepages/fitness/images/gallery/5.jpg')}}" alt=""></a>
                    </div>
                    <div class="portfolio-description">
                        <a href="portfolio-page-grid-gallery.html">
                            <h3>Let's Go Outside</h3>
                            <span>Illustrations / Graphics</span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="portfolio-item img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="#"><img src="{{asset('assets/homepages/fitness/images/gallery/4.jpg')}}" alt=""></a>
                    </div>
                    <div class="portfolio-description">
                        <a href="portfolio-page-grid-gallery.html">
                            <h3>Let's Go Outside</h3>
                            <span>Illustrations / Graphics</span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="portfolio-item img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="#"><img src="{{asset('assets/homepages/fitness/images/gallery/3.jpg')}}" alt=""></a>
                    </div>
                    <div class="portfolio-description">
                        <a href="portfolio-page-grid-gallery.html">
                            <h3>Let's Go Outside</h3>
                            <span>Illustrations / Graphics</span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="portfolio-item img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="#"><img src="{{asset('assets/homepages/fitness/images/gallery/2.jpg')}}" alt=""></a>
                    </div>
                    <div class="portfolio-description">
                        <a href="portfolio-page-grid-gallery.html">
                            <h3>Let's Go Outside</h3>
                            <span>Illustrations / Graphics</span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="portfolio-item img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="#"><img src="{{asset('assets/homepages/fitness/images/gallery/1.jpg')}}" alt=""></a>
                    </div>
                    <div class="portfolio-description">
                        <a href="portfolio-page-grid-gallery.html">
                            <h3>Let's Go Outside</h3>
                            <span>Illustrations / Graphics</span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="portfolio-item img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="#"><img src="{{asset('assets/homepages/fitness/images/gallery/10.jpg')}}" alt=""></a>
                    </div>
                    <div class="portfolio-description">
                        <a href="portfolio-page-grid-gallery.html">
                            <h3>Let's Go Outside</h3>
                            <span>Illustrations / Graphics</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@php $no=1; @endphp
@foreach($articles as $explore)
@php $no++; @endphp
@if($no %2==0)
<section id="image-block" class="no-padding">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 p-40">
                <div class=" text-center">
                    <h5 class="font-gold font-italic">{{$explore->note_1}}</h5>
                    <h2 class="font-green font-weight-600 text-uppercase m-t-20 m-b-30">{{$explore->name}}</h2>
                    <p>{{$explore->brief}}</p>
                    <a class="btn btn-green-custom btn-rounded m-t-40" href="{{$explore->url}}">See More</a>
                </div>
            </div>
            <div class="col-lg-6" style="background:url(@if($explore->thumb){{IMAGE_URL.$explore->thumb}}@else{{asset('assets/homepages/fashion/images/author.jpg')}} @endif) 50% 50% / cover no-repeat;"></div>
        </div>
    </div>
</section>
@else
<section id="image-block" class="no-padding">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6" style="background:url(@if($explore->thumb){{IMAGE_URL.$explore->thumb}}@else{{asset('assets/homepages/fashion/images/author.jpg')}} @endif) 50% 50% / cover no-repeat;"></div>
            <div class="col-lg-6 p-40">
                <div class="text-center">
                    <h5 class="font-gold font-italic">Awards</h5>
                    <h2 class="font-green font-weight-600 text-uppercase m-t-20 m-b-30">{{$explore->note_1}}</h2>
                    <p>{{$explore->brief}}</p>
                    <a class="btn btn-green-custom btn-rounded m-t-40" href="{{$explore->url}}">See More</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
@endforeach
@stop

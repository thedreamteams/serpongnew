@extends('layout.layout')

@section('content')
<div id="slider" class="inspiro-slider slider-halfscreen padding-slider dots-creative d-none d-sm-block d-sm-none d-md-block" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    <div class="slide background-image" data-bg-image="{{IMAGE_URL.$banners->image_1}}">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class=" text-center text-light">
                <h2 class="text-medium text-light font-weight-700 font-italic">Town Management</h2>
                <p class="lead text-light font-weight-600">{{$banners->brief}}</p>
            </div>
        </div>
    </div>
</div>

<section id="page-title" class="text-light d-md-none d-lg-none d-xl-none" data-bg-parallax="{{IMAGE_URL.$banners->image_1}}">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title text-center">
            <h2 class="text-medium text-light font-weight-700 font-italic">{{$banners->name}}</h2>
            <p class="lead text-light font-weight-600">{{$banners->brief}}</p>
        </div>
    </div>
</section>

<section class="p-t-80 p-b-60">
    <div class="container">
        <div class="row text-center">
            @foreach($articles as $article)
            <div class="col-md-3">
                <div class="gallery-overlay card">
                    <a href="{{URL::to('town-management/'.$article->slug)}}">
                        <div class="gallery-overlay-image">
                            <img src="{{IMAGE_URL.$article->thumb}}" alt="{{ $article->name }}" >
                            <div class="gallery-overlay-text d-flex align-items-end overlay-container">
                                <h3 class="text-left">{{ $article->name }}</h3>
                                <div class="overlay-container">
                                    <div class="overlay-content">
                                        <h3 class="card-title text-light m-t-40">{{ $article->name }}</h3>
                                        <p class="text-light">Summarecon Serpong memberikan pelayanan dan keamanan 24 jam yang dilengkapi...</p>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </a>
                </div>                
            </div>
            @endforeach
        </div>
    </div>
</section>



<section class="p-t-60 p-b-0">
    <div class="container container-no-padding">
        @php $no=1; @endphp
        @foreach($town as $itown)
        @php $no++; @endphp
        @if($no %2==0)
        <div class="row m-b-50 m-b-0-m">
            <div class="col-12 d-md-none d-lg-none d-xl-none">
                <img src="{{IMAGE_URL.$itown->thumb}}" class="img-responsive" alt="@if($itown->meta_name) {{$itown->meta_name}} @else Summarecon Serpong - {{$itown->name}} @endif">
            </div>
            <div class="col-lg-5 text-center d-none d-sm-block d-sm-none d-md-block">
                <img src="{{IMAGE_URL.$itown->thumb}}" class="img-fluid" alt="@if($itown->meta_name) {{$itown->meta_name}} @else Summarecon Serpong - {{$itown->name}} @endif">
            </div>
            <div class="col-lg-7 col-12 text-left m-t-0 p-45-m">
                <div class="heading-text heading-section heading-town">
                    <h2 class="font-green m-b-50 line-height-27">{{$itown->name}}</h2>
                    <p class="m-b-0 line-height-27" style="text-align: justify;">{!! $itown->contents !!}</p>

                    <ul class="list-inline">
                        <li class="m-r-10">
                            <a class="btn btn-rounded btn-outline btn-reveal btn-green-custom m-t-10 text-light" href="tel:{{$itown->note_1}}">
                                <span>Phone</span><i class="fas fa-phone"></i>
                            </a>
                        </li>
                        <li>
                            <a class="btn btn-rounded btn-outline btn-reveal btn-green-custom m-t-10 text-light" href="mailto:{{$itown->note_2}}">
                                <span>Email</span><i class="fas fa-envelope"></i>
                            </a>
                        </li>                        
                    </ul>                    
                </div>
            </div>
        </div>
        @else
        <div class="row m-b-50 m-b-0-m">
            <div class="col-12 no-padding d-md-none d-lg-none d-xl-none">
                <img src="{{IMAGE_URL.$itown->thumb}}" class="img-responsive" alt="@if($itown->meta_name) {{$itown->meta_name}} @else Summarecon Serpong - {{$itown->name}} @endif">
            </div>
            <div class="col-lg-7 col-12 text-left m-t-0 p-45-m">
                <div class="heading-text heading-section heading-town">
                    <h2 class="font-green m-b-50 line-height-27">{{$itown->name}}</h2>
                    <p class="m-b-0 line-height-27">{!!$itown->contents!!}</p>
                    <ul class="list-inline">
                        <li class="m-r-10"><a class="btn btn-rounded btn-outline btn-reveal btn-green-custom m-t-10 text-light" href="tel:{{$itown->note_1}}"><span>Phone</span><i class="fas fa-phone"></i></a></li>
                        <li><a id="email-link" class="btn btn-rounded btn-outline btn-reveal btn--custom m-t-10 text-light" href="mailto:{{$itown->note_2}}"><span>Email</span><i class="fas fa-envelope"></i></a></li>
                    </ul>                    
                </div>
            </div>
            <div class="col-lg-5 text-center d-none d-sm-block d-sm-none d-md-block">
                <img src="{{IMAGE_URL.$itown->thumb}}" class="img-fluid" alt="@if($itown->meta_name) {{$itown->meta_name}} @else Summarecon Serpong - {{$itown->name}} @endif">
            </div>
        </div>
        @endif
        @endforeach
    </div>
</section>
@stop

@section('css')
<style>
    .overlay-container {
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: #558055;
        opacity: 0;
        transition: opacity 0.3s ease;
    }
    
    .card {
        border-radius: 3px;
        overflow: hidden; /* Sembunyikan overflow dari gambar */
        box-shadow: rgba(0, 0, 0, 0.3) 0px 19px 38px, rgba(0, 0, 0, 0.22) 0px 15px 12px;
        border: 0;
        background-color: transparent;
        margin-bottom: 25px;
    }
    
    .card:hover .overlay-container {opacity: 1;}
    
    .overlay-content {
        position: absolute;
        bottom: 0; /* Ubah nilai bottom ini */
        left: 10px;
        width: 100%;
        padding: 10px; /* Sesuaikan padding jika diperlukan */
        box-sizing: border-box;
        transition: bottom 0.3s ease;
    }

    .gallery-overlay .gallery-overlay-text {
        position: absolute;
        top: 0; /* Menempatkan overlay di bagian atas gambar */
        left: 0; /* Menempatkan overlay di sebelah kiri gambar */
        width: 100%; /* Menyesuaikan lebar overlay dengan gambar */
        height: 100%;
        background: linear-gradient(0deg, rgb(2, 72, 3) 0%, rgba(1, 80, 40, 0) 30%);
        padding: 10px; /* Atur padding untuk teks overlay */
        text-align: left !important; 
        color: green; /* Atur warna teks overlay */
        box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.2); /* Tambahkan sedikit efek bayangan */
    }

    .gallery-overlay-image{border-radius: 5px;}
</style>

@endsection



@extends('layout.layout')
@section('content')
<div id="slider" class="inspiro-slider slider-halfscreen padding-slider dots-creative d-none d-sm-block d-sm-none d-md-block" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    <div class="slide background-image" data-bg-image="{{IMAGE_URL.$banners->image_1}}">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class=" text-center text-light">
                <h2 class="text-medium text-light font-weight-700 font-italic">Town Management</h2>
                <p class="lead text-light font-weight-600">{{$banners->brief}}</p>
            </div>
        </div>
    </div>
</div>

<section id="page-title" class="text-light d-md-none d-lg-none d-xl-none" data-bg-parallax="{{IMAGE_URL.$banners->image_1}}">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title text-center">
            <h2 class="text-medium text-light font-weight-700 font-italic">{{$banners->name}}</h2>
            <p class="lead text-light font-weight-600">{{$banners->brief}}</p>
        </div>
    </div>
</section>

<section class="p-t-80 p-b-60">
    <div class="container">
        <div class="row m-b-0 m-b-0-m">
            <div class="col-12 no-padding d-md-none d-lg-none d-xl-none">
                <img src="{{IMAGE_URL.$town->thumb}}" class="img-responsive" alt="@if($town->meta_name) {{$town->meta_name}} @else Summarecon Serpong - {{$town->name}} @endif">
            </div>
            <div class="col-lg-5 text-center d-none d-sm-block d-sm-none d-md-block">
                <img src="{{IMAGE_URL.$town->thumb}}" class="img-fluid" alt="@if($town->meta_name) {{$town->meta_name}} @else Summarecon Serpong - {{$town->name}} @endif">
            </div>
            <div class="col-lg-7 col-12 text-left m-t-0 p-45-m">
                <div class="heading-text heading-section heading-town">
                    <h2 class="font-green m-b-50 line-height-27">{{$town->name}}</h2>
                    <p class="m-b-0 line-height-27">{!!$town->contents!!}</p>
                    <ul class="list-inline">
                        <li class="m-r-10"><a class="btn btn-rounded btn-outline btn-reveal btn-green-custom m-t-10 text-light " href="tel:{{$town->note_1}}"><span>Phone</span><i class="fas fa-phone"></i></a></li>
                        <li><a class="btn btn-rounded btn-outline btn-reveal btn-green-custom m-t-10 text-light" href="mailto:{{$town->note_2}}" ><span>Email</span><i class="fas fa-envelope"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>    
</section>

<section id="new-section" class="p-t-30 p-b-60">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="font-gold m-b-30 line-height-27 text-uppercase text-italic">View Others</h3>
            </div>
        </div>
        <div class="row">
            @foreach($articles as $article)
            <div class="col-md-4">
                <div class="gallery-overlay">
                    <a href="{{URL::to('town-management/'.$article->slug)}}">
                        <div class="gallery-overlay-image">
                            <img src="{{IMAGE_URL.$article->thumb}}" alt="{{ $article->name }}">
                            <div class="gallery-overlay-text text-center d-flex justify-content-center align-items-end">
                                <h3 class="fs-22 text-left">{{ $article->name }}</h3>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            @endforeach         
        </div>        
    </div>
</section>


@stop

@section('css')
<style>
    .gallery-overlay{
        border: 0;
        background-color: transparent !important;
    }

    .gallery-overlay-image {
        box-shadow: rgba(0, 0, 0, 0.3) 0px 19px 38px, rgba(0, 0, 0, 0.22) 0px 15px 12px;
        height: 350px;
        border-radius: 6px;
    }
   
    .gallery-overlay .gallery-overlay-text {
        background: linear-gradient(0deg, rgb(2, 72, 3) 0%, rgba(1, 80, 40, 0) 30%);
        padding: 10px;
    }
</style>
@endsection
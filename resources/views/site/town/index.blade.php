@extends('layout.layout')
@section('content')
<div id="slider" class="inspiro-slider slider-halfscreen padding-slider dots-creative d-none d-sm-block d-sm-none d-md-block" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    <div class="slide background-image" data-bg-image="{{IMAGE_URL.$banners->image_1}}">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class=" text-center text-light">
                <h2 class="text-medium text-light font-weight-700 font-italic">{{$banners->name}}</h2>
                <p class="lead text-light font-weight-600">{{$banners->brief}}</p>
            </div>
        </div>
    </div>
</div>

<section id="page-title" class="text-light d-md-none d-lg-none d-xl-none" data-bg-parallax="{{IMAGE_URL.$banners->image_1}}">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title text-center">
            <h2 class="text-medium text-light font-weight-700 font-italic">{{$banners->name}}</h2>
            <p class="lead text-light font-weight-600">{{$banners->brief}}</p>
        </div>
    </div>
</section>

<section class="p-t-60  p-b-20 p-t-0-m">
    <div class="container container-no-padding">
        @php $no=1; @endphp
        @foreach($town as $article)
        @php $no++; @endphp
        @if($no %2==0)
        <div class="row m-b-50 m-b-0-m">
            <div class="col-12 no-padding d-md-none d-lg-none d-xl-none">
                <img src="{{IMAGE_URL.$article->thumb}}" class="img-responsive" alt="@if($article->meta_name) {{$article->meta_name}} @else Summarecon Serpong - {{$article->name}} @endif">
            </div>
            <div class="col-lg-5 text-center d-none d-sm-block d-sm-none d-md-block">
                <img src="{{IMAGE_URL.$article->thumb}}" class="img-fluid rounded-circle" alt="@if($article->meta_name) {{$article->meta_name}} @else Summarecon Serpong - {{$article->name}} @endif">
            </div>
            <div class="col-lg-7 col-12 text-left m-t-20 p-45-m">
                <div class="heading-text heading-section heading-town">
                    <h2 class="font-green m-b-50">{{$article->name}}</h2>
                    <p class="m-b-0 line-height-27">{!!$article->contents!!}</p>
                    <ul class="list-inline">
                        <li class="m-r-10"><a class="btn btn-rounded btn-outline btn-reveal btn-green-custom m-t-10 text-light " href="tel:{{$article->note_1}}"><span>Phone</span><i class="fas fa-phone"></i></a></li>
                        <li><a class="btn btn-rounded btn-outline btn-reveal btn-green-custom m-t-10 text-light" href="mailto:{{$article->note_2}}"><span>Email</span><i class="fas fa-envelope"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        @else
        <div class="row m-b-50 m-b-0-m">
            <div class="col-12 no-padding d-md-none d-lg-none d-xl-none">
                <img src="{{IMAGE_URL.$article->thumb}}" class="img-responsive" alt="@if($article->meta_name) {{$article->meta_name}} @else Summarecon Serpong - {{$article->name}} @endif">
            </div>
            <div class="col-lg-7 col-12 text-left m-t-20 p-45-m">
                <div class="heading-text heading-section heading-town">
                    <h2 class="font-green m-b-50">{{$article->name}}</h2>
                    <p class="m-b-0 line-height-27">{!!$article->contents!!}</p>                    
                    <ul class="list-inline">
                        <li class="m-r-10"><a class="btn btn-rounded btn-outline btn-reveal btn-green-custom m-t-10 text-light" href="tel:{{$article->note_1}}"><span>Phone</span><i class="fas fa-phone"></i></a></li>
                        <li><a class="btn btn-rounded btn-outline btn-reveal btn-green-custom m-t-10 text-light" href="mailto:{{$article->note_2}}"><span>Email</span><i class="fas fa-envelope"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-5 text-center d-none d-sm-block d-sm-none d-md-block">
                <img src="{{IMAGE_URL.$article->thumb}}" class="img-fluid rounded-circle" alt="@if($article->meta_name) {{$article->meta_name}} @else Summarecon Serpong - {{$article->name}} @endif">
            </div>
        </div>
        @endif
        @endforeach
    </div>
</section>
@stop
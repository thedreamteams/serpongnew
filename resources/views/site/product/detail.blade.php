@extends('layout.layout')
@section('content')
<div id="slider" class="inspiro-slider slider-halfscreen padding-slider dots-creative d-none d-sm-block d-sm-none d-md-block" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    <div class="slide background-image" data-bg-image="@if($project->image_4){{IMAGE_URL.'project/'.$project->image_4}} @else {{IMAGE_URL.'project/'.$project->image_1}} @endif">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class=" text-center text-light">
                <h2 class="text-medium text-light font-weight-700 font-italic">{{$project->name}}</h2>
                <p class="lead text-light font-weight-600">@if($areadetail->name !='Other'){{$areadetail->name}}@endif</p>
            </div>
        </div>
    </div>
</div>

<section class="text-light d-md-none d-lg-none d-xl-none" style="background-image:url('@if($project->image_3) {{IMAGE_URL.'project/'.$project->image_3}} @else {{IMAGE_URL.'project/'.$project->image_1}} @endif');height:768px;background-position:center center;background-size:cover;top:-80px;">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title text-center" style="position:relative; top:350px;">
            <h2 class="text-medium text-light font-weight-700 font-italic">{{$project->name}}</h2>
            <p class="lead text-light font-weight-600">@if($areadetail->name !='Other'){{$areadetail->name}}@endif</p>
        </div>
    </div>
</section>

<section class="p-b-0 p-t-0-m">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-12 text-center">
                <img alt="@if($project->meta_name) {{$project->meta_name}} @else {{$project->name}} @endif" src="{{IMAGE_URL.'project/'.$project->thumb}}" class="img-responsive @if($agent->isMobile()) w-50 p-3 @endif">
            </div>
            <div class="col-md-9 col-12">
                <p>{{$project->description}}</p>
            </div>
        </div>
    </div>
</section>

@foreach($others as $other)
@if($other->category_slug=='facilities')
@if(isset($galleryOther[$other->slug]))
<section class="p-t-0 p-b-60">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center font-italic m-t-30">
                <img src="{{asset('assets/images/serpong/icon/kutip.svg')}}" class="img-kutip m-b-10">
                <h2 class="font-green m-b-40 font-weight-600">{{$other->brief}}</h2>
                <div class="carousel arrows-visibile" data-items="3" data-dots="false" data-margin="0" data-arrows="true"  data-autoplay="false">
                    @foreach($galleryOther[$other->slug] as $image)
                    <img src="{{$image}}"  alt="Facilities {{$project->name}}"/>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endif
@endif
@endforeach

<section class="p-b-40 p-t-0">
    <div class="container">
        @foreach($others as $other)
        @if($other->category_slug=='facilities')
        <div class="row">
            <div class="col-lg-12">
                <h2 class="font-green m-b-30">Facilities & Features</h2>
                {!!$other->contents!!}
            </div>
        </div>
        @endif
        @endforeach
    </div>
</section>

<section class="p-t-0 p-b-5">
    <div class="container">
    @if($category)
    @foreach($category as $cat)
        @if(strpos($cat->slug, 'type') !==FALSE)
        <div class="row">
            <div class="col-lg-12">
                <h2 class="font-green m-b-10">Type</h2>
                <div class="page-menu m-b-10-m">
                    <nav>
                        <ul>
                            @foreach($category as $cat)
                            @if(strpos($cat->slug, 'type') !==FALSE)
                            <li class="active dropdown disabled"><a href="">{{$cat->name}}</a>
                                <ul class="dropdown-menu">
                                @foreach($others as $other)
                                    @if($other->category_slug==$cat->slug)
                                    <li data-slug="{{$other->category_slug}}-{{$other->slug}}" class="a-type"> <a href="#{{$other->category_slug}}-{{$other->slug}}">{{$other->name}}</a></li>
                                    @endif
                                @endforeach                                    
                                </ul>
                            @endif
                            </li>
                            @endforeach
                        </ul>
                    </nav>
                    <div id="pageMenu-trigger" style="display:contents !important;">
                        <!-- <i class="fa fa-bars"></i> -->
                        <div class="row mt-4">
                            <div class="col-12 no-padding">
                                <div class="p-dropdown d-md-none d-lg-none d-xl-none" style="width:100% !important;">
                                    <a class="btn btn-outline btn-block btn-outline-green btn-rounded" style="font-size:15px; font-weight:400; letter-spacing:3.5px;">Select Type</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @break
        @endif
        @endforeach

        <?php $key=1; ?>
        @foreach($others as $other)
        @if(strpos($other->category_slug, 'type') !==FALSE)
        @if(isset($galleryOther[$other->slug]))
        <div class="row">
            <div class="col-lg-8 no-padding list-type" data-animate="fadeInLeft" data-slug="{{$other->category_slug}}-{{$other->slug}}" style="display: {{ ($key=='1')?'block':'none' }};">
                <div class="carousel" data-items="1" data-arrows="false">
                    @foreach($galleryOther[$other->slug] as $image)
                    <img src="{{$image}}" class="img-responsive" alt="Galery">
                    @endforeach
                </div>
            </div>
            <div class="col-lg-4 no-padding background-grey list-type" data-animate="fadeInRight" data-slug="{{$other->category_slug}}-{{$other->slug}}" style="display: {{ ($key=='1')?'block':'none' }};">
                <div class="p-30">
                    <div class="heading-project-detail heading-text heading-section">
                        <h2>{{ $other->category }} - {{$other->name}}</h2>
                    </div>
                    {!!$other->contents!!}
                    <ul class="list-unstyled">
                    <li>
                        @if($typeOther[$other->slug]->bedroom)
                        <img src="{{asset('assets/images/serpong/icon/bedroom-01.svg')}}" class="img-icon m-r-5">{{$typeOther[$other->slug]->bedroom}} Bedroom
                        @endif
                    </li>
                    <li>
                        @if($typeOther[$other->slug]->bathroom)
                        <img src="{{asset('assets/images/serpong/icon/bathroom-01.svg')}}" class="img-icon m-r-5">{{$typeOther[$other->slug]->bathroom}} Bathroom
                        @endif
                    </li>
                    <li>
                        @if($typeOther[$other->slug]->floor)
                        <img src="{{asset('assets/images/serpong/icon/floor-01.svg')}}" class="img-icon m-r-5">{{$typeOther[$other->slug]->floor}} Floor
                        @endif
                    </li>
                    <li>
                        @if($typeOther[$other->slug]->balcony)
                        <img src="{{asset('assets/images/serpong/icon/balcony-01.svg')}}" class="img-icon m-r-5">{{$typeOther[$other->slug]->balcony}} Balcony
                        @endif
                    </li>
                    <li>
                        @if($typeOther[$other->slug]->living_room)
                        <img src="{{asset('assets/images/serpong/icon/living-room-01.svg')}}" class="img-icon m-r-5">{{$typeOther[$other->slug]->living_room}} Living Room
                        @endif
                    </li>
                    <li>
                        @if($typeOther[$other->slug]->dining_room)
                        <img src="{{asset('assets/images/serpong/icon/dining-room-01.svg')}}" class="img-icon m-r-5">{{$typeOther[$other->slug]->dining_room}} Dining Room
                        @endif
                    </li>
                    <li>
                        @if($typeOther[$other->slug]->kitchen)
                        <img src="{{asset('assets/images/serpong/icon/kitchen-01.svg')}}" class="img-icon m-r-5">{{$typeOther[$other->slug]->kitchen}} Kitchen
                        @endif
                    </li>
                    <li>
                        @if($typeOther[$other->slug]->garden)
                        <img src="{{asset('assets/images/serpong/icon/garden-01.svg')}}" class="img-icon m-r-5">{{$typeOther[$other->slug]->garden}} Garden
                        @endif
                    </li>
                    <li>
                        @if($typeOther[$other->slug]->void_area)
                        <img src="{{asset('assets/images/serpong/icon/void-01.svg')}}" class="img-icon m-r-5">{{$typeOther[$other->slug]->void_area}} Void Area
                        @endif
                    </li>
                    <li>
                        @if($typeOther[$other->slug]->terrace)
                        <img src="{{asset('assets/images/serpong/icon/terrace-01.svg')}}" class="img-icon m-r-5">{{$typeOther[$other->slug]->terrace}} Terrace
                        @endif
                    </li>
                    <li>
                        @if($typeOther[$other->slug]->carpot)
                        <img src="{{asset('assets/images/serpong/icon/car-park-01.svg')}}" class="img-icon m-r-5">{{$typeOther[$other->slug]->carpot}} Carpot
                        @endif
                    </li>
                    </ul>
                </div>
            </div>
        </div>
        <?php $key++; ?>
        @endif
        @endif
        @endforeach
        @endif
    </div>
</section>

<!-- Siteplan -->
<section class="p-t-50 p-b-40">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-left">
                <h2 class="font-green m-b-20">Siteplan</h2>
                <img src="{{IMAGE_URL.'project/'.$project->image_2}}" class="img-responsive" alt="@if($project->meta_name) {{$project->meta_name}} @else {{$project->name}} @endif" />
            </div>
        </div>
    </div>
</section>
<!-- Siteplan -->

@if($projectarticle360)
<section class="text-left p-t-20 p-b-40">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-left">
                <h2 class="font-green m-b-10">3D Virtual Tour</h2>
                @foreach($projectarticle360 as $article360)
                <a href="{{URL::to('virtual-tour/'.$article360->slug)}}" target="_blank"> <h3 class="font-italic m-b-20 font-weight-400">{{$article360->name}}</h3></a>
                <iframe width="100%" height="400px" src="{{ $article360->url }}" class="margin-two-bottom" style="border:0 !important;"></iframe>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endif

@if($project->video)
<section class="p-t-20 p-b-40">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <iframe width="100%" height="420" src="https://www.youtube.com/embed/{{ $project->video }}" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>
@endif
@if($project->video_2)
<section class="p-t-20 p-b-40">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <iframe width="100%" height="420" src="https://www.youtube.com/embed/{{ $project->video_2 }}" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>
@endif

<section class="call-to-action background-call-to-action">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 text-center">
                @foreach($others as $other)
                @if($other->files!='')
                <h3 class="text-light m-t-0"><strong>Get Brochure</strong></h3>
                @break
                @endif
                @endforeach
            </div>
            <div class="col-lg-6 text-left">
                @foreach($others as $other)
                @if($other->files!='')                
                <!-- <a href="{{ FILE_URL.$other->files  }}" class="btn btn-rounded btn-reveal btn-light-custom m-t-0"><span>@if($other->name=='E-Brochure')download pdf @else download pdf {{$other->name}} @endif</span><i class="fa fa-download"></i></a>                 -->
                
                <button type="button" class="a-brochure btn btn-rounded btn-reveal btn-light-custom m-t-0" data-toggle="modal" data-target="#modalBrochure" data-aid="{{$other->article_id}}" data-fil="{{FILE_URL.$other->files}}" data-bro="{{$other->name}}"><span>@if($other->name=='E-Brochure')download pdf @else download pdf {{$other->name}} @endif</span><i class="fa fa-download"></i></button>
                @endif
                @endforeach
            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="modalBrochure" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Form Get Brochure</h4>
            </div>
            <div class="modal-body text-center">
                <form method="post" action="{{URL::to('project/get-brochure/'.$project->slug.'/'.$project->project_id)}}" id="form-brochure">
                    <input type="hidden" value="{{csrf_token()}}" name="_token">
                    <input type="hidden" value="{{$project->name}}" name="project" id="brochure-project-name">
                    <input type="hidden" value="brochure" name="type">
                    <input type="hidden" name="article_id" class="article_id">
                    <input type="hidden" name="file" class="file">
                    <input type="hidden" name="projectName" class="projectName">
                    
                    <div class="row">
                        <div class="form-group col-md-12">                            
                            <input class="form-control form-control-us char-only" minlength="3" maxlength="50" id="name-brochure" type="text" class="validate" placeholder="Your Name" name="name" required><br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">                            
                        <input class="form-control form-control-us" minlength="3" maxlength="50" id="email-brochure" type="text" class="validate" placeholder="Your Email" name="email" required><br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">                            
                        <input class="form-control form-control-us number-only" minlength="10" maxlength="15" id="phone-brochure" type="text" class="validate" placeholder="Your Phone" name="phone" required>
                        </div>
                    </div>
                    <div class="form-group text-center">
                            {!!Recaptcha::render()!!}
                        </div>

                    <input type="hidden" name="utm_source" id="utm_source" value={{ app('request')->input('utm_source') }}>
                    <input type="hidden" name="utm_medium" id="utm_medium" value={{ app('request')->input('utm_medium') }}>
                    <input type="hidden" name="utm_campaign" id="utm_campaign" value={{ app('request')->input('utm_campaign') }}>
                    <span class="required text-left">*Please complete all fields correctly</span>
                    
                    <div class="modal-footer text-center">
                    <div class="form-status"></div>
                    <button class="btn btn-gold-custom btn-radius no-margin-bottom " type="submit">Submit</button><br>
                </form>
            </div>
        </div>
        </div>
    </div>
</div>
</div>

@if($project->latitude)
<div class="mapouter">
    <div class="gmap_canvas">
    <div id="map-canvas" style="width:100%;height:500px;" data-latitude="{{$project->latitude}}" data-longitude="{{$project->longitude}}" data-title="{{$project->name}}"></div>
    <!-- <iframe width="100%" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=summarecon%20serpong&t=&z=9&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div> -->
</div>
@endif
@stop

@section('js')
<script type="text/javascript">
$('#modalBrochure').on('shown.bs.modal', function () {
        $(".modal-backdrop.in").hide();
    })

    $('.a-type').click(function() {
        var menuSlug = $(this).data('slug');
        $('.list-type').each(function(i, obj) {
            if($(this).data('slug')==menuSlug) {
                $(this).show()
                $(this).find('.flickity-viewport').css('height','443.594px')
            } else {
                $(this).hide()
            }
        });
    })

    $(".arrange-view").trigger('click');
    $('.a-brochure').click(function() {
        var aid = $(this).data('aid');
        var fil = $(this).data('fil');
        var bro = $(this).data('bro');

        $('#form-brochure').find('.article_id').val(aid);
        $('#form-brochure').find('.file').val(fil);
        $('#form-brochure').find('.projectName').val(bro);
    })

    $("#form-brochure").submit(function(e) {
        e.preventDefault();
        // var instance = M.Modal.getInstance(elem);
        var url = $(this).attr('action');
        var data = $(this).serializeArray();
        $.ajax({
            url: url,
            type: 'post',
            data: data,
            beforeSend: function() {
                var status = $(".form-status");
                status.append('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn();
            },
            success: function(text) {
                var brochureProject = $('#brochure-project-name');
                if (text.response == 'success') {
                    window.location.href = "{{URL::to('download')}}" + '/' + text.proname + '/' + text.articleid;
                    // window.open(
                        // text.file,
                        // '_blank' // <- This is what makes it open in a new window.
                        // );
                    // window.location.href = text.file;
                    swal({
                        title: 'Terima Kasih',
                        html: text.message,
                        type: 'success'
                    });
                    $('#name-brochure').val('');
                    $('#email-brochure').val('');
                    $('#phone-brochure').val('');
                    var status = $(".form-status");
                    status.html('');
                    
                        // window.location.href = text.file;
                } else {
                    swal({
                        title: 'Error on Connection',
                        html: text.message,
                        type: 'error'
                    });
                    
                }
                // $('#modalBrochure').leanModal();
                // $('.modal').modal('hide');
                // $('.btn-close').trigger();
            },
            error: function() {
                swal({
                    title: 'Error',
                    text: 'Error!!',
                    type: 'error'
                });
                var status = $(".form-status");
                status.html('');
            }
        });
    });
</script>
@stop
@extends('layout.layout')
@section('content')
<div id="slider" class="inspiro-slider slider-halfscreen padding-slider dots-creative d-none d-sm-block d-sm-none d-md-block" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    <div class="slide background-image" data-bg-image="{{IMAGE_URL.$banners->image_1}}">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class=" text-center text-light">
                <h2 class="text-medium text-light font-weight-700 font-italic">Press Release</h2>
                <p class="lead text-light font-weight-600">{{$banners->brief}}</p>
            </div>
        </div>
    </div>
</div>

<section id="page-title" class="text-light d-md-none d-lg-none d-xl-none" data-bg-parallax="{{IMAGE_URL.$banners->image_1}}">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title text-center">
            <h2 class="text-medium text-light font-weight-700 font-italic">Press Release</h2>
            <p class="lead text-light font-weight-600">{{$banners->brief}}</p>
        </div>
    </div>
</section>

<!-- PRESS RELEASE-->
<section class="p-t-60 p-b-0 p-b-0-m p-t-40-m">
    <div class="container">
        <div class="row"> 
            @foreach($articles as $article)                       
            <div class=" col-lg-4 col-12">
                <div class="icon-box effect medium clean">
                    <div class="icon"> 
                        <a href="{{ FILE_URL.$article->files  }}" target="_blank"><i class="fa fa-file-pdf"></i></a> 
                    </div>
                    <h3><a href="{{ FILE_URL.$article->files  }}" target="_blank" style="color:#000;">{{$article->name}}</a></h3>
                </div>      
            </div>
            @endforeach  
        </div>
    </div>
</section>
<!-- END : PRESS RELEASE-->
@stop


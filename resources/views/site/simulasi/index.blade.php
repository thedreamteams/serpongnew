@extends('layout.layout')
@section('content')
<div id="slider" class="inspiro-slider slider-halfscreen padding-slider dots-creative d-none d-sm-block d-sm-none d-md-block" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    @foreach($banners as $banner)
    <div class="slide background-image" data-bg-image="{{IMAGE_URL.$banner->image_1}}">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class=" text-center text-light">
                <h2 class="text-medium text-light font-weight-700 font-italic">Hitung KPR</h2>
                <p class="lead text-light font-weight-600">Wonderful Living at Summarecon Serpong</p>
            </div>
        </div>
    </div>
    @endforeach
</div>

@foreach($banners as $banner)
<section id="page-title" class="text-light d-md-none d-lg-none d-xl-none" data-bg-parallax="{{IMAGE_URL.$banner->image_1}}">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title text-center">
            <h2 class="text-medium text-light font-weight-700 font-italic">Hitung KPR</h2>
            <p class="lead text-light font-weight-600">Wonderful Living at Summarecon Serpong</p>
        </div>
    </div>
</section>
@endforeach

<section class="p-t-40 p-b-40">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h3 class="font-green">Simulasi KPR - Loan Calculator</h3>
                <!-- <p class="text-muted m-b-30">You will receive an email notification to confirm this action inorder to completed changes.</p> -->

                <form id="form-kpr" class="form-validate" novalidate="novalidate">

                    <div class="form-row m-b-30">
                        <div class="form-group col-md-6">
                            <label class="font-green">Harga Property</label>
                            <input type="text" class="form-control money input-kpr" id="harga" value="400000000">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-green">Jangka Waktu</label>
                            <select class="form-control input-kpr" id="periode">
                                @for ($i=1; $i<=30; $i++)
                                <option value="{{$i}}" {{ ($i==15)?'selected':'' }}>{{$i}} Tahun</option>
                                @endfor
                            </select>
                        </div>
                    </div>

                    <div class="form-row m-b-30">
                        <div class="form-group col-md-6">
                            <label class="font-green">Uang Muka / DP 20%</label>
                            <input type="text" class="form-control money" id="dp" readonly="">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-green">Estimasti Suku Bunga / Tahun</label>
                            <input type="text" class="form-control input-kpr percent" id="suku-bunga" value="7.70">
                        </div>
                    </div>
                </form>

                <div class="card-header">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="font-green">Angsuran Perbulan</label>
                            <label class="text-dark font-weight-600 total-angsuran">Rp 3.456.897</label>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="font-green">Jumlah Pinjaman</label>
                            <label class="text-dark font-weight-600 total-pinjaman">Rp 200.000.000</label>
                        </div>
                    </div>
                </div>

                <p class="text-muted m-t-20 font-italic">Simulasi di atas hanya perhitungan dengan suku bunga tetap (fixed)</p>

            </div>

            <div class="col-md-3 offset-md-1 d-none d-sm-block d-sm-none d-md-block">
                <h3 class="font-gold text-uppercase m-b-30 m-t-30-m m-b-10-m">Recent Project</h3>
                <div class="carousel team-members text-left-m" data-items="1" data-dots="false">
                @foreach($projects as $project)
                    <div class="team-member text-left-m m-b-0">
                        <a href="{{URL::to('projects/'.$project->category_slug.'/'.$project->slug)}}"><img src="{{IMAGE_URL.'project/'.$project->image_1}}" alt="{{$project->meta_name}}"></a>

                        <div class="team-desc p-t-20">
                            <h3><a href="{{URL::to('projects/'.$project->category_slug.'/'.$project->slug)}}" class="font-green">{{$project->name}}</a></h3>
                            <span>{{trim($project->category)}}@if($project->area) | {{$project->area}}@endif</span>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="text-center text-left-m">
                    <a href="{{URL::to('projects/rumah')}}" class="btn btn-outline-green btn-outline btn-rounded ">See All Project</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="p-t-0 p-b-40 p-t-0-m">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12">
                <h4 class="font-green m-b-30">Informasi Penting Tentang KPR</h4>
                <div class="tabs tabs-folder d-none d-sm-block d-sm-none d-md-block">
                    <ul class="nav nav-tabs nav-simulasi" id="myTab3" role="tablist">
                        @php $no=1; @endphp
                        @foreach($articles as $tips)
                        <li class="nav-item">
                            <a class="nav-link @if($no==1) active @endif" id="{{$tips->slug}}-tab" data-toggle="tab" href="#{{$tips->slug}}3" role="tab" aria-controls="{{$tips->slug}}" aria-selected="@if($no==1) true @else false  @endif">{{$tips->name}}</a>
                        </li>
                        @php $no++; @endphp
                        @endforeach
                    </ul>
                    <div class="tab-content" id="myTabContent3">
                        @php $ang=1; @endphp
                        @foreach($articles as $tips)
                        <div class="@if($ang==1) tab-pane fade show active @else tab-pane fade @endif" id="{{$tips->slug}}3" role="tabpanel" aria-labelledby="{{$tips->slug}}-tab">
                        {!!$tips->contents!!}
                        </div>
                        @php $ang++; @endphp
                        @endforeach
                    </div>
                </div>

                <div class="accordion accordion-shadow d-md-none d-lg-none d-xl-none">
                    @php $no=1; @endphp
                    @foreach($articles as $tips)
                    <div class="ac-item">
                        <h5 class="ac-title">{{$tips->name}}</h5>
                        @php $ang=1; @endphp
                        @foreach($articles as $tips)
                        <div class="ac-content">
                            {!!$tips->contents!!}
                        </div>
                        @php $ang++; @endphp
                        @endforeach
                    </div>
                    @php $no++; @endphp
                    @endforeach
                </div>
            </div>
            <div class="col-12 d-md-none d-lg-none d-xl-none">
                <h3 class="font-gold text-uppercase m-b-30 m-t-30-m m-b-10-m">Recent Project</h3>
                <div class="carousel team-members text-left-m" data-items="1" data-dots="false">
                @foreach($projects as $project)
                    <div class="team-member text-left-m m-b-0">
                        <div>
                        <img src="{{IMAGE_URL.'project/'.$project->image_1}}" alt="{{$project->meta_name}}">
                        </div>
                        <div class="team-desc p-t-20">
                        <h3><a href="{{URL::to('projects/'.$project->category_slug.'/'.$project->slug)}}" class="font-green">{{$project->name}}</a></h3>
                            <span>{{trim($project->category)}}@if($project->area) | {{$project->area}}@endif</span>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="text-center text-left-m">
                    <a href="{{URL::to('projects/rumah')}}" class="btn btn-outline-green btn-outline btn-rounded ">See All Project</a>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('css')
<style>
input:not([type="checkbox"]):not([type="radio"]), select {
    padding: 10px 16px !important;
}
</style>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
<script>
    $(function(){
        $('.money').mask('000.000.000.000.000', {reverse: true});
        $('.percent').mask('0#.00%', {reverse: true});
        hitungKpr()
        $('.input-kpr').change(function(){
            hitungKpr()
        })
        $('.input-kpr').keyup(function(){
            hitungKpr()
        })
    })
    function hitungKpr(){
        // Validasi Percentage
        if($('#suku-bunga').val()=='' || $('#suku-bunga').val()=='0') {
            $('.total-angsuran').html( 'Rp 0')
            return;
        }

        let harga=removeCurrency($('#harga').val())
        let periode=parseFloat($('#periode').val())
        let bunga=parseFloat(removePercentage($('#suku-bunga').val()))

        let totalDp = harga*0.2
        let totalPinjam = harga*0.8
        $('#dp').val( formatNumber(totalDp) )
        $('.total-pinjaman').html( 'Rp '+formatNumber(totalPinjam) )

        let totalBulan = periode*12
        let cicilanPokok = Math.round(totalPinjam*(bunga/100)/12)
        let bungaPerBulan = 1-( (1+(bunga/100)/12) ** (-1*totalBulan))
        totalAngsuran = Math.round(cicilanPokok/bungaPerBulan)
        $('.total-angsuran').html( 'Rp '+formatNumber(totalAngsuran) )
        console.log(bungaPerBulan)
    }
    function removeCurrency(str) {
        return parseFloat(str.replace(/\./g, ''));
    }
    function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    }
    function removePercentage(str) {
        return parseFloat(str.replace(/\,/g, '.'));
    }
</script>
@endsection

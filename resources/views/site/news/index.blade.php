@extends('layout.layout')
@section('content')
<div id="slider" class="inspiro-slider slider-halfscreen padding-slider dots-creative d-none d-sm-block d-sm-none d-md-block" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    <div class="slide background-image" data-bg-image="{{IMAGE_URL.$banners->image_1}}">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class=" text-center text-light">
                <h2 class="text-medium text-light font-weight-700 font-italic">{{$banners->name}}</h2>
                <p class="lead text-light font-weight-600">{{$banners->brief}}</p>
            </div>
        </div>
    </div>
</div>

<section id="page-title" class="text-light d-md-none d-lg-none d-xl-none" data-bg-parallax="{{IMAGE_URL.$banners->image_1}}">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title text-center">
            <h2 class="text-medium text-light font-weight-700 font-italic">{{$banners->name}}</h2>
            <p class="lead text-light font-weight-600">{{$banners->brief}}</p>
        </div>
    </div>
</section>

<section class="p-t-0 p-b-0 background-grey">
    <div class="container-fullscreen">
        <div class="row">
            <div class="col-lg-12 no-padding">
                <div class="carousel arrows-visibile testimonial testimonial-single testimonial-left" data-items="1" data-animate-in="fadeIn" data-animate-out="fadeOut" data-arrows="false">
                    <div class="testimonial-item p-0 m-b-0">
                        <div class="row">
                            <div class="col-12 no-padding d-md-none d-lg-none d-xl-none">
                                <img src="{{IMAGE_URL.$hot->thumb}}" class="img-responsive">
                            </div>
                            <div class="col-lg-6 text-center d-none d-sm-block d-sm-none d-md-block" style="height:auto;background:url({{IMAGE_URL.$hot->thumb}}) 50% 50% / cover no-repeat;">
                            </div>
                            <div class="col-lg-6 text-left">
                                <div class="p-30 p-b-40 p-t-20-m">
                                    <h5 class="m-b-15 font-gold font-italic">{{$hot->category}}</h5>
                                    <a href="{{URL::to('whats-on/'.$hot->category_slug.'/'.$hot->slug)}}">
                                        <h3 class="font-green">{{$hot->name}}</h3>
                                    </a>
                                    <div class="heading-text heading-line">
                                        <h5 class="m-b-0 font-grey">{{date('F d, Y',strtotime($hot->start_date))}}</h5>
                                    </div>
                                    <p class="p-t-20">{{$hot->brief}}</p>
                                    <a href="{{URL::to('whats-on/'.$hot->category_slug.'/'.$hot->slug)}}" class="m-t-20 btn btn-dark btn-outline btn-rounded btn-outline-green">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- <section class="p-b-50">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="carousel arrows-visibile testimonial testimonial-single testimonial-left" data-items="1" data-animate-in="fadeIn" data-animate-out="fadeOut" data-arrows="false">
                    <div class="testimonial-item p-0 m-b-0">
                        <div class="row">
                            <div class="col-lg-5 text-center">
                                <img src="{{IMAGE_URL.$hot->thumb}}" alt="@if($hot->meta_name) {{$hot->meta_name}} @else Summarecon Serpong - {{$hot->name}} @endif" class="img-fluid rounded-circle img-450">
                            </div>
                            <div class="col-lg-7 text-left m-t-0">
                                <h5 class="m-b-15 font-gold font-italic">{{$hot->category_slug}}</h5>
                                <h3 class="font-green">{{$hot->name}}</h3>
                                <div class="heading-text heading-line">
                                    <h5 class="m-b-0 font-grey">{{date('F d, Y',strtotime($hot->start_date))}}</h5>
                                </div>
                                <p class="p-t-20">{{$hot->brief}}</p>
                                <a href="{{URL::to('whats-on/'.$hot->category_slug.'/'.$hot->slug)}}" class="m-t-20 btn btn-dark btn-outline btn-rounded btn-outline-green">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->

<section class="p-t-0 p-b-60">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="font-green m-t-40 m-b-20 text-center">Articles</h3>
                <div class="tabs tabs-custom tabs-sales text-center">
                    <nav class="nav nav-tabs display-flex">
                        <a class="nav-item nav-link @if(Request::is('whats-on/news*')) active @endif" href="{{URL::to('whats-on/news')}}">News & Update</a>
                        <a class="nav-item nav-link @if(Request::is('whats-on/information*')) active @endif" href="{{URL::to('whats-on/information')}}">Information & Tips</a>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="page-content" class="p-t-0 p-b-0-m">
    <div class="container">
        <div id="blog" class="grid-layout post-3-columns post-news m-b-30 grid-loaded" data-item="post-item">
            @foreach($articlesNew->data as $article)
            <div class="post-item">
                <div class="post-item-wrap">
                    <div class="post-image">
                        <a href="{{URL::to('whats-on/'.$article->category_slug.'/'.$article->slug)}}">
                            <img alt="@if($article->meta_name) {{$article->meta_name}} @else Summarecon Serpong - {{$article->name}} @endif" src="{{IMAGE_URL.$article->thumb}}">
                        </a>
                    </div>
                    <div class="post-item-description post-mobile-m p-0 p-t-20">
                        <a href="{{URL::to('whats-on/'.$article->category_slug.'/'.$article->slug)}}">
                            <h4 class="font-green">{{$article->name}}</h4>
                        </a>
                        <span class="post-meta-date font-italic font-gold">{{date('F d, Y',strtotime($article->start_date))}}</span>
                        <p class="d-none d-sm-block d-sm-none d-md-block">{{$article->brief}}</p>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- <div class="grid-loader"></div> -->
        </div>
        <!-- <div id="pagination" class="infinite-scroll">
            <a href="{{$nextPage}}">
                <span><i class="fa fa-chevron-right"></i></span>
            </a>
        </div> -->
        <!-- <div id="showMore">
            <a href="#" class="btn btn-rounded btn-light"><i class="icon-refresh-cw"></i> Load More Posts</a>
        </div> -->
    </div>
</section>

<section class="p-t-0 p-b-0" data-bg-parallax="{{asset('assets/images/serpong/BANNER-INGIN-PUNYA-PROPERTY-DI-SS-1280x768px.jpg')}}">
    <div class="container text-center text-light">
        <div class="row">
            <div class="col-md-6 display-center p-60">
                <div class="bg-overlay bg-overlay-custom"></div>
                <div class="heading-text heading-line m-b-50 text-center text-light">
                    <h4>Wujudkan mimpi memiliki properti di Summarecon Serpong</h4>
                </div>
                <p class="lead m-b-20 text-light font-weight-800">Saatnya bergabung menjadi keluarga besar Summarecon Serpong dan wujudkan kehidupan dan masa depan yang lebih baik karena di sini Anda akan mendapatkan hunian dengan balutan konsep terbaik dan lingkungan hijau yang tepat untuk kenyamanan Anda dan keluarga. Amankan investasi masa depan Anda sekarang juga.</p>
                <a href="{{URL::to('projects/rumah')}}" class="btn btn-light btn-outline btn-rounded text-uppercase btn-block">Lihat Product</a>
            </div>
        </div>
    </div>
</section>
@stop
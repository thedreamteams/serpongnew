@extends('layout.layout')
@section('content')
<div id="slider" class="inspiro-slider slider-halfscreen padding-slider dots-creative d-none d-sm-block d-sm-none d-md-block" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    <div class="slide background-image" data-bg-image="{{IMAGE_URL.$banners->image_1}}">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class=" text-center text-light">
                <h2 class="text-medium text-light font-weight-700 font-italic">{{$banners->name}}</h2>
                <p class="lead text-light font-weight-600">{{$banners->brief}}</p>
            </div>
        </div>
    </div>
</div>

<section id="page-title" class="text-light d-md-none d-lg-none d-xl-none" data-bg-parallax="{{IMAGE_URL.$banners->image_1}}">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title text-center">
            <h2 class="text-medium text-light font-weight-700 font-italic">{{$banners->name}}</h2>
            <p class="lead text-light font-weight-600">{{$banners->brief}}</p>
        </div>
    </div>
</section>

<section id="page-content" class="sidebar-right p-t-20-m">
    <div class="container">
        <div class="row">
            <div class="content col-lg-9">
                <div id="blog" class="single-post">
                    <div class="post-item p-b-0-m">
                        <div class="post-item-wrap">
                            <h5 class="m-b-15 font-gold font-italic">{{$articleCat->category}}</h5>
                            <h3 class="font-green">{{$page->name}}</h3>
                            <h5 class="m-b-0 font-grey">{{date('d F Y',strtotime($page->start_date))}}</h5>
                            <div class="post-item-description p-b-0-m">
                                {!! $page->contents !!}
                            </div>
                            @if(isset($gallery))
                            <div class="carousel arrows-visible" data-items="1" data-dots="false" data-loop="true" data-autoplay="true">
                                @foreach($gallery as $img)
                                <div class="item"><img src="{{$img}}" alt="" /></div>
                                @endforeach
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="sidebar sticky-sidebar col-lg-3">
                <h3 class="font-gold m-b-20-m">Other Articles</h3>
                <div class="widget">
                    <div class="post-thumbnail-list">
                        @foreach($others->data as $other) 
                        @if($other->slug!=collect(request()->segments())->last())
                        <div class="post-thumbnail-entry">
                            <img src="{{IMAGE_URL.$other->thumb}}" alt="@if($other->meta_name) {{$other->meta_name}} @else Summarecon Serpong - {{$other->name}} @endif">
                            <div class="post-thumbnail-content">
                                <a href="{{URL::to('whats-on/'.$other->category_slug.'/'.$other->slug)}}">{{$other->name}}</a>
                                <span class="post-date">{{date('F d, Y',strtotime($other->start_date))}}</span>
                            </div>
                        </div>
                        @endif
                        @endforeach 
                    </div>
                    <!-- <div class="carousel team-members" data-items="1" data-dots="false">
                        @foreach($others->data as $other) 
                        @if($other->slug!=collect(request()->segments())->last())
                        <div class="team-member m-b-0">
                            <div>
                            <img src="{{IMAGE_URL.$other->thumb}}" alt="@if($other->meta_name) {{$other->meta_name}} @else Summarecon Serpong - {{$other->name}} @endif">
                            </div>
                            <div class="team-desc">
                                <h3><a href="{{URL::to('whats-on/'.$other->category_slug.'/'.$other->slug)}}" class="font-green">{{$other->name}}</a></h3>
                                <span>{{date('F d, Y',strtotime($other->start_date))}}</span>
                            </div>
                        </div>
                        @endif
                        @endforeach   
                    </div> -->
                    <div class="text-center text-left-m">
                        <a href="{{URL::to('whats-on/news')}}" class="btn btn-outline-green btn-outline btn-rounded m-t-30-m m-t-30">See All News</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
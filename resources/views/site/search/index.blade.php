@extends('layout.layout')
@section('content')

<!-- BAR SUMMA-->
<div class="bar-slider" style="background-image: url({{asset('assets/homepages/fashion/slider/bar-page.jpg')}})"></div>

<!-- TITLE -->
<section class="p-b-0 p-t-40">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <div class="font-line m-b-50">
                    <h3 class="text-bold text-uppercase">SEARCH</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END : TITLE -->

<section class="call-to-action background-call-to-action">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
            <form action="{{URL::to('search/action')}}" method="get">
                    <div class="form-group row m-t-20">                    
                        <div class="col-10">                        
                            <input class="form-control form-custom" type="text" name="search" value="" id="" placeholder="Cari properti di Summarecon Serpong &quot;Enter&quot;">                            
                        </div>
                        <div class="col-2">
                            <button type="submit" class="btn btn-block btn-rounded form-control">Search Now</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- SEARCH -->
<section class="p-t-0 p-b-0">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="seperator dotted"><i class="fa fa-search fa-5x"></i></div>
            </div>
        </div>
        <div class="row">
        @if(count($projects)>0)
            <div class="col-md-6">
                <h3 class="text-uppercase font-search">Products</h3>
                @foreach($projects as $project)
                <div class="post-item m-b-20">
                    <div class="post-content-details">
                        <div class="post-title"><h4 class="m-b-0">{{$project->name}}</h4></div>
                    </div>
                    <div class="post-description">
                        <p class="m-b-15">{{str_limit($project->description, $limit = 170, $end = '...')}}</p>
                    </div>
                    <div class="post-read-more">
                        <a class="read-more" href="{{URL::to('projects/'.$project->category_slug.'/'.$project->slug)}}">Read More <i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
                @endforeach
            </div>
            @endif

            @if(count($articlesNew)>0 || count($articles)>0)
            <div class="col-md-6">
                <h3 class="text-uppercase font-search">Articles</h3>
                @if(count($articlesNew)>0)
                @foreach($articlesNew as $article)
                @if($article->parent_category_slug=='leisure' || $article->parent_category_slug=='explore' ||$article->parent_category_slug=='whats-on')
                <div class="post-item m-b-20">
                    <div class="post-content-details">                        
                        <div class="post-title"><h4 class="m-b-0">{{$article->name}}</h4></div>
                        <div class="post-info">{{date('F d, Y', strtotime($article->start_date))}}</div>                        
                    </div>
                    <div class="post-description">
                        @if($article->parent_category_slug=='whats-on')
                        <p class="m-b-15">{{str_limit($article->brief, $limit = 170, $end = '...')}}</p>
                        @endif
                        <div class="post-read-more">
                            @if($article->parent_category_slug=='leisure')
                            <a class="read-more" href="{{URL::to('explore-detail')}}" target="_blank">Go To Explore  <i class="fa fa-long-arrow-right"></i></a>
                            @elseif($article->parent_category_slug=='explore')
                            <a class="read-more" href="{{URL::to('explore-detail/'.$article->category_slug)}}" target="_blank">Go To Explore  <i class="fa fa-long-arrow-right"></i></a>
                            @elseif($article->parent_category_slug=='whats-on')
                                @if($article->category_slug=='news')
                                <a class="read-more" href="{{URL::to('whats-on/'.$article->category_slug.'/'.$article->slug)}}">Read More  <i class="fa fa-long-arrow-right"></i></a>
                                @elseif($article->category_slug=='information')
                                <a class="read-more" href="{{URL::to('whats-on/'.$article->category_slug.'/'.$article->slug)}}">Read More  <i class="fa fa-long-arrow-right"></i></a>
                                @else
                                <a class="read-more" href="">Read More  <i class="fa fa-long-arrow-right"></i></a>
                                @endif                            
                            @else
                            <!-- <a class="read-more" href="{{URL::to(strtolower($article->slug))}}">Read More  <i class="fa fa-long-arrow-right"></i></a> -->
                            @endif
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
            @endif
            @if(count($articles)>0)
                @foreach($articles as $article)
                @if($article->parent_category_slug=='whats-on')
                <div class="post-item m-b-20">
                    <div class="post-content-details">
                        <div class="post-title"><h4 class="m-b-0">{{$article->name}}</h4></div>
                        <div class="post-info">{{date('F d, Y', strtotime($article->start_date))}}</div>
                    </div>
                    <div class="post-description">
                        <p class="m-b-15">{{str_limit($article->brief, $limit = 170, $end = '...')}}</p>
                        <div class="post-read-more">
                                <a class="read-more" href="{{URL::to('whats-on/'.$article->category_slug.'/'.$article->slug)}}">Read More  <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
                @endif
            </div>
            @endif

            
        </div>
    </div>
</section>
<!-- END : SEARCH -->
@stop
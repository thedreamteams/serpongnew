@extends('layout.layout')
@section('content')

<div id="slider" class="inspiro-slider slider-halfscreen padding-slider dots-creative d-none d-sm-block d-sm-none d-md-block" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    @foreach($banners as $banner)
    <div class="slide background-image" data-bg-image="{{IMAGE_URL.$banner->image_1}}">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class=" text-center text-light">
                <h2 class="text-medium text-light font-weight-700 font-italic">{{$page->name}}</h2>

            </div>
        </div>
    </div>
    @endforeach
</div>

@foreach($banners as $banner)
<section id="page-title" class="text-light d-md-none d-lg-none d-xl-none" data-bg-parallax="{{IMAGE_URL.$banner->image_1}}">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title text-center">
            <h2 class="text-medium text-light font-weight-700 font-italic"> {{$page->name}} </h2>
        </div>
    </div>
</section>
@endforeach

<!-- VIRTUAL -->
<section class="p-b-30 p-t-30 p-b-10-m">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center m-b-30">
                <iframe width="100%" height="600px" src="{{$page->url}}" style="border:0 !important;"></iframe>
            </div>
        </div>
    </div>
</section>
@stop
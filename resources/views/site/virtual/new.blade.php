@extends('layout.layout')
@section('content')
<div id="slider" class="inspiro-slider slider-halfscreen padding-slider dots-creative d-none d-sm-block d-sm-none d-md-block" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    @foreach($banners as $banner)
    <div class="slide background-image" data-bg-image="{{IMAGE_URL.$banner->image_1}}">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class=" text-center text-light">
                <h2 class="text-medium text-light font-weight-700 font-italic">{{$banner->name}}</h2>
                <p class="lead text-light font-weight-600">{{$banner->brief}}</p>
            </div>
        </div>
    </div>
    @endforeach
</div>

@foreach($banners as $banner)
<section id="page-title" class="text-light d-md-none d-lg-none d-xl-none" data-bg-parallax="{{IMAGE_URL.$banner->image_1}}">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title text-center">
            <h2 class="text-medium text-light font-weight-700 font-italic">{{$banner->name}}</h2>
            <p class="lead text-light font-weight-600">{{$banner->brief}}</p>
        </div>
    </div>
</section>
@endforeach





<section class="p-t-40 p-b-20 project-sale">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div id="portfolio" class="grid-layout portfolio-3-columns" data-margin="20">

                    @foreach($articles360 as $article)
                    <div class="portfolio-item overlay-dark item-project">
                        <a href="{{URL::to('virtual-tour/'.$article->slug)}}">
                            <div class="portfolio-item-wrap">
                                <div class="portfolio-image">
                                    <a href="{{URL::to('virtual-tour/'.$article->slug)}}">
                                        <img alt="" src="{{IMAGE_URL.$article->thumb}}">
                                    </a>
                                </div>
                                <div class="portfolio-description">
                                    <a href="{{URL::to('virtual-tour/'.$article->slug)}}">
                                        <h3>View</h3>
                                    </a>
                                </div>
                            </div>
                            <div class="product-description btn-green-custom p-10 p-b-15 m-b-35">
                                <div class="product-title text-center" style="height: 45px;">
                                    <h3 class="text-light font-weight-400"><a href="{{URL::to('virtual-tour/'.$article->slug)}}" class="text-light">{{$article->name}}</a></h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>

            </div>
        </div>
    </div>
</section>



<section id="parallax-home" class="p-t-50 p-b-50" data-bg-parallax="{{asset('assets/images/serpong/BANNER-INGIN-PUNYA-PROPERTY-DI-SS-1280x768px.jpg')}}">
    <div class="bg-overlay bg-overlay-home"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 text-light text-center">
                <div class="heading-project heading-text heading-section text-center">
                    <h2 class="text-small font-weight-600 m-b-80 m-b-50-m">Wujudkan mimpi memiliki properti di Summarecon Serpong</h2>
                </div>
                <p>Saatnya bergabung menjadi keluarga besar Summarecon Serpong dan wujudkan kehidupan dan masa depan yang lebih baik karena di sini Anda akan mendapatkan hunian dengan balutan konsep terbaik dan lingkungan hijau yang tepat untuk kenyamanan Anda dan keluarga. Amankan investasi masa depan Anda sekarang juga.</p>
            </div>
            <div class="col-lg-5 offset-lg-1">
                <form id="form2" class="widget-contact-form" novalidate="novalidate" action="{{URL::to('contact/action')}}" method="post">
                    <input name="_token" value="{{csrf_token()}}" type="hidden">
                    <input type="hidden" name="subject" value="about">
                    <input type="hidden" name="message" value="about Summarecon Serpong">
                    <div class="form-group m-b-20">
                        <input type="text" name="name" class="form-control form-custom input-f" placeholder="Nama" required="">
                    </div>
                    <div class="form-group m-b-20">
                        <input type="text" aria-required="true" name="phone" required class="form-control form-custom inout-f required phone number-only" placeholder="Nomor Whatsapp">
                    </div>
                    <div class="form-group">
                        {{-- {!! Recaptcha::render() !!} --}}
                        {!! RecaptchaV3::field('contact') !!}
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-rounded btn-light" id="form-submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@stop

@section('js')
<!-- INFINITE SCROLL -->
<script src="{{asset('assets/plugins/infinite-scroll/infinite-ajax-scroll/jquery-ias.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/infinite-scroll/infinite-ajax-scroll/jquery.infinitescroll.js')}}" defer></script>
<script src="{{asset('assets/js/functions/infinite_scroll.js')}}" defer></script>
@stop

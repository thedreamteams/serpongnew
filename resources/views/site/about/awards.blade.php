@extends('layout.layout')
@section('content')

<div id="slider" class="inspiro-slider slider-halfscreen padding-slider dots-creative d-none d-sm-block d-sm-none d-md-block" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    <div class="slide background-image" data-bg-image="{{IMAGE_URL.$banners->image_1}}">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class=" text-center text-light">
                <h2 class="text-medium text-light font-weight-700 font-italic">Awards</h2>
                <p class="lead text-light font-weight-600">{{$banners->brief}}</p>
            </div>
        </div>
    </div>
</div>

<section id="page-title" class="text-light d-md-none d-lg-none d-xl-none" data-bg-parallax="{{IMAGE_URL.$banners->image_1}}">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title text-center">
            <h2 class="text-medium text-light font-weight-700 font-italic">Awards</h2>
            <p class="lead text-light font-weight-600">{{$banners->brief}}</p>
        </div>
    </div>
</section>

<!-- AWARDS -->
<section id="page-content" class="p-b-0 p-t-0-m">
    <div class="container">
        <div class="row justify-content-center">
            <div class="content col-lg-12">
                <ul class="timeline">
                    <?php $i = 1; ?>
                    @foreach($articles as $year=>$awards)
                    <li class="timeline-item">
                        <div class="timeline-icon">{{$i}}</div>
                        <div class="timeline-item-date">{{$year}}</div>
                        <div class="carousel arrows-visible" data-items="4" data-dots="true" data-loop="false" data-autoplay="false">
                            @foreach($awards as $award)
                            <div class="item">
                                <img src="{{IMAGE_URL.$award->thumb}}" alt="" class="img-responsive">
                                <div class="post-content-details text-center">
                                    <div class="post-title"><h4 class="m-b-0">{{$award->name}}</h4></div>
                                    <div class="post-description"><p class="m-b-10">{!!$award->contents!!}</p></div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </li>
                    @endforeach
                    <?php $i++;?>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- END : AWARDS -->

<section id="parallax-home" class="p-t-50 p-b-50" data-bg-parallax="{{asset('assets/images/serpong/BANNER-INGIN-PUNYA-PROPERTY-DI-SS-1280x768px.jpg')}}">
    <div class="bg-overlay bg-overlay-home"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 text-light text-center">
                <div class="heading-project heading-text heading-section text-center">
                    <h2 class="text-small font-weight-600 m-b-80 m-b-50-m">Wujudkan mimpi memiliki properti di Summarecon Serpong</h2>
                </div>
                <p>Saatnya bergabung menjadi keluarga besar Summarecon Serpong dan wujudkan kehidupan dan masa depan yang lebih baik karena di sini Anda akan mendapatkan hunian dengan balutan konsep terbaik dan lingkungan hijau yang tepat untuk kenyamanan Anda dan keluarga. Amankan investasi masa depan Anda sekarang juga.</p>
            </div>
            <div class="col-lg-5 offset-lg-1">
                <form id="form2" class="widget-contact-form"  novalidate="novalidate" action="{{URL::to('contact/action')}}" method="post">
                <input name="_token" value="{{csrf_token()}}" type="hidden">
            <input type="hidden" name="subject" value="about">
                <input type="hidden" name="message" value="about Summarecon Serpong">
                    <div class="form-group m-b-20">
                        <input type="text" name="name" class="form-control form-custom input-f" placeholder="Nama" required="">
                    </div>
                    <div class="form-group m-b-20">
                    <input type="text" aria-required="true" name="phone" required class="form-control form-custom inout-f required phone number-only" placeholder="Nomor Whatsapp">
                    </div>
                    <div class="form-group">
                        {{-- {!! Recaptcha::render() !!} --}}
                        {!! RecaptchaV3::field('contact') !!}
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-rounded btn-light" id="form-submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- <div class="carousel clients-carousel p-b-20 carousel-awards" data-carousel-col="4" data-carousel-dots="true" data-carousel-center="true" data-carousel-loop="true">
    @foreach($awards as $award)
    <div class="post-item p-b-0 m-b-0">
        <div class="post-image "><img src="{{IMAGE_URL.$award->thumb}}" alt=""> </div>
        <div class="post-content-details text-center">
            <div class="post-title"><h4 class="m-b-0">{{$award->name}}</h4></div>
            <div class="post-description"><p class="m-b-10">{!!$award->contents!!}</p></div>
        </div>
    </div>
    @endforeach
</div> -->
@stop

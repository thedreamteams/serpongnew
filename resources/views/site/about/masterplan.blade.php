@extends('layout.layout')
@section('content')
<div id="slider" class="inspiro-slider slider-halfscreen padding-slider dots-creative d-none d-sm-block d-sm-none d-md-block" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    <div class="slide background-image" data-bg-image="{{IMAGE_URL.$banners->image_1}}">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class=" text-center text-light">
                <h2 class="text-medium text-light font-weight-700 font-italic">Masterplan & Map</h2>
                <p class="lead text-light font-weight-600">{{$banners->brief}}</p>
            </div>
        </div>
    </div>
</div>

<section id="page-title" class="text-light d-md-none d-lg-none d-xl-none" data-bg-parallax="{{IMAGE_URL.$banners->image_1}}">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title text-center">
            <h2 class="text-medium text-light font-weight-700 font-italic">Masterplan & Map</h2>
            <p class="lead text-light font-weight-600">{{$banners->brief}}</p>
        </div>
    </div>
</section>

<section class="parallax text-light p-t-0 p-b-0">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-12">
				<div class="litetooltip-hotspot-wrapper" >
					<div class="litetooltip-hotspot-container" style="padding-bottom: 141.48845166809238%">
						<img src="{{asset('assets/images/serpong/masterplan.jpg')}}" class="img-responsive" />
						<div class="hotspot blink ihotspot" id="ihotspot1" style="top: 9.250302297460701%; left: 49.786142001710864%; width: 2.0530367835757057%; height: 1.4510278113663846%;"></div>

						<div class="hotspot blink ihotspot" id="ihotspot2" style="top: 23.276905396541057%; left: 57.39948674080411%; width: 2.0530367835757057%; height: 1.4510278113663846%;"></div>

						<div class="hotspot blink ihotspot" id="ihotspot3" style="top: 29.020557149866328%; left: 61.42001710863987%; width: 1.9674935842600514%; height: 1.3905683192261185%;"></div>

						<div class="hotspot blink ihotspot" id="ihotspot4" style="top: 32.10399032648126%; left: 65.61163387510693%; width: 2.0530367835757057%; height: 1.4510278113663846%;"></div>

						<div class="hotspot blink ihotspot" id="ihotspot5" style="top: 25.2720677146312%; left: 52.609067579127455%; width: 2.0530367835757057%; height: 1.4510278113663846%;"></div>

						<div class="hotspot blink ihotspot" id="ihotspot6" style="top: 28.355501813784766%; left: 49.35842600513259%; width: 2.0530367835757057%; height: 1.4510278113663846%;"></div>

						<div class="hotspot blink ihotspot" id="ihotspot7" style="top: 26.964933494558647%; left: 44.22583404619333%; width: 2.1385799828913603%; height: 1.5114873035066507%;"></div>

						<div class="hotspot blink ihotspot" id="ihotspot8" style="top: 20.556227327690447%; left: 41.48845166809239%; width: 2.1385799828913603%; height: 1.5114873035066507%"></div>

						<div class="hotspot blink ihotspot" id="ihotspot9" style="top: 26.17896009673519%; left: 29.34131736526946%; width: 2.1385799828913603%; height: 1.5114873035066507%;"></div>

						<div class="hotspot blink ihotspot" id="ihotspot10" style="top: 29.987908101571946%; left: 28.485885372112918%; width: 2.1385799828913603%; height: 1.5114873035066507%;"></div>

						<div class="hotspot blink ihotspot" id="ihotspot11" style="top: 30.53204353083434%; left: 46.27887082976903%; width: 2.1385799828913603%; height: 1.5114873035066507%;"></div>

						<div class="hotspot blink ihotspot" id="ihotspot12" style="top: 35.42926239419589%; left: 48.50299401197605%; width: 2.1385799828913603%; height: 1.5114873035066507%;"></div>

						<div class="hotspot blink ihotspot" id="ihotspot13" style="top: 35.18742442563482%; left: 54.49101796407185%; width: 2.1385799828913603%; height: 1.5114873035066507%;"></div>

						<div class="hotspot blink ihotspot" id="ihotspot14" style="top: 41.051995163240626%; left: 51.32591958939264%; width: 2.0530367835757057%; height: 1.4510278113663846%;"></div>

						<div class="hotspot blink ihotspot" id="ihotspot15" style="top: 42.80532043530834%; left: 63.55859709153122%; width: 2.1385799828913603%; height: 1.5114873035066507%;"></div>

						<div class="hotspot blink ihotspot" id="ihotspot16" style="top: 50%; left: 62.104362703165094%; width: 2.1385799828913603%; height: 1.5114873035066507%;"></div>

						<div class="hotspot blink ihotspot" id="ihotspot17" style="top: 53.869407496977026%; left: 61.84773310521814%; width: 2.1385799828913603%; height: 1.5114873035066507%;"></div>

						<div class="hotspot blink ihotspot" id="ihotspot18" style="top: 45.52599758162032%; left: 15.911035072711718%; width: 2.2241231822070144%; height: 1.5719467956469164%;"></div>

						<div class="hotspot blink ihotspot" id="ihotspot19" style="top: 43.349455864570736%; left: 26.2617621899059%; width: 2.1385799828913603%; height: 1.5114873035066507%;"></div>

						<div class="hotspot blink ihotspot" id="ihotspot20" style="top: 46.67472793228537%; left: 29.255774165953806%; width: 2.0530367835757057%; height: 1.4510278113663846%; "></div>

						<div class="hotspot blink ihotspot" id="ihotspot21" style="top: 43.89359129383313%; left: 30.025662959794698%; width: 2.1385799828913603%; height: 1.5114873035066507%"></div>

						<div class="hotspot blink ihotspot" id="ihotspot22" style="top: 40.145102781136636%; left: 32.76304533789563%; width: 2.0530367835757057%; height: 1.4510278113663846%;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="parallax-home" class="p-t-50 p-b-50" data-bg-parallax="{{asset('assets/images/serpong/BANNER-INGIN-PUNYA-PROPERTY-DI-SS-1280x768px.jpg')}}">
    <div class="bg-overlay bg-overlay-home"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 text-light text-center">
                <div class="heading-project heading-text heading-section text-center">
                    <h2 class="text-small font-weight-600 m-b-80 m-b-50-m">Wujudkan mimpi memiliki properti di Summarecon Serpong</h2>
                </div>
                <p>Saatnya bergabung menjadi keluarga besar Summarecon Serpong dan wujudkan kehidupan dan masa depan yang lebih baik karena di sini Anda akan mendapatkan hunian dengan balutan konsep terbaik dan lingkungan hijau yang tepat untuk kenyamanan Anda dan keluarga. Amankan investasi masa depan Anda sekarang juga.</p>
            </div>
            <div class="col-lg-5 offset-lg-1">
                <form id="form2" class="widget-contact-form"  novalidate="novalidate" action="{{URL::to('contact/action')}}" method="post">
                <input name="_token" value="{{csrf_token()}}" type="hidden">
            <input type="hidden" name="subject" value="about">
                <input type="hidden" name="message" value="about Summarecon Serpong">
                    <div class="form-group m-b-20">
                        <input type="text" name="name" class="form-control form-custom input-f" placeholder="Nama" required="">
                    </div>
                    <div class="form-group m-b-20">
                    <input type="text" aria-required="true" name="phone" required class="form-control form-custom inout-f required phone number-only" placeholder="Nomor Whatsapp">
                    </div>
                    <div class="form-group">
                        {{-- {!! Recaptcha::render() !!} --}}
                        {!! RecaptchaV3::field('contact') !!}
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-rounded btn-light" id="form-submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@stop



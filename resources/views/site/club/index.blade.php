@extends('layout.layout')
@section('content')
<div id="slider" class="inspiro-slider slider-halfscreen padding-slider dots-creative" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    @foreach($banners as $banner)
    <div class="slide background-image" data-bg-image="{{$banner}}">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class=" text-center text-light">
                <h2 class="text-medium text-light font-weight-700 font-italic">Summarecon Serpong Club</h2>
                <p class="lead text-light font-weight-600">Discover the latest news about Summarecon Serpong</p>
            </div>
        </div>
    </div>
    @endforeach
</div>

<section class="p-t-20 p-b-40">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div data-animate="fadeInUp" data-animate-delay="0" class="animated fadeInUp visible p-b-40">
                    <h3 class="font-green m-t-40 m-b-20">Persembahan Bagi Konsumen</h3>
                    <p>Lorem ipsum dolor sit amet, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis aliquam. Lorem ipsum dolor sit amet, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis aliquam. Lorem ipsum dolor sit amet, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis aliquam. Lorem ipsum dolor sit amet, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis aliquam.</p>
                </div>
            </div>
        </div> 
        <div class="row">
            <div class="col-lg-4">
                <div class="icon-box text-center effect border color">
                    <div class="icon">
                        <a href="#"><i class="fa fa-rocket"></i></a>
                    </div>
                    <h5 class="font-weight-500 font-green">Resident Only</h5>
                    <p class="lead">Lorem ipsum dolor sit amet, consecte adipiscing elit. Suspendisse condimentum porttitor cursumus.</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="icon-box text-center effect border color">
                    <div class="icon">
                        <a href="#"><i class="fa fa-flask"></i></a>
                    </div>
                    <h5 class="font-weight-500 font-green">Special Promo</h5>
                    <p class="lead">Lorem ipsum dolor sit amet, consecte adipiscing elit. Suspendisse condimentum porttitor cursumus.</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="icon-box text-center effect border color">
                    <div class="icon">
                        <a href="#"><i class="fa fa-umbrella"></i></a>
                    </div>
                    <h5 class="font-weight-500 font-green">Loyalti Program</h5>
                    <p class="lead">Lorem ipsum dolor sit amet, consecte adipiscing elit. Suspendisse condimentum porttitor cursumus.</p>
                </div>
            </div>
        </div> 
    </div>
</section>

<section class="p-t-100 p-b-100" data-bg-parallax="{{asset('assets/images/homepages/holidays/1.jpg')}}">
    <div class="bg-overlay"></div>
    <div class="container text-center text-light">
        <div class="row">
            <div class="col-lg-12">
                <div class="heading-text heading-line text-center m-b-50">
                    <h4>Yuk Bergabung Bersama<br>Summarecon Serpong Club(SSC)</h4>
                </div>
                <p class="lead">Nulla varius consequat magna, id molestie ipsum volutpat quis. A true story,
                    that never been told!. Fusce id mi diam, non ornare. Fusce id mi diam, non ornare orci.
                    Pellentesque ipsum erat, id molestie ipsum volutpat quis. facilisis ut venenatis eu.</p>
                <a href="#services" class="btn btn-light btn-outline btn-rounded">Daftar Member Baru</a>
                <a href="#services" class="btn btn-light btn-outline btn-rounded">Daftar Merchant Baru</a>
            </div>
        </div>
    </div>
</section>


<section class="p-t-20 p-b-20">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h3 class="font-green m-t-40 m-b-20">Nikmati Promo Merchant SSC</h3>
            </div>
            <div class="col-lg-4">
                <div class="grid-item">
                    <div class="grid-item-wrap">
                        <div class="grid-image"> 
                            <img alt="Image Lightbox" src="{{asset('assets/images/homepages/holidays/1.jpg')}}">
                        </div>
                        <div class="grid-description">
                            <a data-lightbox="image" href="{{asset('assets/images/homepages/holidays/1.jpg')}}" class="btn btn-light btn-rounded">Zoom</a>
                        </div>
                    </div>
                </div>
            </div>
    
            <div class="col-lg-4">
                <div class="grid-item">
                    <div class="grid-item-wrap">
                        <div class="grid-image"> 
                            <img alt="Image Lightbox" src="{{asset('assets/images/homepages/holidays/1.jpg')}}">
                        </div>
                        <div class="grid-description">
                            <a data-lightbox="image" href="{{asset('assets/images/homepages/holidays/1.jpg')}}" class="btn btn-light btn-rounded">Zoom</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="grid-item">
                    <div class="grid-item-wrap">
                        <div class="grid-image"> 
                            <img alt="Image Lightbox" src="{{asset('assets/images/homepages/holidays/1.jpg')}}">
                        </div>
                        <div class="grid-description">
                            <a data-lightbox="image" href="{{asset('assets/images/homepages/holidays/1.jpg')}}" class="btn btn-light btn-rounded">Zoom</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 text-center">
                <a href="#services" class="btn btn-outline-green btn-outline btn-rounded m-t-30">Follow Instagram Kami @summarecon_serpong</a>
            </div>
        </div>
    </div>
</section> 
@stop

@extends('layout.layout')
@section('content')
<div id="slider" class="inspiro-slider slider-halfscreen padding-slider dots-creative d-none d-sm-block d-sm-none d-md-block" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    @foreach($banners as $banner)
    <div class="slide background-image" data-bg-image="{{IMAGE_URL.$banner->image_1}}">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class=" text-center text-light">
                <h2 class="text-medium text-light font-weight-700 font-italic">{{$banner->name}}</h2>
                <p class="lead text-light font-weight-600">{{$banner->brief}}</p>
            </div>
        </div>
    </div>
    @endforeach
</div>

@foreach($banners as $banner)
<section id="page-title" class="text-light d-md-none d-lg-none d-xl-none" data-bg-parallax="{{IMAGE_URL.$banner->image_1}}">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title text-center">
            <h2 class="text-medium text-light font-weight-700 font-italic">{{$banner->name}}</h2>
            <p class="lead text-light font-weight-600">{{$banner->brief}}</p>
        </div>
    </div>
</section>
@endforeach

<section class="call-to-action background-call-to-action p-b-0-m p-t-0-m">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
            <form action="{{URL::to('search/action')}}" method="get">
                    <div class="form-group row m-t-20">
                        <div class="col-lg-10 col-12 m-b-10-m">
                            <input class="form-control form-custom" type="text" name="search" value="" id="" placeholder="Cari properti di Summarecon Serpong &quot;Enter&quot;">
                        </div>
                        <div class="col-lg-2 col-12">
                            <button type="submit" class="btn btn-block btn-rounded btn-green-custom">Search Now</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@if($banner)
<section class="p-t-20 p-b-0">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div data-animate="fadeInUp" data-animate-delay="0" class="animated fadeInUp visible p-b-40">
                    <h3 class="font-green m-t-40 m-b-20 m-t-20-m">{{$banner->note_3}}</h3>
                    {!!$banner->contents!!}
                </div>
            </div>
        </div>
    </div>
</section>
@endif

<section class="p-t-20 p-b-20 background-grey project-sale">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div data-animate="fadeInUp" data-animate-delay="0" class="animated fadeInUp visible p-b-20 text-center">
                    <h3 class="font-green m-t-40 m-b-0">On Sale Projects</h3>
                    <p>@if($banner->note_1) {{$banner->note_1}} @endif</p>
                </div>
                <div class="row infinite-scroll  text-center"  data-margin="10">
                    @foreach($projects->data as $project)
                    <div class="portfolio-item col-md-3 overlay-dark m-t-15 infinite-scroll-item item-project"
                    style="padding: 0px 10px 10px 0px;">
                        <a href="{{URL::to('projects/'.$project->category_slug.'/'.$project->slug)}}">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <a href="{{URL::to('projects/'.$project->category_slug.'/'.$project->slug)}}">
                                    <img alt="@if($project->meta_name) {{$project->meta_name}} @else Welcome To {{$project->name}} @endif" src="{{IMAGE_URL.'project/'.$project->image_1}}">
                                </a>
                            </div>
                            <div class="portfolio-description">
                                <a href="{{URL::to('projects/'.$project->category_slug.'/'.$project->slug)}}">
                                    <h3>View More</h3>
                                </a>
                            </div>
                        </div>
                        <div class="product-description m-t-10">
                            <div class="product-title text-center">
                                <h3 class="font-green"><a href="{{URL::to('projects/'.$project->category_slug.'/'.$project->slug)}}" class="font-green">{{$project->name}}</a></h3>
                            </div>
                            <div class="product-category text-center">
                                <span> @if($project->area !='Other'){{$project->area}}@endif</span>
                            </div>
                        </div>
                        </a>
                    </div>
                    @endforeach
                    <div class="infinite-pagination text-center">
                        <a href="{{$nextPage}}" class="infinite-next text-center">next</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="p-t-20 p-b-20 background-white">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if($projects3->data)
                <div data-animate="fadeInUp" data-animate-delay="0" class="animated fadeInUp visible p-b-20 p-b-0-m text-center">
                    <h3 class="font-green m-t-40 m-b-0 m-t-20-m">Past Projects</h3>
                    <p>@if($banner->note_2) {{$banner->note_2}} @endif</p>
                </div>

                <div  class="grid-layout portfolio-4-columns" data-margin="10">
                    @foreach($projects3->data as $project)
                    <div class="portfolio-item portfolio-past no-overlay m-t-15">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <img alt="@if($project->meta_name) {{$project->meta_name}} @else Welcome To {{$project->name}} @endif" src="{{IMAGE_URL.'project/'.$project->image_1}}">
                            </div>
                            <div class="portfolio-description m-t-20">
                                <h3 class="font-green">{{$project->name}}</h3>
                                <span>@if($project->area) {{$project->area}}@endif</span>
							</div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endif
            </div>
            <div class="col-md-12 text-center m-b-20 m-t-20">
                <a href="{{URL::to('past-projects/rumah')}}" class="btn btn-rounded btn-reveal btn-green-custom"><span class="text-light">See All Past Projects</span><i class="fa fa-angle-double-right" style="margin-top:-10px;"></i></a>
            </div>
        </div>
    </div>
</section>

<section id="parallax-home" class="p-t-50 p-b-50" data-bg-parallax="{{asset('assets/images/serpong/BANNER-INGIN-PUNYA-PROPERTY-DI-SS-1280x768px.jpg')}}">
    <div class="bg-overlay bg-overlay-home"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 text-light text-center">
                <div class="heading-project heading-text heading-section text-center">
                    <h2 class="text-small font-weight-600 m-b-80 m-b-50-m">Wujudkan mimpi memiliki properti di Summarecon Serpong</h2>
                </div>
                <p>Saatnya bergabung menjadi keluarga besar Summarecon Serpong dan wujudkan kehidupan dan masa depan yang lebih baik karena di sini Anda akan mendapatkan hunian dengan balutan konsep terbaik dan lingkungan hijau yang tepat untuk kenyamanan Anda dan keluarga. Amankan investasi masa depan Anda sekarang juga.</p>
            </div>
            <div class="col-lg-5 offset-lg-1">
                <form id="form2" class="widget-contact-form"  novalidate="novalidate" action="{{URL::to('contact/action')}}" method="post">
                <input name="_token" value="{{csrf_token()}}" type="hidden">
                <input type="hidden" name="subject" value="about">
                <input type="hidden" name="message" value="about Summarecon Serpong">
                    <div class="form-group m-b-20">
                        <input type="text" name="name" class="form-control form-custom input-f" placeholder="Nama" required="">
                    </div>
                    <div class="form-group m-b-20">
                    <input type="text" aria-required="true" name="phone" required class="form-control form-custom inout-f required phone number-only" placeholder="Nomor Whatsapp">
                    </div>
                    <div class="form-group">
                        {{-- {!! Recaptcha::render() !!} --}}
                        {!! RecaptchaV3::field('contact') !!}
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-rounded btn-light" id="form-submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@foreach($virtual as $product)
@if($product->link360)
<section id="slider" class="p-t-60 p-b-80">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="font-green m-b-5">3D Virtual Tour</h2>
                <p class="lead text-dark font-weight-600 font-italic">Discover The Charms of Living at Summarecon Serpong Houses</p>
                <div class="carousel" data-video="true" data-items="1">
                @foreach($virtual as $product)
                @if($product->link360)
                    <div class="col-md-12 p-0 no-padding text-center">
                        <iframe width="100%" height="400px" src="{{$product->link360}}" class="margin-two-bottom" style="border:0 !important;"></iframe>
                    </div>
                    @endif
                @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@break
@endif
@endforeach

@stop
@section('js')
<!-- INFINITE SCROLL -->
<script src="{{asset('assets/plugins/infinite-scroll/infinite-ajax-scroll/jquery-ias.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/infinite-scroll/infinite-ajax-scroll/jquery.infinitescroll.js')}}" defer></script>
<script src="{{asset('assets/js/functions/infinite_scroll.js')}}" defer></script>
@stop

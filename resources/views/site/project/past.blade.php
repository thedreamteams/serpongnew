@extends('layout.layout')
@section('content')
<div id="slider" class="inspiro-slider slider-halfscreen padding-slider dots-creative d-none d-sm-block d-sm-none d-md-block" data-height-xs="360" data-autoplay="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    @foreach($banners as $banner)
    <div class="slide background-image" data-bg-image="{{IMAGE_URL.$banner->image_1}}">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class=" text-center text-light">
                <h2 class="text-medium text-light font-weight-700 font-italic">Karya Properti Unggulan Summarecon Serpong</h2>
            </div>
        </div>
    </div>
    @endforeach
</div>

@foreach($banners as $banner)
<section id="page-title" class="text-light d-md-none d-lg-none d-xl-none" data-bg-parallax="{{IMAGE_URL.$banner->image_1}}">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title text-center">
            <h2 class="text-medium text-light font-weight-700 font-italic">Karya Properti Unggulan Summarecon Serpong</h2>
        </div>
    </div>
</section>
@endforeach

<section class="p-t-20 p-b-20 background-grey">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div data-animate="fadeInUp" data-animate-delay="0" class="animated fadeInUp visible p-b-20 text-center">
                    <h3 class="font-green m-t-40 m-b-0 m-t-20-m">Past Projects</h3>
                    <p class="lead font-weight-400">Properti terbaik Summarecon Serpong mulai dari rumah tapak dan ruko komersial yang telah serah terima tepat waktu dan memiliki occupancy huni tinggi.</>
                </div>
                
                <ul style="justify-content: center;" class="text-center portfolio-filter nav nav-tabs nav-project nav-tabs-light m-b-50 m-b-20-m no-border-bottom wow fadeInUp" data-wow-duration="300ms">                        
                    <li>
                        <a class="btn btn-outline-green btn-rounded btn-sm btn-sm-custom @if(Request::is('past-projects/rumah')) active @else btn-transparent-custom  @endif m-l-5 text-light" href="{{URL::to('past-projects/rumah')}}">Rumah</a>                                 
                    </li>
                    <li>
                        <a class="btn btn-outline-green btn-rounded btn-sm btn-sm-custom @if(Request::is('past-projects/ruko-komersial')) active @else btn-transparent-custom  @endif m-l-5 text-light" href="{{URL::to('past-projects/ruko-komersial')}}">Ruko Komersial</a>                                 
                    </li>
                    <!-- <li>
                        <a class="btn btn-outline-green btn-rounded btn-sm @if(Request::is('past-projects/apartment-condovillas')) active @else btn-transparent-custom  @endif m-l-5 text-light" href="{{URL::to('past-projects/apartment-condovillas')}}">Apartment & Condovillas</a>                                 
                    </li> -->
                </ul>
                <div class="row infinite-scroll  text-center"  data-margin="10"> 
                    @foreach($projects->data as $project)
                    <div class="portfolio-item portfolio-past col-md-3 overlay-dark m-t-15 infinite-scroll-item" style="padding: 0px 10px 10px 0px;">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <img alt="@if($project->meta_name) {{$project->meta_name}} @else Welcome To {{$project->name}} @endif" src="{{IMAGE_URL.'project/'.$project->image_1}}">
                            </div>
                        </div>
                        <div class="product-description">
                            <div class="product-title text-center">
                                <h4 class="font-green">{{$project->name}}</h4>
                            </div>
                            <div class="product-category text-center">
                                <span>@if($project->area)  {{$project->area}}@endif</span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="infinite-pagination text-center">
                        <a href="{{$nextPage}}" class="infinite-next text-center">next</a>
                    </div>                
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('js')
<!-- INFINITE SCROLL -->
<script src="{{asset('assets/plugins/infinite-scroll/infinite-ajax-scroll/jquery-ias.min.js')}}" defer></script>
<script src="{{asset('assets/plugins/infinite-scroll/infinite-ajax-scroll/jquery.infinitescroll.js')}}" defer></script>
<script src="{{asset('assets/js/functions/infinite_scroll.js')}}" defer></script>
@stop
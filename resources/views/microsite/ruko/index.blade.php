<!DOCTYPE html>
<html lang="en">
<head>
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-42299418-1', 'summareconserpong.com');
        ga('send', 'pageview');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-42299418-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-42299418-1');
    </script>

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NQ8XHHG');
    </script>
    <!-- End Google Tag Manager -->

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='0'>
    <meta http-equiv='pragma' content='no-cache'>
    <link href="{{asset('assets/microsite/images/favicon.ico')}}" rel="shortcut icon">
    {!!SEO::generate()!!}
    <!-- OWL CAROUSEL & POP UP -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="{{asset('assets/microsite/vendor/magnific-popup/magnific-popup.css')}}" rel="stylesheet">
    <!-- STYLE CSS POP UP FORM NOTIFICATION -->
    <link href="{{asset('assets/microsite/css/styleheader.css')}}" rel="stylesheet">
    <link href="{{asset('assets/microsite/vendor/sweetalert/sweetalert.css')}}" rel="stylesheet">
    <!-- NEW LINK POLO -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
    <link href="{{asset('assets/css/plugins.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/custom.css')}}" rel="stylesheet"/>
    <!-- GMAPS -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqgVIaPYgQ2NhYwOzDjSyMp7v47HdQnak"></script>
    <!-- LOAD JQUERY LIBRARY -->
    {{-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script> --}}


    <!-- CandyPixel V3.7.1 -->

    <!--
    Warning:   CandyPixel v3.7.0 onwards is incompatible with snippets and custom code
            associated with previous versions of CandyPixel.

            All MSIE browsers are no longer supported.
    -->
    <script>
        var CandyPixel4 = (function (s, u, g, a, r) {
            a = u.createElement("script");
            r = u.getElementsByTagName("script")[0];
            a.async = true;
            a.src = g;
            r.parentNode.insertBefore(a, r);

            return {
                config: {
                    debugMode: false,
                    token: "d4620087-8bc89f2d-1717123083988",
                    pid: "2989",    // deprecated
                    psid: "SRCP#fb4054d7-ef40-46da-ab83-c840d8263828",  // deprecated

                    // form class or ids to be ignored e.g. [".secretForms", "#loginForm"]
                    ignoreFormsWithAttributeValues: [],

                    // field class or ids to be ignored  e.g. [".secretField", "#password"]
                    ignoreFieldsWithAttributeValues: [],

                    submitAction: "submit", // click | submit
                    detectAdBlocker: null,  // set to true or false to override server-side setting
                    enableTracking: true,   // set to true or false to enableTracking

                    // feature flags:
                    ff_useLegacyRadioInputHandling: false, // set to true if you have trouble with radio form fields
                },
                app: {},
                onInit: function () {
                    console.log("onInit", this);
                    // this.app.setConfig(this.config);
                    // register callbacks here, if any

                },
                onReady: function () {
                    console.log("--------CandyPixel is Ready---------");
                    // this.app.getInfo();
                },
            }
        })(window, document, "https://s.cdy.ai/candypixel/app.v3.7.8.js");
    </script>

    <!-- Meta Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '573486941398806');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=573486941398806&ev=PageView&noscript=1"
        />
    </noscript>
    <!-- End Meta Pixel Code -->

    <style>
        @font-face {
    		font-family: 'Conv_DIN-Medium 2_0';
    		src: url('../fonts/DIN-Medium 2_0.eot');
    		src: local('☺'), url('../fonts/DIN-Medium 2_0.woff') format('woff'), url('../fonts/DIN-Medium 2_0.ttf') format('truetype'), url('../fonts/DIN-Medium 2_0.svg') format('svg');
    		font-weight: normal;
    		font-style: normal;
    	}

    	@font-face {
    		font-family: 'Conv_DIN-Light';
    		src: url('../fonts/DIN-Light.eot');
    		src: local('☺'), url('../fonts/DIN-Light.woff') format('woff'), url('../fonts/DIN-Light.ttf') format('truetype'), url('../fonts/DIN-Light.svg') format('svg');
    		font-weight: normal;
    		font-style: normal;
    	}
    	h2{
            font-family: 'Conv_DIN-Medium 2_0' !important;
            line-height: 36px;
            color: #86ad3c;
        }
        #book {
            background-color: #8C6746;
            border: 10px solid rgba(255,255,255,.2);
            border-radius: 0px;
            text-align: center;
            padding-top: 30px;
            padding-left: 30px;
            padding-right: 30px;
            width: 500px;
            z-index: 999;
        }
        .p-t-250{padding-top:250px !important;}
        .p-b-250{padding-bottom:220px !important;}
        .m-t-250{margin-top: 230px !important;}
        .height-m{
            height: 50px !important;
            line-height: 20px;
        }
        .img-logo{
            width: 160px;
            margin-bottom: 10px;
        }
        .img-icon-micro{
            width: 60px !important;
            height: auto !important;
            padding-right: 15px;
        }
        .img-icon-car{
            width: 20px !important;
            margin-right:10px;
        }
        .border-bottom-line{
            border-bottom: 1px solid;
            margin-bottom: 10px;
            padding-bottom: 10px;
        }
        .text-large{
            font-size: 50px;
            line-height: 55px;
        }
        button.btn-light-custom{color: #fff !important;}
        button.btn.btn-slide.btn-lg > i, .btn:not(.close):not(.mfp-close).btn-slide.btn-lg > i, a.btn:not([href]):not([tabindex]).btn-slide.btn-lg > i {
            left: 15px;
            top: 15px;
            font-size: 2em;
        }
        .btn-form{
            background: linear-gradient(87deg, #BD8E54 0%, #D5B578 100%)!important;
            border-color: linear-gradient(87deg, #BD8E54 0%, #D5B578 100%)!important;
            border-color: #BE8F55 !important;
            border-radius: 0 !important;
            color: #fff;
        }

        .btn-book{
            background: linear-gradient(87deg, #BD8E54 0%, #D5B578 100%)!important;
            border-color: linear-gradient(87deg, #BD8E54 0%, #D5B578 100%)!important;
            border-color: #BD8E54 !important;
        }
        .form-control-custom{
            border: 0;
    	    border-bottom: 1px solid #ced4da !important;
            border-radius: 0;
            padding: 10px 0 !important;
            min-height: 50px !important;
            color: #75C0CC !important;
            background-color: transparent;
        }
        .form-control-custom::placeholder {
            color: #fff;
            font-weight: 500;
            font-size:15px;
        }
        .font-gold-micro{color: #C89A5E;}
        .font-brown{color: #1E130D;}
        .background-brown{background-color:#36291F;}
        .background-mocca{background-color:#CDBAAA;}
        .grecaptcha-badge { visibility: hidden !important; }
        .scrollTop {bottom: 165px !important;}

        @media  only screen and (max-width: 768px ) {
            .p-b-0-m{padding-bottom: 0;}
            .p-r-0-m{padding-right: 0;}
            .p-l-15-m{margin-left: 15px !important;}
            .m-t-250{margin-top: 0px !important;}
            #book {
                background-color: rgba(197, 174, 143, 0.9);
                width: 100%;
                position: relative;
                left: 0%;
                top: 0;
                height: 100%;
            }
            .d-none-m{display: block !important;}
            #footer .social-icons {float: none !important;}
            .line-height-mobile{line-height:17px;}
            h2{line-height: 30px;}
            .text-left-m{text-align: left !important;}
        }
    </style>
    {!! RecaptchaV3::initJs() !!}

    {{-- TIKTOK --}}
    <script>
        !function (w, d, t) {
          w.TiktokAnalyticsObject=t;var ttq=w[t]=w[t]||[];ttq.methods=["page","track","identify","instances","debug","on","off","once","ready","alias","group","enableCookie","disableCookie"],ttq.setAndDefer=function(t,e){t[e]=function(){t.push([e].concat(Array.prototype.slice.call(arguments,0)))}};for(var i=0;i<ttq.methods.length;i++)ttq.setAndDefer(ttq,ttq.methods[i]);ttq.instance=function(t){for(var e=ttq._i[t]||[],n=0;n<ttq.methods.length;n++)ttq.setAndDefer(e,ttq.methods[n]);return e},ttq.load=function(e,n){var i="https://analytics.tiktok.com/i18n/pixel/events.js";ttq._i=ttq._i||{},ttq._i[e]=[],ttq._i[e]._u=i,ttq._t=ttq._t||{},ttq._t[e]=+new Date,ttq._o=ttq._o||{},ttq._o[e]=n||{};var o=document.createElement("script");o.type="text/javascript",o.async=!0,o.src=i+"?sdkid="+e+"&lib="+t;var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(o,a)};

          ttq.load('CNGM0NBC77U6RIGJDUK0');
          ttq.page();
        }(window, document, 'ttq');
    </script>
</head>

<body>

    <div class="body-inner">
        @if(isset($banners[0]))
        <section class="text-center p-t-200 p-b-200 text-light fullscreen d-none d-lg-block d-xl-block" style="background-image:url({{IMAGE_URL.$banners[0]->image_1}}); background-position: center center; background-repeat: no-repeat; background-size: cover;">
            <div class="container">
                <hr class="space">
                <hr class="space">
                <hr class="space">
                <hr class="space">
                <hr class="space">
                <div class="row">
                    <div class="col-6 center">
                        <div data-animate-delay="300" data-animate="fadeInUp">
                            <a href="#formcontact" class="btn btn-gold-ardea btn-form btn-reveal btn-reveal-left scroll-to"><span>Book Now</span><i class="fa fa-arrow-down"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

       <!--  <section class="p-t-0 p-b-0 d-block d-sm-block d-md-block d-lg-none d-xl-none">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 no-padding">
                        <img src="{{IMAGE_URL.$banners[0]->image_2}}" class="img-responsive">
                    </div>
                </div>
            </div>
        </section>
 -->
         <section class="text-center p-t-200 p-b-200 text-light  d-block d-sm-block d-md-block d-lg-none d-xl-none" style="background-image:url({{IMAGE_URL.$banners[0]->image_2}}); background-position: center center; background-repeat: no-repeat; background-size: cover;">
            <div class="container">
                <hr class="space">
                <hr class="space">
                <hr class="space">
                <hr class="space">
                <div class="row">
                    <div class="col-6 center">
                        <div data-animate-delay="300" data-animate="fadeInUp">
                            <a href="#formcontact" class="btn btn-gold-ardea btn-form btn-reveal btn-reveal-left scroll-to"><span>Book Now</span><i class="fa fa-arrow-down"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @endif

        <section class="p-b-0">
            <div class="container-fullscreen">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-xl-5">
                            <h4 class="p-b-0 p-b-0-m text-uppercase"><div class="font-weight-500 text-green text-left-m">{!! $articlesfirst[0]->name !!}</div></h4>
                            <h2 class="p-b-10 p-b-20-m text-uppercase" data-animate-delay="900" data-animate="zoomIn"><div class="font-weight-500 font-gold text-left-m">{!! $articlesfirst[0]->brief !!}</div></h2>
                        </div>
                        <div class="offset-lg-2 col-lg-5 col-xl-5">
                            <div class="text-justify">{!! $articlesfirst[0]->contents !!}</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="p-t-250 p-b-250 m-t-50 m-t-20-m" data-bg-parallax="{{IMAGE_URL.$articlesfirst[0]->image_1}}">
            <div class="bg-overlay"></div>
        </section>

        <section class="p-b-0">
            <div class="container-fullscreen">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-xl-5">
                            <h4 class="p-b-0 p-b-0-m text-uppercase"><div class="font-weight-500 text-green text-left-m">{!! $articlessecond[0]->brief !!}</div></h4>
                            <h2 class="p-b-10 p-b-20-m text-uppercase" data-animate-delay="1200" data-animate="zoomIn"><div class="font-weight-500 font-gold text-left-m">{!! $articlessecond[0]->name !!}</div></h2>
                        </div>
                        <div class="offset-lg-2 col-lg-5 col-xl-5">
                            <div class="text-justify">{!! $articlessecond[0]->contents !!}</div>
                        </div>
                        {{--
                        <div class="col-lg-12 col-xl-12 m-t-40 m-t-10-m">
                            <div class="row">
                                <div class="col-lg-4 col-6">
                                    <ul class="list-unstyled text-center-m">
                                        <li class="d-flex d-none-m"><img src="{{asset('assets/images/serpong/microsite/icon/club-house.svg')}}" class="img-icon-micro p-r-0-m"><h5 class="m-t-30 m-t-0-m m-b-10-m height-m">Full air condition gathering clubhouse</h5></li>
                                        <li class="d-flex d-none-m"><img src="{{asset('assets/images/serpong/microsite/icon/nature.svg')}}" class="img-icon-micro p-r-0-m"><h5 class="m-t-30 m-t-0-m m-b-10-m height-m">Nature Walkways</h5></li>
                                        <li class="d-flex d-none-m"><img src="{{asset('assets/images/serpong/microsite/icon/fitness.svg')}}" class="img-icon-micro p-r-0-m"><h5 class="m-t-30 m-t-0-m m-b-10-m height-m">Private GYM</h5></li>
                                        <li class="d-flex d-none-m d-block d-sm-block d-md-block d-lg-none d-xl-none"><img src="{{asset('assets/images/serpong/microsite/icon/playground.svg')}}" class="img-icon-micro p-r-0-m"><h5 class="m-t-30 m-t-0-m m-b-10-m height-m">Children Playground</h5></li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-6">
                                    <ul class="list-unstyled text-center-m">
                                        <li class="d-flex d-none-m"><img src="{{asset('assets/images/serpong/microsite/icon/jacuzzi.svg')}}" class="img-icon-micro p-r-0-m"><h5 class="m-t-30 m-t-0-m m-b-10-m height-m">Jacuzzi</h5></li>
                                        <li class="d-flex d-none-m"><img src="{{asset('assets/images/serpong/microsite/icon/sauna.svg')}}" class="img-icon-micro p-r-0-m"><h5 class="m-t-30 m-t-0-m m-b-10-m height-m">Private-Family Sauna</h5></li>
                                        <li class="d-flex d-none-m"><img src="{{asset('assets/images/serpong/microsite/icon/koi.svg')}}" class="img-icon-micro p-r-0-m"><h5 class="m-t-30 m-t-0-m m-b-10-m height-m">Aqua-Zen Garden</h5></li>
                                        <li class="d-flex d-none-m d-block d-sm-block d-md-block d-lg-none d-xl-none"><img src="{{asset('assets/images/serpong/microsite/icon/kolam-renang.svg')}}" class="img-icon-micro p-r-0-m"><h5 class="m-t-30 m-t-0-m m-b-10-m height-m">Swimming Pool</h5></li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 d-none d-lg-block d-xl-block">
                                    <ul class="list-unstyled text-center-m">
                                        <li class="d-flex d-none-m"><img src="{{asset('assets/images/serpong/microsite/icon/playground.svg')}}" class="img-icon-micro"><h5 class="m-t-30 m-t-0-m m-b-10-m height-m">Children Playground</h5></li>
                                        <li class="d-flex d-none-m"><img src="{{asset('assets/images/serpong/microsite/icon/kolam-renang.svg')}}" class="img-icon-micro"><h5 class="m-t-30 m-t-0-m m-b-10-m height-m">Swimming Pool</h5></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        --}}
                    </div>
                </div>
            </div>
        </section>

        <section class="p-b-0">
            <div class="container-fullscreen">
                <div class="row">
                    <div class="col-lg-4 p-0">
                        <img src="{{IMAGE_URL.$articlesthree[0]->image_1}}" class="img-fluid">
                    </div>
                    <div class="col-lg-4 p-20" style="background-color:#CDBAAA;">
                        <h2 data-animate-delay="1500" data-animate="zoomIn" class="p-b-10 p-b-0-m m-t-100 p-t-40 text-uppercase"><div class="font-weight-500 font-brown">{!! $articlesthree[0]->name !!}</div></h2>
                        <div class="text-justify m-l-20">{!! $articlesthree[0]->contents !!}</div>
                    </div>
                    <div class="col-lg-4 p-0">
                        <img src="{{IMAGE_URL.$articlesthree[0]->image_2}}" class="img-fluid">
                    </div>
                </div>
            </div>
        </section>


        <section class="p-b-0 p-t-30">
            <div class="container-fullscreen">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="p-b-0 p-b-0-m text-uppercase">
                            <div class="font-weight-500 text-green text-center m-t-30">Type Ruko</div>
                        </h4>
                        <h2 class="p-b-10 p-b-0-m m-b-50 text-uppercase" data-animate-delay="1800" data-animate="zoomIn">
                            <div class="font-weight-500 font-gold-micro text-center">CARSON<br>SUMMARECON SERPONG</div>
                        </h2>
                        <div class="carousel" data-items="1" data-dots="false" data-lightbox="gallery" data-margin="0" data-loop="false">
                            @foreach($products as $gallery)
                                <div class="portfolio-item img-zoom">
                                    <div class="portfolio-item-wrap">
                                        <div class="portfolio-image">
                                            <a href="#"><img src="{{ IMAGE_URL.$gallery->image_1 }}" alt="{{ $gallery->name }}"></a>
                                        </div>
                                        <div class="portfolio-description">
                                            <a title="{{ $gallery->name }}" data-lightbox="gallery-image" href="{{ IMAGE_URL.$gallery->image_1 }}" class="btn btn-light btn-rounded">Zoom</a>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3 background-brown p-10">
                                                <h4 class="text-center text-uppercase text-light m-b-0 m-t-20" style="margin-left:15px;">{{ $gallery->name }}</h4>
                                                <h5 class="text-center text-uppercase text-light">{{ $gallery->note_1 }}</h5>
                                            </div>
                                            <div class="col-lg-9 background-mocca p-l-15-m">
                                                <div class="row">
                                                    <div class="col-lg-7 p-15 p-b-0-m">
                                                        <ul class="list-unstyled m-b-0 m-t-20">
                                                            <li class="font-brown">Land Area:&nbsp;<span class="font-weight-700 font-brown">{{ $gallery->note_2 }}</span></li>
                                                            <li  class="font-brown">Building Area:&nbsp;<span class="font-weight-700 font-brown">{{ $gallery->note_3 }}</span></li>
                                                        </ul>
                                                    </div>

                                                    <div class="col-lg-5 p-15 text-right text-left-m">
                                                        {{-- <ul class="list-unstyled list-inline m-t-20">
                                                            <li>
                                                                <img src="{{asset('assets/microsite/images/microsite/Group.svg')}}" style="width:40px;margin-right:10px;" class="img m-b-0 h-auto">
                                                                @if($typeGallery[$gallery->slug]->bedroom)
                                                                    <p class="text-center font-weight-500 font-brown m-b-0 m-r-10">{{ $typeGallery[$gallery->slug]->bedroom }}</p>
                                                                @endif
                                                            </li>
                                                            <li>
                                                                <img src="{{asset('assets/microsite/images/microsite/Group45.svg')}}" style="width:40px;margin-right:10px;" class="img m-b-0 h-auto">
                                                                @if($typeGallery[$gallery->slug]->bathroom)
                                                                    <p class="text-center font-weight-500 font-brown m-b-0 m-r-10">{{ $typeGallery[$gallery->slug]->bathroom }}</p>
                                                                @endif
                                                            </li>
                                                            <li>
                                                                <img src="{{asset('assets/microsite/images/microsite/Group46.svg')}}" style="width:40px;margin-right:10px;" class="img m-b-0 h-auto">
                                                                @if($typeGallery[$gallery->slug]->carpot)
                                                                    <p class="text-center font-weight-500 font-brown m-b-0 m-r-10">{{ $typeGallery[$gallery->slug]->carpot }}</p>
                                                                @endif
                                                            </li>
                                                        </ul> --}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>

        {{--
        <section>
            <div class="container-fullscreen">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h4 class="p-b-0 p-b-0-m text-uppercase"><div class="font-weight-500 text-green text-left-m">Facilities</div></h4>
                            <h2 class="p-b-10 p-b-0-m text-uppercase" data-animate-delay="2100" data-animate="zoomIn"><div class="font-weight-500 font-gold-micro text-left-m">PREMIUM FEATURES</div></h2>
                        </div>

                        <div class="col-lg-12 col-xl-12 m-t-40">
                            <div class="row">
                                <div class="col-lg-2 col-6 text-center">
                                    <img src="{{asset('assets/images/serpong/microsite/icon/fasilitas/smart-switch.svg')}}" class="img-icon-micro p-r-0">
                                    <h6>Smart Switch</h6>
                                </div>
                                <div class="col-lg-2 col-6 text-center">
                                    <img src="{{asset('assets/images/serpong/microsite/icon/fasilitas/smart-socket.svg')}}" class="img-icon-micro p-r-0">
                                    <h6>Smart Socket</h6>
                                </div>
                                <div class="col-lg-2 col-6 text-center">
                                    <img src="{{asset('assets/images/serpong/microsite/icon/fasilitas/cctv-icon.svg')}}" class="img-icon-micro p-r-0">
                                    <h6>Indoor/Outdoor<br>Smart CCTV</h6>
                                </div>
                                <div class="col-lg-2 col-6 text-center">
                                    <img src="{{asset('assets/images/serpong/microsite/icon/fasilitas/door-censor.svg')}}" class="img-icon-micro p-r-0">
                                    <h6>Door Censor</h6>
                                </div>
                                <div class="col-lg-2 col-6 text-center">
                                    <img src="{{asset('assets/images/serpong/microsite/icon/fasilitas/smart-doorlock-icon.svg')}}" class="img-icon-micro p-r-0">
                                    <h6>Smart Door Lock</h6>
                                </div>
                                <div class="col-lg-2 col-6 text-center">
                                    <img src="{{asset('assets/images/serpong/microsite/icon/fasilitas/smart-ir.svg')}}" class="img-icon-micro p-r-0">
                                    <h6>Smart IR</h6>
                                </div>
                            </div>
                            <div class="row p-t-40 p-t-0-m">
                                <div class="col-lg-2 col-6 text-center">
                                    <img src="{{asset('assets/images/serpong/microsite/icon/fasilitas/motion-detector-icon.svg')}}" class="img-icon-micro p-r-0">
                                    <h6>Motion Detector</h6>
                                </div>
                                <div class="col-lg-2 col-6 text-center">
                                    <img src="{{asset('assets/images/serpong/microsite/icon/fasilitas/Frame 155.svg')}}" class="img-icon-micro p-r-0">
                                    <h6>EV Charging Ready<br>(Installation)</h6>
                                </div>
                                <div class="col-lg-2 col-6 text-center">
                                    <img src="{{asset('assets/images/serpong/microsite/icon/fasilitas/usb-power-charging-icon.svg')}}" class="img-icon-micro p-r-0">
                                    <h6>USB Power Charging</h6>
                                </div>
                                <div class="col-lg-2 col-6 text-center">
                                    <img src="{{asset('assets/images/serpong/microsite/icon/fasilitas/automatic-garage-door-icon.svg')}}" class="img-icon-micro p-r-0">
                                    <h6>Automatic Garage Door</h6>
                                </div>
                                <div class="col-lg-2 col-6 text-center">
                                    <img src="{{asset('assets/images/serpong/microsite/icon/fasilitas/lift-ready-icon.svg')}}" class="img-icon-micro p-r-0">
                                    <h6>Lift Ready</h6>
                                </div>
                                <div class="col-lg-2 col-6 text-center">
                                    <img src="{{asset('assets/images/serpong/microsite/icon/fasilitas/smart-panel-icon.svg')}}" class="img-icon-micro p-r-0">
                                    <h6>Smart Panel</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        --}}
        {{--
        <section class="p-b-50 p-t-40" style="background-image: url({{asset('assets/images/serpong/microsite/background-lokasi.jpg')}});">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="p-b-0 p-b-0-m text-uppercase"><div class="font-weight-500 text-white">Lokasi</div></h4>
                        <h2 class="p-b-10 p-b-0-m text-uppercase m-b-20" data-animate-delay="2400" data-animate="zoomIn"><div class="font-weight-500 font-gold">LOKASI TERBAIK DENGAN<br>AKSES YANG MUDAH</div></h2>
                    </div>
                     <div class="col-lg-8 offset-lg-2">
                        <img src="{{asset('assets/images/serpong/microsite/maps.jpg')}}" class="img-responsive">
                    </div>
                </div>
            </div>
        </section>
        --}}
        <section class="p-b-0 p-t-50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="p-b-10 p-b-0-m m-b-30 text-uppercase" data-animate-delay="2700" data-animate="zoomIn"><div class="font-weight-500 font-gold">ACCESSIBILITY</div></h2>
                    </div>
                </div>
            </div>
        </section>

        <section class="p-b-0 p-t-0" style="background-color:#36291F;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6 col-12 m-t-20-m p-0">
                        <img src="{{IMAGE_URL.$articlesfour[0]->image_1}}" class="img-fluid">
                    </div>
                    <div class="col-lg-6 col-12 p-0">
                        <img src="{{IMAGE_URL.$articlesfour[0]->image_2}}" class="img-fluid">
                    </div>
                </div>
            </div>
        </section>

        {{-- <section class="p-t-0 p-b-0">
            <div class="row">
                <div class="col-lg-12">
                    <div class="carousel" data-items="3" data-dots="false" data-lightbox="gallery" data-margin="0" data-autoplay="false" data-autoplay-timeout="3600">
                        <div class="portfolio-item img-zoom">
                            <div class="portfolio-item-wrap">
                                <div class="portfolio-image">
                                    <a href="#"><img src="{{asset('assets/images/serpong/microsite/Cluster-Ardea-Gallery-01.jpg')}}" alt=""></a>
                                </div>
                                <div class="portfolio-description">
                                    <a title="Cluster Ardea" data-lightbox="gallery-image" href="{{asset('assets/images/serpong/microsite/Cluster-Ardea-Gallery-01.jpg')}}" class="btn btn-light btn-rounded">Zoom</a>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-item img-zoom">
                            <div class="portfolio-item-wrap">
                                <div class="portfolio-image">
                                    <a href="#"><img src="{{asset('assets/images/serpong/microsite/Cluster-Ardea-Gallery-02.jpg')}}" alt=""></a>
                                </div>
                                <div class="portfolio-description">
                                    <a title="Cluster Ardea" data-lightbox="gallery-image" href="{{asset('assets/images/serpong/microsite/Cluster-Ardea-Gallery-02.jpg')}}" class="btn btn-light btn-rounded">Zoom</a>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-item img-zoom">
                            <div class="portfolio-item-wrap">
                                <div class="portfolio-image">
                                    <a href="#"><img src="{{asset('assets/images/serpong/microsite/Cluster-Ardea-Gallery-03.jpg')}}" alt=""></a>
                                </div>
                                <div class="portfolio-description">
                                    <a title="Cluster Ardea" data-lightbox="gallery-image" href="{{asset('assets/images/serpong/microsite/Cluster-Ardea-Gallery-03.jpg')}}" class="btn btn-light btn-rounded">Zoom</a>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-item img-zoom">
                            <div class="portfolio-item-wrap">
                                <div class="portfolio-image">
                                    <a href="#"><img src="{{asset('assets/images/serpong/microsite/Cluster-Ardea-Gallery-04.jpg')}}" alt=""></a>
                                </div>
                                <div class="portfolio-description">
                                    <a title="Cluster Ardea" data-lightbox="gallery-image" href="{{asset('assets/images/serpong/microsite/Cluster-Ardea-Gallery-04.jpg')}}" class="btn btn-light btn-rounded">Zoom</a>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-item img-zoom">
                            <div class="portfolio-item-wrap">
                                <div class="portfolio-image">
                                    <a href="#"><img src="{{asset('assets/images/serpong/microsite/Cluster-Ardea-Gallery-05.jpg')}}" alt=""></a>
                                </div>
                                <div class="portfolio-description">
                                    <a title="Cluster Ardea" data-lightbox="gallery-image" href="{{asset('assets/images/serpong/microsite/Cluster-Ardea-Gallery-05.jpg')}}" class="btn btn-light btn-rounded">Zoom</a>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-item img-zoom">
                            <div class="portfolio-item-wrap">
                                <div class="portfolio-image">
                                    <a href="#"><img src="{{asset('assets/images/serpong/microsite/Cluster-Ardea-Gallery-06.jpg')}}" alt=""></a>
                                </div>
                                <div class="portfolio-description">
                                    <a title="Cluster Ardea" data-lightbox="gallery-image" href="{{asset('assets/images/serpong/microsite/Cluster-Ardea-Gallery-06.jpg')}}" class="btn btn-light btn-rounded">Zoom</a>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-item img-zoom">
                            <div class="portfolio-item-wrap">
                                <div class="portfolio-image">
                                    <a href="#"><img src="{{asset('assets/images/serpong/microsite/Cluster-Ardea-Gallery-07.jpg')}}" alt=""></a>
                                </div>
                                <div class="portfolio-description">
                                    <a title="Cluster Ardea" data-lightbox="gallery-image" href="{{asset('assets/images/serpong/microsite/Cluster-Ardea-Gallery-07.jpg')}}" class="btn btn-light btn-rounded">Zoom</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}

        <!-- GALLERY -->
        <section class="p-t-0 p-b-0">
            <div class="row">
                <div class="col-lg-12">
                    <div class="carousel" data-items="3" data-dots="false" data-lightbox="gallery" data-margin="0" data-autoplay="false" data-autoplay-timeout="3600">
                        @for ($i = 0; $i < count($image_gallery); $i++)
                        <div class="portfolio-item img-zoom">
                            <div class="portfolio-item-wrap">
                                <div class="portfolio-image">
                                    <a href="#"><img src="{{$image_gallery[$i]}}" alt=""></a>
                                </div>
                                <div class="portfolio-description">
                                    <a title="Paper Pouch!" data-lightbox="gallery-image" href="{{$image_gallery[$i]}}" class="btn btn-light btn-rounded">Zoom</a>
                                </div>
                            </div>
                        </div>
                        @endfor
                    </div>
                </div>
            </div>
        </section>

        <section class="p-b-40 p-t-40" style="background-image: url({{asset('assets/images/serpong/microsite/background-lokasi.jpg')}});" id="formcontact">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 d-block d-sm-block d-md-block d-lg-none d-xl-none">
                        <h2 class="p-b-10 p-b-0-m text-uppercase text-large font-gold-micro" data-animate-delay="3000" data-animate="zoomIn"><div class="font-weight-500 text-left">MILIKI SEGERA<br>PROPERTI<br>IMPIAN ANDA!</div></h2>
                        <p class="text-left text-light m-b-20">Isi Form untuk Info Lengkap & Promo Eksklusif<br>Carson Commercial Summarecon Serpong.</p>
                    </div>
                    <div class="col-lg-6">
                        <div id="book" class="text-light">
                            <form method="post" action="{{URL::to(Library::getDomainPrefix().'/contact-micro/action')}}" class="form-contact" id="form-contact">
                                <input name="_token" value="{{csrf_token()}}" type="hidden">
                                <div class="text-center">
                                    <img src="{{ (isset($forms)) ? IMAGE_URL.$forms[0]->thumb : '' }}" class="img img-responsive img-logo">
                                </div>
                                <div class="text-center">
                                    <p>{!! (isset($forms)) ? $forms[0]->contents : '' !!}</p>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group m-b-20">
                                            <input aria-required="true" name="name" class="form-control form-control-custom char-only required" id="name" minlength="3" maxlength="50" placeholder="Fullname" type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group m-b-20">
                                            <input class="form-control form-control-custom number-only required" id="phone" name="phone" placeholder="Phone No" minlength="10" maxlength="15" type="tel" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group m-b-20">
                                            <input aria-required="true" name="email" id="email" class="form-control form-control-custom required" placeholder="Email" type="email">
                                        </div>
                                    </div>
                                    <input type="hidden" name="utm_source" id="utm_source" class="utm_source" value={{ app('request')->input('utm_source') }}>
                                    <input type="hidden" name="utm_medium" id="utm_medium" class="utm_medium" value={{ app('request')->input('utm_medium') }}>
                                    <input type="hidden" name="utm_campaign" id="utm_campaign" class="utm_campaign" value={{ app('request')->input('utm_campaign') }}>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {!! RecaptchaV3::field('contact') !!}
                                        </div>
                                        <div class="form-group text-left">
                                            <button class="btn btn-form btn-block text-light" type="submit"><i class="fa fa-paper-plane"></i>&nbsp;Submit</button>
                                            <div class="form-status"></div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-6 d-none d-lg-block d-xl-block">
                        <h2 class="p-b-10 p-b-0-m text-uppercase text-large font-gold-micro"><div class="font-weight-500 text-right">MILIKI SEGERA<br>PROPERTI<br>IMPIAN ANDA!</div></h2>
                        <p class="text-right text-light">Isi Form untuk Info Lengkap & Promo Eksklusif<br>Carson Commercial Summarecon Serpong.</p>
                    </div>
                </div>
            </div>
        </section>

        {{--
        <!-- GALLERY -->
        <section class="p-t-20 p-b-0">
            <div class="row">
                <div class="col-lg-12">
                    <div class="carousel" data-items="3" data-dots="false" data-lightbox="gallery" data-margin="0" data-autoplay="false" data-autoplay-timeout="3600">
                        @for ($i = 0; $i < count($gallery); $i++)
                        <div class="portfolio-item img-zoom">
                            <div class="portfolio-item-wrap">
                                <div class="portfolio-image">
                                    <a href="#"><img src="{{$gallery[$i]}}" alt=""></a>
                                </div>
                                <div class="portfolio-description">
                                    <a title="Paper Pouch!" data-lightbox="gallery-image" href="{{$gallery[$i]}}" class="btn btn-light btn-rounded">Zoom</a>
                                </div>
                            </div>
                        </div>
                        @endfor
                    </div>
                </div>
            </div>
        </section>
        --}}

        {{--
        <section id="page-content" class="p-t-0 p-b-0">
            <div class="container-fluid">
                <!-- <div class="owl-carousel owl-theme">
                    @for ($i = 0; $i < count($gallery); $i++)
                    <div class="item">
                        <a href="{{$gallery[$i]}}" class="popup">
                            <img src="{{$gallery[$i]}}" alt="">
                        </a>
                    </div>
                    @endfor
                </div> -->
                <div class="row">
                    <div class="col-md-12 no-padding">
                        @if($video->url)
                        <iframe width="100%" height="500px" src="https://www.youtube.com/embed/{{$video->url}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        @else
                        <img class="img-responsive" src="{{IMAGE_URL.$video->thumb}}" alt="{{$video->name}}">
                        @endif
                    </div>
                </div>
            </div>
        </section>
        <!-- END : GALLERY -->
        --}}

        {{--
        <section class="p-t-0 p-b-0">
            <div class="container-fullscreen">
                <div class="row">
                    <div class="col-lg-12">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15863.738583762375!2d106.6441161!3d-6.2723246!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6a14c974b8604dbb!2sCluster%20Ardea%20Summarecon%20Serpong!5e0!3m2!1sid!2sid!4v1666935283696!5m2!1sid!2sid" width="100%" height="450" style="border:0; margin-bottom: -7px;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>
            </div>
        </section>
        --}}
        @include('layout.footer')

    </div>

    <script type="text/javascript" src="{{asset('assets/js/jquery-1.11.2.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins.js')}}"></script>
    <script src="{{asset('assets/js/functions.js')}}"></script>

    <!-- Scroll top -->
    {{--<a id="scrollTop"><i class="fas fa-chevron-up"></i><i class="fas fa-chevron-up"></i></a>--}}

    <!-- Book Now -->
    <div class="scrollTop d-none d-sm-block d-sm-none d-md-block"><a class="btn btn-lg btn-slide btn-shadow btn-book scroll-to" data-width="180" href="#formcontact" style="width: 60px;"><i class="icon-chevron-down fa-lg"></i><span>Book Now</span></a></div>

    <a href="https://wa.me/628111409008?text=Halo%20Saya%20tertarik%20dengan%20Produk%20Summarecon%20Serpong%20ingin%20dapat%20informasi%20lebih%20lanjut%20dari%20Website%20Summarecon%20Serpong" class="float d-none d-sm-block d-sm-none d-md-block" target="_blank" rel="noopener">
        <div class="hotline-phone-ring-circle-wa"></div>
        <div class="hotline-phone-ring-circle-fill-wa"></div>
        <i class="fab fa-whatsapp my-float"></i>
    </a>

    <a href="https://wa.me/628111409008?text=Halo%20Saya%20tertarik%20dengan%20Produk%20Summarecon%20Serpong%20ingin%20dapat%20informasi%20lebih%20lanjut%20dari%20Website%20Summarecon%20Serpong" class="float d-md-none d-lg-none d-xl-none" target="_blank" rel="noopener">
        <div class="hotline-phone-ring-circle-wa"></div>
        <div class="hotline-phone-ring-circle-fill-wa"></div>
        <i class="fab fa-whatsapp my-float"></i>
    </a>

    <!-- OLD LINK -->
    <!-- LOADING PAGE LOADER -->
    <script type="text/javascript">
        var baseURL = "{{ URL::to('') }}";
    </script>

    <!-- Theme Base, Components and Settings -->
    <script src="{{asset('assets/microsite/js/functions/google_map.js')}}"></script>
    <script src="{{asset('assets/microsite/vendor/sweetalert/sweetalert.min.js')}}" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        $(".popup").magnificPopup({
            type: "image",
            removalDelay: 160,
            preloader: false,
            fixedContentPos: true,
            gallery: {
            enabled: true
            }
        });
    });
    $(".owl-carousel").owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        dots: false,
        startPosition: 2,
        responsive: {
            0: {
            items: 1
            },
            600: {
            items: 1
            },
            1000: {
            items: 4
            }
        }
        });

        $(".form-contact").submit(function(event) {
            event.preventDefault();
            var formData = $(this).serializeArray();
            formData.push({
                name: "subject",
                value: "Contact Us LP Ruko Modern Serpong"
            });
            formData.push({
                name: "message",
                value: "Contact Us LP Ruko Modern Serpong"
            });
            var name = $(this).find("input[name='name']").val();
            var phone = $(this).find("input[name='phone']").val();
            var email = $(this).find("input[name='email']").val();
            var button = $("button", this);

            if ($.trim(name) == "") {
                swal({
                    title: "Error!",
                    text: "Name could not empty!",
                    type: "error",
                    confirmButtonText: "OK"
                });
                return;
            }
            if ($.trim(phone) == "") {
                swal({
                    title: "Error!",
                    text: "Phone could not empty!",
                    type: "error",
                    confirmButtonText: "OK"
                });
                return;
            }
            if ($.trim(email) == "") {
                swal({
                    title: "Error!",
                    text: "Email could not empty!",
                    type: "error",
                    confirmButtonText: "OK"
                });
                return;
            }

            ttq.identify({
                "email": email, // string. The email of the customer if available. It must be hashed with SHA-256 on the client side.
                "phone_number": phone, // string. The phone number of the customer if available. It must be hashed with SHA-256 on the client side.
                // "external_id": "<hashed_extenal_id>" // string. A unique ID from the advertiser such as user or external cookie IDs. It must be hashed with SHA256 on the client side.
            });
            ttq.track('SubmitForm', {
                "value": "Contact Us LP Ruko Modern Serpong", // number. Value of the order or items sold. Example: 100.
                "contents": [{
                    // "content_id": "<content_identifier>", // string. ID of the product. Example: "1077218".
                    "content_type": "rumah", // string. Either product or product_group.
                    "content_name": "Cluster Ardea", // string. The name of the page or product. Example: "shirt".
                }]
            });
            ttq.track('ViewContent', {
                "value": "Contact Us LP Ruko Modern Serpong", // number. Value of the order or items sold. Example: 100.
                "contents": [{
                    "content_id": "", // string. ID of the product. Example: "1077218".
                    "content_type": "rumah", // string. Either product or product_group.
                    "content_name": "Cluster Ardea", // string. The name of the page or product. Example: "shirt".
                    "brand": "Summarecon Serpong" // string. The brand name of the page or product. Example: "Nike"
                }]
            });

            var action = $(this).attr('action');
            var status = $(".form-status", this);
            $.ajax({
                url: action,
                type: 'post',
                data: formData,
                beforeSend: function() {
                    status.append('<p class="text-dark"><i class="fa fa-spinner fa-spin text-dark"></i> Email is sending...</p>').fadeIn();
                    // button.attr("disabled","disabled");
                },
                success: function(data) {
                    if (data.response == 'success') {
                        swal({
                            title: "Success!",
                            text: data.message,
                            type: "success",
                            confirmButtonText: "OK"
                        });
                        $(this).trigger("reset");

                        window.location.href = "{{URL::to(Library::getDomainPrefix().'/response_serpong/success')}}";
                        gtag('event', 'click', {
                            'event_category': 'SubmitFormLP',
                            'transport_type': 'beacon',
                            'event_action': 'click',
                            'event_label': 'RukoMaxwell',
                        });
                    } else {
                        swal({
                            title: "Error!",
                            text: data.message,
                            type: "error",
                            confirmButtonText: "OK"
                        });
                        status.html('');
                        button.removeAttr("disabled");
                    }
                }
            });
        });

        //FOR PROTECT NAME FORM
        $(".char-only").keypress(function(event) {
            var theEvent = event || window.event;
            var key = theEvent.keyCode || theEvent.which;
            if ((key < 97 || key > 122) && (key < 65 || key > 90) && !(key == 8 || key == 9 || key == 13 || key == 32 || key == 37 || key == 38 || key == 39 || key == 40 || key == 46)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        })


        //FOR PROTECT NUMBER PHONE FORM
        $(".number-only").keypress(function(event) {
            var theEvent = event || window.event;
            var key = theEvent.keyCode || theEvent.which;
            if ((key < 48 || key > 57) && !(key == 8 || key == 9 || key == 13 || key == 37 || key == 38 || key == 39 || key == 40 || key == 46)) {
                console.log(key)
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        })
    </script>
    <script type="text/javascript">
        (function() {
            var RFP = window.RFP = window.RFP || {}; RFP.ConvContext = RFP.ConvContext || {}; RFP.ConvContext.queue = RFP.ConvContext.queue || [];
            RFP.ConvContext.queue.push({
                "advertiser_id": 9579,
                "price": 0,
                "convtype": 0,
                "dat": ""
            });
            var el = document.createElement('script'); el.type = 'text/javascript'; el.async = true;
            el.src = 'https://js.rfp.fout.jp/rfp-conversion.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(el, s);
        })();
    </script>
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
	<script>
		(function(i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function() {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
			a = s.createElement(o),
				m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
		ga('create', 'UA-42299418-1', 'summareconserpong.com');
		ga('send', 'pageview');
	</script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-42299418-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-42299418-1');
	</script>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<link href="{{asset('assets/microsite/images/favicon.ico')}}" rel="shortcut icon">
	{!!SEO::generate()!!}

	<!-- BOOTSTRAP CORE CSS -->
	<link href="{{asset('assets/microsite/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('assets/microsite/vendor/fontawesome/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
	<link href="{{asset('assets/microsite/vendor/animateit/animate.min.css')}}" rel="stylesheet">
	<!-- VENDOR CSS -->
	<link href="{{asset('assets/microsite/vendor/owlcarousel/owl.carousel.css')}}" rel="stylesheet">
	<link href="{{asset('assets/microsite/vendor/magnific-popup/magnific-popup.css')}}" rel="stylesheet">
	<!-- TEMPLATE BASE -->
	<link href="{{asset('assets/microsite/css/theme-base.css')}}" rel="stylesheet">
	<!-- TEMPLATE ELEMENTS -->
	<link href="{{asset('assets/microsite/css/theme-elements.css')}}" rel="stylesheet">
	<!-- RESPONSIVE CLASSES-->
	<link href="{{asset('assets/microsite/css/responsive.css')}}" rel="stylesheet">
	<!-- LOAD GOOGLE FONTS -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,800,700,600%7CRaleway:100,300,600,700,800" rel="stylesheet" type="text/css" />
	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link href="{{asset('assets/microsite/vendor/rs-plugin/css/settings.css')}}" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
	<link href="{{asset('assets/microsite/css/rs-plugin-styles.css')}}" rel="stylesheet" type="text/css" />
	<!-- STYLE CSS -->
	<link href="{{asset('assets/microsite/css/style.css')}}" rel="stylesheet">
	<!-- STYLE CSS -->
	<link href="{{asset('assets/microsite/css/styleheader.css')}}" rel="stylesheet">
	<link href="{{asset('assets/microsite/vendor/sweetalert/sweetalert.css')}}" rel="stylesheet">
	<!--VENDOR SCRIPT-->
	<script src="{{asset('assets/microsite/vendor/jquery/jquery-1.11.2.min.js')}}"></script>
	<script src="{{asset('assets/microsite/vendor/plugins-compressed.js')}}"></script>
	<!-- GMAPS -->
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqgVIaPYgQ2NhYwOzDjSyMp7v47HdQnak">
	</script>
</head>

<body class="wide">

	<section class="hidden-xs hidden-sm" style="background-image:url({{asset('assets/microsite/images/thankyoupage/SUCCESSFORMEDDESKTOP.jpg')}});background-size: cover;background-position: center;height: 100%; position: absolute;">
		<div class="container-fullscreen">
			<div class="text-middle">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<!-- <img src="{{asset('assets/microsite/images/microsite/'.$status['thumb'])}}" class="img-responsive img-center"/>
						<h2 class="text-font text-center m-b-0">{{$status['statusHeader']}}</h2>
						<p class="text-center">{{$status['statusMessage']}}</p> -->
							<a class="btn btn-shadow btn-large btn-block btn-contact" style="position: relative;top: 200px; background-color: green; color:#fff; letter-spacing: 3px;" href="https://www.summareconserpong.com">Lihat Rumah Lainnya</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="hidden-md hidden-lg" style="background-image:url({{asset('assets/microsite/images/thankyoupage/SUCCESSFORMEDMOBILE.jpg')}});background-size: cover;background-position: center;height: 100%; position: absolute;">
		<div class="container-fullscreen">
			<div class="text-middle">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<!-- <img src="{{asset('assets/microsite/images/microsite/'.$status['thumb'])}}" class="img-responsive img-center"/>
						<h2 class="text-font text-center m-b-0">{{$status['statusHeader']}}</h2>
						<p class="text-center">{{$status['statusMessage']}}</p> -->
							<a class="btn btn-shadow btn-large btn-block btn-contact" style="position: relative;top: 220px; background-color: green; color:#fff; letter-spacing: 3px;" href="https://www.summareconserpong.com">Lihat Rumah Lainnya</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<script type="text/javascript">
		(function() {
			var RFP = window.RFP = window.RFP || {};
			RFP.ConvContext = RFP.ConvContext || {};
			RFP.ConvContext.queue = RFP.ConvContext.queue || [];
			RFP.ConvContext.queue.push({
				"advertiser_id": 7193,
				"price": 0,
				"convtype": 4,
				"dat": ""
			});
			var el = document.createElement('script');
			el.type = 'text/javascript';
			el.async = true;
			el.src = 'https://js.rfp.fout.jp/rfp-conversion.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(el, s);
		})();
	</script>

</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <link href="{{asset('assets/microsite/images/favicon.ico')}}" rel="shortcut icon">
    {!!SEO::generate()!!}

    <!-- BOOTSTRAP CORE CSS -->
    <link href="{{asset('assets/microsite/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/microsite/vendor/fontawesome/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('assets/microsite/vendor/animateit/animate.min.css')}}" rel="stylesheet">

    <!-- VENDOR CSS -->
    <link href="{{asset('assets/microsite/vendor/owlcarousel/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('assets/microsite/vendor/magnific-popup/magnific-popup.css')}}" rel="stylesheet">

    <!-- TEMPLATE BASE -->
    <link href="{{asset('assets/microsite/css/theme-base.css')}}" rel="stylesheet">

    <!-- TEMPLATE ELEMENTS -->
    <link href="{{asset('assets/microsite/css/theme-elements.css')}}" rel="stylesheet">

    <!-- RESPONSIVE CLASSES-->
    <link href="{{asset('assets/microsite/css/responsive.css')}}" rel="stylesheet">

    <!-- LOAD GOOGLE FONTS -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,800,700,600%7CRaleway:100,300,600,700,800" rel="stylesheet" type="text/css" />

    <!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
    <link href="{{asset('assets/microsite/vendor/rs-plugin/css/settings.css')}}" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
    <link href="{{asset('assets/microsite/css/rs-plugin-styles.css')}}" rel="stylesheet" type="text/css" />

    <!-- STYLE CSS -->
    <link href="{{asset('assets/microsite/css/style.css')}}" rel="stylesheet">

    <!-- STYLE CSS -->
    <link href="{{asset('assets/microsite/css/styleheader.css')}}" rel="stylesheet">
    <link href="{{asset('assets/microsite/vendor/sweetalert/sweetalert.css')}}" rel="stylesheet">
    <!--VENDOR SCRIPT-->
    <script src="{{asset('assets/microsite/vendor/jquery/jquery-1.11.2.min.js')}}"></script>
    <script src="{{asset('assets/microsite/vendor/plugins-compressed.js')}}"></script>
    <!-- GMAPS -->
    <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqgVIaPYgQ2NhYwOzDjSyMp7v47HdQnak">
    </script>
    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '2071176866234488');
      fbq('track', 'PageView');
    </script>

    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=2071176866234488&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-42299418-3"></script>

    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-42299418-3');
    </script>

    <!-- Global site tag (gtag.js) - Google Ads: 786753706 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-786753706"></script>
    <script>
     window.dataLayer = window.dataLayer || [];
     function gtag(){dataLayer.push(arguments);}
     gtag('js', new Date());

     gtag('config', 'AW-786753706');
    </script>
    <style>
        .grecaptcha-badge { visibility: hidden !important; }
    </style>
    {!! RecaptchaV3::initJs() !!}
</head>

<body class="wide">

    <!-- WRAPPER -->
    <div class="wrapper">

        <!-- Header -->
        <header id="header" class="header-logo-center  visible-lg visible-md">
            <div id="header-wrap">
                <div class="container">
                    <div id="logo">
                        <a href="{{URL::to(Library::getDomainPrefix())}}" class="logo" data-dark-logo="images/logo-dark.png">
                            <img src="{{asset('assets/microsite/images/microsite/logorainbow.png')}}" alt="Logo Rainbow">
                        </a>
                    </div>

                    <div id="mainMenu-trigger">
                        <button class="lines-button x"> <span class="lines"></span> </button>
                    </div>

                    <div id="mainMenu" class="light">
                        <div class="container">
                            <nav>
                                <ul>
                                    <li><a href="{{URL::to(Library::getDomainPrefix())}}">Home</a></li>
                                    <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix())}}#about">About</a></li>
                                    <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix())}}#concept">Concept</a></li>
                                    <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix())}}#type">Type</a></li>
                                </ul>
                                <ul>
                                    <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix())}}#siteplan">Siteplan</a></li>
                                    <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix().'/download-form/21/4536')}}">E-Brochure</a></li>
                                    <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix())}}#articles">Articles</a></li>
                                    <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix())}}#contact">Contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- END : Header -->

        <!-- Header Mobile -->
        <header id="header" class="header-logo-center visible-xs visible-sm">
            <div id="header-wrap">
                <div class="container">

                    <div id="logo">
                        <a href="{{URL::to(Library::getDomainPrefix())}}" class="logo" data-dark-logo="images/logo-dark.png">
                             <img src="{{asset('assets/microsite/images/microsite/logorainbow.png')}}" alt="Logo Rainbow">
                        </a>
                    </div>

                    <div class="nav-main-menu-responsive">
                        <button class="lines-button x">
                            <span class="lines"></span>
                        </button>
                    </div>

                    <div class="navbar-collapse collapse main-menu-collapse navigation-wrap">
                        <div class="container">
                            <nav id="mainMenu" class="main-menu mega-menu">
                                <ul class="main-menu nav nav-pills">
                                    <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix())}}"><i class="fa fa-home"></i></a></li>
                                    <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix())}}#about">About</a></li>
                                    <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix())}}#concept">Concept</a></li>
                                    <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix())}}#type">Type</a></li>
                                    <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix())}}#siteplan">Siteplan</a></li>
                                    <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix().'/download-form/21/4536')}}">E-Brochure</a></li>
                                    <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix())}}#articles">Articles</a></li>
                                    <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix())}}#contact">Contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- END: Header Mobile -->

        <!-- CONTENT -->
        <section class="content p-t-40 p-b-0">
            <div class="container">
                <div class="row">
                    <div class="post-content post-custom-detail col-md-9">
                        <h3 class="m-b-0">{{isset($article)?$article->name:''}}</h3>
                        <small><i class="fa fa-calendar"></i>&nbsp{{date('d F Y',strtotime($article->start_date))}}</small>
                        <div class="post-item m-t-20 m-b-20">
                            <div class="post-description">
                                <img alt="{{$article->name}}" src="{{IMAGE_URL.$article->thumb}}" class="img-responsive" class="m-b-20 p-b-20">
                                {!!isset($article)?$article->contents:''!!}
                            </div>
                            <h4>Share your thought or comment here!</h4>
                        	<div id="comments-container"></div>
                        </div>
                    </div>
                    @if(isset($others))
                    <!-- SIDEBAR LATEST NEWS-->
                    <div class="sidebar sidebar-modern sidebar-custom col-md-3">
                        <div class="widget clearfix widget-blog-articles">
                            <div class="font-line m-t-0 m-b-20">
                                <h4 class="text-uppercase text-bold">Latest Articles</h4>
                            </div>
                            <ul class="list-posts list-medium list-lines">
                                @foreach($others as $other)
                                <li>
                                    <a href="{{URL::to(Library::getDomainPrefix().'/'.$other->slug)}}">{{$other->name}}</a>
                                    <small class="m-t-10"><i class="fa fa-calendar"></i>&nbsp{{date('d F Y',strtotime($other->start_date))}}</small>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </section>
        <!-- END: CONTENT -->

        <!-- Footer -->
        <footer id="footer" style="background-color:#fff;">
            <div class="footer-content p-b-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-4">
                            <h3><i class="text-green">Contact Us</i></h3>
                        </div>
                        <div class="col-md-4">
                            <div class="widget clearfix widget-contact-us">
                                <h4 class="widget-title text-green text-uppercase m-b-10">Marketing Gallery</h4>
                                <ul class="list-large list-icons">
                                    <li class="p-l-0">
                                        <strong class="text-phone">(021) 546 6610</strong>
                                        <br>Summarecon Mal Serpong Lt 2
                                        <br>(sebelah cinema XXI)
                                        <br>Tangerang Selatan 15310
                                        <br>Indonesia
                                    </li>
                                </ul>

                                <h4 class="widget-title text-green text-uppercase m-b-0">Show Unit</h4>
                                <ul class="list-large list-icons">
                                    <li class="p-l-0">
                                        Jl. Gading Golf Boulevard
                                        <br>Tangerang Selatan 15332
                                        <br>Indonesia
                                        <br>
                                        <ul class="list-inline">
                                            <li class="p-l-0"><img src="{{asset('assets/microsite/images/microsite/icons8-google-maps-500.png')}}" style="width:40px; height:40px;">
                                            <li class="p-l-0"><span class="text-blue"><a href="https://goo.gl/maps/tbTRrFTTWQm" target="_blank">Open In Google Maps</a></span>
                                        </ul>
                                    </li>
                                </ul>

                                <div class="social-icons social-icons-border social-icons-rounded social-icons-colored-hover float-left m-t-20">
                                    <ul>
                                        <li class="social-facebook"><a href="https://www.facebook.com/summareconserpong/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                        <li class="social-instagram"><a href="https://www.instagram.com/summarecon_serpong/?hl=id" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                        <li class="social-youtube"><a href="https://www.youtube.com/channel/UColuLhICEhcPWh_kBqTvgUg /" target="_blank"><i class="fa fa-youtube"></i></a></li>
                                        <li class="social-twitter"><a href="https://twitter.com/summarecon_srpg" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-8" id="contact">
                            <form action="{{URL::to(Library::getDomainPrefix().'/contact-micro/action')}}" method="post" class="form-contact">
                                <input name="_token" value="{{csrf_token()}}" type="hidden">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="upper" for="name">First Name*</label>
                                            <input type="text" class="form-control required" name="name" placeholder="First Name" id="name" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="upper" for="name">Last Name</label>
                                            <input type="text" class="form-control required" name="last-name" placeholder="Last Name" id="lastname" aria-required="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="email">Email Address*</label>
                                            <input type="text" class="form-control required" name="email" id="email" aria-required="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="phone">Phone Number</label>
                                            <input type="text" class="form-control required" name="phone" id="phone" aria-required="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="upper" for="comment">Your Message</label>
                                            <textarea type="text" name="message" rows="5" class="form-control required" placeholder="Enter your Message"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="utm_source" id="utm_source" class="utm_source" value={{ app('request')->input('utm_source') }}>
                        <input type="hidden" name="utm_medium" id="utm_medium" class="utm_medium" value={{ app('request')->input('utm_medium') }}>
                        <input type="hidden" name="utm_campaign" id="utm_campaign" class="utm_campaign" value={{ app('request')->input('utm_campaign') }}>
                                <div class="row">
                                    <div class="col-md-12">
                                         <div class="form-group">
                                             {{-- {!! Recaptcha::render() !!} --}}
                                             {!! RecaptchaV3::field('contact') !!}
                                        </div>
                                        <div class="form-group text-left">
                                            <button class="btn btn-default btn-green" type="submit"><i class="fa fa-paper-plane"></i>&nbsp;Submit</button>
                                            <div class="form-status"></div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright-content p-b-0" style="background-color:#f1f1f1;">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-6 visible-xs visible-sm">
                            <img alt="" src="{{asset('assets/microsite/images/microsite/logoijo.png')}}" class="img-responsive m-t-20">
                        </div>
                        <div class="col-xs-6 visible-xs visible-sm">
                            <img alt="" src="{{asset('assets/microsite/images/logo-ss.png')}}" class="img-responsive">
                        </div>
                        <div class="col-md-2 visible-lg visible-md">
                            <img alt="" src="{{asset('assets/microsite/images/microsite/logoijo.png')}}" class="img-responsive m-t-20">
                        </div>
                        <div class="col-md-2 visible-lg visible-md">
                            <img alt="" src="{{asset('assets/microsite/images/logo-ss.png')}}" class="img-responsive">
                        </div>
                        <div class="col-md-8 ">
                            <h5 class="text-green text-uppercase">For More Info About Summarecon Serpong</h5>
                            <p><a href="http://summareconserpong.com/" target="_blank"><i class="fa fa-angle-right"></i> Summareconserpong.com</a></p>
                            <div class="copyright-text">© All rights reserved.Summarecon Serpong</div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row no-padding">
                        <div class="col-md-12 no-padding">
                            <img src="{{asset('assets/microsite/images/microsite/Acuan_bar_dan_logo_grup.jpg')}}" class="img-responsive" style="height:15px !important;">
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- END: Footer -->

    </div>
    <!-- END: Wrapper -->

    <!-- WA -->
    <div id="wa">
        <a href="https://api.whatsapp.com/send?phone=628111409008&text=Halo%20Saya%20tertarik%20dengan%20Summarecon%20Rainbow%20Condovillas%20ingin%20dapat%20informasi%20lebih%20lanjut%20" target="_blank">
            <img src="{{asset('assets/microsite/images/microsite/PNG-round-whatsapp.png')}}" border="0">
        </a>
    </div>
    <!-- END : WA -->

    <!-- GO TOP BUTTON -->
    <a class="gototop gototop-button" href="#"><i class="fa fa-chevron-up"></i></a>
    <!-- Small modal -->
    <div class="modal fade comments-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Please register to submit comment and then click send button</h3>
            </div>
            <div class="modal-body">
                <form action="{{URL::to(Library::getDomainPrefix().'/comment/register')}}" method="post" class="form-register-comment">
                    <input type="hidden" value="{{csrf_token()}}" name="_token">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="upper" for="name">Full Name*</label>
                                <input type="text" class="form-control required" name="commenter_name" placeholder="Full Name" id="name" aria-required="true">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="upper" for="email">Email Address*</label>
                                <input type="email" class="form-control required" name="commenter_email" id="email" placeholder="Email" aria-required="true">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group text-right">
                                <button class="btn btn-default btn-green btn-comment-register" type="submit"><i class="fa fa-paper-plane"></i>&nbsp;Register</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      </div>
    </div>
    <!-- SLIDER REVOLUTION 5.x SCRIPTS  -->
    <script type="text/javascript" src="{{asset('assets/microsite/vendor/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/microsite/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>


    <!-- LOADING PAGE LOADER -->
    <script type="text/javascript"> var baseURL = "{{ URL::to('') }}"; </script>

    <!-- Theme Base, Components and Settings -->
    <script src="{{asset('assets/microsite/js/theme-functions.js')}}"></script>
    <script src="{{asset('assets/microsite/js/functions/google_map.js')}}"></script>
    <script src="{{asset('assets/microsite/vendor/sweetalert/sweetalert.min.js')}}" defer></script>

    <link rel="stylesheet" type="text/css" href="{{asset('assets/microsite/vendor/viima-jquery-comments/css/jquery-comments.css')}}">
    <script type="text/javascript" src="{{asset('assets/microsite/vendor/viima-jquery-comments/js/jquery-comments.js')}}"></script>

    <script type="text/javascript">
    $(".form-contact").submit(function(event){
        event.preventDefault();
        var formData = $(this).serializeArray();
        formData.push({name:"subject", value:"Contact Us From Rainbow Springs CondoVilla"});
        formData.push({name:"message", value:"Contact Us From Rainbow Springs CondoVilla"});
        var name = $(this).find("input[name='name']").val();
        var phone = $(this).find("input[name='phone']").val();
        var email = $(this).find("input[name='email']").val();
        var button = $("button",this);

        if($.trim(name) == ""){
          swal({   title: "Error!",   text: "Name could not empty!",   type: "error",   confirmButtonText: "OK" });
          return;
        }
        if($.trim(phone) == ""){
          swal({   title: "Error!",   text: "Phone could not empty!",   type: "error",   confirmButtonText: "OK" });
          return;
        }
        if($.trim(email) == ""){
          swal({   title: "Error!",   text: "Email could not empty!",   type: "error",   confirmButtonText: "OK" });
          return;
        }

        var action = $(this).attr('action');
        var status = $(".form-status",this);
        $.ajax({
          url : action,
          type:'post',
          data:formData,
          beforeSend: function(){
            status.append('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn();
            // button.attr("disabled","disabled");
          },
          success:function(data){
            if(data.response == 'success'){
                swal({   title: "Success!",   text: data.message,   type: "success",   confirmButtonText: "OK" });
                $(this).trigger("reset");
                gtag('event','status_contact_form',{
                    'event_category':'CondoVilla Contact',
                    'event_label':'success',
                    'value':1
                });
                window.location.href="{{URL::to(Library::getDomainPrefix().'/response/success')}}";
            }
            else{
                swal({   title: "Error!",   text: data.message,   type: "error",   confirmButtonText: "OK" });
                status.html('');
                button.removeAttr("disabled");
                gtag('event','status_contact_form',{
                    'event_category':'CondoVilla Contact',
                    'event_label':'failed',
                    'value':0
                });
            }
          }
        });
      });
	var userComment = localStorage.getItem('userComment') || "";
    if(localStorage.getItem('userComment')!='')
        localStorage.setItem('userComment','');

    $('#comments-container').comments({
        profilePictureURL: "{{asset('assets/microsite/microsite/rainbow/images/chat.svg')}}",
        enableNavigation:false,
        enableUpvoting:false,
        getUsers: function(success, error) {
            $.ajax({
                url:"{{URL::to(Library::getDomainPrefix().'/comment/get-user-comments')}}",
                type:'GET',
                success:function(data){
                    console.log(data);
                    success(data);
                },
                error:error
            });
        },
        getComments: function(success, error) {
            // var commentsArray = [{
            //     id: 1,
            //     created: '2015-10-01',
            //     content: 'Lorem ipsum dolort sit amet',
            //     fullname: 'Simon Powell',
            //     upvote_count: 2,
            //     user_has_upvoted: false
            // },{
            //     id: 2,
            //     created: '2015-10-01',
            //     content: 'Lorem ipsum dolort sit amet',
            //     fullname: 'Simon Powell',
            //     upvote_count: 2,
            //     user_has_upvoted: false
            // }];
            // var commentsArray = [{
            //     id : 'c2',
            //     article_id:11159,
            //     parent:0,
            //     created:'2018-11-08 08:59:14.203',
            //     modified:'2018-11-08 08:59:14.203',
            //     content:'ini hanya tes saja',
            //     file : null,
            //     file_url:null,
            //     file_mime_type:null,
            //     fullname:'noval',
            //     profile_picture_url:'http://localhost/serpong/assets/microsite/rainbow/images/chat.svg',
            //     created_by_admin:null,
            //     created_by_current_user:true,
            //     upvote_count:0,
            //     user_has_upvoted:false
            // }];
            $.ajax({
                url:"{{URL::to(Library::getDomainPrefix().'/comment/get-all/'.$article->article_id)}}",
                type:'GET',
                success:function(data){
                    success(data);
                },
                error:error
            });
            // success(commentsArray);
        },
        postComment: function(data, success, error) {
            if(localStorage.getItem('userComment')==""){
                $('.comments-modal').modal('show');
                return true;
            };
            // success(data);
            $.ajax({
                url:"{{URL::to(Library::getDomainPrefix().'/comment/post/'.$article->article_id)}}?name=" + localStorage.getItem('userComment'),
                type:'POST',
                data:data,
                success:function(data){
                    success(data);
                },
                error:error
            });
        },
        upvoteComment: function(data, success, error) {
            setTimeout(function() {
                success(data);
            }, 500);
        },
    });
    $(".form-register-comment").validate({
      submitHandler: function(form) {
          jQuery(form).ajaxSubmit({
              success: function(data) {
                localStorage.setItem('userComment',data['fullname']);
                $('.comments-modal').modal('hide');
              },
          });
      }
    });
    </script>

</body>
</html>

<style type="text/css">
    @font-face {
        font-family: 'Plantin-Bold-Italic';
        src: url('.../Plantin-Bold-Italic.ttf');
        src: local('☺'), url('../Plantin-Bold-Italic.ttf') format('truetype');
        font-weight: normal;
        font-style: normal;
    }
    h1,h2,h3,h4,h5{font-family:Plantin-Bold-Italic !important; }

    p{line-height:26px;}
    /*#header.header-sticky:not(.header-static) #header-wrap,
    #header-wrap,
    #header #header-wrap #logo{
        height:100px;
    }
    #header{
        height:100px;
        line-height:100px;
    }
    #header #header-wrap #logo a > img{margin-top: 10px;}*/
    #mainMenu nav > ul > li > a,
    .text-green{
        color:#5b8442 !important;
    }
    .btn-green{
        background-color:#5b8442 !important;
        border-color:#5b8442 !important;
    }
    .background-green{background-color:#5b8442; }
    .text-phone{font-size:22px;}
    .copyright-content {border-top:0px solid #222;}
    .text-font{color:#4d793c !important;}
    .separator-font{
        width:30%;
        margin: 50px auto;
    }
    .separator-font::before, .separator-font::after {border-bottom: 4px solid #4d793c;}
    #footer ul li,
    #footer label{
        color:#6f6f6d;
    }
    .social-icons.social-icons-border li a {
        border: 1px solid #5b8442;
        background: #5b8442;
        color: #fff !important;
    }
    #footer .social-icons:not(.social-icons-colored) li a:hover {background-color: #faa61a;}
    .copyright-text {
        font-size: 13px;
        font-weight:600;
        text-transform: none;
        margin-bottom:15px;
    }
    /*#header.header-logo-center:not(.header-sticky) #mainMenu {float: none !important;}*/
    #mainMenu {float: none;}
    .btn-pdf{
        z-index: 1;
        position: absolute;
        bottom: 42%;
        right: 11.5%;
    }
    .text-pdf{
        z-index: 1;
        position: absolute;
        bottom:46%;
        right:10.5%;
        color: #5b8442;
    }
    #wa {
        position: fixed;
        right: 1%;
        top: 89%;
        z-index: 8;
        background-color: transparent;
    }
    .text-blue a{color:#2741a0;}
    .text-blue:hover{text-decoration:underline;}
    .btn-lg{ padding: 16px 26px;}
    a.btn-green:hover{background-color: #6a9d41 !important;}

    #header.header-sticky:not(.header-static) #header-wrap{height: 105px;}

    #book {
        background-color: rgba(255,255,255,0.7);
        border: 2px solid rgba(255,255,255,0.7);
        border-radius: 0px;
        padding: 30px;
        text-align: center;
        padding-top: 0px;
        margin-top:100px;
    }

    #book input {
        padding: 10px 20px;
        height: 40px;
        border: 1px solid #aaa;
    }

    .whatson-title .post-item .post-title h3 a{
        font-size: 20px;
        color:#5b8442;
    }

    .font-line {border-bottom: 3px solid #5b8442;}

    .carousel-description-style .owl-carousel .owl-controls .owl-nav .owl-prev,
    .carousel-description-style .owl-carousel .owl-controls .owl-nav .owl-next{
        background-color:#5b8442 !important;
    }

    .read-more-green i {color:#5b8442 !important;}
    .post-info{color: #696f6f;}

    @media (max-width: 767px) {
        .home,.introducing{
            padding-top:0 !important;
            padding-bottom:0px !important;
        }
        #about,#concept{
            padding-top:20px !important;
            padding-bottom:0px !important;
        }
        .btn.btn-lg {
            font-size: 10px;
            height: 40px;
            letter-spacing: 1px;
            line-height: 40px;
            padding: 0 15px;
        }
        .btn-pdf {
            bottom: 35%;
            right: 8%;
        }
        .text-pdf {
            bottom: 41%;
            right: 6.5%;
        }
        .footer-content{padding-top:20px;}
        .copyright-content h5{
            padding-top:10px;
            margin-bottom:5px;
        }
        .copyright-content{padding:20px 0;}
        .copyright-content p{margin-bottom:0;}

        nav .main-menu a{color:#5b8442 !important;}

        .lines, .lines::before, .lines::after {background-color: #5b8442;}
        .social-icons li {
            float: left !important;
            padding-right: 5px;
        }

        #book {margin-top: 10px;}
    }

</style>


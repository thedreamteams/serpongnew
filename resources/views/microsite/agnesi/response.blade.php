<!DOCTYPE html>
<html lang="en">

<head >
  	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '2071176866234488');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=2071176866234488&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-42299418-3"></script>
	<script>
	 window.dataLayer = window.dataLayer || [];
	 function gtag(){dataLayer.push(arguments);}
	 gtag('js', new Date());

	 gtag('config', 'UA-42299418-3');
	</script>
	<script>
		fbq('track', 'CompleteRegistration');
	</script>

	<!-- Global site tag (gtag.js) - Google Ads: 786753706 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-786753706"></script>
	<script>
	 window.dataLayer = window.dataLayer || [];
	 function gtag(){dataLayer.push(arguments);}
	 gtag('js', new Date());

	 gtag('config', 'AW-786753706');
	</script>

	@if(str_contains(url()->current(),'response/success') )  
	<!-- advertisingtransito@gmail.com  tracking tag -->
	<!-- Global site tag (gtag.js) - Google Ads: 755028096 --> 
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-755028096"></script> 
	<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-755028096'); </script>
	<!-- Event snippet for Summarecon Leads conversion page --> 
	<script> gtag('event', 'conversion', {'send_to': 'AW-755028096/IARACK_n0JcBEIChg-gC'}); </script>
	@endif

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	
	<link href="{{asset('assets/microsite/images/favicon.ico')}}" rel="shortcut icon">
	{!!SEO::generate()!!}

	<!-- BOOTSTRAP CORE CSS -->
	<link href="{{asset('assets/microsite/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('assets/microsite/vendor/fontawesome/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
	<link href="{{asset('assets/microsite/vendor/animateit/animate.min.css')}}" rel="stylesheet">

	<!-- TEMPLATE BASE -->
	<link href="{{asset('assets/microsite/css/theme-base.css')}}" rel="stylesheet">

	<!-- TEMPLATE ELEMENTS -->
	<link href="{{asset('assets/microsite/css/theme-elements.css')}}" rel="stylesheet">	
	
	<!-- RESPONSIVE CLASSES-->
	<link href="{{asset('assets/microsite/css/responsive.css')}}" rel="stylesheet">

	<!-- LOAD GOOGLE FONTS -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,800,700,600%7CRaleway:100,300,600,700,800" rel="stylesheet" type="text/css" />

	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link href="{{asset('assets/microsite/vendor/rs-plugin/css/settings.css')}}" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
	<link href="{{asset('assets/microsite/css/rs-plugin-styles.css')}}" rel="stylesheet" type="text/css" />

    <!-- STYLE CSS -->
	<link href="{{asset('assets/microsite/css/style.css')}}" rel="stylesheet">
	
	<!-- STYLE CSS -->
	<link href="{{asset('assets/microsite/css/styleheader.css')}}" rel="stylesheet">
    <!--VENDOR SCRIPT-->
    <script src="{{asset('assets/microsite/vendor/jquery/jquery-1.11.2.min.js')}}"></script>
    <script src="{{asset('assets/microsite/vendor/plugins-compressed.js')}}"></script>

</head>

<script>
 fbq('track', 'CompleteRegistration');
</script>

<!-- Event snippet for Complete Registration conversion page -->
<script>
 gtag('event', 'conversion', {'send_to': 'AW-786753706/JpqeCNTN7IsBEKrRk_cC'});
</script>

<body class="wide">
	
	<!-- WRAPPER -->
	<div class="wrapper">

		<!-- Header -->
     	<header id="header" class="header-logo-center  visible-lg visible-md">
            <div id="header-wrap">
                <div class="container">
                    <div id="logo">
                        <a href="{{URL::to(Library::getDomainPrefix())}}" class="logo" data-dark-logo="images/logo-dark.png">
                            <img src="{{asset('assets/microsite/images/microsite/logorainbow.png')}}" alt="Logo Rainbow">
                        </a>
                    </div>
                                                 
                    <div id="mainMenu-trigger">
                        <button class="lines-button x"> <span class="lines"></span> </button>
                    </div>
                   
                    <div id="mainMenu" class="light">
                        <div class="container">
                            <nav>                       
                                <ul>
                                    <li><a href="{{URL::to('/')}}">Home</a></li>
                                    <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix())}}#about">About</a></li>
                                    <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix())}}#concept">Concept</a></li>
                                    <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix())}}#type">Type</a></li>
                                </ul>                      
                                <ul>
                                    <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix())}}#siteplan">Siteplan</a></li>
                                     <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix().'/download-form/'.$project->project_id.'/4536')}}">E-Brochure</a></li>
                                    <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix())}}#contact">Contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- END : Header -->

        <!-- Header Mobile -->
		<header id="header" class="header-logo-center visible-xs visible-sm">
			<div id="header-wrap">
				<div class="container">

					<div id="logo">
						<a href="{{URL::to(Library::getDomainPrefix())}}" class="logo" data-dark-logo="images/logo-dark.png">
							 <img src="{{asset('assets/microsite/images/microsite/logorainbow.png')}}" alt="Logo Rainbow">
						</a>
					</div>
					
					<div class="nav-main-menu-responsive">
						<button class="lines-button x">
							<span class="lines"></span>
						</button>
					</div>
					
					<div class="navbar-collapse collapse main-menu-collapse navigation-wrap">
						<div class="container">
							<nav id="mainMenu" class="main-menu mega-menu">
								<ul class="main-menu nav nav-pills">
									<li><a class="scroll-to" href="{{URL::to('/')}}"><i class="fa fa-home"></i></a></li>
									<li><a class="scroll-to" href="#about">About</a></li>
									<li><a class="scroll-to" href="#concept">Concept</a></li>
									<li><a class="scroll-to" href="#type">Type</a></li>
									<li><a class="scroll-to" href="#siteplan">Siteplan</a></li>
                                    <li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix().'/download-form/'.$project->project_id.'/4536')}}">E-Brochure</a></li>
									<li><a class="scroll-to" href="#contact">Contact</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!-- END: Header Mobile -->

		<!-- About -->
		<section id="about" class="background-white p-t-50 p-b-50">
			<div class="container">
				<div class="row text-center">
					<div class="col-md-8 col-md-offset-2">
						<img src="{{asset('assets/microsite/images/microsite/'.$status['thumb'])}}" class="img-responsive img-center"/>
						<h2 class="text-font m-b-0">{{$status['statusHeader']}}</h2>
						<!-- <h4 class="m-b-30"><i class="text-font">A new home for a better way of living</i></h4> -->
						<p>{{$status['statusMessage']}}</p>
					</div>
				</div>
			</div>
		</section>
		<!-- END : About -->

		
		<!-- Footer -->
		<footer id="footer" style="background-color:#fff;position: absolute;width: 100%;bottom: 0;">
			<div class="copyright-content p-b-0" style="background-color:#f1f1f1;">
				<div class="container">
					<div class="row">
						<div class="col-xs-6 visible-xs visible-sm">
							<img alt="" src="{{asset('assets/microsite/images/microsite/logoijo.png')}}" class="img-responsive m-t-20">
						</div>
						<div class="col-xs-6 visible-xs visible-sm">
							<img alt="" src="{{asset('assets/microsite/images/logo-ss.png')}}" class="img-responsive">
						</div>
						<div class="col-md-2 visible-lg visible-md">
							<img alt="" src="{{asset('assets/microsite/images/microsite/logoijo.png')}}" class="img-responsive m-t-20">
						</div>
						<div class="col-md-2 visible-lg visible-md">
							<img alt="" src="{{asset('assets/microsite/images/logo-ss.png')}}" class="img-responsive">
						</div>
						<div class="col-md-8 ">
							<h5 class="text-green text-uppercase">For More Info About Summarecon Serpong</h5>
							<p><a href="http://summareconserpong.com/" target="_blank"><i class="fa fa-angle-right"></i> Summareconserpong.com</a></p>
							<div class="copyright-text">© All rights reserved.Summarecon Serpong</div>
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<div class="row no-padding">
						<div class="col-md-12 no-padding">
							<img src="{{asset('assets/microsite/images/microsite/Acuan_bar_dan_logo_grup.jpg')}}" class="img-responsive" style="height:15px !important;">
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- END: Footer -->

	</div>
	<!-- END: Wrapper -->

	<!-- WA -->
	<div id="wa">
		<a href="https://api.whatsapp.com/send?phone=628111409008&text=Halo%20Saya%20tertarik%20dengan%20Summarecon%20Rainbow%20Condovillas%20ingin%20dapat%20informasi%20lebih%20lanjut%20" target="_blank">
			<img src="{{asset('assets/microsite/images/microsite/PNG-round-whatsapp.png')}}" border="0">
		</a>
	</div>
	<!-- END : WA -->

	<!-- GO TOP BUTTON -->
	<a class="gototop gototop-button" href="#"><i class="fa fa-chevron-up"></i></a>
	
	<script type="text/javascript"> var baseURL = "{{ URL::to('') }}"; </script>
    
	<!-- Theme Base, Components and Settings -->
	<script src="{{asset('assets/microsite/js/theme-functions.js')}}"></script>
</body>
</html>

<style type="text/css">
	body{
		background-color: #fff !important;
	}
	.img-center {
	    display: block;
	    margin-left: auto;
	    margin-right: auto;
	    width: 20%;
	}
	@font-face {
	  	font-family: 'Plantin-Bold-Italic';
	  	src: url('.../Plantin-Bold-Italic.ttf');
	  	src: local('☺'), url('../Plantin-Bold-Italic.ttf') format('truetype');
	  	font-weight: normal;
	  	font-style: normal;
	}
 	h1,h2,h3,h4,h5{font-family:Plantin-Bold-Italic !important; }

	p{line-height:26px;}
	/*#header.header-sticky:not(.header-static) #header-wrap,
	#header-wrap,
	#header #header-wrap #logo{
		height:100px;
	}
	#header{
		height:100px;
		line-height:100px;
	}
	#header #header-wrap #logo a > img{margin-top: 10px;}*/
	#mainMenu nav > ul > li > a,
	.text-green{
		color:#5b8442 !important;
	}
	.btn-green{
		background-color:#5b8442 !important; 
		border-color:#5b8442 !important;  
	}
	.background-green{background-color:#5b8442; }
	.text-phone{font-size:22px;}
	.copyright-content {border-top:0px solid #222;}
	.text-font{color:#4d793c !important;}
	.separator-font{
		width:30%;
		margin: 50px auto;
	}
	.separator-font::before, .separator-font::after {border-bottom: 4px solid #4d793c;}
	footer{
		position:absolute;
	}
	#footer ul li,
	#footer label{
		color:#6f6f6d; 
	}
	.social-icons.social-icons-border li a {
    	border: 1px solid #5b8442;
    	background: #5b8442;
    	color: #fff !important;
    }
    #footer .social-icons:not(.social-icons-colored) li a:hover {background-color: #faa61a;}
    .copyright-text {
    	font-size: 13px;
    	font-weight:600;
    	text-transform: none;
    	margin-bottom:15px;
	}
	/*#header.header-logo-center:not(.header-sticky) #mainMenu {float: none !important;}*/
	#mainMenu {float: none;}
	.btn-pdf{
		z-index: 1;
    	position: absolute;
    	bottom: 42%;
    	right: 11.5%;
	}
	.text-pdf{
		z-index: 1;
    	position: absolute;
    	bottom:46%;
    	right:10.5%;
    	color: #5b8442;
	}
	#wa {
    	position: fixed;
    	right: 1%;
    	top: 89%;
    	z-index: 8;
    	background-color: transparent;
	}
	.text-blue a{color:#2741a0;}
	.text-blue:hover{text-decoration:underline;}
	.btn-lg{ padding: 16px 26px;}
	a.btn-green:hover{background-color: #6a9d41 !important;}

	#header.header-sticky:not(.header-static) #header-wrap{height: 105px;}

	@media (max-width: 767px) {
		.home,.introducing{
			padding-top:0 !important; 
			padding-bottom:0px !important;
		}
		#about,#concept{
			padding-top:20px !important;
			padding-bottom:0px !important;
		}
		.btn.btn-lg {
		    font-size: 10px;
		    height: 40px;
		    letter-spacing: 1px;
		    line-height: 40px;
		    padding: 0 15px;
		}
		.btn-pdf {
    		bottom: 35%;
    		right: 8%;
    	}
    	.text-pdf {
    		bottom: 41%;
    		right: 6.5%;
    	}
    	.footer-content{padding-top:20px;}
    	.copyright-content h5{
    		padding-top:10px;
    		margin-bottom:5px;
    	}
    	.copyright-content{padding:20px 0;}
    	.copyright-content p{margin-bottom:0;}

    	nav .main-menu a{color:#5b8442 !important;}

    	.lines, .lines::before, .lines::after {background-color: #5b8442;}
    	.social-icons li {
    		float: left !important;
    		padding-right: 5px;
    	}
	}

</style>

	
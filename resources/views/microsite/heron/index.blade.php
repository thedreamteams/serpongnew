<!DOCTYPE html>
<html lang="en">
<head>
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-42299418-1', 'summareconserpong.com');
        ga('send', 'pageview');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-42299418-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-42299418-1');
    </script>

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NQ8XHHG');
    </script>
    <!-- End Google Tag Manager -->

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='0'>
    <meta http-equiv='pragma' content='no-cache'>
    <link href="{{asset('assets/microsite/images/favicon.ico')}}" rel="shortcut icon">
    {!!SEO::generate()!!}
    <!-- OWL CAROUSEL & POP UP -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="{{asset('assets/microsite/vendor/magnific-popup/magnific-popup.css')}}" rel="stylesheet">
    <!-- STYLE CSS POP UP FORM NOTIFICATION -->
    <link href="{{asset('assets/microsite/css/styleheader.css')}}" rel="stylesheet">
    <link href="{{asset('assets/microsite/vendor/sweetalert/sweetalert.css')}}" rel="stylesheet">
    <!-- NEW LINK POLO -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
    <link href="{{asset('assets/css/plugins.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/custom.css')}}" rel="stylesheet"/>
    <!-- GMAPS -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqgVIaPYgQ2NhYwOzDjSyMp7v47HdQnak"></script>
    <!-- LOAD JQUERY LIBRARY -->
    {{-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script> --}}

    <!-- CandyPixel V3.7.1 -->
    <script>
        var CandyPixel4 = (function (s, u, g, a, r) {
            a = u.createElement("script");
            r = u.getElementsByTagName("script")[0];
            a.async = true;
            a.src = g;
            r.parentNode.insertBefore(a, r);

            return {
                config: {
                    debugMode: false,
                    token: "d4620087-83b4b929-1665972789573",
                    pid: "2711",    // deprecated
                    psid: "SRCP#41dbe5fb-9678-47a8-a197-31f264a45578",  // deprecated

                    // form class or ids to be ignored e.g. [".secretForms", "#loginForm"]
                    ignoreFormsWithAttributeValues: [],

                    // field class or ids to be ignored  e.g. [".secretField", "#password"]
                    ignoreFieldsWithAttributeValues: [],

                    submitAction: "submit", // click | submit
                    detectAdBlocker: null,  // set to true or false to override server-side setting
                    enableTracking: true,   // set to true or false to enableTracking

                    // feature flags:
                    ff_useLegacyRadioInputHandling: false, // set to true if you have trouble with radio form fields
                },
                app: {},
                onInit: function () {
                    console.log("onInit", this);
                    // this.app.setConfig(this.config);
                    // register callbacks here, if any

                },
                onReady: function () {
                    console.log("--------CandyPixel is Ready---------");
                    // this.app.getInfo();
                },
            }
        })(window, document, "https://s.cdy.ai/candypixel/app.v3.7.1.js");
    </script>
    <style>
        @font-face {
    		font-family: 'Conv_DIN-Medium 2_0';
    		src: url('../fonts/DIN-Medium 2_0.eot');
    		src: local('☺'), url('../fonts/DIN-Medium 2_0.woff') format('woff'), url('../fonts/DIN-Medium 2_0.ttf') format('truetype'), url('../fonts/DIN-Medium 2_0.svg') format('svg');
    		font-weight: normal;
    		font-style: normal;
    	}

    	@font-face {
    		font-family: 'Conv_DIN-Light';
    		src: url('../fonts/DIN-Light.eot');
    		src: local('☺'), url('../fonts/DIN-Light.woff') format('woff'), url('../fonts/DIN-Light.ttf') format('truetype'), url('../fonts/DIN-Light.svg') format('svg');
    		font-weight: normal;
    		font-style: normal;
    	}
    	h2{
            font-family: 'Conv_DIN-Medium 2_0' !important;
            line-height: 36px;
            color: #86ad3c;
        }
        #book {
            background-color: rgba(0,0,0,.6);
            border: 10px solid rgba(255,255,255,.2);
            border-radius: 0px;
            text-align: center;
            padding-top: 30px;
            padding-left: 30px;
            padding-right: 30px;
            width: 370px;
            position: absolute;
            z-index: 999;
            left: 65%;
            top: 0;
            height: 100%;
        }
        .img-logo{
            width: 160px;
            margin-bottom: 10px;
        }
        button.btn-light-custom{color: #fff !important;}
        .btn-light-custom{
            background: linear-gradient(90deg,  rgb(36, 179, 168) 0%, rgb(161, 197, 74) 100%);
            border-color: linear-gradient(90deg,  rgb(36, 179, 168) 0%, rgb(161, 197, 74) 100%);
            color: #fff;
        }
        .form-control-custom{
            border: 0;
    	    border-bottom: 1px solid #ced4da !important;
            border-radius: 0;
            padding: 10px 0 !important;
            min-height: 50px !important;
            color: #75C0CC !important;
            background-color: transparent;
        }
        .form-control-custom::placeholder {
            color: #fff;
            font-weight: 500;
            font-size:15px;
        }

        @media  only screen and (max-width: 768px ) {
            .p-b-0-m{
                padding-bottom: 0;
            }
            #book {
                background-color: rgba(197, 174, 143, 0.9);
                width: 100%;
                position: relative;
                left: 0%;
                top: 0;
                height: 100%;
            }
            #footer .social-icons {float: none !important;}
            .line-height-mobile{line-height:17px;}
            h2{line-height: 30px;}
        }
    </style>
    <style>
        .grecaptcha-badge { visibility: hidden !important; }
    </style>
    {!! RecaptchaV3::initJs() !!}
</head>

<body>

    <div class="body-inner">
        <section id="section1" class="fullscreen p-t-0 p-b-0 d-none d-lg-block d-xl-block" data-bg-image="{{IMAGE_URL.$banners[0]->image_1}}"></section>

        <section class="p-t-0 p-b-0 d-block d-sm-block d-md-block d-lg-none d-xl-none">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 no-padding">
                        <img src="{{IMAGE_URL.$banners[0]->image_2}}" class="img-responsive">
                    </div>
                </div>
            </div>
        </section>

        <div id="book" class="text-light">
            <form method="post" action="{{URL::to(Library::getDomainPrefix().'/contact-micro/action')}}" class="form-contact">
                <input name="_token" value="{{csrf_token()}}" type="hidden">
                <div class="text-center">
                    <img src="{{IMAGE_URL.$forms[0]->thumb}}" class="img img-responsive img-logo">
                </div>
                <div class="text-center">
                    <p>{!! $forms[0]->contents !!}</p>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group m-b-20">
                            <input aria-required="true" name="name" class="form-control form-control-custom char-only required" id="name" minlength="3" maxlength="50" placeholder="Fullname" type="text">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group m-b-20">
                            <input class="form-control form-control-custom number-only required" id="phone" name="phone" placeholder="Phone No" minlength="10" maxlength="15" type="tel" aria-required="true">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group m-b-20">
                            <input aria-required="true" name="email" id="email" class="form-control form-control-custom required" placeholder="Email" type="email">
                        </div>
                    </div>
                    <input type="hidden" name="utm_source" id="utm_source" class="utm_source" value={{ app('request')->input('utm_source') }}>
                    <input type="hidden" name="utm_medium" id="utm_medium" class="utm_medium" value={{ app('request')->input('utm_medium') }}>
                    <input type="hidden" name="utm_campaign" id="utm_campaign" class="utm_campaign" value={{ app('request')->input('utm_campaign') }}>
                    <div class="col-md-12">
                        <div class="form-group">
                            {{-- {!! Recaptcha::render() !!} --}}
                            {!! RecaptchaV3::field('contact') !!}
                        </div>
                        <div class="form-group text-left">
                            <button class="btn btn-lg btn-light-custom btn-form text-light" type="submit"><i class="fa fa-paper-plane"></i>&nbsp;Submit</button>
                            <div class="form-status"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <section class="p-b-0">
            <div class="container-fullscreen">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                @foreach ($articles as $article)
                                    <div class="col-lg-4 col-12">
                                        <h2 class="p-b-15 p-b-0-m"><div class="font-weight-500">{!!$article->name!!}</div></h2>
                                        {!!$article->contents!!}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- GALLERY -->
        <section class="p-t-20 p-b-0">
            <div class="row">
                <div class="col-lg-12">
                    <div class="carousel" data-items="3" data-dots="false" data-lightbox="gallery" data-margin="0" data-autoplay="false" data-autoplay-timeout="3600">
                        @for ($i = 0; $i < count($gallery); $i++)
                        <div class="portfolio-item img-zoom">
                            <div class="portfolio-item-wrap">
                                <div class="portfolio-image">
                                    <a href="#"><img src="{{$gallery[$i]}}" alt=""></a>
                                </div>
                                <div class="portfolio-description">
                                    <a title="Paper Pouch!" data-lightbox="gallery-image" href="{{$gallery[$i]}}" class="btn btn-light btn-rounded">Zoom</a>
                                </div>
                            </div>
                        </div>
                        @endfor
                    </div>
                </div>
            </div>
        </section>

        <section id="page-content" class="p-t-0 p-b-0">
            <div class="container-fluid">
                <!-- <div class="owl-carousel owl-theme">
                    @for ($i = 0; $i < count($gallery); $i++)
                    <div class="item">
                        <a href="{{$gallery[$i]}}" class="popup">
                            <img src="{{$gallery[$i]}}" alt="">
                        </a>
                    </div>
                    @endfor
                </div> -->
                <div class="row">
                    <div class="col-md-12 no-padding">
                        @if($video->url)
                        <iframe width="100%" height="500px" src="https://www.youtube.com/embed/{{$video->url}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        @else
                        <img class="img-responsive" src="{{IMAGE_URL.$video->thumb}}" alt="{{$video->name}}">
                        @endif
                    </div>
                </div>
            </div>
        </section>
        <!-- END : GALLERY -->

        <section class="p-t-0 p-b-0">
            <div class="container-fullscreen">
                <div class="row">
                    <div class="col-lg-12">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15863.738583762375!2d106.6441161!3d-6.2723246!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6a14c974b8604dbb!2sCluster%20Heron%20Summarecon%20Serpong!5e0!3m2!1sid!2sid!4v1666935283696!5m2!1sid!2sid" width="100%" height="450" style="border:0; margin-bottom: -7px;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>
            </div>
        </section>

        @include('layout.footer')

    </div>

    <script type="text/javascript" src="{{asset('assets/js/jquery-1.11.2.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins.js')}}"></script>
    <script src="{{asset('assets/js/functions.js')}}"></script>

    <!-- GO TOP BUTTON -->
    {{-- <a class="gototop gototop-button" href="#"><i class="fa fa-chevron-up"></i></a> --}}

    <!-- OLD LINK -->
    <!-- LOADING PAGE LOADER -->
    <script type="text/javascript">
        var baseURL = "{{ URL::to('') }}";
    </script>

    <!-- Theme Base, Components and Settings -->
    <script src="{{asset('assets/microsite/js/functions/google_map.js')}}"></script>
    <script src="{{asset('assets/microsite/vendor/sweetalert/sweetalert.min.js')}}" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        $(".popup").magnificPopup({
            type: "image",
            removalDelay: 160,
            preloader: false,
            fixedContentPos: true,
            gallery: {
            enabled: true
            }
        });
    });
    $(".owl-carousel").owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        dots: false,
        startPosition: 2,
        responsive: {
            0: {
            items: 1
            },
            600: {
            items: 1
            },
            1000: {
            items: 4
            }
        }
        });

        $(".form-contact").submit(function(event) {
            event.preventDefault();
            var formData = $(this).serializeArray();
            formData.push({
                name: "subject",
                value: "Contact Us LP Cluster Heron"
            });
            formData.push({
                name: "message",
                value: "Contact Us LP Cluster Heron"
            });
            var name = $(this).find("input[name='name']").val();
            var phone = $(this).find("input[name='phone']").val();
            var email = $(this).find("input[name='email']").val();
            var button = $("button", this);

            if ($.trim(name) == "") {
                swal({
                    title: "Error!",
                    text: "Name could not empty!",
                    type: "error",
                    confirmButtonText: "OK"
                });
                return;
            }
            if ($.trim(phone) == "") {
                swal({
                    title: "Error!",
                    text: "Phone could not empty!",
                    type: "error",
                    confirmButtonText: "OK"
                });
                return;
            }
            if ($.trim(email) == "") {
                swal({
                    title: "Error!",
                    text: "Email could not empty!",
                    type: "error",
                    confirmButtonText: "OK"
                });
                return;
            }

            var action = $(this).attr('action');
            var status = $(".form-status", this);
            $.ajax({
                url: action,
                type: 'post',
                data: formData,
                beforeSend: function() {
                    status.append('<p class="text-dark"><i class="fa fa-spinner fa-spin text-dark"></i> Email is sending...</p>').fadeIn();
                    // button.attr("disabled","disabled");
                },
                success: function(data) {
                    if (data.response == 'success') {
                        swal({
                            title: "Success!",
                            text: data.message,
                            type: "success",
                            confirmButtonText: "OK"
                        });
                        $(this).trigger("reset");

                        window.location.href = "{{URL::to(Library::getDomainPrefix().'/response_serpong/success')}}";
                        gtag('event', 'click', {
                            'event_category': 'SubmitFormLP',
                            'transport_type': 'beacon',
                            'event_action': 'click',
                            'event_label': 'RukoMaxwell',
                        });
                    } else {
                        swal({
                            title: "Error!",
                            text: data.message,
                            type: "error",
                            confirmButtonText: "OK"
                        });
                        status.html('');
                        button.removeAttr("disabled");
                    }
                }
            });
        });

        //FOR PROTECT NAME FORM
        $(".char-only").keypress(function(event) {
            var theEvent = event || window.event;
            var key = theEvent.keyCode || theEvent.which;
            if ((key < 97 || key > 122) && (key < 65 || key > 90) && !(key == 8 || key == 9 || key == 13 || key == 32 || key == 37 || key == 38 || key == 39 || key == 40 || key == 46)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        })


        //FOR PROTECT NUMBER PHONE FORM
        $(".number-only").keypress(function(event) {
            var theEvent = event || window.event;
            var key = theEvent.keyCode || theEvent.which;
            if ((key < 48 || key > 57) && !(key == 8 || key == 9 || key == 13 || key == 37 || key == 38 || key == 39 || key == 40 || key == 46)) {
                console.log(key)
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        })
    </script>
    <script type="text/javascript">
        (function() {
            var RFP = window.RFP = window.RFP || {}; RFP.ConvContext = RFP.ConvContext || {}; RFP.ConvContext.queue = RFP.ConvContext.queue || [];
            RFP.ConvContext.queue.push({
                "advertiser_id": 9579,
                "price": 0,
                "convtype": 0,
                "dat": ""
            });
            var el = document.createElement('script'); el.type = 'text/javascript'; el.async = true;
            el.src = 'https://js.rfp.fout.jp/rfp-conversion.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(el, s);
        })();
    </script>
</body>

</html>

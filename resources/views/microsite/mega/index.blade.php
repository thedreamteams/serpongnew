@extends('layout.layout')
@section('content')
<!-- FORM DEKSTOP -->
<section id="section-1" class="fullscreen p-t-0 p-b-0 background-mobile d-none d-lg-block d-xl-block" data-bg-image="{{IMAGE_URL.$banners[0]->image_1}}">
	<div class="container-fullscreen">
		<div class="container">
			<div class="row">
				<div class="col-md-4 offset-md-7 col-sm-12 col-lg-4 offset-lg-7">
					<div id="book" class="text-center m-t-0">
                        <div class="text-center">
                            <img class="img" alt="logoputih summarecon serpong" style="width:150px;" src="{{IMAGE_URL.$banners[1]->thumb}}">
                        </div>
						<p class="m-t-20 m-b-10 text-center text-dark">{!!$banners[1]->contents!!}</p>
						<form method="post" action="{{URL::to(Library::getDomainPrefix().'/contact-micro/action')}}" class="form-contact">
							<input name="_token" value="{{csrf_token()}}" type="hidden">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group m-b-10">
										<input aria-required="true" name="name" class="form-control char-only required" id="name" minlength="3" maxlength="50" placeholder="Fullname" type="text">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group m-b-10">
										<input class="form-control number-only required" id="phone" name="phone" placeholder="Phone No" minlength="10" maxlength="15" type="tel" aria-required="true">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group m-b-10">
										<input aria-required="true" name="email" id="email" class="form-control required" placeholder="Email" type="email">
									</div>
								</div>
								<input type="hidden" name="utm_source" id="utm_source" class="utm_source" value={{ app('request')->input('utm_source') }}>
								<input type="hidden" name="utm_medium" id="utm_medium" class="utm_medium" value={{ app('request')->input('utm_medium') }}>
								<input type="hidden" name="utm_campaign" id="utm_campaign" class="utm_campaign" value={{ app('request')->input('utm_campaign') }}>
								<div class="col-md-12">
									<div class="form-group">
										{{-- {!! Recaptcha::render() !!} --}}
                                        {!! RecaptchaV3::field('contact') !!}
									</div>
									<div class="form-group text-left">
										<button class="btn btn-red btn-form btn-block" type="submit"><i class="fa fa-paper-plane"></i>&nbsp;Submit</button>
										<div class="form-status"></div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- FORM MOBILE -->
<section id="section-2" class="d-block d-sm-block d-md-block d-lg-none d-xl-none p-t-0">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 no-padding">
				<img src="{{IMAGE_URL.$banners[0]->image_2}}" class="img-responsive">
			</div>
			<div class="col-xs-8" style="background-color: #CA1818">
				<div id="book">
					<div class="row" style="margin-left:-20px; margin-right: -20px;">
						<div class="col-md-12 no-padding">
							<img class="img-responsive img-center img-mobile" src="{{IMAGE_URL.$banners[1]->thumb}}">
						</div>
					</div>

					<p class="m-t-20 m-b-10 text-center text-light">{!!$banners[1]->contents!!}</p>
					<form method="post" action="{{URL::to(Library::getDomainPrefix().'/contact-micro/action')}}" class="form-contact">
						<input name="_token" value="{{csrf_token()}}" type="hidden">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group m-b-10">
									<input aria-required="true" name="name" class="form-control char-only required" id="name" minlength="3" maxlength="50" placeholder="Fullname" type="text">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group m-b-10">
									<input class="form-control number-only required" id="phone" name="phone" minlength="10" maxlength="15" placeholder="Phone No" type="tel" aria-required="true">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group m-b-10">
									<input aria-required="true" name="email" id="email" class="form-control required" placeholder="Email" type="email">
								</div>
							</div>
							<input type="hidden" name="utm_source" id="utm_source" class="utm_source" value={{ app('request')->input('utm_source') }}>
							<input type="hidden" name="utm_medium" id="utm_medium" class="utm_medium" value={{ app('request')->input('utm_medium') }}>
							<input type="hidden" name="utm_campaign" id="utm_campaign" class="utm_campaign" value={{ app('request')->input('utm_campaign') }}>
							<div class="col-md-12">
								<div class="form-group">
									{{-- {!! Recaptcha::render() !!} --}}
                                    {!! RecaptchaV3::field('contact') !!}
								</div>
								<div class="form-group text-left">
									<button class="btn btn-red btn-form btn-block" type="submit"><i class="fa fa-paper-plane"></i>&nbsp;Submit</button>
									<div class="form-status"></div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<!--  GET BROCHURE -->
<!-- <div class="jumbotron background-colored text-light m-b-0" style="background-color: #808080 !important">
    <div class="container">
        <h3 class="font-weight-300" style="letter-spacing: 1px;">LIHAT E-BROSUR RUKO MAXWELL</h3>
        <a class="button medium transparent rounded effect icon-left btn-brochure" href="https://online.flipbuilder.com/tkkk/kkqc/" target="_blank"><span><i class="fa fa-chevron-right"></i>See Now!</span></a>
    </div>
</div> -->

<div class="modal fade" id="modal" tabindex="-1" role="modal" aria-labelledby="modal-label" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="modal-label">Get Brochure</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<form id="form-brochure-online">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group m-b-10 col-md-12 col-sm-12">
										<input class="form-control char-only required" name="nama" placeholder="Name" id="nama" aria-required="true" type="text">
									</div>
									<div class="form-group m-b-10 col-md-12 col-sm-12">
										<input class="form-control number-only required" name="telepon" placeholder="Phone Number" id="telepon" aria-required="true" type="text">
									</div>
									<div class="form-group m-b-10 col-md-12 col-sm-12">
										<input class="form-control required" name="email" placeholder="Email Address" id="email" aria-required="true" type="text">
									</div>

									<!-- <input type="hidden" name="utm_source" id="utm_source" value={{ app('request')->input('utm_source') }}>
                                    <input type="hidden" name="utm_medium" id="utm_medium" value={{ app('request')->input('utm_medium') }}>
                                    <input type="hidden" name="utm_campaign" id="utm_campaign" value={{ app('request')->input('utm_campaign') }}> -->

									<div class="form-group m-b-10 col-md-12 col-sm-12">
										<button type="button" id="brochure-submit-button" class="btn btn-colored float-right btn-block" href="#">Get Brochure</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--  END : GET BROCHURE -->

<!-- ABOUT US -->
<section class="p-t-40 p-b-40 background-denah">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h2 style="letter-spacing: 1px;">{{$articles[0]->name}}</h2>
				<div class="text-justify">{!!$articles[0]->contents!!}</div>
			</div>
			<div class="col-md-4">
				<h2 style="letter-spacing: 1px;">{{$articles[1]->name}}</h2>
				<div class="text-justify">{!!$articles[1]->contents!!}</div>
			</div>
			<div class="col-md-4">
				<h2 style="letter-spacing: 1px;">{{$articles[2]->name}}</h2>
				<div class="text-justify" style="margin-left:25px;">{!!$articles[2]->contents!!}</div>
			</div>
		</div>
	</div>
</section>
<!-- END : ABOUT US -->

<!-- GALLERY -->
<section class="p-t-20 p-b-0">
    <div class="row">
        <div class="col-lg-12">
            <div class="carousel" data-items="2" data-dots="false" data-lightbox="gallery" data-margin="0" data-autoplay="false" data-autoplay-timeout="3600">
                @for ($i = 0; $i < count($gallery); $i++)
                <div class="portfolio-item img-zoom">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <a href="#"><img src="{{$gallery[$i]}}" alt=""></a>
                        </div>
                        <div class="portfolio-description">
                            <a title="Paper Pouch!" data-lightbox="gallery-image" href="{{$gallery[$i]}}" class="btn btn-light btn-rounded">Zoom</a>
                        </div>
                    </div>
                </div>
                @endfor
            </div>
        </div>
    </div>
</section>


{{--
 <section id="page-content" class="p-b-0">
    <div class="container-fluid no-padding">
        <div id="portfolio" class="grid-layout portfolio-3-columns" data-margin="0">
        	@for ($i = 0; $i < count($gallery); $i++)
            <div class="portfolio-item img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="{{$gallery[$i]}}"><img src="{{$gallery[$i]}}" alt="{{$gallery[$i]}}"></a>
                    </div>
                    <div class="portfolio-description">
                        <a title="" data-lightbox="image" href="{{$gallery[$i]}}"><i class="icon-maximize"></i></a>
                    </div>
                </div>
            </div>
            @endfor
        </div>

        <div id="portfolio" class="grid-layout portfolio-4-columns" data-margin="0">
        	@for ($i = 0; $i < count($siteplan); $i++)
            <div class="portfolio-item img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="{{$siteplan[$i]}}"><img src="{{$siteplan[$i]}}" alt="{{$siteplan[$i]}}"></a>
                    </div>
                    <div class="portfolio-description">
                        <a title="" data-lightbox="image" href="{{$siteplan[$i]}}"><i class="icon-maximize"></i></a>
                    </div>
                </div>
            </div>
            @endfor
        </div>

        <div class="row m-t-5">
	        <div class="col-md-12 no-padding">
	        	@if($video->url)
				<iframe width="100%" height="60%" src="https://www.youtube.com/embed/{{$video->url}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				@else
				<img class="img-responsive" src="{{IMAGE_URL.$video->thumb}}" alt="{{$video->name}}">
				@endif
	        </div>
	    </div> 
    </div>
</section>
--}}
<!-- END : GALLERY -->

{{--
<!--  MAP -->
<section class="no-padding">
	<div class="row">
		<div class="col-md-12 no-padding">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.9569161615327!2d106.6030594147693!3d-6.269396695462191!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69fd30ed06828d%3A0x2bd4e82977a764db!2sLeonora%20Summarecon%20Serpong%20%40%20Symphonia!5e0!3m2!1sid!2sid!4v1643945658446!5m2!1sid!2sid" width="100%" height="400px" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
		</div>
	</div>
</section>
<!-- END : MAP -->
--}}

@stop


@section('css')
<style type="text/css">
	@font-face {
		font-family: 'Conv_DIN-Medium 2_0';
		src: url('../fonts/DIN-Medium 2_0.eot');
		src: local('☺'), url('../fonts/DIN-Medium 2_0.woff') format('woff'), url('../fonts/DIN-Medium 2_0.ttf') format('truetype'), url('../fonts/DIN-Medium 2_0.svg') format('svg');
		font-weight: normal;
		font-style: normal;
	}
	@font-face {
		font-family: 'Conv_DIN-Light';
		src: url('../fonts/DIN-Light.eot');
		src: local('☺'), url('../fonts/DIN-Light.woff') format('woff'), url('../fonts/DIN-Light.ttf') format('truetype'), url('../fonts/DIN-Light.svg') format('svg');
		font-weight: normal;
		font-style: normal;
	}
	h2,
	h3,
	h4,
	h5 {
		font-family: 'Conv_DIN-Medium 2_0';
	}
	p,
	ul.list-icon li {
		font-size: 15px;
	}
	.background-footer {background: rgb(229, 224, 218) !important;}
	#book {
    	background-color: rgba(0,0,0,0.3);
    	border: 0;
    	padding: 30px;
    	padding-top: 15px;
    	padding-bottom: 15px;
    	border: 2px solid #7B0005;
	}
	.heading::after {
		border-top: 3px solid #A14041;
	}

	.btn-form {
		background-color: #D77A67;
		border-color: #CCCCCC;
		color: #d0aabd;
	}
	.btn-purple{
		background-color:#990891 !important;
		border-color: #CCCCCC !important;
		color:#fff !important;
	}
	.btn-red{
		background-color:#7B0005 !important;
		border-color: #CCCCCC !important;
		color:#fff !important;
	}
	.img-logo {
		text-align: center;
		display: block;
	}
	.img-footer {width: 170px;}
	.button-wa-footer .post-thumbnail-entry {
		float: right;
		width: 100%;
		background: #fff;
		border-radius: 50px;
		padding: 6px 15px;
	}
	.button-wa-footer .post-thumbnail-list .post-thumbnail-entry>img {
		float: right !important;
		height: auto !important;
		width: auto !important;
		margin-right: 0;
	}
	.post-thumbnail-list .post-thumbnail-entry img+.post-thumbnail-content {
		padding-left: 10px;
	}
	.button-wa-footer .post-thumbnail-entry .post-thumbnail-content a {
		font-size: 18px !important;
		margin-top: 15px !important;
		font-style: italic !important;
		margin-bottom: 5px !important;
		font-weight: 600 !important;
	}
	#footer a:not(.btn) {
		color: #558055 !important;
	}
	address {
		color: #484848;
	}
	input[type="text"],
	input[type="tel"],
	input[type="email"],
	textarea {
		background-color: #F3E4E1;
		border-radius: 5px;
		color: #d0aabd;
	}

	@media only screen and (min-width: 1920px) {
		#book {
			position: relative !important;
			float: right !important;
			right: -55% !important;
		}
	}
</style>
@stop

@section('js')
    <script>
        $(".form-contact").submit(function(event) {
            event.preventDefault();
            var formData = $(this).serializeArray();
            formData.push({
                name: "subject",
                value: "Contact Us LP Special Promo"
            });
            formData.push({
                name: "message",
                value: "Contact Us LP Special Promo"
            });
            var name = $(this).find("input[name='name']").val();
            var phone = $(this).find("input[name='phone']").val();
            var email = $(this).find("input[name='email']").val();
            var button = $("button", this);

            if ($.trim(name) == "") {
                swal({
                    title: "Error!",
                    text: "Name could not empty!",
                    type: "error",
                    confirmButtonText: "OK"
                });
                return;
            }
            if ($.trim(phone) == "") {
                swal({
                    title: "Error!",
                    text: "Phone could not empty!",
                    type: "error",
                    confirmButtonText: "OK"
                });
                return;
            }
            if ($.trim(email) == "") {
                swal({
                    title: "Error!",
                    text: "Email could not empty!",
                    type: "error",
                    confirmButtonText: "OK"
                });
                return;
            }

            var action = $(this).attr('action');
            var status = $(".form-status", this);
            $.ajax({
                url: action,
                type: 'post',
                data: formData,
                beforeSend: function() {
                    status.append('<p class="text-dark"><i class="fa fa-spinner fa-spin text-dark"></i> Email is sending...</p>').fadeIn();
                    // button.attr("disabled","disabled");
                },
                success: function(data) {
                    if (data.response == 'success') {
                        swal({
                            title: "Success!",
                            text: data.message,
                            type: "success",
                            confirmButtonText: "OK"
                        });
                        $(this).trigger("reset");

                        window.location.href = "{{URL::to(Library::getDomainPrefix().'/response_serpong/success')}}";
                        gtag('event', 'click', {
                            'event_category': 'SubmitFormLP',
                            'transport_type': 'beacon',
                            'event_action': 'click',
                            'event_label': 'RukoMaxwell',
                        });
                    } else {
                        swal({
                            title: "Error!",
                            text: data.message,
                            type: "error",
                            confirmButtonText: "OK"
                        });
                        status.html('');
                        button.removeAttr("disabled");
                    }
                }
            });
        });

        //FOR PROTECT NAME FORM
        $(".char-only").keypress(function(event) {
            var theEvent = event || window.event;
            var key = theEvent.keyCode || theEvent.which;
            if ((key < 97 || key > 122) && (key < 65 || key > 90) && !(key == 8 || key == 9 || key == 13 || key == 32 || key == 37 || key == 38 || key == 39 || key == 40 || key == 46)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        })


        //FOR PROTECT NUMBER PHONE FORM
        $(".number-only").keypress(function(event) {
            var theEvent = event || window.event;
            var key = theEvent.keyCode || theEvent.which;
            if ((key < 48 || key > 57) && !(key == 8 || key == 9 || key == 13 || key == 37 || key == 38 || key == 39 || key == 40 || key == 46)) {
                console.log(key)
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        })
    </script>
@endsection

@extends('layout.layout_ads')
@section('content')

<section class="hidden-xs hidden-sm" style="background-image:url({{asset('assets/microsite/images/thankyoupage/SUCCESSFORMEDDESKTOP.jpg')}});background-size: cover;background-position: center;height: 100%; position: absolute;">
	<div class="container-fullscreen">
		<div class="text-middle">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<!-- <img src="{{asset('assets/microsite/images/microsite/'.$status['thumb'])}}" class="img-responsive img-center"/>
						<h2 class="text-font text-center m-b-0">{{$status['statusHeader']}}</h2>
						<p class="text-center">{{$status['statusMessage']}}</p> -->
						<a class="btn btn-shadow btn-large btn-block btn-contact" style="position: relative;top: 200px; background-color: green; color:#fff; letter-spacing: 3px;" href="https://www.summareconserpong.com">Lihat Rumah Lainnya</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="hidden-md hidden-lg" style="background-image:url({{asset('assets/microsite/images/thankyoupage/SUCCESSFORMEDMOBILE.jpg')}});background-size: cover;background-position: center;height: 100%; position: absolute;">
	<div class="container-fullscreen">
		<div class="text-middle">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<!-- <img src="{{asset('assets/microsite/images/microsite/'.$status['thumb'])}}" class="img-responsive img-center"/>
						<h2 class="text-font text-center m-b-0">{{$status['statusHeader']}}</h2>
						<p class="text-center">{{$status['statusMessage']}}</p> -->
						<a class="btn btn-shadow btn-large btn-block btn-contact" style="position: relative;top: 220px; background-color: green; color:#fff; letter-spacing: 3px;" href="https://www.summareconserpong.com">Lihat Rumah Lainnya</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('js')
{{-- Freakout --}}
<script type="text/javascript">
    (function() {
        var RFP = window.RFP = window.RFP || {}; RFP.ConvContext = RFP.ConvContext || {}; RFP.ConvContext.queue = RFP.ConvContext.queue || [];
        RFP.ConvContext.queue.push({
            "advertiser_id": 7193,
            "price": 0,
            "convtype": 8,
            "dat": ""
        });
        var el = document.createElement('script'); el.type = 'text/javascript'; el.async = true;
        el.src = 'https://js.rfp.fout.jp/rfp-conversion.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(el, s);
    })();
</script>

-<!--
Event snippet for SERPONG on https://www.summareconserpong.com/response_serpong/success: Please do not remove.
Place this snippet on pages with events you’re tracking. 
Creation date: 03/17/2023
-->
<script>
  gtag('event', 'conversion', {
    'allow_custom_scripts': true,
    'send_to': 'DC-12599727/invmedia/serpo0+standard'
  });
</script>
<noscript>
<img src="https://ad.doubleclick.net/ddm/activity/src=12599727;type=invmedia;cat=serpo0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;gdpr=${GDPR};gdpr_consent=${GDPR_CONSENT_755};ord=1?" width="1" height="1" alt=""/>
</noscript>
<!-- End of event snippet: Please do not remove -->
@stop

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<link href="{{asset('assets/microsite/images/favicon.ico')}}" rel="shortcut icon">
	{!!SEO::generate()!!}
	<!-- BOOTSTRAP CORE CSS -->
	<link href="{{asset('assets/microsite/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('assets/microsite/vendor/fontawesome/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
	<link href="{{asset('assets/microsite/vendor/animateit/animate.min.css')}}" rel="stylesheet">
	<!-- VENDOR CSS -->
	<link href="{{asset('assets/microsite/vendor/owlcarousel/owl.carousel.css')}}" rel="stylesheet">
	<link href="{{asset('assets/microsite/vendor/magnific-popup/magnific-popup.css')}}" rel="stylesheet">
	<!-- TEMPLATE BASE -->
	<link href="{{asset('assets/microsite/css/theme-base.css')}}" rel="stylesheet">
	<!-- TEMPLATE ELEMENTS -->
	<link href="{{asset('assets/microsite/css/theme-elements.css')}}" rel="stylesheet">
	<!-- RESPONSIVE CLASSES-->
	<link href="{{asset('assets/microsite/css/responsive.css')}}" rel="stylesheet">
	<!-- LOAD GOOGLE FONTS -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,800,700,600%7CRaleway:100,300,600,700,800" rel="stylesheet" type="text/css" />
	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link href="{{asset('assets/microsite/vendor/rs-plugin/css/settings.css')}}" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
	<link href="{{asset('assets/microsite/css/rs-plugin-styles.css')}}" rel="stylesheet" type="text/css" />
	<!-- STYLE CSS -->
	<link href="{{asset('assets/microsite/css/style.css')}}" rel="stylesheet">
	<!-- STYLE CSS -->
	<link href="{{asset('assets/microsite/css/styleheader.min.css')}}" rel="stylesheet">
	<link href="{{asset('assets/microsite/vendor/sweetalert/sweetalert.css')}}" rel="stylesheet">
	<!-- STYLECONDOVILLA CSS -->
	<link href="{{asset('assets/microsite/css/stylecondovilla.min.css')}}" rel="stylesheet">
	<!--VENDOR SCRIPT-->
	<script src="{{asset('assets/microsite/vendor/jquery/jquery-1.11.2.min.js')}}"></script>
	<script src="{{asset('assets/microsite/vendor/plugins-compressed.js')}}"></script>
	<!-- GMAPS -->
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqgVIaPYgQ2NhYwOzDjSyMp7v47HdQnak">
	</script>
	<!-- Facebook Pixel Code -->
	<script>
		! function(f, b, e, v, n, t, s) {
			if (f.fbq) return;
			n = f.fbq = function() {
				n.callMethod ?
					n.callMethod.apply(n, arguments) : n.queue.push(arguments)
			};
			if (!f._fbq) f._fbq = n;
			n.push = n;
			n.loaded = !0;
			n.version = '2.0';
			n.queue = [];
			t = b.createElement(e);
			t.async = !0;
			t.src = v;
			s = b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t, s)
		}(window, document, 'script',
			'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '2071176866234488');
		fbq('track', 'PageView');
	</script>
	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-NQ8XHHG');
	</script>
	<!-- End Google Tag Manager -->
	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=2071176866234488&ev=PageView&noscript=1" />
	</noscript>
	<!-- End Facebook Pixel Code -->

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-42299418-3"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-42299418-3');
	</script>
    <style>
        .grecaptcha-badge { visibility: hidden !important; }
    </style>
    {!! RecaptchaV3::initJs() !!}
</head>

<body class="wide">

	<!-- WRAPPER -->
	<div class="wrapper">

		<!-- Header -->
		<header id="header" class="header-logo-center  visible-lg visible-md">
			<div id="header-wrap">
				<div class="container">
					<div id="logo">
						<a href="{{URL::to(Library::getDomainPrefix())}}" class="logo" data-dark-logo="images/logo-dark.png">
							<img src="{{asset('assets/microsite/images/microsite/logorainbow.png')}}" alt="Logo Rainbow">
						</a>
					</div>

					<div id="mainMenu-trigger">
						<button class="lines-button x"> <span class="lines"></span> </button>
					</div>

					<div id="mainMenu" class="light">
						<div class="container">
							<nav>
								<ul>
									<li><a href="{{URL::to('/')}}">Home</a></li>
									<li><a class="scroll-to" href="#about">About</a></li>
									<li><a class="scroll-to" href="#concept">Concept</a></li>
								</ul>
								<ul>
								<li><a class="scroll-to" href="#type">Type</a></li>
									<li><a class="scroll-to" href="#siteplan">Siteplan</a></li>
									<li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix().'/download-form/'.$project->project_id.'/'.$brochure->article_id)}}">E-Brochure</a></li>
									<!-- <li><a class="scroll-to" href="brochure">E-Brochure</a></li> -->
									<!-- <li><a class="scroll-to" href="#articles">Articles</a></li> -->
									<!-- <li><a class="scroll-to" href="#contact">Contact</a></li> -->
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!-- END : Header -->

		<!-- Header Mobile -->
		<header id="header" class="header-logo-center visible-xs visible-sm">
			<div id="header-wrap">
				<div class="container">

					<div id="logo">
						<a href="{{URL::to(Library::getDomainPrefix())}}" class="logo" data-dark-logo="images/logo-dark.png">
							<img src="{{asset('assets/microsite/images/microsite/logorainbow.png')}}" alt="Logo Rainbow">
						</a>
					</div>

					<div class="nav-main-menu-responsive">
						<button class="lines-button x">
							<span class="lines"></span>
						</button>
					</div>

					<div class="navbar-collapse collapse main-menu-collapse navigation-wrap">
						<div class="container">
							<nav id="mainMenu" class="main-menu mega-menu">
								<ul class="main-menu nav nav-pills">
									<li><a class="scroll-to" href="{{URL::to('/')}}"><i class="fa fa-home"></i></a></li>
									<li><a class="scroll-to" href="#about">About</a></li>
									<li><a class="scroll-to" href="#concept">Concept</a></li>
									<li><a class="scroll-to" href="#type">Type</a></li>
									<li><a class="scroll-to" href="#siteplan">Siteplan</a></li>
									<li><a class="scroll-to" href="{{URL::to(Library::getDomainPrefix().'/download-form/'.$project->project_id.'/'.$brochure->article_id)}}">E-Brochure</a></li>
									<!-- <li><a class="scroll-to" href="siteplan">E-Brochure</a></li> -->
									<!-- <li><a class="scroll-to" href="#articles">Articles</a></li> -->
									<!-- <li><a class="scroll-to" href="#contact">Contact</a></li> -->
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!-- END: Header Mobile -->

		<section class="p-t-0 p-b-0 hidden-lg hidden-md background-yellow">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 no-padding">
						<img src="{{IMAGE_URL.$banner->image_2}}" class="img-responsive">
						<div id="book">
							@if(isset($registrasi))
							<h4 class="m-b-0 m-t-20">{{$registrasi->name}}</h4>
							<div class="separator-font separator separator-custom-form"></div>
							<h5>{!!$registrasi->contents!!}</h5>
							@endif
							<form action="{{URL::to(Library::getDomainPrefix().'/contact-micro/action')}}" method="post" class="form-contact">
								<input name="_token" value="{{csrf_token()}}" type="hidden">

								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<input type="text" class="form-control char-only required" name="name" placeholder="Full Name" id="name" minlength="3" maxlength="50" aria-required="true">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<input type="email" class="form-control email required" name="email" id="email" aria-required="true" placeholder="Email">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<input type="text" class="form-control number-only required" name="phone" id="phone" aria-required="true" minlength="8" maxlength="15" placeholder="Phone">
										</div>
									</div>
								</div>
								<input type="hidden" name="utm_source" id="utm_source" class="utm_source" value={{ app('request')->input('utm_source') }}>
								<input type="hidden" name="utm_medium" id="utm_medium" class="utm_medium" value={{ app('request')->input('utm_medium') }}>
								<input type="hidden" name="utm_campaign" id="utm_campaign" class="utm_campaign" value={{ app('request')->input('utm_campaign') }}>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											{{-- {!! Recaptcha::render() !!} --}}
                                            {!! RecaptchaV3::field('contact') !!}
										</div>
										<div class="form-group text-left">
											<button class="btn btn-default btn-green btn-block" type="submit"><i class="fa fa-paper-plane"></i>&nbsp;Submit</button>
											<div class="form-status"></div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- Home -->
		<section class="home fullscreen parallax p-t-0 p-b-0 hidden-xs hidden-sm" style="background-image:url({{IMAGE_URL.$banner->image_1}});" data-stellar-background-ratio="0.1">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-xs-12"></div>
					<div class="col-md-4 col-md-offset-2 col-xs-12">
						<div id="book">
							@if(isset($registrasi))
							<h4 class="m-b-0 m-t-20">{{$registrasi->name}}</h4>
							<div class="separator-font separator separator-custom-form"></div>
							<h5>{!!$registrasi->contents!!}</h5>
							@endif
							<form action="{{URL::to(Library::getDomainPrefix().'/contact-micro/action')}}" method="post" class="form-contact">
								<input name="_token" value="{{csrf_token()}}" type="hidden">

								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<input type="text" class="form-control char-only required" name="name" placeholder="Full Name" id="name" minlength="3" maxlength="50" aria-required="true">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<input type="text" class="form-control email required" name="email" id="email" aria-required="true" placeholder="Email">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<input type="text" class="form-control number-only required" name="phone" id="phone" minlength="8" maxlength="15" aria-required="true" placeholder="Phone">
										</div>
									</div>
								</div>
								<input type="hidden" name="utm_source" id="utm_source" class="utm_source" value={{ app('request')->input('utm_source') }}>
								<input type="hidden" name="utm_medium" id="utm_medium" class="utm_medium" value={{ app('request')->input('utm_medium') }}>
								<input type="hidden" name="utm_campaign" id="utm_campaign" class="utm_campaign" value={{ app('request')->input('utm_campaign') }}>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											{{-- {!! Recaptcha::render() !!} --}}
                                            {!! RecaptchaV3::field('contact') !!}
										</div>
										<div class="form-group text-left">
											<button class="btn btn-default btn-green btn-block" type="submit"><i class="fa fa-paper-plane"></i>&nbsp;Submit</button>
											<div class="form-status"></div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- END : Home -->

		<!-- About -->
		<section id="about" class="background-white p-t-40 p-b-40">
			<div class="container">
				<div class="row text-center">
					<div class="col-md-8 col-md-offset-2">
						<h2 class="text-font m-b-0">{{$introducing->name}}</h2>
						<h4 class="m-b-30"><i class="text-font">{{$introducing->brief}}</i></h4>
						{!!$introducing->contents!!}
					</div>
				</div>
			</div>
		</section>
		<!-- END : About -->

		<!-- Introducing -->
		<section class="introducing fullscreen parallax" style="background-image:url({{IMAGE_URL.$paralax[0]->image_1}});" data-stellar-background-ratio="0.1"></section>
		<!-- END : Introducing -->

		<!-- Arsitek -->
		<section id="concept" class="background-white p-t-40 p-b-40">
			<div class="container">
				<div class="row text-center">
					<div class="col-md-8 col-md-offset-2">
						<h2 class="text-font m-b-0">{{$facilities->name}}</h2>
						{!!$facilities->contents!!}
					</div>
				</div>
			</div>
		</section>
		<!-- END : Arsitek -->

		<!-- Designed -->
		<section class="introducing fullscreen parallax" style="background-image:url({{IMAGE_URL.$paralax[1]->image_1}});" data-stellar-background-ratio="0.1">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 no-padding">
						<!-- <img src="{{asset('assets/microsite/images/microsite/designed.jpg')}}" class="img-responsive"> -->
					</div>
				</div>
			</div>
		</section>
		<!-- END : Designed -->

		<!-- Isi Designed -->
		<section class="background-white p-t-0 p-b-0">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 no-padding">
						<img src="{{asset('assets/microsite/images/microsite/ISI-01.png')}}" class="img-responsive">
					</div>
				</div>
			</div>
		</section>
		<!-- END : Isi Designed -->

		<!-- Activity -->
		<section class="fullscreen parallax" style="background-image:url({{IMAGE_URL.$paralax[2]->image_1}});" data-stellar-background-ratio="0.1">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 no-padding">
						<!-- <img src="{{asset('assets/microsite/images/microsite/activity.jpg')}}" class="img-responsive"> -->
					</div>
				</div>
			</div>
		</section>
		<!-- END : Activity -->
		@if(isset($news))
		<!-- ARTICLES-->
		<section class="p-t-40 p-b-0 padd-mobile" id="articles">
			<div class="container carousel-description-style">
				<h3 class="m-b-50 font-wight-600 text-uppercase text-center" style="color:#5b8442;letter-spacing: 6px;">Articles</h3>
				<div class="post-content carousel whatson-title" data-carousel-col="3" data-carousel-col-sm="2" data-carousel-arrow="false">


					@foreach($news as $new)
					<div class="post-item m-b-20" data-animation="fadeInUp" data-animation-delay="0">
						<div class="post-image image-box effect juna m-b-0">
							<a href="{{URL::to(Library::getDomainPrefix().'/'.$new->slug)}}">
								<img alt="{{$new->name}}" src="{{IMAGE_URL.$new->thumb}}" class="img-responsive">
								<div class="image-box-content">
									<p class="image-box-links">
										<i class="fa fa-search-plus fa-2x m-t-50"></i>
									</p>
								</div>
							</a>
						</div>
						<div class="post-content-details post-light-background background-white p-15" style="margin-top: 250px;">
							<div class="post-title">
								<h3 class="m-b-10"><a href="{{URL::to(Library::getDomainPrefix().'/'.$new->slug)}}">{{$new->name}}</a></h3>
								<p>{!!$new->brief!!}</p>
								<a class="read-more read-more-green" href="{{URL::to(Library::getDomainPrefix().'/'.$new->slug)}}">
									<div class="post-info"><i class="fa fa-chevron-right"></i> read more</div>
								</a>
							</div>
						</div>
					</div>
					@endforeach

				</div>
			</div>
		</section>
		<!-- END : ARTICLES-->
		@endif
		<!-- Isi Activity -->
		<section class="background-green p-t-0 p-b-0  hidden-lg hidden-xl">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 no-padding">
						<img src="{{IMAGE_URL.$facilitiesPic->image_2}}" class="img-responsive">
					</div>
				</div>
			</div>
		</section>

		<section class="background-green p-t-0 p-b-0  hidden-xs hidden-sm hidden-md">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 no-padding">
						<img src="{{IMAGE_URL.$facilitiesPic->image_1}}" class="img-responsive">
					</div>
				</div>
			</div>
		</section>

		<!-- END : Isi Activity -->

		<section class="fullscreen parallax" style="background-image:url({{IMAGE_URL.$paralax[3]->image_1}});" data-stellar-background-ratio="0.1">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 no-padding">
						<!-- <img src="{{asset('assets/microsite/images/microsite/activity.jpg')}}" class="img-responsive"> -->
					</div>
				</div>
			</div>
		</section>

		<!-- Type -->
		<section class="background-green p-t-0 p-b-0" id="type">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 no-padding">
						<img src="{{IMAGE_URL.$layout[0]->image_1}}" class="img-responsive">
					</div>
				</div>
			</div>
		</section>
		<!-- END : Type -->

		<!-- Siteplan -->
		<section class="background-green p-t-0 p-b-0" id="siteplan">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 no-padding">
						<img src="{{IMAGE_URL.$layout[1]->image_1}}" class="img-responsive">
					</div>
				</div>
			</div>
		</section>
		<!-- END : Siteplan -->
		<section class="fullscreen parallax" style="background-image:url({{IMAGE_URL.$paralax[4]->image_1}});" data-stellar-background-ratio="0.1">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 no-padding">
						<!-- <img src="{{asset('assets/microsite/images/microsite/activity.jpg')}}" class="img-responsive"> -->
					</div>
				</div>
			</div>
		</section>
		<!-- Footer -->
		<footer id="footer" style="background-color:#fff;">
			<div class="footer-content p-b-20">
				<div class="container">
					<div class="row">
						<!-- CODE BARU -->
						<!-- <div class="col-md-8 col-md-offset-2">
							<h3><i class="text-green">Contact Us</i></h3>
						</div> -->
						<div class="col-md-6">
							<div class="widget clearfix widget-contact-us">
								<h4 class="widget-title text-green text-uppercase m-b-10">{{$videocondovillas->name}}</h4>
								<div class="blog-images bg-white">
									<iframe width="100%" height="350" src="https://www.youtube.com/embed/{{$videocondovillas->url}}"></iframe>
								</div>
							</div>
						</div>

						<div class="col-md-3">
							<div class="widget clearfix widget-contact-us">
								<h4 class="widget-title text-green text-uppercase m-b-10">Marketing Gallery</h4>
								<ul class="list-large list-icons">
									<li class="p-l-0">
										<strong class="text-phone">0811 140 9008</strong>
										<br>Summarecon Mal Serpong Lt 2
										<br>(sebelah cinema XXI)
										<br>Tangerang Selatan 15310
										<br>Indonesia
									</li>
								</ul>


							</div>
						</div>
						<div class="col-md-3">
							<div class="widget clearfix widget-contact-us">


								<h4 class="widget-title text-green text-uppercase m-b-0">Show Unit</h4>
								<ul class="list-large list-icons">
									<li class="p-l-0">
										Jl. Gading Golf Boulevard
										<br>Tangerang Selatan 15332
										<br>Indonesia
										<br>
										<ul class="list-inline">
											<li class="p-l-0"><img src="{{asset('assets/microsite/images/microsite/icons8-google-maps-500.png')}}" style="width:40px; height:40px;">
											<li class="p-l-0"><span class="text-blue"><a href="https://goo.gl/maps/tbTRrFTTWQm" target="_blank">Open In Google Maps</a></span>
										</ul>
									</li>
								</ul>
							</div>
							<div class="social-icons social-icons-border social-icons-rounded social-icons-colored-hover float-left m-t-20">
								<ul>
									<li class="social-facebook"><a href="https://www.facebook.com/summareconserpong/" target="_blank"><i class="fa fa-facebook"></i></a></li>
									<li class="social-instagram"><a href="https://www.instagram.com/summarecon_serpong/?hl=id" target="_blank"><i class="fa fa-instagram"></i></a></li>
									<li class="social-youtube"><a href="https://www.youtube.com/channel/UColuLhICEhcPWh_kBqTvgUg /" target="_blank"><i class="fa fa-youtube"></i></a></li>
									<li class="social-twitter"><a href="https://twitter.com/summarecon_srpg" target="_blank"><i class="fa fa-twitter"></i></a></li>
								</ul>
							</div>
						</div>


						<!-- CODE LAMA -->
						<!-- <div class="col-md-8 col-md-offset-4">
							<h3><i class="text-green">Contact Us</i></h3>
						</div>
						<div class="col-md-4">
							<div class="widget clearfix widget-contact-us">
								<h4 class="widget-title text-green text-uppercase m-b-10">Marketing Gallery</h4>
								<ul class="list-large list-icons">
									<li class="p-l-0">
										<strong class="text-phone">0811 140 9008</strong>
										<br>Summarecon Mal Serpong Lt 2
										<br>(sebelah cinema XXI)
										<br>Tangerang Selatan 15310
										<br>Indonesia
									</li>
								</ul>

								<h4 class="widget-title text-green text-uppercase m-b-0">Show Unit</h4>
								<ul class="list-large list-icons">
									<li class="p-l-0">
										Jl. Gading Golf Boulevard
										<br>Tangerang Selatan 15332
										<br>Indonesia
										<br>
										<ul class="list-inline">
											<li class="p-l-0"><img src="{{asset('assets/microsite/images/microsite/icons8-google-maps-500.png')}}" style="width:40px; height:40px;">
											<li class="p-l-0"><span class="text-blue"><a href="https://goo.gl/maps/tbTRrFTTWQm" target="_blank">Open In Google Maps</a></span>
										</ul>
									</li>
								</ul>

								<div class="social-icons social-icons-border social-icons-rounded social-icons-colored-hover float-left m-t-20">
									<ul>
										<li class="social-facebook"><a href="https://www.facebook.com/summareconserpong/" target="_blank"><i class="fa fa-facebook"></i></a></li>
										<li class="social-instagram"><a href="https://www.instagram.com/summarecon_serpong/?hl=id" target="_blank"><i class="fa fa-instagram"></i></a></li>
										<li class="social-youtube"><a href="https://www.youtube.com/channel/UColuLhICEhcPWh_kBqTvgUg /" target="_blank"><i class="fa fa-youtube"></i></a></li>
										<li class="social-twitter"><a href="https://twitter.com/summarecon_srpg" target="_blank"><i class="fa fa-twitter"></i></a></li>
									</ul>
								</div>
							</div>
						</div>

						<div class="col-md-8" id="contact">
							<form action="{{URL::to(Library::getDomainPrefix().'/contact-micro/action')}}" method="post" class="form-contact">
								<input name="_token" value="{{csrf_token()}}" type="hidden">

								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="upper" for="name">First Name*</label>
											<input type="text" class="form-control required" name="name" placeholder="First Name" id="name" aria-required="true">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="upper" for="name">Last Name</label>
											<input type="text" class="form-control required" name="last-name" placeholder="Last Name" id="lastname" aria-required="true">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="upper" for="email">Email Address*</label>
											<input type="text" class="form-control required" name="email" id="email" aria-required="true">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="upper" for="phone">Phone Number</label>
											<input type="text" class="form-control required" name="phone" id="phone" aria-required="true">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
			                            <div class="form-group">
			                                <label class="upper" for="comment">Your Message</label>
			                                <textarea type="text" name="message" rows="5" class="form-control required" placeholder="Enter your Message"></textarea>
			                            </div>
			                        </div>
								</div>
								<div class="row">
									<div class="col-md-12">
                                        <div class="form-group">
                                            {{-- {!! Recaptcha::render() !!} --}}
                                            {!! RecaptchaV3::field('contact') !!}
				                        </div>
										<div class="form-group text-left">
											<button class="btn btn-default btn-green" type="submit"><i class="fa fa-paper-plane"></i>&nbsp;Submit</button>
											<div class="form-status"></div>
										</div>
									</div>
								</div>
							</form>
						</div> -->
					</div>

				</div>
			</div>
			<div class="copyright-content p-b-0" style="background-color:#f1f1f1;">
				<div class="container">
					<div class="row">
						<div class="col-xs-6 visible-xs visible-sm">
							<img alt="" src="{{asset('assets/microsite/images/microsite/logoijo.png')}}" class="img-responsive m-t-20">
						</div>
						<div class="col-xs-6 visible-xs visible-sm">
							<img alt="" src="{{asset('assets/microsite/images/logo-ss.png')}}" class="img-responsive">
						</div>
						<div class="col-md-2 visible-lg visible-md">
							<img alt="" src="{{asset('assets/microsite/images/microsite/logoijo.png')}}" class="img-responsive m-t-20">
						</div>
						<div class="col-md-2 visible-lg visible-md">
							<img alt="" src="{{asset('assets/microsite/images/logo-ss.png')}}" class="img-responsive">
						</div>
						<div class="col-md-8 ">
							<h5 class="text-green text-uppercase">For More Info About Summarecon Serpong</h5>
							<p><a href="http://summareconserpong.com/" target="_blank"><i class="fa fa-angle-right"></i> Summareconserpong.com</a></p>
							<div class="copyright-text">© All rights reserved.Summarecon Serpong</div>
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<div class="row no-padding">
						<div class="col-md-12 no-padding">
							<img src="{{asset('assets/microsite/images/microsite/Acuan_bar_dan_logo_grup.jpg')}}" class="img-responsive" style="height:15px !important;">
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- END: Footer -->

	</div>
	<!-- END: Wrapper -->

	<!-- WA -->
	<!-- <div id="wa">
		<a href="https://api.whatsapp.com/send?phone=628111409008&text=Halo%20Saya%20tertarik%20dengan%20Summarecon%20Rainbow%20Condovillas%20ingin%20dapat%20informasi%20lebih%20lanjut%20" target="_blank">
			<img src="{{asset('assets/microsite/images/microsite/PNG-round-whatsapp.png')}}" border="0">
		</a>
	</div> -->
	<!-- END : WA -->

	<!-- GO TOP BUTTON -->
	<a class="gototop gototop-button" href="#"><i class="fa fa-chevron-up"></i></a>

	<!-- SLIDER REVOLUTION 5.x SCRIPTS  -->
	<script type="text/javascript" src="{{asset('assets/microsite/vendor/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/microsite/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>

	<!-- LOADING PAGE LOADER -->
	<script type="text/javascript">
		var baseURL = "{{ URL::to('') }}";
	</script>

	<!-- Theme Base, Components and Settings -->
	<script src="{{asset('assets/microsite/js/theme-functions.js')}}"></script>
	<script src="{{asset('assets/microsite/js/functions/google_map.js')}}"></script>
	<script src="{{asset('assets/microsite/vendor/sweetalert/sweetalert.min.js')}}" defer></script>
	<script type="text/javascript">
		$(".form-contact").submit(function(event) {
			event.preventDefault();
			var formData = $(this).serializeArray();
			formData.push({
				name: "subject",
				value: "Contact Us From Rainbow Springs CondoVilla"
			});
			formData.push({
				name: "message",
				value: "Contact Us From Rainbow Springs CondoVilla"
			});
			var name = $(this).find("input[name='name']").val();
			var phone = $(this).find("input[name='phone']").val();
			var email = $(this).find("input[name='email']").val();
			var button = $("button", this);

			if ($.trim(name) == "") {
				swal({
					title: "Error!",
					text: "Name could not empty!",
					type: "error",
					confirmButtonText: "OK"
				});
				return;
			}
			if ($.trim(phone) == "") {
				swal({
					title: "Error!",
					text: "Phone could not empty!",
					type: "error",
					confirmButtonText: "OK"
				});
				return;
			}
			if ($.trim(email) == "") {
				swal({
					title: "Error!",
					text: "Email could not empty!",
					type: "error",
					confirmButtonText: "OK"
				});
				return;
			}

			var action = $(this).attr('action');
			var status = $(".form-status", this);
			$.ajax({
				url: action,
				type: 'post',
				data: formData,
				beforeSend: function() {
					status.append('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn();
					// button.attr("disabled","disabled");
				},
				success: function(data) {
					if (data.response == 'success') {
						swal({
							title: "Success!",
							text: data.message,
							type: "success",
							confirmButtonText: "OK"
						});
						$(this).trigger("reset");
						gtag('event', 'status_contact_form', {
							'event_category': 'CondoVilla Contact',
							'event_label': 'success',
							'value': 1
						});
						window.location.href = "{{URL::to(Library::getDomainPrefix().'/response/success')}}";
					} else {
						swal({
							title: "Error!",
							text: data.message,
							type: "error",
							confirmButtonText: "OK"
						});
						status.html('');
						button.removeAttr("disabled");
						gtag('event', 'status_contact_form', {
							'event_category': 'CondoVilla Contact',
							'event_label': 'failed',
							'value': 0
						});
					}
				}
			});
		});

		//FOR PROTECT NAME FORM
		$(".char-only").keypress(function(event) {
			var theEvent = event || window.event;
			var key = theEvent.keyCode || theEvent.which;
			if ((key < 97 || key > 122) && (key < 65 || key > 90) && !(key == 8 || key == 9 || key == 13 || key == 32 || key == 37 || key == 38 || key == 39 || key == 40 || key == 46)) {
				theEvent.returnValue = false;
				if (theEvent.preventDefault) theEvent.preventDefault();
			}
		})


		//FOR PROTECT NUMBER PHONE FORM
		$(".number-only").keypress(function(event) {
			var theEvent = event || window.event;
			var key = theEvent.keyCode || theEvent.which;
			if ((key < 48 || key > 57) && !(key == 8 || key == 9 || key == 13 || key == 37 || key == 38 || key == 39 || key == 40 || key == 46)) {
				console.log(key)
				theEvent.returnValue = false;
				if (theEvent.preventDefault) theEvent.preventDefault();
			}
		})
	</script>

</body>

</html>

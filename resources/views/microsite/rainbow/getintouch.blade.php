@extends('layout.layout')
@section('content')

<section class="p-t-0 p-b-0 fullscreen" style="background-image:url({{asset('assets/microsite/images/microsite/Smart-dekstop.jpg')}}); background-size: cover; background-repeat: no-repeat;">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-6 background-mobile">
				<div id="book">
					<form method="post" action="{{URL::to(Library::getDomainPrefix().'/contact-micro/action')}}" class="form-contact">
						<input name="_token" value="{{csrf_token()}}" type="hidden">
						<div class="row">
							<div class="col-md-12 text-light text-center">
								<h3>New Cluster Agnesi<br>Summarecon Serpong</h3>
								<div class="separator-font separator"></div>
								<p>Cluster Terbaru Agnesi Symphonia Summarecon Serpong.<br>Dapatkan Promo Voucher IKEA hingga 15 juta.<br>Isikan data disini untuk informasi lebih detail New Cluster Agnesi @Symphonia</p>
							</div>
							<div class="col-md-12">
								<div class="form-group m-b-20">
									<input aria-required="true" name="name" class="form-control required" id="name" placeholder="Fullname" type="text">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group m-b-20">
									<input class="form-control required" id="phone" name="phone" placeholder="Phone No" type="text" aria-required="true">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group m-b-20">
									<input aria-required="true" name="email" id="email" class="form-control required" placeholder="Email" type="text">
								</div>
							</div>
							<input type="hidden" name="utm_source" id="utm_source" class="utm_source" value={{ app('request')->input('utm_source') }}>
                        <input type="hidden" name="utm_medium" id="utm_medium" class="utm_medium" value={{ app('request')->input('utm_medium') }}>
                        <input type="hidden" name="utm_campaign" id="utm_campaign" class="utm_campaign" value={{ app('request')->input('utm_campaign') }}>
							<div class="col-md-12">
								<div class="form-group">
								{{-- {!! Recaptcha::render() !!} --}}
                                {!! RecaptchaV3::field('contact') !!}
							</div>
							<div class="form-group text-left">
								<button class="btn btn-default btn-orange btn-block" type="submit"><i class="fa fa-paper-plane"></i>&nbsp;Submit</button>
								<div class="form-status"></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="row background-mobile">
			<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 p-b-20 m-t-10">
				<div class="social-icons social-icons-border social-icons-rounded social-icons-colored-hover float-left m-t-30">
					<ul>
						<li><h4 class="m-l-10 m-r-10 m-t-5">Connect With Us</h4></li>
						<li class="social-facebook m-r-5"><a href="https://www.facebook.com/summareconserpong/" target="_blank"><i class="fa fa-facebook"></i></a></li>
						<li class="social-instagram m-r-5"><a href="https://www.instagram.com/summarecon_serpong/?hl=id" target="_blank"><i class="fa fa-instagram"></i></a></li>
						<li class="social-youtube m-r-5"><a href="https://www.youtube.com/channel/UColuLhICEhcPWh_kBqTvgUg/featured" target="_blank"><i class="fa fa-youtube"></i></a></li>
						<li class="social-twitter m-r-5"><a href="https://twitter.com/summarecon_srpg" target="_blank"><i class="fa fa-twitter"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('js')
<script type="text/javascript">
$(".form-contact").submit(function(event){
    event.preventDefault();
    var formData = $(this).serializeArray();
    formData.push({name:"subject", value:"Contact Us From Get In Touch"});
    formData.push({name:"message", value:"Contact Us From Get In Touch"});
    var name = $(this).find("input[name='name']").val();
    var phone = $(this).find("input[name='phone']").val();
    var email = $(this).find("input[name='email']").val();
    var button = $("button",this);

    if($.trim(name) == ""){
      swal({   title: "Error!",   text: "Name could not empty!",   type: "error",   confirmButtonText: "OK" });
      return;
    }
    if($.trim(phone) == ""){
      swal({   title: "Error!",   text: "Phone could not empty!",   type: "error",   confirmButtonText: "OK" });
      return;
    }
    if($.trim(email) == ""){
      swal({   title: "Error!",   text: "Email could not empty!",   type: "error",   confirmButtonText: "OK" });
      return;
    }

    var action = $(this).attr('action');
    var status = $(".form-status",this);
    $.ajax({
      url : action,
      type:'post',
      data:formData,
      beforeSend: function(){
        status.append('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn();
        // button.attr("disabled","disabled");
      },
      success:function(data){
      	if(data.response == 'success'){
	        swal({   title: "Success!",   text: data.message,   type: "success",   confirmButtonText: "OK" });
	        $(this).trigger("reset");

	        window.location.href="{{URL::to(Library::getDomainPrefix().'/response_serpong/success')}}";
	    }
	    else{
	    	swal({   title: "Error!",   text: data.message,   type: "error",   confirmButtonText: "OK" });
	        status.html('');
	        button.removeAttr("disabled");
    	}
      }
    });
  });
</script>
@stop

@section('css')
<style type="text/css">
	@font-face {
	  font-family: 'Montserrat-Regular';
	  src: url('../Montserrat-Regular.otf');
	  src: local('☺'), url('../Montserrat-Regular.otf') format('truetype');
	  font-weight: normal;
	  font-style: normal;
	}
	h1,h2,h3,h4,h5{font-family: 'Montserrat-Regular';}
	#book {
	  border:0px solid rgba(255, 255, 255, 0.2);
	  border-radius: 3px;
	  padding: 30px;
	  background-color:rgba(228,175,55,0.9);
	}
	.social-icons.social-icons-border li a {
	  border: 1px solid #046d19;
	  background: #046d19;
	  color:#fff;
	}
	.social-icons h4{color:#fff !important;}
	.separator-font{
	  width:30%;
	  margin: 15px auto;
	}
	.separator-font::before, .separator-font::after {border-bottom: 4px solid #fff;}
	.btn-orange{
	  background-color: #046d19;
	  border: 1px solid #046d19;
	}
	@media (min-width: 320px) and (max-width: 767px) {
		.wide{background-image: none !important;}
		.section{padding-top:0 !important;}
		.container {padding: 0 15px;}
		.background-gren{background-color: #b4cfc0;}
		.social-icons{float:none !important;}
		.social-icons li {float: left;}
		.background-mobile{background-color:#f9e084;}
	}
	@media (min-width:768px) and (max-width: 991px){
		.container{padding:0px;}
		.wide{background-image: none !important;}
		.section{padding-top:0 !important;}
		.background-gren{background-color: #b4cfc0;}
		.col-md-4{width:100%;}
		.social-icons li {float: left;}
		.background-mobile{background-color:#f9e084;}
	}
</style>
@stop






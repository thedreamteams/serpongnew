<!DOCTYPE html>
<html lang="en">
<head>
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-42299418-1', 'summareconserpong.com');
        ga('send', 'pageview');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-42299418-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-42299418-1');
    </script>

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NQ8XHHG');
    </script>
    <!-- End Google Tag Manager -->

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='0'>
    <meta http-equiv='pragma' content='no-cache'>
    <link href="{{asset('assets/microsite/images/favicon.ico')}}" rel="shortcut icon">
    {{-- <!-- {!!SEO::generate()!!} --> --}}
    <title>Cluster Strozzi Rumah Cluster Baru Di Summarecon Serpong</title>
    <meta name="description" content="Cluster Strozzi mempersembahkan dinamisnya Work Life Balance antara keluarga dan pribadi dalam sebuah harmoni kehidupan yang sehat dekat dengan alam">
    <meta name="keywords" content="summarecon serpong, cluster strozzi, cluster baru serpong, cluster baru summarecon serpong, rumah baru serpong, rumah baru summarecon serpong, rumah cluster serpong, rumah cluster summarecon serpong">
    <meta property="og:title" content="Summarecon Serpong" />
    <!-- OWL CAROUSEL & POP UP -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="{{asset('assets/microsite/vendor/magnific-popup/magnific-popup.css')}}" rel="stylesheet">
    <!-- STYLE CSS POP UP FORM NOTIFICATION -->
    <link href="{{asset('assets/microsite/css/styleheader.css')}}" rel="stylesheet">
    <link href="{{asset('assets/microsite/vendor/sweetalert/sweetalert.css')}}" rel="stylesheet">
    <!-- NEW LINK POLO -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
    <link href="{{asset('assets/css/plugins.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/custom.css')}}" rel="stylesheet"/>
    <!-- GMAPS -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqgVIaPYgQ2NhYwOzDjSyMp7v47HdQnak"></script>
    <!-- LOAD JQUERY LIBRARY -->
    {{-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script> --}}

    <!-- CandyPixel V3.7.1 -->

    <!--
    Warning:   CandyPixel v3.7.0 onwards is incompatible with snippets and custom code
            associated with previous versions of CandyPixel.

            All MSIE browsers are no longer supported.
    -->
    <script>
    var CandyPixel4 = (function (s, u, g, a, r) {
        a = u.createElement("script");
        r = u.getElementsByTagName("script")[0];
        a.async = true;
        a.src = g;
        r.parentNode.insertBefore(a, r);

        return {
            config: {
                debugMode: false,
                token: "d4620087-2966586b-1683876273937",
                pid: "2835",    // deprecated
                psid: "SRCP#280fa3f7-78a7-4937-a366-0c2478148ecd",  // deprecated

                // form class or ids to be ignored e.g. [".secretForms", "#loginForm"]
                ignoreFormsWithAttributeValues: [],

                // field class or ids to be ignored  e.g. [".secretField", "#password"]
                ignoreFieldsWithAttributeValues: [],

                submitAction: "submit", // click | submit
                detectAdBlocker: null,  // set to true or false to override server-side setting
                enableTracking: true,   // set to true or false to enableTracking

                // feature flags:
                ff_useLegacyRadioInputHandling: false, // set to true if you have trouble with radio form fields
            },
            app: {},
            onInit: function () {
                console.log("onInit", this);
                // this.app.setConfig(this.config);
                // register callbacks here, if any

            },
            onReady: function () {
                console.log("--------CandyPixel is Ready---------");
                // this.app.getInfo();
            },
        }
    })(window, document, "https://s.cdy.ai/candypixel/app.v3.7.1.js");
    </script>

    <!-- Meta Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '573486941398806');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=573486941398806&ev=PageView&noscript=1"
        />
    </noscript>
    <!-- End Meta Pixel Code -->
    <style>
        @font-face {
    		font-family: 'Conv_DIN-Medium 2_0';
    		src: url('../fonts/DIN-Medium 2_0.eot');
    		src: local('☺'), url('../fonts/DIN-Medium 2_0.woff') format('woff'), url('../fonts/DIN-Medium 2_0.ttf') format('truetype'), url('../fonts/DIN-Medium 2_0.svg') format('svg');
    		font-weight: normal;
    		font-style: normal;
    	}

    	@font-face {
    		font-family: 'Conv_DIN-Light';
    		src: url('../fonts/DIN-Light.eot');
    		src: local('☺'), url('../fonts/DIN-Light.woff') format('woff'), url('../fonts/DIN-Light.ttf') format('truetype'), url('../fonts/DIN-Light.svg') format('svg');
    		font-weight: normal;
    		font-style: normal;
    	}
        ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
            color: rgb(144,148,153) !important;
            opacity: 0.6; /* Firefox */
            font-size:1rem;
        }
        :-ms-input-placeholder { /* Internet Explorer 10-11 */
            color: rgb(144,148,153) !important;
            opacity: 0.6;
            font-size:1rem;
        }
        ::-ms-input-placeholder { /* Microsoft Edge */
            color: rgb(144,148,153) !important;
            opacity: 0.6;
            font-size:1rem;
        }

        h3, h4, h5 {font-family: none !important;}
        h4{font-weight:400;}
    	h2{
            font-family: 'Conv_DIN-Medium 2_0' !important;
            line-height: 36px;
            color: #86ad3c;
        }
        input:not([type="checkbox"]):not([type="radio"]){font-size:1.2rem;}
        button.btn-light-custom{color: #fff !important;}
        .text-green{color:rgb(0,115,53) !important;}
        .display-inline{display:inline !important;}
        .heading-text.heading-section > h2 {
            font-size: 35px;
            line-height:45px;
        }
        .heading-text.heading-section>h2:before {
            height: 3px;
            background-color: rgb(0,115,53) !important;
            bottom: -35px;
        }
        #book {
            background-color: rgb(229,224,218);
            border: 5px solid rgba(255,255,255,.2);
            border-radius: 30px;
            text-align: center;
            padding-top: 20px;
            padding-left: 30px;
            padding-right: 30px;
            z-index: 999;
            top: -80px;
            height: 100%;
            position:relative ;
        }
        .img-logo{
            width: 160px;
            margin-bottom: 10px;
        }
        .btn-light-custom{
            background: linear-gradient(90deg,  rgb(36, 179, 168) 0%, rgb(161, 197, 74) 100%);
            border-color: linear-gradient(90deg,  rgb(36, 179, 168) 0%, rgb(161, 197, 74) 100%);
            color: #fff;
        }
        .form-control-custom{
            border: 0;
    	    border-bottom: 1px solid #ced4da !important;
            border-radius: 10px;
            padding: 10px!important;
            min-height: 50px !important;
            color: rgb(0,115,53) !important;
            background-color:#fff;
        }
        .form-control-custom::placeholder {
            color: #fff;
            font-weight: 500;
            font-size:15px;
        }

        .g-recaptcha {display: inline-block;}

        @media  only screen and (max-width: 768px ) {
            h2{line-height: 30px;}
            #book {
                background-color: rgb(229,224,218);
                border: 5px solid rgba(255,255,255,.2);
                width: 100%;
                position: relative;
                left: 0%;
                top: -25px;
                height: 100%;
            }
            .p-60 {padding: 30px !important;}
            .p-b-0-m{padding-bottom: 0;}
            .m-t-0-m{margin-top:0 !important;}
            .m-t-15-m{margin-top:15px !important;}
            .m-t-40-m{margin-top:40px !important;}
            .m-b-15-m{margin-bottom:15px !important;}
            #footer .social-icons {float: none !important;}
            .line-height-mobile{line-height:17px;}
            .text-justify-m{text-align: justify;}
            .text-left-m{text-align: left !important;}
            .heading-text.heading-section > h2 {
                font-size: 25px;
                line-height: 30px;
            }
        }
    </style>
    <style>
        .grecaptcha-badge { visibility: hidden !important; }
    </style>
    {!! RecaptchaV3::initJs() !!}
    {{-- TIKTOK --}}
    <script>
        !function (w, d, t) {
          w.TiktokAnalyticsObject=t;var ttq=w[t]=w[t]||[];ttq.methods=["page","track","identify","instances","debug","on","off","once","ready","alias","group","enableCookie","disableCookie"],ttq.setAndDefer=function(t,e){t[e]=function(){t.push([e].concat(Array.prototype.slice.call(arguments,0)))}};for(var i=0;i<ttq.methods.length;i++)ttq.setAndDefer(ttq,ttq.methods[i]);ttq.instance=function(t){for(var e=ttq._i[t]||[],n=0;n<ttq.methods.length;n++)ttq.setAndDefer(e,ttq.methods[n]);return e},ttq.load=function(e,n){var i="https://analytics.tiktok.com/i18n/pixel/events.js";ttq._i=ttq._i||{},ttq._i[e]=[],ttq._i[e]._u=i,ttq._t=ttq._t||{},ttq._t[e]=+new Date,ttq._o=ttq._o||{},ttq._o[e]=n||{};var o=document.createElement("script");o.type="text/javascript",o.async=!0,o.src=i+"?sdkid="+e+"&lib="+t;var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(o,a)};
        
          ttq.load('CNGM0NBC77U6RIGJDUK0');
          ttq.page();
        }(window, document, 'ttq');
    </script>
</head>

<body>

    <div class="body-inner">


        <section id="section1" class="fullscreen p-t-0 p-b-0 d-none d-lg-block d-xl-block" data-bg-image="{{IMAGE_URL.$banners[0]->image_1}}"></section>

        <section class="p-t-0 p-b-0 d-block d-sm-block d-md-block d-lg-none d-xl-none">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 no-padding">
                        <img src="{{IMAGE_URL.$banners[0]->image_2}}" class="img-responsive">
                    </div>
                </div>
            </div>
        </section>

        <div class="p-t-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-xl-8 offset-lg-2 offset-xl-2">
                        <div id="book">
                            <form method="post" action="{{URL::to(Library::getDomainPrefix().'/contact-micro/action')}}" class="form-contact">
                                <input name="_token" value="{{csrf_token()}}" type="hidden">

                                <div class="heading-text heading-section text-center">
                                    <h2 class="text-uppercase text-green">Get More <div class="font-weight-800 display-inline">Informations</div></h2>
                                    <p class="m-b-0">Anda tertarik dengan penawaran ini?</p>
                                    <p style="font-size:11px;">Silahkan input data diri Anda pada form berikut dan sales eksekutif kami akan segera menghubungi Anda</p>
                                    {{--<div class="text-dark">{!! $forms[0]->contents !!}</div>--}}
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group m-b-20">
                                            <input aria-required="true" name="name" class="form-control form-control-custom char-only required" id="name" minlength="3" maxlength="50" placeholder="Fullname*" type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group m-b-20">
                                            <input class="form-control form-control-custom number-only required" id="phone" name="phone" placeholder="Phone No*" minlength="10" maxlength="15" type="tel" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group m-b-20">
                                            <input aria-required="true" name="email" id="email" class="form-control form-control-custom required" placeholder="Email*" type="email">
                                        </div>
                                    </div>
                                    <input type="hidden" name="utm_source" id="utm_source" class="utm_source" value={{ app('request')->input('utm_source') }}>
                                    <input type="hidden" name="utm_medium" id="utm_medium" class="utm_medium" value={{ app('request')->input('utm_medium') }}>
                                    <input type="hidden" name="utm_campaign" id="utm_campaign" class="utm_campaign" value={{ app('request')->input('utm_campaign') }}>
                                    <div class="col-md-12 col-12 text-center">
                                        <div class="form-group">
                                            {{-- {!! Recaptcha::render() !!} --}}
                                            {!! RecaptchaV3::field('contact') !!}
                                        </div>
                                        <div class="form-group col-xl-10 col-lg-10 offset-lg-1 offset-xl-1 col-12 col-md-12 col-xs-12">
                                            <button class="btn btn-lg btn-light-custom btn-block btn-form text-light" type="submit"><i class="fa fa-paper-plane"></i>&nbsp;Submit</button>
                                            <div class="form-status"></div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="p-t-40 p-b-0">
            <div class="container-fluid">
                {{-- <div class="row">
                    <div class="col-lg-12 col-xl-12 text-center m-b-70 m-b-15-m">
                        <h2 class="text-green text-uppercase"><div class="font-weight-800 display-inline text-medium">Strozzi At Symphonia</div></h2>
                        <p>Experience The Comfort of The Real Attic</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-xl-6 no-padding d-block d-sm-block d-md-block d-lg-none d-xl-none">
                        <img src="{{IMAGE_URL.$articlesfirst[0]->thumb}}" class="img-fluid">
                    </div>
                    <div class="col-lg-6 col-xl-6 col-xxl-6 text-left p-70">
                        <h2 class="p-b-10 p-b-0-m"><div class="font-weight-500 text-green text-left-m">{!! $articlesfirst[0]->name !!}</div></h2>
                        <div class="text-justify-m">{!! $articlesfirst[0]->contents !!}</div>
                    </div>
                    <div class="col-lg-6 col-xl-6 col-xxl-6 no-padding d-none d-lg-block d-xl-block">
                        <img src="{{IMAGE_URL.$articlesfirst[0]->thumb}}" class="img-fluid">
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6 col-xl-6 col-xxl-6 no-padding">
                        <img src="{{IMAGE_URL.$articlesbottom[0]->thumb}}" class="img-fluid">
                    </div>
                    <div class="col-lg-6 col-xl-6 col-xxl-6 p-80 text-right">
                        <h2 class="p-b-10 p-b-0-m"><div class="font-weight-500 text-green text-left-m">{!! $articlesbottom[0]->name !!}</div></h2>
                        <div class="text-justify-m">{!! $articlesbottom[0]->contents !!}</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-xl-6 no-padding d-block d-sm-block d-md-block d-lg-none d-xl-none">
                        <img src="{{IMAGE_URL.$articlestop[0]->thumb}}" class="img-fluid">
                    </div>
                    <div class="col-lg-6 col-xl-6 text-left p-30">
                        <h2 class="p-b-10 p-b-0-m"><div class="font-weight-500 text-green text-left-m">{!! $articlestop[0]->name !!}</div></h2>
                        <div class="text-left-m p-l-20">{!! $articlestop[0]->contents !!}</div>
                    </div>
                    <div class="col-lg-6 col-xl-6 no-padding d-none d-lg-block d-xl-block">
                        <img src="{{IMAGE_URL.$articlestop[0]->thumb}}" class="img-fluid">
                    </div>
                </div> --}}

                @foreach ($articles as $key => $article)
                    @if($key % 2 == 0)
                        <div class="row align-items-start p-t-30 p-b-30">
                            <div class="col-lg-6 col-xl-6 no-padding d-block d-sm-block d-md-block d-lg-none d-xl-none">
                                <img src="{{IMAGE_URL.$article->thumb}}" class="img-fluid">
                            </div>
                            <div class="col-lg-6 col-xl-6 col-xxl-6 text-left pt-0 p-l-30 p-r-30 p-b-30">
                                <h2 class="p-b-10 p-b-0-m p-l-20"><div class="font-weight-500 text-green text-left-m">{!! $article->name !!}</div></h2>
                                <div class="text-left-m p-l-20">{!! $article->contents !!}</div>
                            </div>
                            <div class="col-lg-6 col-xl-6 col-xxl-6 no-padding d-none d-lg-block d-xl-block">
                                <img src="{{IMAGE_URL.$article->thumb}}" class="img-fluid">
                            </div>
                        </div>
                    @else
                        <div class="row align-items-start p-t-30 p-b-30">
                            <div class="col-lg-6 col-xl-6 col-xxl-6 no-padding">
                                <img src="{{IMAGE_URL.$article->thumb}}" class="img-fluid">
                            </div>
                            <div class="col-lg-6 col-xl-6 col-xxl-6 text-left pt-0 p-l-30 p-r-30 p-b-30">
                                <h2 class="p-b-10 p-b-0-m p-l-20"><div class="font-weight-500 text-green text-left-m">{!! $article->name !!}</div></h2>
                                <div class="text-left-m p-l-20">{!! $article->contents !!}</div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </section>

        <section class="p-t-40 p-b-0">
            <div class="container-fluid">
                <div class="row align-items-start p-t-30 p-b-30">
                    @foreach ($programs as $program)
                        <div class="col-lg-6 col-xl-6 col-xxl-6">
                            <div class="m-b-20">
                                <img src="{{IMAGE_URL.$program->thumb}}" class="img-fluid">
                            </div>
                            <h2 class="p-b-10 p-b-0-m p-l-20"><div class="font-weight-500 text-green text-left-m">{!! $program->name !!}</div></h2>
                            <div class="text-left-m p-l-20">{!! $program->contents !!}</div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
        {{--
        <section class="p-t-0 p-b-30 p-b-0-m">
            <div class="container">
                <div class="row">
                    @foreach ($articles as $article)
                    <div class="col-lg-6 col-xl-6">
                        <div class="row">
                            <div class="col-lg-12 col-xl-12">
                                <img src="{{IMAGE_URL.$article->thumb}}" class="img-fluid">
                                <div class="text-justify m-t-30 m-b-40">{!!$article->contents!!}</div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
        --}}

        {{--
        <section id="page-content" class="p-t-0 p-b-0">
            <div class="container">
                <div class="owl-carousel owl-theme">
                    @for ($i = 0; $i < count($gallery); $i++)
                    <div class="item">
                        <a href="{{$gallery[$i]}}" class="popup">
                            <img src="{{$gallery[$i]}}" alt="">
                        </a>
                    </div>
                    @endfor
                </div>
                <div class="row">
                    <div class="col-lg-12 col-xl-12">
                        @if($video->url)
                        <iframe width="100%" height="500px" src="https://www.youtube.com/embed/{{$video->url}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        @else
                        <img class="img-responsive" src="{{IMAGE_URL.$video->thumb}}" alt="{{$video->name}}">
                        @endif
                    </div>
                </div>
            </div>
        </section>
        --}}
        <section class="p-t-10 p-b-0">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-xl-12">
                        <h2 class="text-green text-uppercase m-t-60 m-t-0-m m-b-30">Our&nbsp<div class="font-weight-800 display-inline">Video</div></h2>
                        @if($video->url)
                        <iframe width="100%" height="600px" src="https://www.youtube.com/embed/{{$video->url}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        @else
                        <img class="img-responsive" src="{{IMAGE_URL.$video->thumb}}" alt="{{$video->name}}">
                        @endif
                    </div>
                    <div class="col-lg-12 col-xl-12">
                        <h2 class="text-green text-uppercase m-t-60 m-t-0-m m-b-30">Our&nbsp<div class="font-weight-800 display-inline">Location</div></h2>
                        <img src="{{asset('assets/images/drive/maps_symphonia_photo.jpg')}}" class="img-responsive" style="width:100%;">
                    </div>
                </div>
            </div>
        </section>
        @include('layout.footer')

    </div>

    <script type="text/javascript" src="{{asset('assets/js/jquery-1.11.2.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins.js')}}"></script>
    <script src="{{asset('assets/js/functions.js')}}"></script>

    <!-- GO TOP BUTTON -->
    {{-- <a class="gototop gototop-button" href="#"><i class="fa fa-chevron-up"></i></a> --}}

    <!-- OLD LINK -->
    <!-- LOADING PAGE LOADER -->
    <script type="text/javascript">
        var baseURL = "{{ URL::to('') }}";
    </script>

    <!-- Theme Base, Components and Settings -->
    <script src="{{asset('assets/microsite/js/functions/google_map.js')}}"></script>
    <script src="{{asset('assets/microsite/vendor/sweetalert/sweetalert.min.js')}}" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        $(".popup").magnificPopup({
            type: "image",
            removalDelay: 160,
            preloader: false,
            fixedContentPos: true,
            gallery: {
            enabled: true
            }
        });
    });
    $(".owl-carousel").owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        dots: false,
        startPosition: 2,
        responsive: {
            0: {
            items: 1
            },
            600: {
            items: 1
            },
            1000: {
            items: 4
            }
        }
        });

        $(".form-contact").submit(function(event) {
            event.preventDefault();
            var formData = $(this).serializeArray();
            formData.push({
                name: "subject",
                value: "Contact Us LP Cluster Strozzi"
            });
            formData.push({
                name: "message",
                value: "Contact Us LP Cluster Strozzi"
            });
            var name = $(this).find("input[name='name']").val();
            var phone = $(this).find("input[name='phone']").val();
            var email = $(this).find("input[name='email']").val();
            var button = $("button", this);

            if ($.trim(name) == "") {
                swal({
                    title: "Error!",
                    text: "Name could not empty!",
                    type: "error",
                    confirmButtonText: "OK"
                });
                return;
            }
            if ($.trim(phone) == "") {
                swal({
                    title: "Error!",
                    text: "Phone could not empty!",
                    type: "error",
                    confirmButtonText: "OK"
                });
                return;
            }
            if ($.trim(email) == "") {
                swal({
                    title: "Error!",
                    text: "Email could not empty!",
                    type: "error",
                    confirmButtonText: "OK"
                });
                return;
            }

            ttq.identify({
                "email": email, // string. The email of the customer if available. It must be hashed with SHA-256 on the client side.
                "phone_number": phone, // string. The phone number of the customer if available. It must be hashed with SHA-256 on the client side.
                // "external_id": "<hashed_extenal_id>" // string. A unique ID from the advertiser such as user or external cookie IDs. It must be hashed with SHA256 on the client side.
            });
            ttq.track('SubmitForm', {
                "value": "Contact Us LP Cluster Ardea", // number. Value of the order or items sold. Example: 100.
                "contents": [{
                    // "content_id": "<content_identifier>", // string. ID of the product. Example: "1077218".
                    "content_type": "rumah", // string. Either product or product_group.
                    "content_name": "Cluster Ardea", // string. The name of the page or product. Example: "shirt".
                }]
            });
            ttq.track('ViewContent', {
                "value": "Contact Us LP Cluster Ardea", // number. Value of the order or items sold. Example: 100.
                "contents": [{
                    "content_id": "", // string. ID of the product. Example: "1077218".
                    "content_type": "rumah", // string. Either product or product_group.
                    "content_name": "Cluster Ardea", // string. The name of the page or product. Example: "shirt".
                    "brand": "Summarecon Serpong" // string. The brand name of the page or product. Example: "Nike"
                }]
            });

            var action = $(this).attr('action');
            var status = $(".form-status", this);
            $.ajax({
                url: action,
                type: 'post',
                data: formData,
                beforeSend: function() {
                    status.append('<p class="text-dark"><i class="fa fa-spinner fa-spin text-dark"></i> Email is sending...</p>').fadeIn();
                    // button.attr("disabled","disabled");
                },
                success: function(data) {
                    if (data.response == 'success') {
                        swal({
                            title: "Success!",
                            text: data.message,
                            type: "success",
                            confirmButtonText: "OK"
                        });
                        $(this).trigger("reset");

                        window.location.href = "{{URL::to(Library::getDomainPrefix().'/response_serpong/success')}}";
                        gtag('event', 'click', {
                            'event_category': 'SubmitFormLP',
                            'transport_type': 'beacon',
                            'event_action': 'click',
                            'event_label': 'Strozzi',
                        });
                    } else {
                        swal({
                            title: "Error!",
                            text: data.message,
                            type: "error",
                            confirmButtonText: "OK"
                        });
                        status.html('');
                        button.removeAttr("disabled");
                    }
                }
            });
        });

        //FOR PROTECT NAME FORM
        $(".char-only").keypress(function(event) {
            var theEvent = event || window.event;
            var key = theEvent.keyCode || theEvent.which;
            if ((key < 97 || key > 122) && (key < 65 || key > 90) && !(key == 8 || key == 9 || key == 13 || key == 32 || key == 37 || key == 38 || key == 39 || key == 40 || key == 46)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        })


        //FOR PROTECT NUMBER PHONE FORM
        $(".number-only").keypress(function(event) {
            var theEvent = event || window.event;
            var key = theEvent.keyCode || theEvent.which;
            if ((key < 48 || key > 57) && !(key == 8 || key == 9 || key == 13 || key == 37 || key == 38 || key == 39 || key == 40 || key == 46)) {
                console.log(key)
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        })
    </script>

    <script type="text/javascript">
        (function() {
            var RFP = window.RFP = window.RFP || {}; RFP.ConvContext = RFP.ConvContext || {}; RFP.ConvContext.queue = RFP.ConvContext.queue || [];
            RFP.ConvContext.queue.push({
                "advertiser_id": 9579,
                "price": 0,
                "convtype": 0,
                "dat": ""
            });
            var el = document.createElement('script'); el.type = 'text/javascript'; el.async = true;
            el.src = 'https://js.rfp.fout.jp/rfp-conversion.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(el, s);
        })();
    </script>
</body>

</html>

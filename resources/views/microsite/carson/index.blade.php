<!DOCTYPE html>
<html lang="en">
<head>
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-42299418-1', 'summareconserpong.com');
        ga('send', 'pageview');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-42299418-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-42299418-1');
    </script>

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NQ8XHHG');
    </script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link href="{{asset('assets/microsite/images/favicon.ico')}}" rel="shortcut icon">
    {!!SEO::generate()!!}

    <!-- BOOTSTRAP CORE CSS -->
    <link href="{{asset('assets/microsite/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/microsite/vendor/fontawesome/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('assets/microsite/vendor/animateit/animate.min.css')}}" rel="stylesheet">
    <!-- VENDOR CSS -->
    {{-- <link href="{{asset('assets/microsite/vendor/owlcarousel/owl.carousel.css')}}" rel="stylesheet"> --}}
    <!-- OWL CAROUSEL -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="{{asset('assets/microsite/vendor/magnific-popup/magnific-popup.css')}}" rel="stylesheet">
    <!-- TEMPLATE BASE -->
    <link href="{{asset('assets/microsite/css/theme-base.css')}}" rel="stylesheet">
    <!-- TEMPLATE ELEMENTS -->
    <link href="{{asset('assets/microsite/css/theme-elements.css')}}" rel="stylesheet">
    <!-- RESPONSIVE CLASSES-->
    <link href="{{asset('assets/microsite/css/responsive.css')}}" rel="stylesheet">
    <!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
    <link href="{{asset('assets/microsite/vendor/rs-plugin/css/settings.css')}}" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
    <link href="{{asset('assets/microsite/css/rs-plugin-styles.css')}}" rel="stylesheet" type="text/css" />
    <!-- STYLE CSS -->
    <link href="{{asset('assets/microsite/css/styleheader.css')}}" rel="stylesheet">
    <link href="{{asset('assets/microsite/vendor/sweetalert/sweetalert.css')}}" rel="stylesheet">
    <!--VENDOR SCRIPT-->
    <script src="{{asset('assets/microsite/vendor/jquery/jquery-1.11.2.min.js')}}"></script>
    <script src="{{asset('assets/microsite/vendor/plugins-compressed.js')}}"></script>
    <!-- GMAPS -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqgVIaPYgQ2NhYwOzDjSyMp7v47HdQnak"></script>

    <!-- NEW LINK POLO -->
    <!-- STYLESHEETS & FONTS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
    <link href="{{asset('assets/css/plugins.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/custom.css')}}" rel="stylesheet"/>
    <!-- LOAD JQUERY LIBRARY -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script>
    <!-- LOADING FONTS AND ICONS -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:700%2C600" rel="stylesheet" property="stylesheet" type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/slider-revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/slider-revolution/fonts/font-awesome/css/font-awesome.css')}}">
    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/slider-revolution/css/settings.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/slider-revolution/css/layers.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/slider-revolution/css/navigation.css')}}">
    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="{{asset('assets/plugins/slider-revolution/js/jquery.themepunch.tools.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/slider-revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script type="text/javascript" src="{{asset('assets/plugins/slider-revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/slider-revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/slider-revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/slider-revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/slider-revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/slider-revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/slider-revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/slider-revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/slider-revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
    <script type="text/javascript">
        function setREVStartSize(e) {
            try {
                var i = jQuery(window).width(),
                    t = 9999,
                    r = 0,
                    n = 0,
                    l = 0,
                    f = 0,
                    s = 0,
                    h = 0;
                if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                        f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                    }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                    var u = (e.c.width(), jQuery(window).height());
                    if (void 0 != e.fullScreenOffsetContainer) {
                        var c = e.fullScreenOffsetContainer.split(",");
                        if (c) jQuery.each(c, function (e, i) {
                            u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                        }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                    }
                    f = u
                } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                e.c.closest(".rev_slider_wrapper").css({
                    height: f
                })
            } catch (d) {
                console.log("Failure at Presize of Slider:" + d)
            }
        };
    </script>
    <!-- CandyPixel V3.7.1 -->
    <script>
        var CandyPixel4 = (function (s, u, g, a, r) {
            a = u.createElement("script");
            r = u.getElementsByTagName("script")[0];
            a.async = true;
            a.src = g;
            r.parentNode.insertBefore(a, r);

            return {
                config: {
                    debugMode: false,
                    token: "d4620087-83b4b929-1665972789573",
                    pid: "2711",    // deprecated
                    psid: "SRCP#41dbe5fb-9678-47a8-a197-31f264a45578",  // deprecated

                    // form class or ids to be ignored e.g. [".secretForms", "#loginForm"]
                    ignoreFormsWithAttributeValues: [],

                    // field class or ids to be ignored  e.g. [".secretField", "#password"]
                    ignoreFieldsWithAttributeValues: [],

                    submitAction: "submit", // click | submit
                    detectAdBlocker: null,  // set to true or false to override server-side setting
                    enableTracking: true,   // set to true or false to enableTracking

                    // feature flags:
                    ff_useLegacyRadioInputHandling: false, // set to true if you have trouble with radio form fields
                },
                app: {},
                onInit: function () {
                    console.log("onInit", this);
                    // this.app.setConfig(this.config);
                    // register callbacks here, if any

                },
                onReady: function () {
                    console.log("--------CandyPixel is Ready---------");
                    // this.app.getInfo();
                },
            }
        })(window, document, "https://s.cdy.ai/candypixel/app.v3.7.1.js");
    </script>
    <style>
        @font-face {
		font-family: 'Conv_DIN-Medium 2_0';
		src: url('../fonts/DIN-Medium 2_0.eot');
		src: local('☺'), url('../fonts/DIN-Medium 2_0.woff') format('woff'), url('../fonts/DIN-Medium 2_0.ttf') format('truetype'), url('../fonts/DIN-Medium 2_0.svg') format('svg');
		font-weight: normal;
		font-style: normal;
	}

	@font-face {
		font-family: 'Conv_DIN-Light';
		src: url('../fonts/DIN-Light.eot');
		src: local('☺'), url('../fonts/DIN-Light.woff') format('woff'), url('../fonts/DIN-Light.ttf') format('truetype'), url('../fonts/DIN-Light.svg') format('svg');
		font-weight: normal;
		font-style: normal;
	}

	h2{
        font-family: 'Conv_DIN-Medium 2_0' !important;
        line-height: 22px;
        color: #558055;
    }

    .m-t-125{margin-top: 80px;}
    button.btn-light-custom{color: #fff !important;}
    .btn-light-custom{
        background: linear-gradient(90deg, rgb(255, 85, 124) 0%, rgb(243, 130, 60) 100%);
        border-color: linear-gradient(90deg, rgb(255, 85, 124) 0%, rgb(243, 130, 60) 100%);
        color: #fff;
    }
    .form-control-custom{
        border: 0;
	    border-bottom: 1px solid #ced4da !important;
        border-radius: 0;
        padding: 10px 0 !important;
        min-height: 50px !important;
        color: #75C0CC !important;
    }
    .form-control-custom::placeholder {
        color: #558055;
        font-weight: 500;
        font-size:15px;
    }

    @media  only screen and (max-width: 768px ) {
        .m-t-125{margin-top:20px;}
        #footer .social-icons {float: none !important;}
        .line-height-mobile{line-height:17px;}
        #slide-896-layer-3{top:160px;}
        #slide-896-layer-2{
            top:-200px !important;
            font-size: 52px !important;
            left: 0px !important;
        }
        #slide-896-layer-9{
            top:-100px !important;
            font-size: 45px !important;
            left: -20px !important;
        }

        #slide-896-layer-8{
            top:-50px !important;
            font-size: 45px !important;
            left: -20px !important;
        }
    }
    </style>
    <style>
        .grecaptcha-badge { visibility: hidden !important; }
    </style>
    {!! RecaptchaV3::initJs() !!}
</head>

<body>
    <div class="body-inner">
        <section id="slider" class="d-none d-md-block">
            <div id="rev_slider_314_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="mountain-parallax-header" data-source="gallery" style="background:rgba(255,255,255,0);padding:0px;">
                <div id="rev_slider_314_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.3.1">
                    <ul>
                        <li data-index="rs-896" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                            <!-- MAIN IMAGE -->
                            {{-- <img src="{{asset('assets/images/serpong/microsite/LP-DESKTOP-1920X1280-BG-ONLY-PUTIH.jpg')}}" alt="" data-bgposition="bottom center" data-kenburns="on" data-duration="5000" data-ease="Power4.easeOut" data-scalestart="150" data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-blurstart="20" data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg hidden-xs hidden-sm" data-no-retina> --}}
                            <img src="{{IMAGE_URL.$banners[0]->image_1}}" alt="" data-bgposition="bottom center" data-kenburns="on" data-duration="5000" data-ease="Power4.easeOut" data-scalestart="150" data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-blurstart="20" data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg hidden-xs hidden-sm" data-no-retina>
                            <!-- LAYERS -->
                            <!-- LAYER NR. 1 -->
                            {{-- <div class="tp-caption   tp-resizeme  blurslider-gradient rs-parallaxlevel-14" id="slide-896-layer-2" data-x="['right','right','right','right']" data-hoffset="['1','1','1','1']" data-y="['middle','middle','middle','middle']" data-voffset="['-229','-229','-189','-149']" data-fontsize="['74','74','74','80']" data-lineheight="['90','90','90','60']" data-letterspacing="['-2px','-2px','-2px','-5']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":50,"speed":3000,"frame":"0","from":"y:50px;sX:0.3;sY:0.3;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-textAlign="['center','center','center','center']" data-paddingtop="[20,20,20,20]" data-paddingright="[10,10,10,10]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[10,10,10,10]" style="z-index: 5; white-space: nowrap; font-size: 74px; line-height: 90px; font-weight: 400; color: #ffffff; letter-spacing:0px;font-family:Poppins;">Revealing Soon!</div> --}}
                            <!-- LAYER NR. 1 -->
                            <!-- LAYER NR. 9 -->
                            {{-- <div class="tp-caption   tp-resizeme  blurslider-gradient rs-parallaxlevel-14" id="slide-896-layer-9" data-x="['right','right','right','right']" data-hoffset="['1','1','1','1']" data-y="['middle','middle','middle','middle']" data-voffset="['-140','-140','-189','-149']" data-fontsize="['50','50','50','80']" data-lineheight="['90','90','90','60']" data-letterspacing="['2px','2px','2px','-5']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":50,"speed":3000,"frame":"0","from":"y:50px;sX:0.3;sY:0.3;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-textAlign="['center','center','center','center']" data-paddingtop="[20,20,20,20]" data-paddingright="[10,10,10,10]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[10,10,10,10]" style="z-index: 5; white-space: nowrap; font-size: 74px; line-height: 90px; font-weight: 400; color: #ffffff; letter-spacing:0px;font-family: 'Conv_DIN-Medium 2_0' !important;">Redefining Luxury</div> --}}
                            <!-- LAYER NR. 9 -->
                            <!-- LAYER NR. 8 -->
                            {{-- <div class="tp-caption   tp-resizeme  blurslider-gradient rs-parallaxlevel-14" id="slide-896-layer-8" data-x="['right','right','right','right']" data-hoffset="['1','1','1','1']" data-y="['middle','middle','middle','middle']" data-voffset="['-80','-80','-189','-149']" data-fontsize="['50','50','50','80']" data-lineheight="['90','90','90','60']" data-letterspacing="['2px','2px','2px','-5']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":50,"speed":3000,"frame":"0","from":"y:50px;sX:0.3;sY:0.3;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-textAlign="['center','center','center','center']" data-paddingtop="[20,20,20,20]" data-paddingright="[10,10,10,10]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[10,10,10,10]" style="z-index: 5; white-space: nowrap; font-size: 74px; line-height: 90px; font-weight: 200; color: #ffffff; letter-spacing:0px;font-family: 'Conv_DIN-Medium 2_0' !important;">through Healthy Living</div> --}}
                            <!-- LAYER NR. 8 -->

                            <!-- LAYER NR. 2 -->
                            {{-- <div class="tp-caption   tp-resizeme" id="slide-896-layer-1" data-x="['left','left','left','left']" data-hoffset="['-350','-350','-350','-350']" data-y="['middle','middle','middle','middle']" data-voffset="['130','130','130','80']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="image" data-responsive_offset="on" data-frames='[{"delay":50,"speed":3000,"frame":"0","from":"sX:0.7;sY:0.7;opacity:0;fb:10px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6;"><img src="{{asset('assets/images/serpong/microsite/BurungR7.png')}}" alt="" data-ww="['1050px','1050px','861','689px']" data-hh="['610px','610px','500px','400px']" width="1050" height="610" data-no-retina> </div> --}}
                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-896-layer-10" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['0','0','0','0']" data-width="full" data-height="200" data-whitespace="nowrap" data-type="shape" data-basealign="slide" data-responsive_offset="on" data-frames='[{"delay":50,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7;background:linear-gradient(180deg, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);"> </div>
                            <!-- LAYER NR. 4 -->
                            {{-- <div class="tp-caption   tp-resizeme" id="slide-896-layer-4" data-x="['left','left','left','left']" data-hoffset="['100','100','100','100']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['10','10','10','10']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="image" data-basealign="slide" data-responsive_offset="on" data-frames='[{"delay":300,"speed":2000,"frame":"0","from":"y:100px;sX:1.1;sY:1.1;opacity:0;fb:10px;","to":"o:1;rY:180;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8;">
                                <div class="rs-looped rs-slideloop" data-easing="Linear.easeNone" data-speed="40" data-xs="0" data-xe="500" data-ys="0" data-ye="0"><img src="{{asset('assets/images/serpong/microsite/cloud1.png')}}" alt="" data-ww="['500px','500px','500px','500px']" data-hh="['273px','273px','273px','273px']" width="500" height="273" data-no-retina> </div>
                            </div> --}}
                            <!-- LAYER NR. 5 -->
                            {{-- <div class="tp-caption   tp-resizeme" id="slide-896-layer-5" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['10','10','10','10']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="image" data-basealign="slide" data-responsive_offset="on" data-frames='[{"delay":300,"speed":2000,"frame":"0","from":"sX:1.1;sY:1.1;opacity:0;fb:10px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 9;">
                                <div class="rs-looped rs-slideloop" data-easing="Linear.easeNone" data-speed="100" data-xs="0" data-xe="500" data-ys="0" data-ye="0"><img src="{{asset('assets/images/serpong/microsite/cloud2.png')}}" alt="" data-ww="['500px','500px','500px','500px']" data-hh="['273px','273px','273px','273px']" width="600" height="278" data-no-retina> </div>
                            </div> --}}
                            <!-- LAYER NR. 6 -->
                            {{-- <div class="tp-caption   tp-resizeme" id="slide-896-layer-6" data-x="['right','right','right','right']" data-hoffset="['150','150','150','150']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['10','10','10','10']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="image" data-basealign="slide" data-responsive_offset="on" data-frames='[{"delay":300,"speed":2000,"frame":"0","from":"sX:1.1;sY:1.1;opacity:0;fb:10px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 10;">
                                <div class="rs-looped rs-slideloop" data-easing="Linear.easeNone" data-speed="120" data-xs="0" data-xe="500" data-ys="0" data-ye="0"><img src="{{asset('assets/images/serpong/microsite/cloud3.png')}}" alt="" data-ww="['500px','500px','500px','500px']" data-hh="['273px','273px','273px','273px']" width="738" height="445" data-no-retina> </div>
                            </div> --}}
                            <!-- LAYER NR. 7 -->
                            {{-- <div class="tp-caption   tp-resizeme" id="slide-896-layer-11" data-x="['left','left','left','left']" data-hoffset="['-200','-200','-200','-200']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['10','10','10','10']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="image" data-basealign="slide" data-responsive_offset="on" data-frames='[{"delay":300,"speed":2000,"frame":"0","from":"sX:1.1;sY:1.1;opacity:0;fb:10px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 11;">
                                <div class="rs-looped rs-slideloop" data-easing="Linear.easeNone" data-speed="80" data-xs="0" data-xe="500" data-ys="0" data-ye="0"><img src="{{asset('assets/images/serpong/microsite/cloud3.png')}}" alt="" data-ww="['500px','500px','500px','500px']" data-hh="['273px','273px','273px','273px']" width="738" height="445" data-no-retina> </div>
                            </div> --}}
                            <!-- LAYER NR. 8 -->
                            {{-- <div class="tp-caption   tp-resizeme" id="slide-896-layer-12" data-x="['right','right','right','right']" data-hoffset="['-200','-200','-200','-200']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['10','10','10','10']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="image" data-basealign="slide" data-responsive_offset="on" data-frames='[{"delay":300,"speed":2000,"frame":"0","from":"y:100px;sX:1.1;sY:1.1;opacity:0;fb:10px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 12;">
                                <div class="rs-looped rs-slideloop" data-easing="Linear.easeNone" data-speed="200" data-xs="0" data-xe="500" data-ys="0" data-ye="0"><img src="{{asset('assets/images/serpong/microsite/cloud1.png')}}" alt="" data-ww="['500px','500px','500px','500px']" data-hh="['273px','273px','273px','273px']" width="500" height="273" data-no-retina> </div>
                            </div> --}}
                            <!-- LAYER NR. 9 -->
                            <div class="tp-caption rev-btn " id="slide-896-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['100','100','100','150']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":"","speed":"1500","ease":"Power1.easeInOut"}]' data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"y:50px;sX:1;sY:1;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"200","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;fb:0;","style":"c:rgba(255,255,255,1);bg:linear-gradient(90deg, rgba(243,130,60,1) 0%, rgba(255,85,124,1) 100%);bs:solid;bw:0 0 0 0;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[25,25,25,25]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[25,25,25,25]" style="z-index: 13; white-space: nowrap; font-size: 15px; line-height: 60px; font-weight: 500; color: rgba(255,255,255,1); letter-spacing:2px;font-family:Poppins;background:linear-gradient(90deg, rgba(255,85,124,1) 0%, rgba(243,130,60,1) 100%);border-radius:50px;border-color:rgba(0,0,0,1);-webkit-box-shadow:0px 10px 20px 0px rgba(0,0,0,0.35);-moz-box-shadow:0px 10px 20px 0px rgba(0,0,0,0.35);box-shadow:0px 10px 20px 0px rgba(0,0,0,0.35);cursor:pointer;">Click To Find More </div>
                        </li>
                    </ul>
                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                </div>
            </div>
            <!-- END REVOLUTION SLIDER -->
            <script type="text/javascript">
                var revapi314,
                    tpj = jQuery;
                tpj(document).ready(function () {
                    if (tpj("#rev_slider_314_1").revolution == undefined) {
                        revslider_showDoubleJqueryError("#rev_slider_314_1");
                    } else {
                        revapi314 = tpj("#rev_slider_314_1").show().revolution({
                            sliderType: "hero",
                            jsFileLocation: "plugins/slider-revolution/js/",
                            sliderLayout: "fullscreen",
                            dottedOverlay: "none",
                            delay: 9000,
                            responsiveLevels: [1240, 1024, 778, 480],
                            visibilityLevels: [1240, 1024, 778, 480],
                            gridwidth: [1240, 1024, 778, 480],
                            gridheight: [868, 768, 960, 720],
                            lazyType: "none",
                            parallax: {
                                type: "scroll",
                                origo: "slidercenter",
                                speed: 400,
                                levels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 48, 49, 50, 51, 55],
                            },
                            shadow: 0,
                            spinner: "spinner3",
                            autoHeight: "off",
                            fullScreenAutoWidth: "off",
                            fullScreenAlignForce: "off",
                            fullScreenOffsetContainer: "",
                            disableProgressBar: "on",
                            hideThumbsOnMobile: "off",
                            hideSliderAtLimit: 0,
                            hideCaptionAtLimit: 0,
                            hideAllCaptionAtLilmit: 0,
                            debugMode: false,
                            fallbacks: {
                                simplifyAll: "off",
                                disableFocusListener: false,
                            }
                        });
                    }
                    try {
                        initSocialSharing("314")
                    } catch (e) {}
                }); /*ready*/
            </script>
        </section>
        <!-- end: Revolution Slider-->

        <section id="slider" class="d-block d-md-none">
            <div id="rev_slider_314_2_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="mountain-parallax-header" data-source="gallery" style="background:rgba(255,255,255,0);padding:0px;">
                <div id="rev_slider_314_2" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.3.1">
                    <ul>
                        <li data-index="rs-896" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                            <!-- MAIN IMAGE -->
                            {{-- <img src="{{asset('assets/images/serpong/microsite/LP-DESKTOP-1920X1280-BG-ONLY-PUTIH.jpg')}}" alt="" data-bgposition="bottom center" data-kenburns="on" data-duration="5000" data-ease="Power4.easeOut" data-scalestart="150" data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-blurstart="20" data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg hidden-xs hidden-sm" data-no-retina> --}}
                            <img src="{{IMAGE_URL.$banners[0]->image_2}}" alt="" data-bgposition="bottom center" data-kenburns="on" data-duration="5000" data-ease="Power4.easeOut" data-scalestart="150" data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-blurstart="20" data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg hidden-xs hidden-sm" data-no-retina>
                            <!-- LAYERS -->
                            <!-- LAYER NR. 1 -->
                            {{-- <div class="tp-caption   tp-resizeme  blurslider-gradient rs-parallaxlevel-14" id="slide-896-layer-2" data-x="['right','right','right','right']" data-hoffset="['1','1','1','1']" data-y="['middle','middle','middle','middle']" data-voffset="['-229','-229','-189','-149']" data-fontsize="['74','74','74','80']" data-lineheight="['90','90','90','60']" data-letterspacing="['-2px','-2px','-2px','-5']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":50,"speed":3000,"frame":"0","from":"y:50px;sX:0.3;sY:0.3;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-textAlign="['center','center','center','center']" data-paddingtop="[20,20,20,20]" data-paddingright="[10,10,10,10]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[10,10,10,10]" style="z-index: 5; white-space: nowrap; font-size: 74px; line-height: 90px; font-weight: 400; color: #ffffff; letter-spacing:0px;font-family:Poppins;">Revealing Soon!</div> --}}
                            <!-- LAYER NR. 1 -->
                            <!-- LAYER NR. 9 -->
                            {{-- <div class="tp-caption   tp-resizeme  blurslider-gradient rs-parallaxlevel-14" id="slide-896-layer-9" data-x="['right','right','right','right']" data-hoffset="['1','1','1','1']" data-y="['middle','middle','middle','middle']" data-voffset="['-140','-140','-189','-149']" data-fontsize="['50','50','50','80']" data-lineheight="['90','90','90','60']" data-letterspacing="['2px','2px','2px','-5']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":50,"speed":3000,"frame":"0","from":"y:50px;sX:0.3;sY:0.3;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-textAlign="['center','center','center','center']" data-paddingtop="[20,20,20,20]" data-paddingright="[10,10,10,10]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[10,10,10,10]" style="z-index: 5; white-space: nowrap; font-size: 74px; line-height: 90px; font-weight: 400; color: #ffffff; letter-spacing:0px;font-family: 'Conv_DIN-Medium 2_0' !important;">Redefining Luxury</div> --}}
                            <!-- LAYER NR. 9 -->
                            <!-- LAYER NR. 8 -->
                            {{-- <div class="tp-caption   tp-resizeme  blurslider-gradient rs-parallaxlevel-14" id="slide-896-layer-8" data-x="['right','right','right','right']" data-hoffset="['1','1','1','1']" data-y="['middle','middle','middle','middle']" data-voffset="['-80','-80','-189','-149']" data-fontsize="['50','50','50','80']" data-lineheight="['90','90','90','60']" data-letterspacing="['2px','2px','2px','-5']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":50,"speed":3000,"frame":"0","from":"y:50px;sX:0.3;sY:0.3;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-textAlign="['center','center','center','center']" data-paddingtop="[20,20,20,20]" data-paddingright="[10,10,10,10]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[10,10,10,10]" style="z-index: 5; white-space: nowrap; font-size: 74px; line-height: 90px; font-weight: 200; color: #ffffff; letter-spacing:0px;font-family: 'Conv_DIN-Medium 2_0' !important;">through Healthy Living</div> --}}
                            <!-- LAYER NR. 8 -->

                            <!-- LAYER NR. 2 -->
                            {{-- <div class="tp-caption   tp-resizeme" id="slide-896-layer-1" data-x="['left','left','left','left']" data-hoffset="['-350','-350','-350','-350']" data-y="['middle','middle','middle','middle']" data-voffset="['130','130','130','80']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="image" data-responsive_offset="on" data-frames='[{"delay":50,"speed":3000,"frame":"0","from":"sX:0.7;sY:0.7;opacity:0;fb:10px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6;"><img src="{{asset('assets/images/serpong/microsite/BurungR7.png')}}" alt="" data-ww="['1050px','1050px','861','689px']" data-hh="['610px','610px','500px','400px']" width="1050" height="610" data-no-retina> </div> --}}
                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-896-layer-10" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['0','0','0','0']" data-width="full" data-height="200" data-whitespace="nowrap" data-type="shape" data-basealign="slide" data-responsive_offset="on" data-frames='[{"delay":50,"speed":1000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7;background:linear-gradient(180deg, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);"> </div>
                            <!-- LAYER NR. 4 -->
                            {{-- <div class="tp-caption   tp-resizeme" id="slide-896-layer-4" data-x="['left','left','left','left']" data-hoffset="['100','100','100','100']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['10','10','10','10']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="image" data-basealign="slide" data-responsive_offset="on" data-frames='[{"delay":300,"speed":2000,"frame":"0","from":"y:100px;sX:1.1;sY:1.1;opacity:0;fb:10px;","to":"o:1;rY:180;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8;">
                                <div class="rs-looped rs-slideloop" data-easing="Linear.easeNone" data-speed="40" data-xs="0" data-xe="500" data-ys="0" data-ye="0"><img src="{{asset('assets/images/serpong/microsite/cloud1.png')}}" alt="" data-ww="['500px','500px','500px','500px']" data-hh="['273px','273px','273px','273px']" width="500" height="273" data-no-retina> </div>
                            </div> --}}
                            <!-- LAYER NR. 5 -->
                            {{-- <div class="tp-caption   tp-resizeme" id="slide-896-layer-5" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['10','10','10','10']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="image" data-basealign="slide" data-responsive_offset="on" data-frames='[{"delay":300,"speed":2000,"frame":"0","from":"sX:1.1;sY:1.1;opacity:0;fb:10px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 9;">
                                <div class="rs-looped rs-slideloop" data-easing="Linear.easeNone" data-speed="100" data-xs="0" data-xe="500" data-ys="0" data-ye="0"><img src="{{asset('assets/images/serpong/microsite/cloud2.png')}}" alt="" data-ww="['500px','500px','500px','500px']" data-hh="['273px','273px','273px','273px']" width="600" height="278" data-no-retina> </div>
                            </div> --}}
                            <!-- LAYER NR. 6 -->
                            {{-- <div class="tp-caption   tp-resizeme" id="slide-896-layer-6" data-x="['right','right','right','right']" data-hoffset="['150','150','150','150']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['10','10','10','10']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="image" data-basealign="slide" data-responsive_offset="on" data-frames='[{"delay":300,"speed":2000,"frame":"0","from":"sX:1.1;sY:1.1;opacity:0;fb:10px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 10;">
                                <div class="rs-looped rs-slideloop" data-easing="Linear.easeNone" data-speed="120" data-xs="0" data-xe="500" data-ys="0" data-ye="0"><img src="{{asset('assets/images/serpong/microsite/cloud3.png')}}" alt="" data-ww="['500px','500px','500px','500px']" data-hh="['273px','273px','273px','273px']" width="738" height="445" data-no-retina> </div>
                            </div> --}}
                            <!-- LAYER NR. 7 -->
                            {{-- <div class="tp-caption   tp-resizeme" id="slide-896-layer-11" data-x="['left','left','left','left']" data-hoffset="['-200','-200','-200','-200']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['10','10','10','10']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="image" data-basealign="slide" data-responsive_offset="on" data-frames='[{"delay":300,"speed":2000,"frame":"0","from":"sX:1.1;sY:1.1;opacity:0;fb:10px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 11;">
                                <div class="rs-looped rs-slideloop" data-easing="Linear.easeNone" data-speed="80" data-xs="0" data-xe="500" data-ys="0" data-ye="0"><img src="{{asset('assets/images/serpong/microsite/cloud3.png')}}" alt="" data-ww="['500px','500px','500px','500px']" data-hh="['273px','273px','273px','273px']" width="738" height="445" data-no-retina> </div>
                            </div> --}}
                            <!-- LAYER NR. 8 -->
                            {{-- <div class="tp-caption   tp-resizeme" id="slide-896-layer-12" data-x="['right','right','right','right']" data-hoffset="['-200','-200','-200','-200']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['10','10','10','10']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="image" data-basealign="slide" data-responsive_offset="on" data-frames='[{"delay":300,"speed":2000,"frame":"0","from":"y:100px;sX:1.1;sY:1.1;opacity:0;fb:10px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 12;">
                                <div class="rs-looped rs-slideloop" data-easing="Linear.easeNone" data-speed="200" data-xs="0" data-xe="500" data-ys="0" data-ye="0"><img src="{{asset('assets/images/serpong/microsite/cloud1.png')}}" alt="" data-ww="['500px','500px','500px','500px']" data-hh="['273px','273px','273px','273px']" width="500" height="273" data-no-retina> </div>
                            </div> --}}
                            <!-- LAYER NR. 9 -->
                            <div class="tp-caption rev-btn " id="slide-896-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['100','100','100','150']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":"","speed":"1500","ease":"Power1.easeInOut"}]' data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"y:50px;sX:1;sY:1;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"200","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;fb:0;","style":"c:rgba(255,255,255,1);bg:linear-gradient(90deg, rgba(243,130,60,1) 0%, rgba(255,85,124,1) 100%);bs:solid;bw:0 0 0 0;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[25,25,25,25]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[25,25,25,25]" style="z-index: 13; white-space: nowrap; font-size: 15px; line-height: 60px; font-weight: 500; color: rgba(255,255,255,1); letter-spacing:2px;font-family:Poppins;background:linear-gradient(90deg, rgba(255,85,124,1) 0%, rgba(243,130,60,1) 100%);border-radius:50px;border-color:rgba(0,0,0,1);-webkit-box-shadow:0px 10px 20px 0px rgba(0,0,0,0.35);-moz-box-shadow:0px 10px 20px 0px rgba(0,0,0,0.35);box-shadow:0px 10px 20px 0px rgba(0,0,0,0.35);cursor:pointer;">Click To Find More </div>
                        </li>
                    </ul>
                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                </div>
            </div>
            <!-- END REVOLUTION SLIDER -->
            <script type="text/javascript">
                var revapi314,
                    tpj = jQuery;
                tpj(document).ready(function () {
                    if (tpj("#rev_slider_314_2").revolution == undefined) {
                        revslider_showDoubleJqueryError("#rev_slider_314_2");
                    } else {
                        revapi314 = tpj("#rev_slider_314_2").show().revolution({
                            sliderType: "hero",
                            jsFileLocation: "plugins/slider-revolution/js/",
                            sliderLayout: "fullscreen",
                            dottedOverlay: "none",
                            delay: 9000,
                            responsiveLevels: [1240, 1024, 778, 480],
                            visibilityLevels: [1240, 1024, 778, 480],
                            gridwidth: [1240, 1024, 778, 480],
                            gridheight: [868, 768, 960, 720],
                            lazyType: "none",
                            parallax: {
                                type: "scroll",
                                origo: "slidercenter",
                                speed: 400,
                                levels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 48, 49, 50, 51, 55],
                            },
                            shadow: 0,
                            spinner: "spinner3",
                            autoHeight: "off",
                            fullScreenAutoWidth: "off",
                            fullScreenAlignForce: "off",
                            fullScreenOffsetContainer: "",
                            disableProgressBar: "on",
                            hideThumbsOnMobile: "off",
                            hideSliderAtLimit: 0,
                            hideCaptionAtLimit: 0,
                            hideAllCaptionAtLilmit: 0,
                            debugMode: false,
                            fallbacks: {
                                simplifyAll: "off",
                                disableFocusListener: false,
                            }
                        });
                    }
                    try {
                        initSocialSharing("314")
                    } catch (e) {}
                }); /*ready*/
            </script>
        </section>
        <!-- end: Revolution Slider-->

        <section class="p-b-0">
            <div class="container-fullscreen">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7 col-12">
                            {{-- <h2 class="p-b-15 line-height-mobile"><div class="font-weight-700">Redefining Luxury</div><div class="font-weight-200"><br>through Healthy Living</div></h2>
                            <p>Step into the emblem of true luxury. Here you can enjoy complete access to the community’s world-class facilities, including The Springs Club and its exclusive amenities. Satisfy your curiosity by filling out the form!</p>
                            <img src="{{asset('assets/images/serpong/microsite/IMAGE-LP-690X570.png')}}" class="img-responsive d-none d-lg-block d-xl-block"> --}}
                            {!!$articles[0]->contents!!}

                        </div>
                        <div class="col-lg-5 col-12 m-t-125">
                            {{-- <p class="m-t-20 m-b-10 text-center">{!!$banners[1]->contents!!}</p> --}}
                            <form method="post" action="{{URL::to(Library::getDomainPrefix().'/contact-micro/action')}}" class="form-contact">
                                <input name="_token" value="{{csrf_token()}}" type="hidden">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group m-b-20">
                                            <input aria-required="true" name="name" class="form-control form-control-custom char-only required" id="name" minlength="3" maxlength="50" placeholder="Fullname" type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group m-b-20">
                                            <input class="form-control form-control-custom number-only required" id="phone" name="phone" placeholder="Phone No" minlength="10" maxlength="15" type="tel" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group m-b-20">
                                            <input aria-required="true" name="email" id="email" class="form-control form-control-custom required" placeholder="Email" type="email">
                                        </div>
                                    </div>
                                    <input type="hidden" name="utm_source" id="utm_source" class="utm_source" value={{ app('request')->input('utm_source') }}>
                                    <input type="hidden" name="utm_medium" id="utm_medium" class="utm_medium" value={{ app('request')->input('utm_medium') }}>
                                    <input type="hidden" name="utm_campaign" id="utm_campaign" class="utm_campaign" value={{ app('request')->input('utm_campaign') }}>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {{-- {!! Recaptcha::render() !!} --}}
                                            {!! RecaptchaV3::field('contact') !!}
                                        </div>
                                        <div class="form-group text-left">
                                            <button class="btn btn-lg btn-light-custom btn-form text-light" type="submit"><i class="fa fa-paper-plane"></i>&nbsp;Submit</button>
                                            <div class="form-status"></div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-12 no-padding d-block d-sm-block d-md-block d-lg-none d-xl-none">
                            <img src="{{asset('assets/images/serpong/microsite/IMAGE-LP-690X570.png')}}" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        {{-- @if(isset($gallery))
        <section class="p-t-0 p-b-0">
            <div class="container-fullscreen" style="display:block !important;">
                <div class="carousel" data-lightbox="gallery" data-margin="0" data-items="3" data-loop="true" data-dots="false" data-arrows="false" data-autoplay="false" data-autoplay-timeout="3600">
                    @foreach($gallery as $gallery)
                    <div class="portfolio-item img-zoom p-b-0">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <a href="#"><img src="{{$gallery}}" alt="{{$gallery}}"></a>
                            </div>
                            <div class="portfolio-description">
                                <a data-lightbox="image" href="{{$gallery}}" class="btn btn-light btn-rounded">Zoom</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
        @endif --}}

        <!-- GALLERY -->
        <section id="page-content" class="p-b-0">
            <div class="container-fluid no-padding">
                <div class="owl-carousel owl-theme">
                    @for ($i = 0; $i < count($gallery); $i++)
                    <div class="item">
                        <a href="{{$gallery[$i]}}" class="popup">
                            <img src="{{$gallery[$i]}}" alt="">
                        </a>
                    </div>
                    @endfor
                </div>

                {{-- <div class="row">
                    <div class="col-md-12 no-padding">
                        @if($video->url)
                        <iframe width="100%" height="60%" src="https://www.youtube.com/embed/{{$video->url}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        @else
                        <img class="img-responsive" src="{{IMAGE_URL.$video->thumb}}" alt="{{$video->name}}">
                        @endif
                    </div>
                </div> --}}
            </div>
        </section>
        <!-- END : GALLERY -->

        @include('layout.footer')

    </div>


    <!--Plugins-->
    <script src="{{asset('assets/js/plugins.js')}}"></script>
    <!--Template functions-->
    {{-- <script src="{{asset('assets/js/functions.js')}}"></script> --}}

    <!-- GO TOP BUTTON -->
    {{-- <a class="gototop gototop-button" href="#"><i class="fa fa-chevron-up"></i></a> --}}

    <!-- OLD LINK -->
    <!-- LOADING PAGE LOADER -->
    <script type="text/javascript">
        var baseURL = "{{ URL::to('') }}";
    </script>

    <!-- Theme Base, Components and Settings -->
    {{-- <script src="{{asset('assets/microsite/js/theme-functions.js')}}"></script> --}}
    <script src="{{asset('assets/microsite/js/functions/google_map.js')}}"></script>
    <script src="{{asset('assets/microsite/vendor/sweetalert/sweetalert.min.js')}}" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.min.js"></script>


    <script type="text/javascript">
    $(document).ready(function() {
        $(".popup").magnificPopup({
            type: "image",
            removalDelay: 160,
            preloader: false,
            fixedContentPos: true,
            gallery: {
            enabled: true
            }
        });
    });
    $(".owl-carousel").owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        dots: false,
        startPosition: 2,
        responsive: {
            0: {
            items: 1
            },
            600: {
            items: 1
            },
            1000: {
            items: 4
            }
        }
        });

        $(".form-contact").submit(function(event) {
            event.preventDefault();
            var formData = $(this).serializeArray();
            formData.push({
                name: "subject",
                value: "Contact Us New Cluster"
            });
            formData.push({
                name: "message",
                value: "Contact Us New Cluster"
            });
            var name = $(this).find("input[name='name']").val();
            var phone = $(this).find("input[name='phone']").val();
            var email = $(this).find("input[name='email']").val();
            var button = $("button", this);

            if ($.trim(name) == "") {
                swal({
                    title: "Error!",
                    text: "Name could not empty!",
                    type: "error",
                    confirmButtonText: "OK"
                });
                return;
            }
            if ($.trim(phone) == "") {
                swal({
                    title: "Error!",
                    text: "Phone could not empty!",
                    type: "error",
                    confirmButtonText: "OK"
                });
                return;
            }
            if ($.trim(email) == "") {
                swal({
                    title: "Error!",
                    text: "Email could not empty!",
                    type: "error",
                    confirmButtonText: "OK"
                });
                return;
            }

            var action = $(this).attr('action');
            var status = $(".form-status", this);
            $.ajax({
                url: action,
                type: 'post',
                data: formData,
                beforeSend: function() {
                    status.append('<p class="text-dark"><i class="fa fa-spinner fa-spin text-dark"></i> Email is sending...</p>').fadeIn();
                    // button.attr("disabled","disabled");
                },
                success: function(data) {
                    if (data.response == 'success') {
                        swal({
                            title: "Success!",
                            text: data.message,
                            type: "success",
                            confirmButtonText: "OK"
                        });
                        $(this).trigger("reset");

                        window.location.href = "{{URL::to(Library::getDomainPrefix().'/response_serpong/success')}}";
                        gtag('event', 'click', {
                            'event_category': 'SubmitFormLP',
                            'transport_type': 'beacon',
                            'event_action': 'click',
                            'event_label': 'RukoMaxwell',
                        });
                    } else {
                        swal({
                            title: "Error!",
                            text: data.message,
                            type: "error",
                            confirmButtonText: "OK"
                        });
                        status.html('');
                        button.removeAttr("disabled");
                    }
                }
            });
        });

        //FOR PROTECT NAME FORM
        $(".char-only").keypress(function(event) {
            var theEvent = event || window.event;
            var key = theEvent.keyCode || theEvent.which;
            if ((key < 97 || key > 122) && (key < 65 || key > 90) && !(key == 8 || key == 9 || key == 13 || key == 32 || key == 37 || key == 38 || key == 39 || key == 40 || key == 46)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        })


        //FOR PROTECT NUMBER PHONE FORM
        $(".number-only").keypress(function(event) {
            var theEvent = event || window.event;
            var key = theEvent.keyCode || theEvent.which;
            if ((key < 48 || key > 57) && !(key == 8 || key == 9 || key == 13 || key == 37 || key == 38 || key == 39 || key == 40 || key == 46)) {
                console.log(key)
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        })
    </script>
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NQ8XHHG');</script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<link href="{{asset('assets/microsite/images/favicon.ico')}}" rel="shortcut icon">
	{!!SEO::generate()!!}

    <!-- BOOTSTRAP CORE CSS -->
	<link href="{{asset('assets/microsite/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('assets/microsite/vendor/fontawesome/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
	<link href="{{asset('assets/microsite/vendor/animateit/animate.min.css')}}" rel="stylesheet">
	<!-- VENDOR CSS -->
	<link href="{{asset('assets/microsite/vendor/owlcarousel/owl.carousel.css')}}" rel="stylesheet">
	<link href="{{asset('assets/microsite/vendor/magnific-popup/magnific-popup.css')}}" rel="stylesheet">
	<!-- TEMPLATE BASE -->
	<link href="{{asset('assets/microsite/css/theme-base.css')}}" rel="stylesheet">
	<!-- TEMPLATE ELEMENTS -->
	<link href="{{asset('assets/microsite/css/theme-elements.css')}}" rel="stylesheet">
	<!-- RESPONSIVE CLASSES-->
	<link href="{{asset('assets/microsite/css/responsive.css')}}" rel="stylesheet">
	<!-- LOAD GOOGLE FONTS -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,800,700,600%7CRaleway:100,300,600,700,800" rel="stylesheet" type="text/css" />
	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link href="{{asset('assets/microsite/vendor/rs-plugin/css/settings.css')}}" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
	<link href="{{asset('assets/microsite/css/rs-plugin-styles.css')}}" rel="stylesheet" type="text/css" />
	<!-- STYLE CSS -->
	<link href="{{asset('assets/microsite/css/style.css')}}" rel="stylesheet">
	<!-- STYLE CSS -->
	<link href="{{asset('assets/microsite/css/styleheader.css')}}" rel="stylesheet">
	<link href="{{asset('assets/microsite/vendor/sweetalert/sweetalert.css')}}" rel="stylesheet">
	<!--VENDOR SCRIPT-->
	<script src="{{asset('assets/microsite/vendor/jquery/jquery-1.11.2.min.js')}}"></script>
	<script src="{{asset('assets/microsite/vendor/plugins-compressed.js')}}"></script>
    @yield('css')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-8RPZWZ8W6X"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-8RPZWZ8W6X');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-42299418-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-42299418-1');
    </script>

    <!-- CandyPixel V3.7.1 -->

    <!--
    Warning:   CandyPixel v3.7.0 onwards is incompatible with snippets and custom code
            associated with previous versions of CandyPixel.

            All MSIE browsers are no longer supported.
    -->
    <script>
        var CandyPixel4 = (function (s, u, g, a, r) {
            a = u.createElement("script");
            r = u.getElementsByTagName("script")[0];
            a.async = true;
            a.src = g;
            r.parentNode.insertBefore(a, r);

            return {
                config: {
                    debugMode: false,
                    token: "d4620087-d9c570ad-1593415909109",
                    pid: "1153",    // deprecated
                    psid: "SRCP#c0a31df8-5e92-4f3e-9cde-5985710fb4cb",  // deprecated

                    // form class or ids to be ignored e.g. [".secretForms", "#loginForm"]
                    ignoreFormsWithAttributeValues: [],

                    // field class or ids to be ignored  e.g. [".secretField", "#password"]
                    ignoreFieldsWithAttributeValues: [],

                    submitAction: "submit", // click | submit
                    detectAdBlocker: null,  // set to true or false to override server-side setting
                    enableTracking: true,   // set to true or false to enableTracking

                    // feature flags:
                    ff_useLegacyRadioInputHandling: false, // set to true if you have trouble with radio form fields
                },
                app: {},
                onInit: function () {
                    console.log("onInit", this);
                    // this.app.setConfig(this.config);
                    // register callbacks here, if any

                },
                onReady: function () {
                    console.log("--------CandyPixel is Ready---------");
                    // this.app.getInfo();
                },
            }
        })(window, document, "https://s.cdy.ai/candypixel/app.v3.7.1.js");
    </script>

    <!-- Meta Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '573486941398806');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=573486941398806&ev=PageView&noscript=1"
        />
    </noscript>
    <!-- End Meta Pixel Code -->

    <style>
        .grecaptcha-badge { visibility: hidden !important; }
    </style>
    {!! RecaptchaV3::initJs() !!}
</head>

<body>
    {{-- @include('layout.header') --}}

    @yield('content')
    {{-- <div class="body-inner">
        @include('layout.footer')
    </div> --}}
    <!-- Scroll top -->
    <a id="scrollTop"><i class="fas fa-chevron-up"></i><i class="fas fa-chevron-up"></i></a>
    <!-- Whatsapp -->
    <!-- <a class="scrollTop d-none d-sm-block d-sm-none d-md-block" href="https://wa.me/628111409008?text=Halo%20Saya%20tertarik%20dengan%20Produk%20Summarecon%20Serpong%20ingin%20dapat%20informasi%20lebih%20lanjut%20dari%20Website%20Summarecon%20Serpong" target="_blank"><img src="{{asset('assets/images/serpong/icon/PNG-round-whatsapp.png')}}" class="img-wa"></a> -->

    <a href="https://wa.me/628111409008?text=Halo%20Saya%20tertarik%20dengan%20Produk%20Summarecon%20Serpong%20ingin%20dapat%20informasi%20lebih%20lanjut%20dari%20Website%20Summarecon%20Serpong" class="float d-none d-sm-block d-sm-none d-md-block" target="_blank" rel="noopener">
        <div class="hotline-phone-ring-circle-wa"></div>
        <div class="hotline-phone-ring-circle-fill-wa"></div>
        <i class="fab fa-whatsapp my-float"></i>
    </a>

    <a href="https://wa.me/628111409008?text=Halo%20Saya%20tertarik%20dengan%20Produk%20Summarecon%20Serpong%20ingin%20dapat%20informasi%20lebih%20lanjut%20dari%20Website%20Summarecon%20Serpong" class="float d-md-none d-lg-none d-xl-none" target="_blank" rel="noopener">
        <div class="hotline-phone-ring-circle-wa"></div>
        <div class="hotline-phone-ring-circle-fill-wa"></div>
        <i class="fab fa-whatsapp my-float"></i>
    </a>

    <!-- <div class="d-md-none d-lg-none d-xl-none">
        <button class="btn btn-slide btn-float" data-width="270" href="https://api.whatsapp.com/send?phone=628111409008&text=Halo%20Saya%20tertarik%20dengan%20Summarecon%20Serpong%20ingin%20dapat%20informasi%20lebih%20lanjut%20" target="_blank">
            <i class="fas fa-chevron-left"></i>
            <span>Chat Our Representative <img src="{{asset('assets/images/serpong/icon/whatsapp-01.svg')}}" class="img-wa"></span>
            <i class="fas fa-whatsapp"></i>
        </button>
    </div> -->

    <script type="text/javascript" src="{{asset('assets/js/jquery-1.11.2.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script type="text/javascript" src="{{asset('assets/js/plugins.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/functions.js')}}"></script>
    <!-- Sweetalert Plugin -->
    <script type="text/javascript" src="{{asset('assets/plugins/sweetalert/sweetalert.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('assets/js/contact-form.js')}}"></script>
    <!-- CUSTOM JS FILE -->
	<script src="{{asset('assets/js/custom.js')}}"></script>

    <!-- LITETOOLTIP MASTERPLAN -->
	<script src="{{asset('assets/js/litetooltip.js')}}"></script>

    <!-- Gmaps -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAStRXKCcrS4SqU0I-bfd66Iq5zPPgHvag"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            // Gmaps
            function initialize() {
                var latitude = $("#map-canvas").attr("data-latitude");
                var longitude = $("#map-canvas").attr("data-longitude");
                var title = $("#map-canvas").attr("data-title");

                if (latitude == "" && longitude == "")
                    title = "Plaza Summarecon Serpong";

                if (latitude == "")
                    latitude = -6.244369;

                if (longitude == "")
                    longitude = 106.629026;

                var lat = parseFloat(latitude);
                var lon = parseFloat(longitude);

                var mapOptions = {
                    center: {
                        lat: lat,
                        lng: lon
                    },
                    zoom: 15
                };
                var latLang = new google.maps.LatLng(lat, lon);
                var map = new google.maps.Map(document.getElementById('map-canvas'),
                    mapOptions);

                var marker = new google.maps.Marker({
                    position: latLang,
                    map: map,
                    title: title
                });
            }

            if ($("#map-canvas").html() != undefined)
                google.maps.event.addDomListener(window, 'load', initialize);

            // View All Gallery from Home
            // $("#all-gallery").click(function(event){
            //   //event.preventDefault();
            //   $(".cbp-panel")
            // })

        });

            //FOR PROTECT NAME FORM
            $(".char-only").keypress(function( event ) {
                var theEvent = event || window.event;
                var key = theEvent.keyCode || theEvent.which;
                if ((key < 97 || key > 122) && (key < 65 || key > 90) && !(key == 8 || key == 9 || key == 13 || key == 32 || key == 37 || key == 38 || key == 39 || key == 40 || key == 46 )) {
                    theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
                }
            })


            //FOR PROTECT NUMBER PHONE FORM
            $(".number-only").keypress(function( event ) {
                var theEvent = event || window.event;
                var key = theEvent.keyCode || theEvent.which;
                if ((key < 48 || key > 57) && !(key == 8 || key == 9 || key == 13 || key == 37 || key == 38 || key == 39 || key == 40 || key == 46)) {
                    console.log(key)
                    theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
                }
            })
    </script>

    @yield('js')
    <script type="text/javascript"></script>

</body>

</html>

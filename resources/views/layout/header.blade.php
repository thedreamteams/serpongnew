@if(isset($headerlanding))

@else
<!-- Header -->
<header id="header" data-transparent="true" data-fullwidth="true" class="dark submenu-light">
    <div class="header-inner">
        <div class="container">
            <div id="logo">
                <a href="{{URL::to('/')}}">
                    <span class="logo-default"><img src="{{asset('assets/images/serpong/Logo-SS-new.png')}}"></span>
                    <span class="logo-dark"><img src="{{asset('assets/images/serpong/Logo-White.png')}}" class="d-none d-sm-block d-sm-none d-md-block"></span>
                    <span class="logo-dark"><img src="{{asset('assets/images/serpong/Logo-SS-new.png')}}" class="d-md-none d-lg-none d-xl-none"></span>
                </a>
            </div>
            <div id="search"><a id="btn-search-close" class="btn-search-close" aria-label="Close search form"><i class="icon-x"></i></a>
                <form class="search-form" action="search-results-page.html" method="get">
                    <input class="form-control" name="q" type="search" placeholder="Type & Search..." />
                    <span class="text-muted">Start typing & press "Enter" or "ESC" to close</span>
                </form>
            </div>
            <div id="mainMenu-trigger"> <a class="lines-button x"><span class="lines"></span></a> </div>
            <div id="mainMenu">
                <div class="container">
                    <nav>
                        <ul>
                            <li><a href="{{URL::to('/')}}">Home</a></li>
                            <!-- <li><a href="{{ URL::to('about') }}">About<i class="fa fa-chevron-down"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{URL::to('about/masterplan-akses')}}">Masterplan & Akses </a></li>
                                </ul>
                            </li> -->
                            <li><a href="{{URL::to('about')}}">About</a></li>
                            <li class="dropdown"><a href="">Project</a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown"><a href="{{URL::to('projects/rumah')}}">Rumah</a>
                                    </li>
                                    <li class="dropdown"><a href="{{URL::to('projects/ruko-komersial')}}">Ruko Komersial</a>
                                    </li>
                                    <li class="dropdown"><a href="{{URL::to('projects/apartment-condovillas')}}">Apartement & Condovillas</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="{{URL::to('virtual-tour ')}}">Virtual Tour</a></li>
                            <li><a href="{{URL::to('explore ')}}">Explore </a></li>
                            <li><a href="{{URL::to('town-management')}}">Town Management</a></li>
                            <li class="dropdown"><a href="">News</a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown"><a href="{{URL::to('whats-on/news')}}">News & Update</a></li>
                                    <li class="dropdown"><a href="{{URL::to('whats-on/information')}}">Information & Tips</a></li>
                                    <li class="dropdown"><a href="{{URL::to('press-release')}}">Press Release</a></li>
                                </ul>
                            </li>
                            <li><a href="{{URL::to('simulasi-kpr')}}">Simulasi KPR</a></li>
                            <li class="dropdown"><a href="">Contact</a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{URL::to('contact-us')}}">Contact Us</a></li>
                                    <li><a href="{{URL::to('sales-executive-landed')}}">Sales Executive</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- End: Header -->
@endif

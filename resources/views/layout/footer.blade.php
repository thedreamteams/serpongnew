

<!-- Footer -->
<!-- DEKSTOP -->
<footer id="footer" class="background-footer d-none d-sm-block d-sm-none d-md-block">
    <div class="footer-content p-t-40 p-b-0">
        <div class="container">
            <div class="row">
                <div class="col col-lg-7 col-sm-12 m-b-10">
                    <img src="{{asset('assets/images/serpong/SA.png')}}" class=" img-footer">
                    <address class="m-t-10  m-t-20-m m-b-20-m">
                        <strong class="text-dark">Plaza Summarecon Serpong</strong><br>
                        Jl. Boulevard Raya Gading Serpong Blok M5 No. 3<br>
                        Tangerang 15810, Indonesia<br>
                        <strong class="font-green font-italic"><a href="https://g.page/sms_serpong?share" target="_blank">Open Google Maps</a></strong>
                    </address>
                    <strong>
                        <ul class="list list-legend list-dekstop p-l-0">
                            <li class="text-uppercase m-b-5 font-green" style="font-size:12px !important;"><a href="https://www.summarecon.com/" target="_blank">Summarecon.com</a></li>
                            <li class="font-green" style="font-size:12px !important;">|</li>
                            <li class="text-uppercase m-b-5 font-green" style="font-size:12px !important;"><a href="https://career.summarecon.com/" target="_blank">Career</a></li>
                            <li class="font-green" style="font-size:12px !important;">|</li>
                            <li class="text-uppercase m-b-5 font-green" style="font-size:12px !important;"><a href="{{URL::to('disclaimer')}}">Disclaimer</a></li>
                            <li class="font-green" style="font-size:12px !important;">|</li>
                            <li class="text-uppercase m-b-5 font-green" style="font-size:12px !important;"><a href="{{URL::to('press-release')}}">Press Release</a></li>
                            <li class="font-green" style="font-size:12px !important;">|</li>
                            <li class="text-uppercase m-b-5 font-green" style="font-size:12px !important;"><a href="{{URL::to('contact-us')}}">Contact Us</a></li>
                        </ul>
                    </strong>
                    <div class="copyright-text copyright-mobile text-uppercase text-center font-small text-dark  m-b-20 m-t-0" style="display:inline-block;">© All Rights Reserved 2016 Summarecon Serpong</div>
                </div>
                <div class="col col-lg-4 offset-lg-1 col-sm-12 button-wa-footer">
                    <div class="post-thumbnail-list">
                        <div class="post-thumbnail-entry">
                            <img alt="" src="{{asset('assets/images/serpong/icon/PNG-round-whatsapp.png')}}">
                            <div class="post-thumbnail-content">
                                <a href="https://api.whatsapp.com/send?phone=628111409008&amp;text=Halo%20mau%20tanya%20info%20Summarecon%20Serpong%20" target="_blank">Chat Our Representative</a>
                                <span class="post-date font-weight-600">+62811 409 008</span>
                            </div>
                        </div>
                    </div>

                    <div class="post-thumbnail-list">
                        <div class="post-thumbnail-entry">
                            <img alt="" src="{{asset('assets/images/serpong/icon/email-footer.png')}}">
                            <div class="post-thumbnail-content">
                                <a href="mailto:marketing_ss@summarecon.com">Message Us</a>
                                <span class="post-date font-weight-600">marketing_ss@summarecon.com</span>
                            </div>
                        </div>
                    </div>

                    <div class="social-icons social-icons-light social-icons-colored-hover float-right">
                        <ul>
                            <li><h4 class="font-italic text-dark  m-r-20 m-t-0 font-medium">Stay in Touch</h4></li>
                            <li class="social-instagram"><a href="http://instagram.com/summarecon_serpong" target="_blank"><i class="fab fa-instagram"></i></a></li>
                            <li class="social-instagram"><a href="https://www.facebook.com/summareconserpong/?ref=br_rs" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                            <li class="social-instagram"><a href="https://www.youtube.com/channel/UColuLhICEhcPWh_kBqTvgUg" target="_blank"><i class="fab fa-youtube"></i></a></li>
                            <li class="social-instagram"><a href="https://twitter.com/summarecon_srpg" target="_blank"><i class="fab fa-twitter"></i></a></li>
                            <li class="social-instagram"><a href="https://vt.tiktok.com/ZSJ3786ae/" target="_blank"><i class="fab fa-tiktok"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- END : DEKSTOP -->

<!-- MOBILE -->
<footer id="footer" class="background-footer d-md-none d-lg-none d-xl-none">
    <div class="footer-content text-center p-t-40 p-b-0">
        <div class="container">
            <div class="row">
                <div class="col-12 m-b-10">
                    <img src="{{asset('assets/images/serpong/SA.png')}}" class="img-footer">
                    <address class="m-t-10 m-b-10">
                        <strong class="text-dark">Plaza Summarecon Serpong</strong><br>
                        Jl. Boulevard Raya Gading Serpong Blok M5 No. 3<br>
                        Tangerang 15810, Indonesia<br>
                        <strong class="font-green font-italic"><a href="https://g.page/sms_serpong?share" target="_blank">Open Google Maps</a></strong>
                    </address>
                    <h4 class="font-italic text-dark  m-t-0 font-medium">Stay in Touch</h4>
                    <div class="social-icons social-icons-light social-icons-colored-hover list-mobile m-b-10 p-b-10-m">
                        <ul class="list-mobile-center">
                            <li class="social-instagram"><a href="http://instagram.com/summarecon_serpong" target="_blank"><i class="fab fa-instagram"></i></a></li>
                            <li class="social-instagram"><a href="https://www.facebook.com/summareconserpong/?ref=br_rs" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                            <li class="social-instagram"><a href="https://www.youtube.com/channel/UColuLhICEhcPWh_kBqTvgUg" target="_blank"><i class="fab fa-youtube"></i></a></li>
                            <li class="social-instagram"><a href="https://twitter.com/summarecon_srpg" target="_blank"><i class="fab fa-twitter"></i></a></li>
                            <li class="social-instagram"><a href="https://vt.tiktok.com/ZSJ3786ae/" target="_blank"><i class="fab fa-tiktok"></i></a></li>
                        </ul>
                    </div>
                    <ul class="list list-legend list-mobile">
                        <li class="text-uppercase m-b-5 font-green"><a href="https://www.summarecon.com/" target="_blank">Summarecon.com</a></li>
                        <li class="font-green">|</li>
                        <li class="text-uppercase m-b-5 font-green"><a href="https://career.summarecon.com/" target="_blank">Career</a></li>
                        <li class="font-green">|</li>
                        <li class="text-uppercase m-b-5 font-green"><a href="{{URL::to('disclaimer')}}">Disclaimer</a></li>
                    </ul>
                    <ul class="list list-legend list-mobile">
                        <li class="text-uppercase m-b-5 font-green"><a href="{{URL::to('press-release')}}">Press Release</a></li>
                        <li class="font-green">|</li>
                        <li class="text-uppercase m-b-5 font-green"><a href="{{URL::to('contact-us')}}">Contact Us</a></li>
                    </ul>
                    <div class="copyright-text text-uppercase text-center font-small text-dark m-b-20">© All Rights Reserved 2016 Summarecon Serpong</div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- END : MOBILE -->
<!-- End: Footer -->

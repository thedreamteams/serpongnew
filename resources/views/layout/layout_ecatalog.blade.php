<!DOCTYPE html>
<html lang="en">

<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NQ8XHHG');</script>
<!-- End Google Tag Manager -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='0'>
    <meta http-equiv='pragma' content='no-cache'>
    <link href="{{asset('assets/images/favicon.ico')}}" rel="shortcut icon">
    <meta name="facebook-domain-verification" content="2k4svkdsk4kr8dc8pkmd9g3vqk7lnr" />
    {!!SEO::generate()!!}

    <link rel="stylesheet" href="{{asset('assets/css/plugins.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}?v={{time()}}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
    <!-- Sweetalert Plugin -->
    <link rel="stylesheet" href="{{asset('assets/plugins/sweetalert/sweetalert.css')}}">
    <!-- LITETOOLTIP -->
	<link href="{{asset('assets/css/litetooltip.min.css')}}" rel="stylesheet">
    @yield('css')
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=DC-12599727"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'DC-12599727');
    </script>
    <!-- End of global snippet: Please do not remove -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-8RPZWZ8W6X"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-8RPZWZ8W6X');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-42299418-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-42299418-1');
    </script>

    <!-- Meta Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '573486941398806');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=573486941398806&ev=PageView&noscript=1"
        />
    </noscript>
    <!-- End Meta Pixel Code -->

    <style>
        .grecaptcha-badge { visibility: hidden !important; }
    </style>
    {!! RecaptchaV3::initJs() !!}
</head>

<body>
    @include('layout.header')

    @yield('content')
    <div class="body-inner">
        @include('layout.footer')
    </div>
    <!-- Scroll top -->
    <a id="scrollTop"><i class="fas fa-chevron-up"></i><i class="fas fa-chevron-up"></i></a>
    <!-- Whatsapp -->
    <!-- <a class="scrollTop d-none d-sm-block d-sm-none d-md-block" href="https://wa.me/628111409008?text=Halo%20Saya%20tertarik%20dengan%20Produk%20Summarecon%20Serpong%20ingin%20dapat%20informasi%20lebih%20lanjut%20dari%20Website%20Summarecon%20Serpong" target="_blank"><img src="{{asset('assets/images/serpong/icon/PNG-round-whatsapp.png')}}" class="img-wa"></a> -->

    <a href="https://wa.me/628111409008?text=Halo%20Saya%20tertarik%20dengan%20Produk%20Summarecon%20Serpong%20ingin%20dapat%20informasi%20lebih%20lanjut%20dari%20Website%20Summarecon%20Serpong" class="float d-none d-sm-block d-sm-none d-md-block" target="_blank" rel="noopener">
        <div class="hotline-phone-ring-circle-wa"></div>
        <div class="hotline-phone-ring-circle-fill-wa"></div>
        <i class="fab fa-whatsapp my-float"></i>
    </a>

    <a href="https://wa.me/628111409008?text=Halo%20Saya%20tertarik%20dengan%20Produk%20Summarecon%20Serpong%20ingin%20dapat%20informasi%20lebih%20lanjut%20dari%20Website%20Summarecon%20Serpong" class="float d-md-none d-lg-none d-xl-none" target="_blank" rel="noopener">
        <div class="hotline-phone-ring-circle-wa"></div>
        <div class="hotline-phone-ring-circle-fill-wa"></div>
        <i class="fab fa-whatsapp my-float"></i>
    </a>

    <!-- <div class="d-md-none d-lg-none d-xl-none">
        <button class="btn btn-slide btn-float" data-width="270" href="https://api.whatsapp.com/send?phone=628111409008&text=Halo%20Saya%20tertarik%20dengan%20Summarecon%20Serpong%20ingin%20dapat%20informasi%20lebih%20lanjut%20" target="_blank">
            <i class="fas fa-chevron-left"></i>
            <span>Chat Our Representative <img src="{{asset('assets/images/serpong/icon/whatsapp-01.svg')}}" class="img-wa"></span>
            <i class="fas fa-whatsapp"></i>
        </button>
    </div> -->

    <script type="text/javascript" src="{{asset('assets/js/jquery-1.11.2.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script type="text/javascript" src="{{asset('assets/js/plugins.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/functions.js')}}"></script>
    <!-- Sweetalert Plugin -->
    <script type="text/javascript" src="{{asset('assets/plugins/sweetalert/sweetalert.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('assets/js/contact-form.js')}}"></script>
    <!-- CUSTOM JS FILE -->
	<script src="{{asset('assets/js/custom.js')}}"></script>

    <!-- LITETOOLTIP MASTERPLAN -->
	<script src="{{asset('assets/js/litetooltip.js')}}"></script>

    <!-- Gmaps -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAStRXKCcrS4SqU0I-bfd66Iq5zPPgHvag"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            // Gmaps
            function initialize() {
                var latitude = $("#map-canvas").attr("data-latitude");
                var longitude = $("#map-canvas").attr("data-longitude");
                var title = $("#map-canvas").attr("data-title");

                if (latitude == "" && longitude == "")
                    title = "Plaza Summarecon Serpong";

                if (latitude == "")
                    latitude = -6.244369;

                if (longitude == "")
                    longitude = 106.629026;

                var lat = parseFloat(latitude);
                var lon = parseFloat(longitude);

                var mapOptions = {
                    center: {
                        lat: lat,
                        lng: lon
                    },
                    zoom: 15
                };
                var latLang = new google.maps.LatLng(lat, lon);
                var map = new google.maps.Map(document.getElementById('map-canvas'),
                    mapOptions);

                var marker = new google.maps.Marker({
                    position: latLang,
                    map: map,
                    title: title
                });
            }

            if ($("#map-canvas").html() != undefined)
                google.maps.event.addDomListener(window, 'load', initialize);

            // View All Gallery from Home
            // $("#all-gallery").click(function(event){
            //   //event.preventDefault();
            //   $(".cbp-panel")
            // })

        });

            //FOR PROTECT NAME FORM
            $(".char-only").keypress(function( event ) {
                var theEvent = event || window.event;
                var key = theEvent.keyCode || theEvent.which;
                if ((key < 97 || key > 122) && (key < 65 || key > 90) && !(key == 8 || key == 9 || key == 13 || key == 32 || key == 37 || key == 38 || key == 39 || key == 40 || key == 46 )) {
                    theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
                }
            })


            //FOR PROTECT NUMBER PHONE FORM
            $(".number-only").keypress(function( event ) {
                var theEvent = event || window.event;
                var key = theEvent.keyCode || theEvent.which;
                if ((key < 48 || key > 57) && !(key == 8 || key == 9 || key == 13 || key == 37 || key == 38 || key == 39 || key == 40 || key == 46)) {
                    console.log(key)
                    theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
                }
            })
    </script>

    @yield('js')
    <script type="text/javascript"></script>

</body>

</html>

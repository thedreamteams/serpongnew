//INFINITE SCROLL
var ias = jQuery.ias({
  container:  '.infinite-scroll',
  item:       '.infinite-scroll-item',
  pagination: '.infinite-pagination',
  next:       '.infinite-next'
});
ias.extension(new IASSpinnerExtension());
ias.extension(new IASTriggerExtension({
  offset: 3,
  text:'Load More',
  html: '<hr class="space"><br><div class="ias-trigger ias-trigger-next text-center "  ><a class="read-more button rounded green button-3d effect icon-top center">{text}</a></div>'
}));
ias.extension(new IASNoneLeftExtension({text: ""}));
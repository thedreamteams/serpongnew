var baseUrl =  "http://summareconserpong.com/";
var domainName = document.domain;

if(domainName!=""){
  if(domainName == "localhost")
    baseUrl = "http://"+domainName+'/'+window.location.pathname.split('/')[1]+'/';
  else
    baseUrl = "http://"+domainName+'/';
}

// $(document).scroll(function(){
//   var y = $(this).scrollTop();
//   if(y>385){
//     $("#four").fadeIn();
//   }
//   else{
//     $("#four").fadeOut(); 
//   }
// });
 

// TAB SIDE CONTACT PROJECT
$("#four").creamTabs({
    triggerType: 'click',
    menuType: 'vertical',
    iconSize: 12,
    conBoxWidth: 345,
    conBoxHeight:520,
    mainColor: '#00612F',
    borderLight: '#43AF49',
    borderDark: '#fff',
    xPosition: 'right',
    xCoor: 0,
    yPosition: 'top',
    yCoor: 200,
    iconColor: '#ffffff',
    iconActiveColor: '#EC870E',
    btnActiveColor: '#fbfbfb',
    iconPauseColor: '#fff'
});
// Tab Side
$(".arrange-view").trigger('click');

//SLIDER HOME
var tpj=jQuery;
var revapi25;
tpj(document).ready(function() {
  if(tpj("#rev_slider_25_1").revolution == undefined){
    revslider_showDoubleJqueryError("#rev_slider_25_1");
  }else{
    revapi25 = tpj("#rev_slider_25_1").show().revolution({
      sliderType:"standard",
      jsFileLocation:baseURL+'/assets/vendor/rs-plugin/js/',
      sliderLayout:"auto",
      dottedOverlay:"none",
      delay:9000,
      navigation: {
        keyboardNavigation:"off",
        keyboard_direction: "horizontal",
        mouseScrollNavigation:"off",
        onHoverStop:"on",
        touch:{
          touchenabled:"on",
          swipe_threshold: 75,
          swipe_min_touches: 50,
          swipe_direction: "horizontal",
          drag_block_vertical: false
        }
        ,
        arrows: {
          style:"hades",
          enable:true,
          hide_onmobile:true,
          hide_under:600,
          hide_onleave:true,
          hide_delay:200,
          hide_delay_mobile:1200,
          tmp:'<div class="tp-arr-allwrapper">  <div class="tp-arr-imgholder"></div></div>',
          left: {
            h_align:"left",
            v_align:"center",
            h_offset:30,
            v_offset:0
          },
          right: {
            h_align:"right",
            v_align:"center",
            h_offset:30,
            v_offset:0
          }
        }
        ,
        bullets: {
          enable:true,
          hide_onmobile:true,
          hide_under:600,
          style:"uranus",
          hide_onleave:true,
          hide_delay:200,
          hide_delay_mobile:1200,
          direction:"horizontal",
          h_align:"center",
          v_align:"bottom",
          h_offset:0,
          v_offset:30,
          space:5,
          tmp:'<span class="tp-bullet-inner"></span>'
        }
      },
      responsiveLevels:[1240,1024,778,480],
      visibilityLevels:[1240,1024,778,480],
      gridwidth:[1240,1024,778,480],
      gridheight:[600,600,500,400],
      lazyType:"smart",
      shadow:0,
      spinner:"off",
      stopLoop:"off",
      stopAfterLoops:-1,
      stopAtSlide:-1,
      shuffle:"off",
      autoHeight:"off",
      fullScreenAutoWidth:"off",
      fullScreenAlignForce:"off",
      fullScreenOffsetContainer: "",
      fullScreenOffset: "",
      hideThumbsOnMobile:"off",
      hideSliderAtLimit:0,
      hideCaptionAtLimit:0,
      hideAllCaptionAtLilmit:0,
      debugMode:false,
      fallbacks: {
        simplifyAll:"off",
        nextSlideOnWindowFocus:"off",
        disableFocusListener:false,
      }
    });
  }
}); 

// TOOLTIP MASTERPIECE LOCATION
function blink_hotspot() {
  $('.ihotspot').animate({ "opacity": '0.5' }, 'slow').animate({ 'opacity': '0.9' }, 'fast', function () { blink_hotspot(); });
}

blink_hotspot();

$(".ihotspot").css({
  "border": "solid 1px #009900",
  "background": "#00ffcc",
  "border-radius": "20px"
});

$('#ihotspot1').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>Exit & Inlet Tol (akses baru menuju tol)</h4>' +
    '</div>'
});

$('#ihotspot2').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>Sekolah Terpadu PAHOA</h4>' +
    '</div>'
});

$('#ihotspot3').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>TK PAHOA</h4>' +
    '</div>'
});

$('#ihotspot4').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>General Hospital</h4>' +
    '</div>'
});

$('#ihotspot5').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>Sekolah Tunas Bangsa</h4>' +
    '</div>'
});

$('#ihotspot6').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>Serpong Midtown</h4>' +
    '</div>'
});

$('#ihotspot7').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>Summarecon Mal Serpong</h4>' +
    '</div>'
});

$('#ihotspot8').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>Sekolah BPK Penabur</h4>' +
    '</div>'
});

$('#ihotspot9').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>Sekolah Stella Maris</h4>' +
    '</div>'
});

$('#ihotspot10').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>Sekolah Tarakanita</h4>' +
    '</div>'
});

$('#ihotspot11').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>PLAZA SUMMARECON SERPONG</h4>' +
    '</div>'
});

$('#ihotspot12').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>Kawasan Pondok Hijau Golf</h4>' +
    '</div>'
});

$('#ihotspot13').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>Golf Course</h4>' +
    '</div>'
});

$('#ihotspot14').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>Parkland</h4>' +
    '</div>'
});

$('#ihotspot15').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>Emerald Cove</h4>' +
    '</div>'
});

$('#ihotspot16').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>Kawasan The Springs</h4>' +
    '</div>'
});

$('#ihotspot17').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>The Springs Club</h4>' +
    '</div>'
});

$('#ihotspot18').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>Kawasan Scientia Garden</h4>' +
    '</div>'
});

$('#ihotspot19').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>Scientia Square</h4>' +
    '<ul class="text-align:left">' +
    '<li>Summarecon Digital Center</li>' +
    '<li>Scientia Residences</li>' +
    '<li>Scientia Square Park' +
    '</ul>' +
    '</div>'
});

$('#ihotspot20').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>Scientia Business Park</h4>' +
    '</div>'
});

$('#ihotspot21').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>Univesitas Multimedia Nusantara</h4>' +
    '</div>'
});

$('#ihotspot22').LiteTooltip({
  location: 'right',
  textalign: 'center',
  templatename: 'AtollSea',
  padding:5,
  title:
    '<div class="template">' +
    '<h4>Bursa Mobil Summarecon Serpong</h4>' +
    '</div>'
});

//INFINITE SCROLL
// var ias = jQuery.ias({
//   container:  '.infinite-scroll',
//   item:       '.infinite-scroll-item',
//   pagination: '.infinite-pagination',
//   next:       '.infinite-next'
// });
// ias.extension(new IASSpinnerExtension());
// ias.extension(new IASTriggerExtension({
//   offset: 3,
//   text:'Load More',
//   html: '<hr class="space"><br><div class="ias-trigger ias-trigger-next text-center "  ><a class="button border rounded text-center">{text}</a></div>'
// }));
// ias.extension(new IASNoneLeftExtension({text: ""}));

//CONTACT US
$(".contact-form").validate({
  submitHandler: function(form) {
      jQuery(form).ajaxSubmit({
          beforeSubmit: function(){
              $(".button-contact").button("loading");
          },
          success: function(text) {
              console.log(text);
              if (text.response == 'success') {
                  $.notify({
                      message: text.message
                  }, {
                      type: 'success'
                  });
                  $(form)[0].reset();
              } else {
                  $.notify({
                      message: text.message
                  }, {
                      type: 'danger'
                  });
              }

              $(".button-contact").button("reset");
              Recaptcha.reload();
          },
      });
  }
});

//PROJECT FILTER START
$(".filter-type").change(function(event){
  event.preventDefault();
  var url = baseUrl + "project/browse";
  var queryUrl = "";
  var thisValue = $(this).val();
  if(thisValue!=""){
    var areaValue = $(".filter-area").val();
    var yearValue = $(".filter-year").val();

    if(thisValue!=""){
      queryUrl = queryUrl + "type=" + thisValue;
    }
    if(areaValue!=""){
      queryUrl = queryUrl + "&area=" + areaValue;
    }
    if(yearValue!=""){
      queryUrl = queryUrl + "&year=" + yearValue;
    }

    if(queryUrl!=""){
      url = url + "?" + queryUrl;
    }

    window.location = url;
  }
  else{
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    var newUrl = "";
    $.each(hashes,function(key,value){
      if(value.indexOf('type=')<0){
        newUrl = newUrl + "&" + value;
      };
    });
    if(newUrl != ""){
      url = url + "?" + newUrl;
    }

    url = url.replace("?&","?");
    
    window.location = url;
  }

});  

$(".filter-area").change(function(event){
  event.preventDefault();
  var url = baseUrl + "project/browse";
  var queryUrl = "";
  var thisValue = $(this).val();
  if(thisValue!=""){
    var typeValue = $(".filter-type").val();
    var yearValue = $(".filter-year").val();

    if(typeValue!=""){
      queryUrl = queryUrl + "type=" + typeValue;
    }
    if(thisValue!=""){
      queryUrl = queryUrl + "&area=" + thisValue;
    }
    if(yearValue!=""){
      queryUrl = queryUrl + "&year=" + yearValue;
    }

    if(queryUrl!=""){
      url = url + "?" + queryUrl;
    }
    url = url.replace("?&","?");
    
    window.location = url;
  }
  else{
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    var newUrl = "";
    $.each(hashes,function(key,value){
      if(value.indexOf('area=')<0){
        newUrl = newUrl + "&" + value;
      };
    });
    if(newUrl != ""){
      url = url + "?" + newUrl;
    }

    url = url.replace("?&","?");
    
    window.location = url;
  }

});  

$(".filter-year").change(function(event){
  event.preventDefault();
  var url = baseUrl + "project/browse";
  var queryUrl = "";
  var thisValue = $(this).val();
  if(thisValue!=""){
    var areaValue = $(".filter-area").val();
    var typeValue = $(".filter-type").val();

    if(typeValue!=""){
      queryUrl = queryUrl + "type=" + thisValue;
    }
    if(areaValue!=""){
      queryUrl = queryUrl + "&area=" + areaValue;
    }
    if(thisValue!=""){
      queryUrl = queryUrl + "&year=" + thisValue;
    }

    if(queryUrl!=""){
      url = url + "?" + queryUrl;
    }
    url = url.replace("?&","?");
    window.location = url;
  }
  else{
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    var newUrl = "";
    $.each(hashes,function(key,value){
      if(value.indexOf('year=')<0){
        newUrl = newUrl + "&" + value;
      };
    });
    if(newUrl != ""){
      url = url + "?" + newUrl;
    }

    url = url.replace("?&","?");
    
    window.location = url;
  }
  
});  

// VIDEO YOUTUBE
var tpj=jQuery;         
var revapi74;
tpj(document).ready(function() {
  if(tpj("#rev_slider_74_1").revolution == undefined){
    revslider_showDoubleJqueryError("#rev_slider_74_1");
  }else{
    revapi74 = tpj("#rev_slider_74_1").show().revolution({
      sliderType:"standard",
      jsFileLocation: "vendor/rs-plugin/js/",
      sliderLayout:"auto",
      dottedOverlay:"none",
      delay:9000,
      navigation: {
        keyboardNavigation:"off",
        keyboard_direction: "horizontal",
        mouseScrollNavigation:"off",
        onHoverStop:"off",
        arrows: {
          style:"uranus",
          enable:true,
          hide_onmobile:true,
          hide_under:778,
          hide_onleave:true,
          hide_delay:200,
          hide_delay_mobile:1200,
          tmp:'',
          left: {
            h_align:"left",
            v_align:"center",
            h_offset:20,
            v_offset:0
          },
          right: {
            h_align:"right",
            v_align:"center",
            h_offset:20,
            v_offset:0
          }
        }
        ,
        thumbnails: {
          style:"erinyen",
          enable:true,
          width:200,
          height:113,
          min_width:170,
          wrapper_padding:30,
          wrapper_color:"#333333",
          wrapper_opacity:"1",
          tmp:'<span class="tp-thumb-over"></span><span class="tp-thumb-image"></span><span class="tp-thumb-title">{{title}}</span><span class="tp-thumb-more"></span>',
          visibleAmount:10,
          hide_onmobile:false,
          hide_onleave:false,
          direction:"horizontal",
          span:true,
          position:"outer-bottom",
          space:20,
          h_align:"center",
          v_align:"bottom",
          h_offset:0,
          v_offset:0
        }
      },
      gridwidth:1230,
      gridheight:692,
      lazyType:"none",
      shadow:0,
      spinner:"spinner2",
      stopLoop:"on",
      stopAfterLoops:0,
      stopAtSlide:1,
      shuffle:"off",
      autoHeight:"off",
      disableProgressBar:"on",
      hideThumbsOnMobile:"off",
      hideSliderAtLimit:0,
      hideCaptionAtLimit:0,
      hideAllCaptionAtLilmit:0,
      debugMode:false,
      fallbacks: {
        simplifyAll:"off",
        nextSlideOnWindowFocus:"off",
        disableFocusListener:false,
      }
    });
  }
}); 
// END : VIDEO YOUTUBE 

//GOALS ANALYTIC CONTACT WHATSAPP
$('.btn-wa').click(function(){
  gtag('event', 'click', {
    'event_category': 'ContactWhatsapp',
    'event_label': 'Contact Whatsapp',
    'transport_type': 'beacon',
    'event_action': 'Click'
  });
})

//GOALS ANALYTIC CONTACT SALES
$('.btn-sales').click(function(){
    var analytic = $(this).data('analytic');
    var label = $(this).data('label');
    gtag('event', 'click', {
        'event_category': analytic,
        'transport_type':'beacon',
        'event_action': 'click',
        'event_label' : label,
    });
})



//FOR PROTECT NAME FORM
$(".char-only").keypress(function( event ) {
    var theEvent = event || window.event;
    var key = theEvent.keyCode || theEvent.which;
    if ((key < 97 || key > 122) && (key < 65 || key > 90) && !(key == 8 || key == 9 || key == 13 || key == 32 || key == 37 || key == 38 || key == 39 || key == 40 || key == 46 )) {
        theEvent.returnValue = false;
    if (theEvent.preventDefault) theEvent.preventDefault();
    }
})


//FOR PROTECT NUMBER PHONE FORM
$(".number-only").keypress(function( event ) {
    var theEvent = event || window.event;
    var key = theEvent.keyCode || theEvent.which;
    if ((key < 48 || key > 57) && !(key == 8 || key == 9 || key == 13 || key == 37 || key == 38 || key == 39 || key == 40 || key == 46)) {
        console.log(key)
        theEvent.returnValue = false;
    if (theEvent.preventDefault) theEvent.preventDefault();
    }
})
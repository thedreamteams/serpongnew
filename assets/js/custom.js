/* 

1. Add your custom JavaScript code below
2. Place the this code in your template:

  

*/
// TOOLTIP MASTERPIECE LOCATION
function blink_hotspot() {
    $('.ihotspot').animate({ "opacity": '0.5' }, 'slow').animate({ 'opacity': '0.9' }, 'fast', function () { blink_hotspot(); });
  }
  
  blink_hotspot();
  
  $(".ihotspot").css({
    "border": "solid 1px #009900",
    "background": "#00ffcc",
    "border-radius": "20px"
  });
  
  $('#ihotspot1').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>Exit & Inlet Tol (akses baru menuju tol)</h4>' +
      '</div>'
  });
  
  $('#ihotspot2').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>Sekolah Terpadu PAHOA</h4>' +
      '</div>'
  });
  
  $('#ihotspot3').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>TK PAHOA</h4>' +
      '</div>'
  });
  
  $('#ihotspot4').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>General Hospital</h4>' +
      '</div>'
  });
  
  $('#ihotspot5').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>Sekolah Tunas Bangsa</h4>' +
      '</div>'
  });
  
  $('#ihotspot6').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>Serpong Midtown</h4>' +
      '</div>'
  });
  
  $('#ihotspot7').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>Summarecon Mal Serpong</h4>' +
      '</div>'
  });
  
  $('#ihotspot8').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>Sekolah BPK Penabur</h4>' +
      '</div>'
  });
  
  $('#ihotspot9').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>Sekolah Stella Maris</h4>' +
      '</div>'
  });
  
  $('#ihotspot10').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>Sekolah Tarakanita</h4>' +
      '</div>'
  });
  
  $('#ihotspot11').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>PLAZA SUMMARECON SERPONG</h4>' +
      '</div>'
  });
  
  $('#ihotspot12').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>Kawasan Pondok Hijau Golf</h4>' +
      '</div>'
  });
  
  $('#ihotspot13').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>Golf Course</h4>' +
      '</div>'
  });
  
  $('#ihotspot14').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>Parkland</h4>' +
      '</div>'
  });
  
  $('#ihotspot15').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>Emerald Cove</h4>' +
      '</div>'
  });
  
  $('#ihotspot16').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>Kawasan The Springs</h4>' +
      '</div>'
  });
  
  $('#ihotspot17').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>The Springs Club</h4>' +
      '</div>'
  });
  
  $('#ihotspot18').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>Kawasan Scientia Garden</h4>' +
      '</div>'
  });
  
  $('#ihotspot19').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>Scientia Square</h4>' +
      '<ul class="text-align:left">' +
      '<li>Summarecon Digital Center</li>' +
      '<li>Scientia Residences</li>' +
      '<li>Scientia Square Park' +
      '</ul>' +
      '</div>'
  });
  
  $('#ihotspot20').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>Scientia Business Park</h4>' +
      '</div>'
  });
  
  $('#ihotspot21').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>Univesitas Multimedia Nusantara</h4>' +
      '</div>'
  });
  
  $('#ihotspot22').LiteTooltip({
    location: 'right',
    textalign: 'center',
    templatename: 'AtollSea',
    padding:5,
    title:
      '<div class="template">' +
      '<h4>Bursa Mobil Summarecon Serpong</h4>' +
      '</div>'
  });
$("#contactForm").validator({
	disable: false

}).on("submit", function (event) {
	if (event.isDefaultPrevented()) {
		submitMSG(false, function () {
			$("#contact-form-result").html('<a href="#" class="close" data-dismiss="alert">&times;</a> Please, fill the empty required fields.');
		});


	} else {
		// everything looks good!
		event.preventDefault();
		submitForm();
	}
});

function getBaseUrl() {
	var re = new RegExp(/^.*\//);
	return re.exec(window.location.href);
}

function submitForm() {
	// Initiate Variables With Form Content
	var name = $("#name").val();
	var email = $("#email").val();
	var subject = $("#subject").val();
	var message = $("#message").val();

	var utm_source = $("#utm_source").val();
	var utm_medium = $("#utm_medium").val();
	var utm_campaign = $("#utm_campaign").val();

	$.ajax({
		type: "POST",
		url: getBaseUrl() + "include/contactForm.php",
		data: "name=" + name + "&email=" + email + "&subject=" + subject + "&message=" + message +
			"&utm_source=" + utm_source + "&utm_medium=" + utm_medium + "&utm_campaign=" + utm_campaign,
		success: function (text) {
			if (text == "success") {
				formSuccess();
			} else {
				submitMSG(false, text);
			}
		}
	});
}

function formSuccess() {
	$("#contactForm")[0].reset();
	submitMSG(true, function () {

		$("#contact-form-result").html('<a href="#" class="close" data-dismiss="alert">&times;</a> We have <strong>successfully</strong> received your Message and will get Back to you as soon as possible.')

	});
}

function submitMSG(valid, msg) {
	if (valid) {
		var msgClasses = "alert alert-success";
	} else {
		var msgClasses = "alert alert-danger";
	}
	$("#contact-form-result").removeClass().addClass(msgClasses).text(msg);
}

//FOR PROTECT NAME FORM
    $(".char-only").keypress(function( event ) {
        var theEvent = event || window.event;
        var key = theEvent.keyCode || theEvent.which;
        if ((key < 97 || key > 122) && (key < 65 || key > 90) && !(key == 8 || key == 9 || key == 13 || key == 32 || key == 37 || key == 38 || key == 39 || key == 40 || key == 46 )) {
            theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
        }
    })


    //FOR PROTECT NUMBER PHONE FORM
    $(".number-only").keypress(function( event ) {
        var theEvent = event || window.event;
        var key = theEvent.keyCode || theEvent.which;
        if ((key < 48 || key > 57) && !(key == 8 || key == 9 || key == 13 || key == 37 || key == 38 || key == 39 || key == 40 || key == 46)) {
            console.log(key)
            theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
        }
    })
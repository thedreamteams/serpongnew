// Gmaps
  function initialize() {
    var latitude = $("#map-canvas").attr("data-latitude");
    var longitude = $("#map-canvas").attr("data-longitude");
    var title = $("#map-canvas").attr("data-title");

    if(latitude=="" && longitude=="")
      title="Plaza Summarecon Serpong";

    if(latitude=="")
      latitude = -6.2258568;

    if(longitude=="")
      longitude = 106.9964558;

    var lat = parseFloat(latitude);
    var lon = parseFloat(longitude);

    var mapOptions = {
      center: { lat: lat, lng:lon},
      zoom: 15
    };
    var latLang = new google.maps.LatLng(lat,lon);
    var map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);

    var marker = new google.maps.Marker({
      position: latLang,
      map:map,
      title:title
    });
  }

  if($("#map-canvas").html() != undefined)
    google.maps.event.addDomListener(window, 'load', initialize);
<?php

//HOME
Route::get('/', 'Home\HomeController@index');
// Route::get('/preview', 'Home\HomeController@preview');
Route::post('home/get-brochure', 'Home\HomeController@getBrochure');

// ABOUT
Route::get('about', 'About\AboutController@index');
Route::get('kawasan', 'About\AboutController@kawasan');
Route::get('pengembang', 'About\AboutController@pengembang');
Route::get('about/future-development', 'About\AboutController@masterplan');
Route::get('about/access', 'About\AboutController@access');
Route::get('about/award', 'About\AboutController@awards');

//NEWS
Route::get('whats-on/{category}', 'News\NewsController@index');
Route::get('whats-on/{category}/{slug}', 'News\NewsController@detail');

//TOWN
Route::get('town-management', 'Town\TownController@index_new');
Route::get('town-management/preview', 'Town\TownController@index_new');
Route::get('town-management/{slug}', 'Town\TownController@detail');


//FACILITIES
Route::get('facilities', 'Facilities\FacilitiesController@index');

//CLUB
Route::get('club', 'Club\ClubController@index');

//SIMULASI KPR
Route::get('simulasi-kpr', 'Simulasi\SimulasiController@index');

//CONTACT US
Route::get('contact-us', 'Contact\ContactController@index');
Route::get('sales-executive-landed','Contact\ContactController@detaillanded');
Route::get('sales-executive-landed/{slug}','Contact\ContactController@catalog');
Route::get('sales-executive-strata','Contact\ContactController@detailstrata');
Route::get('sales-executive-strata/{slug}','Contact\ContactController@catalog');
Route::post('contact/action', 'Contact\ContactController@action');
Route::post('contact/action-catalog', 'Contact\ContactController@action_catalog');
Route::get('disclaimer', 'Contact\ContactController@disclaimer');

//SEARCH
Route::get('search', 'Search\SearchController@index');
Route::get('search/action', 'Search\SearchController@action');

//PRODUCT
Route::get('product-detail', 'Product\ProductController@detail');
Route::post('project/get-brochure/{slug}/{id}', 'Project\ProjectController@getBrochure');
Route::get('download/{name}/{id}', 'Project\ProjectController@download');

//EXPLORE
Route::get('explore','Explore\ExploreController@index');
Route::get('explore-detail','Explore\ExploreController@detail');
Route::get('explore-detail/{category}','Explore\ExploreController@detailcategory');
Route::get('explore/shuttle-summarecon-serpong','Explore\ExploreController@shuttle');

//PROJECT
Route::get('projects', 'Project\ProjectController@index');
Route::get('projects/{category}', 'Project\ProjectController@category');
Route::get('projects/{category}/{slug}', 'Project\ProjectController@detail');
Route::get('past-projects', 'Project\ProjectController@index_past');
Route::get('past-projects/{category}', 'Project\ProjectController@category_past');
Route::get('projects/type/{slug}', 'Project\ProjectController@type');

// VIRTUAL TOUR
Route::get('virtual-tour', 'Virtual\VirtualController@new');
Route::get('virtual-tour-new', 'Virtual\VirtualController@new');
Route::get('virtual-tour/{slug}', 'Virtual\VirtualController@detail');

//RELEASE
Route::get('press-release', 'Release\ReleaseController@index');

//CONTACT ACTION LANDING PAGE MICROSITE
Route::post('contact-micro/action', 'Microsite\MicrositeController@action');

//LANDING PAGE MICROSITE
Route::get('ruko-modern-serpong', 'Microsite\MicrositeController@ruko');
Route::get('cluster-strozzi', 'Microsite\MicrositeController@strozzi');
Route::get('mtown-apartment', 'Microsite\MicrositeController@mtown');
Route::get('heron', 'Microsite\MicrositeController@heron');
Route::get('cluster-ardea', 'Microsite\MicrositeController@ardea');
Route::get('downtown-drive', 'Microsite\MicrositeController@downtown');
Route::get('new-cluster', 'Microsite\MicrositeController@carson');
Route::get('mega-release','Microsite\MicrositeController@mega');
Route::get('special-promo','Microsite\MicrositeController@mega');

//GETINTOUCH
Route::get('getintouch', 'Microsite\MicrositeController@getintouch');
// Route::post('contact/action', 'Microsite\MicrositeController@action_getintouch');

// THANK U PAGE
Route::get('response_serpong/{status}', 'Microsite\MicrositeController@response_serpong');
// THANK U PAGE
Route::get('response_rumah/{status}', 'Microsite\MicrositeController@response_rumah');

